<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ppk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mppk');
        $this->load->model('Msipil');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'ppk';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'ppk?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'ppk?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'ppk';
            $config['first_url'] = base_url() . 'ppk';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mppk->total_rows($q);
        $ppk                      = $this->Mppk->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'ppk_data' => $ppk,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'PPK',
            'akses'               => $akses
		);
		return view('ppk.view_index', $data);
        // $this->template->load('layout', 'ppk/view_index', $data);
    }

    public function read($ide)
    {
        $this->cekAkses('read');
        $id  = rapikan($ide);
        $row = $this->Mppk->get_by_id($id);
        if ($row) {
            $data = array(
                'title' => 'Detail ppk',
                'id' => $row->id,
                'nama' => $row->nama,
                'instansi' => $row->instansi,
            );
            $this->template->load('layout', 'ppk/view_read', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('ppk'));
        }
    }

    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button'   => 'Simpan',
            'title'    => 'Tambah Data PPK',
            'action'   => site_url('ppk/create_action'),
            'id'       => set_value('id'),
            'nama_ppk'     => set_value('nama_ppk'),
            'nm_sub_unit' => set_value('nm_sub_unit'),
            'kd_skpd'  => set_value('kd_skpd'),
            'email_ppk'  => set_value('email_ppk'),
            'skpd'     => $this->Msipil->get_opd()
        );
		// $this->template->load('layout', 'ppk/view_form', $data);
		return view('ppk.view_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $instansi = explode("/", $this->input->post('kd_skpd', TRUE));
            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');
            $data = array(
                'nama_ppk' => $this->input->post('nama_ppk', TRUE),
                'email_ppk' => $this->input->post('email_ppk', TRUE),
                'nm_sub_unit' => $instansi[1],
                'kd_skpd' => $instansi[0],
                'created_by' => $this->id_pengguna,
                'created_at' => $datetime_formatted
            );

            $res = $this->Mppk->insert($data);

            if ($res) {

                $row['user'] = $this->menu_db->query("select TOP 1 username from ms_pengguna ORDER BY ref_ppk_id DESC")->row();
                $row['pengawas'] = $this->menu_db->query("select TOP 1 kd_skpd,nm_sub_unit,nama_ppk from ref_ppk ORDER BY id DESC")->row();
                $pesan = $this->load->view('ppk/kontenmail', $row, true);
                $email_to = array($this->input->post('email_ppk', TRUE));
                $subject = "PEMBERITAHUAN USERNAME & PASSWORD";
                $qq = $this->Msipil->sendmail($email_to, $subject, $pesan);
                if ($qq == 1) {
                    set_flashdata('success', 'Data berhasil di simpan. Berhasil Kirim Email');
                } else {
                    set_flashdata('success', 'Data berhasil di simpan. Gagal Kirim Email');
                }
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }


            redirect(site_url('ppk'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mppk->get_by_id($id);

        if ($row) {
            $data = array(
                'title'    => 'Edit ppk',
                'button'   => 'Update',
                'action'   => site_url('ppk/update_action'),
                'id'   => set_value('id', $row->id),
                'nama_ppk'     => set_value('nama_ppk', $row->nama_ppk),
                'nm_sub_unit' => set_value('nm_sub_unit', $row->nm_sub_unit),
                'kd_skpd'  => set_value('kd_skpd', $row->kd_skpd),
                'email_ppk' => set_value('email_ppk', $row->email_ppk),
                'skpd'     => $this->Msipil->get_opd()
			);
			return view('ppk.view_form', $data);
            // $this->template->load('layout', 'ppk/view_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('ppk'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $instansi = explode("/", $this->input->post('kd_skpd', TRUE));
            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');
            $data = array(
                'nama_ppk' => $this->input->post('nama_ppk', TRUE),
                'email_ppk' => $this->input->post('email_ppk', TRUE),
                'nm_sub_unit' => $instansi[1],
                'kd_skpd' => $instansi[0],
                'updated_by' => $this->id_pengguna,
                'updated_at' => $datetime_formatted
            );

            $res = $this->Mppk->update($this->input->post('id', TRUE), $data);
            if ($res) {
                set_flashdata('success', 'Data berhasil di update.');
            } else {
                set_flashdata('warning', 'Data gagal di update.');
            }
            redirect(site_url('ppk'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id  = rapikan($ide);
        $row = $this->Mppk->get_by_id($id);

        if ($row) {
            $res = $this->Mppk->delete($id);
            if ($res) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
            redirect(site_url('ppk'));
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('ppk'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_ppk', 'nama ppk', 'trim|required');
        $this->form_validation->set_rules('kd_skpd', 'instansi', 'trim|required');
        $this->form_validation->set_rules('email_ppk', 'email', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    /* function _rules_update()
    {
        $this->form_validation->set_rules('nama_ppk', 'nama', 'trim|required');
        $this->form_validation->set_rules('instansi', 'instansi', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    } */
}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
