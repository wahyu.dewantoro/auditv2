<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mrole');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Role';
        return cek($this->id_pengguna, $url, $var);
    }




    public function index()
    {
        $akses = $this->cekAkses('read');
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'role?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'role?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'role';
            $config['first_url'] = base_url() . 'role';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mrole->total_rows($q);
        $role = $this->Mrole->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'role_data' => $role,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'title' => 'Role',
            'akses' => $akses
		);
		return view('role.view_index', $data);
    }

    public function read($ide)
    {
        $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Mrole->get_by_id($id);
        if ($row) {
            $data = array(
                'title' => 'Detail role',
                'id_inc' => $row->id_inc,
                'nama_role' => $row->nama_role,
            );
            $this->template->load('layout', 'role/view_read', $data);
        } else {
            set_flashdata('warning', 'data tidak ada');
            redirect(site_url('role'));
        }
    }

    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button'    => 'Simpan',
            'title'     => 'Tambah role',
            'action'    => site_url('role/create_action'),
            'id_inc'    => set_value('id_inc'),
            'nama_role' => set_value('nama_role'),
        );
		// $this->template->load('layout', 'role/view_form', $data);
		return view('role.view_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_role' => $this->input->post('nama_role', TRUE),
            );

            $res = $this->Mrole->insert($data);

            if ($res) {
                set_flashdata('success', 'Data berhasil di simpan.');
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }


            redirect(site_url('role'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mrole->get_by_id($id);

        if ($row) {
            $data = array(
                'title'     => 'Edit role',
                'button'    => 'Update',
                'action'    => site_url('role/update_action'),
                'id_inc'    => set_value('id_inc', $row->id_inc),
                'nama_role' => set_value('nama_role', $row->nama_role),
            );
            return view('role.view_form', $data);
        } else {


            set_flashdata('warning', 'Data tidak ada.');

            redirect(site_url('role'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
                'nama_role' => $this->input->post('nama_role', TRUE),
            );

            $res = $this->Mrole->update($this->input->post('id_inc', TRUE), $data);
            if ($res) {
                set_flashdata('success', 'Data berhasil di update.');
            } else {
                set_flashdata('warning', 'Data gagal di update.');
            }
            redirect(site_url('role'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id = rapikan($ide);
        $row = $this->Mrole->get_by_id($id);

        if ($row) {
            $res = $this->Mrole->delete($id);
            if ($res) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
            redirect(site_url('role'));
        } else {
            set_flashdata('warning', 'Data tidak ada.');
            redirect(site_url('role'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_role', 'nama role', 'trim|required');
        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function setting($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mrole->get_by_id($id);
        if ($row) {
            $role = $this->Mrole->getMenu($id);
            $data = array(
                'id_inc'    => $row->id_inc,
                'role'      => $role,
                'title' => 'Setting Privilege : ' . $row->nama_role
            );
			// $this->template->load('layout', 'role/view_read', $data);
			return view('role.view_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('role'));
        }
    }

    function prosessettingrole()
    {
        $this->cekAkses('update');
        $kode_group = $_POST['kode_group'];
        $roles      = $_POST['role'];


        if (isset($_POST['create'])) {
            $create = $_POST['create'];
            // create
            $idc = '';
            while ($aa = current($create)) {
                if ($aa == '1') {
                    $idc .= key($create) . ',';
                }
                next($create);
            }
            $idc = substr($idc, 0, -1);
        } else {
            // $create=array();
            $idc = null;
        }


        if (isset($_POST['update'])) {
            $update = $_POST['update'];
            // update
            $idu = '';
            while ($ab = current($update)) {
                if ($ab == '1') {
                    $idu .= key($update) . ',';
                }
                next($update);
            }
            $idu = substr($idu, 0, -1);
        } else {
            $idu = null;
            // $update=array();
        }

        if (isset($_POST['delete'])) {
            $delete     = $_POST['delete'];
            // delete
            $idd = '';
            while ($ac = current($delete)) {
                if ($ac == '1') {
                    $idd .= key($delete) . ',';
                }
                next($delete);
            }
            $idd = substr($idd, 0, -1);
        } else {
            // $delete=array();
            $idd = null;
        }

        $data['kode_role'] = $kode_group;
        $data['read'] = implode(',', $roles);
        $data['create'] = $idc;
        $data['update'] = $idu;
        $data['delete'] = $idd;

        $role = $this->Mrole->prosesrole($data);

        // echo $role;
        if ($role) {
            set_flashdata('success', 'Berhasil, Data telah di simpan.');
        } else {
            set_flashdata('warning', 'Berhasil, Data gagal di simpan.');
        }
        redirect('role');
    }

    public function test()
    {
        $tgl = date('Y-m-d');

        echo shortdate_indo($tgl);
        echo "<br/>";
        echo date_indo($tgl);
        echo "<br/>";
        echo mediumdate_indo($tgl);
        echo "<br/>";
        echo longdate_indo($tgl);
        echo "<hr>";
        echo acak('1');
        echo "<br>";
        echo rapikan(acak('1'));
    }
}

/* End of file Role.php */
/* Location: ./application/controllers/Role.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-22 15:57:05 */
/* http://harviacode.com */
