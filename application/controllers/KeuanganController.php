<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KeuanganController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('master', 'Keuangan'));
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }


    public function anggaran()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Anggaran',
            'sub_1'=> 'Keuangan',
            'sub_2'=>  'Anggaran',
            'subunit' => $this->master->SubUnit($tahun)
        );
        return view('keuangan.anggaran', $data);
    }

    function DataAnggaran()
    {
        $Kd_SKPD = $this->input->post('Kd_SKPD');
        if ($Kd_SKPD <> '') {
            $tahun = $this->tahun;
            $nm = $this->master->subunitBykode($tahun, $Kd_SKPD)->Nm_Sub_Unit;
            $data = array(
                'title' => 'Anggaran Pendapatan Belanja ' . $nm . ' Tahun ' . $tahun,
                'tahun' => $tahun,
                'opd' => $Kd_SKPD,
                'anggaran_data' => $this->Keuangan->dataAnggaranAkunDua($tahun, $Kd_SKPD)
            );
            $this->load->view('keuangan/_dataanggaran', $data);
        } else {
            echo '';
        }
    }

    function DataAnggaranDownload()
    {
        $opd = urldecode($this->input->get('opd'));
        $tahun = $this->tahun;
        $nm = $this->master->subunitBykode($tahun, $opd)->Nm_Sub_Unit;
        $data = array(
            'title' => 'Anggaran Pendapatan Belanja ' . $nm . ' Tahun ' . $tahun,
            'tahun' => $tahun,
            'opd' => $opd,
            'anggaran_data' => $this->Keuangan->dataAnggaranAkunDua($tahun, $opd)
        );
        $this->load->view('keuangan/_dataanggaran', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Anggaran.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
    }


    function realisasi()
    {

        $tahun = $this->tahun;
        $data = array(
            'title' => 'Realisasi Anggaran',
            'sub_1'=>'Keuangan',
            'sub_2'=>'Realisasi Anggaran',
            'subunit' => $this->master->SubUnit($tahun)
        );
        return view('keuangan.realisasi', $data);
    }

    function dataRealisasi()
    {
        $Kd_SKPD = $this->input->post('Kd_SKPD');
        if ($Kd_SKPD <> '') {
            $tahun = $this->tahun;
            $nm = $this->master->subunitBykode($tahun, $Kd_SKPD)->Nm_Sub_Unit;
            $data = array(
                'title' => 'Realisasi Anggaran ' . $nm . ' Tahun ' . $tahun,
                'tahun' => $tahun,
                'opd' => $Kd_SKPD,
                'realisasi' => $this->Keuangan->realisasiAnggaran($tahun, $Kd_SKPD)
            );
            $this->load->view('keuangan/_dataRealisasi', $data);
        } else {
            echo '';
        }
    }

    function dataRealisasiDownload()
    {
        $opd = urldecode($this->input->get('opd'));
        $tahun = $this->tahun;
        $nm = $this->master->subunitBykode($tahun, $opd)->Nm_Sub_Unit;
        $data = array(
            'title' => 'Realisasi Anggaran ' . $nm . ' Tahun ' . $tahun,
            'tahun' => $tahun,
            'opd' => $opd,
            'realisasi' => $this->Keuangan->realisasiAnggaran($tahun, $opd)
        );
        $this->load->view('keuangan/_dataRealisasi', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Realisasi_Anggaran.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
    }

    function neraca()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Neraca',
            'data' => $this->Keuangan->getNeraca($tahun)
        );
        return view('keuangan.neraca', $data);
    }

    function cetakNeraca()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Neraca',
            'data' => $this->Keuangan->getNeraca($tahun)
        );
        $this->load->view('keuangan/_neraca', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=neraca.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
    }

    function NeracaSubDetail()
    {
        $jenis  = urldecode($this->input->get('jenis', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tahun = $this->tahun;
        switch ($jenis) {
            case 'saldoawalaset':
                $query = $this->Keuangan->saldoawalaset($tahun, $nm_unit);
                $view = 'keuangan/neraca/saldoawalaset';
                $title = 'Neraca : Saldo Awal Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_saldoawalaset', $data_, true);
                break;

            case 'mutasiaset':
                $query = $this->Keuangan->mutasiAset($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Neraca : Mutasi Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_mutasiaset', $data_, true);
                break;

            case 'saldoawalkewajiban':
                $query = $this->Keuangan->saldoawalKewajiban($tahun, $nm_unit);
                $view = 'keuangan/neraca/saldoawalkewajiban';
                $title = 'Saldo Awal Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_saldoawalkewajiban', $data_, true);
                break;

            case 'mutasikewajiban':
                $query = $this->Keuangan->mutasikewajiban($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Mutasi Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_mutasiaset', $data_, true);
                break;

            default:
                // case 'mutasiekuitas' :
                $query = $this->Keuangan->mutasiekuitas($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Ekuitas';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='3' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;
        }

        $data = array(
            'title' => $title,
            'neraca_data' => $query,
            'nm_unit' => $nm_unit,
            'tahun' => $tahun,
            'kembali' => base_url() . 'neraca',
            'cetak' => base_url() . 'neraca/cetakdetail?jenis=' . urlencode($jenis) . '&nm_unit=' . urlencode($nm_unit),
            'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
            'result' => $result,
            'jenis' => $jenis
        );


        return view($view, $data);
    }

    function NeracadetailCetak()
    {
        $jenis  = urldecode($this->input->get('jenis', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tahun = $this->tahun;
        switch ($jenis) {
            case 'saldoawalaset':
                $query = $this->Keuangan->saldoawalaset($tahun, $nm_unit);
                $view = 'keuangan/neraca/saldoawalaset';
                $title = 'Neraca : Saldo Awal Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_saldoawalaset', $data_, true);
                break;

            case 'mutasiaset':
                $query = $this->Keuangan->mutasiAset($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Neraca : Mutasi Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_mutasiaset', $data_, true);
                break;

            case 'saldoawalkewajiban':
                $query = $this->Keuangan->saldoawalKewajiban($tahun, $nm_unit);
                $view = 'keuangan/neraca/saldoawalkewajiban';
                $title = 'Saldo Awal Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_saldoawalkewajiban', $data_, true);
                break;

            case 'mutasikewajiban':
                $query = $this->Keuangan->mutasikewajiban($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Mutasi Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                $data_ = array(
                    'neraca_data' => $query,
                    'nm_unit' => $nm_unit,
                    'tahun' => $tahun,
                    'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')',
                );
                $result = $this->load->view('keuangan/neraca/_mutasiaset', $data_, true);
                break;

            default:
                // case 'mutasiekuitas' :
                $query = $this->Keuangan->mutasiekuitas($tahun, $nm_unit);
                $view = 'keuangan/neraca/mutasiaset';
                $title = 'Ekuitas';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='3' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;
        }
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=neraca_" . $jenis . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $result;
    }

    function dataLO()
    {
        $tahun = $this->tahun;
        $lodetail = $this->Keuangan->getLoAllData($tahun);
        $data = array(
            'lodetail_data' => $lodetail,
            'title'        => 'Laporan Operasional',
        );
        return view('keuangan.dataLO', $data);
    }

    function dataLOCetak()
    {
        $tahun = $this->tahun;
        $lodetail = $this->Keuangan->getLoAllData($tahun);
        $data = array(
            'lodetail_data' => $lodetail,
            'title'        => 'Laporan Operasional',
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Laporan_operasional.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<h3>Laporan Operasional</h3>';
        echo $this->load->view('keuangan/_dataLO', $data, true);
    }

    function dataLOread()
    {
        $kd_skpd = urldecode($this->input->get('kd_skpd', TRUE));
        $nm_unit = urldecode($this->input->get('nm_unit', TRUE));
        $akun   = urldecode($this->input->get('akun'));
        $tahun = $this->tahun;
        $lodetail                = $this->Keuangan->getLoLevel3($tahun, $nm_unit, $akun);
        $label = $this->db->query("SELECT akun_akrual_2,nm_akrual_2,abs(sum(debet)-sum(kredit)) nilai_realisasi
                                 from tb_spe_detail_lo
                                 where nm_unit='$nm_unit'
                                 and tahun='$tahun'
                                 and Akun_Akrual_2='$akun'
                                 group by akun_akrual_2,nm_akrual_2")->row();

        $data_ = array('lodetail_data' => $lodetail, 'tahun' => $tahun, 'nm_unit' => $nm_unit);
        $result = $this->load->view('keuangan/_dataDetailLO', $data_, true);
        // : '.$nm_unit.' ('.$label->akun_akrual_2 . ' - ' . $label->nm_akrual_2 . ' / Rp.' . number_format($label->nilai_realisasi, 0, '', '.') . ')',
        $data = array(
            'result' => $result,
            'title' => 'Laporan operasional',
            'nm_unit' => $nm_unit,
            'akun' => $akun,
            'kd_skpd' => $kd_skpd
        );
        return view('keuangan.dataDetailLO', $data);
    }

    function dataLOreadCetak()
    {
        $kd_skpd = urldecode($this->input->get('kd_skpd', TRUE));
        $nm_unit = urldecode($this->input->get('nm_unit', TRUE));
        $akun   = urldecode($this->input->get('akun'));
        $tahun = $this->tahun;
        $lodetail                = $this->Keuangan->getLoLevel3($tahun, $nm_unit, $akun);
        $label = $this->db->query("SELECT akun_akrual_2,nm_akrual_2,abs(sum(debet)-sum(kredit)) nilai_realisasi
                                 from tb_spe_detail_lo
                                 where nm_unit='$nm_unit'
                                 and tahun='$tahun'
                                 and Akun_Akrual_2='$akun'
                                 group by akun_akrual_2,nm_akrual_2")->row();

        $data_ = array('lodetail_data' => $lodetail, 'tahun' => $tahun, 'nm_unit' => $nm_unit);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Laporan_operasional_detail.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo 'Laporan Operasional ' . $nm_unit . ' Akun ' . $akun;
        echo  $result = $this->load->view('keuangan/_dataDetailLO', $data_, true);
    }

    function saldoAwal()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Saldo Awal',
            'subunit' => $this->master->SubUnit($tahun)
        );
        return view('keuangan.saldoawal', $data);
    }

    function saldoAwaldata()
    {
        $tahun = $this->tahun;

        $kd_skpd = $this->input->post('Kd_SKPD', true);
        if ($kd_skpd <> '') {
            $res = $this->Keuangan->getSaldoAwal($tahun, $kd_skpd);
            $data = array(
                'saldoawal_data' => $res,
                'start' => 0,
                'Kd_SKPD'=>$kd_skpd,
            );
            echo $this->load->view('keuangan/_saldoawal', $data, true);
        } else {
            echo '';
        }
    }

    function saldoAwaldataCetak()
    {
        $tahun = $this->tahun;

        $kd_skpd = $this->input->get('Kd_SKPD', true);
        if ($kd_skpd <> '') {
            $res = $this->Keuangan->getSaldoAwal($tahun, $kd_skpd);
            $data = array(
                'saldoawal_data' => $res,
                'start' => 0,
                'Kd_SKPD'=>$kd_skpd
            );
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=saldo_awal.xls");
            header("Pragma: no-cache");
            header("Expires: 0");

            echo $this->load->view('keuangan/_saldoawal', $data, true);
        } else {
            echo '';
        }
    }
}
