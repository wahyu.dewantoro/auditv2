<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AsignRole extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('M_AsignRole');
        $this->load->model('Mpengguna');
        $this->load->model('Mrole');
        $this->load->library('form_validation');
        $this->menu_db = $this->load->database('default', true);
    }

    private function cekAkses($var = null)
    {
        $url = 'AsignRole';
        return cek($this->id_pengguna, $url, $var);
    }

    private function alert($type, $msg)
    {
        if ($type == 'success') {
            $icon = 'mdi mdi-check';
        } else {
            $icon = 'mdi mdi-alert-triangle';
        }

        $this->session->set_flashdata('message', '<div class="alert alert-contrast alert-' . $type . ' alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="' . $icon . '"></span></div>
                    <div class="message">' . $msg . '</div>
                  </div>');
    }


    public function index()
    {
        $akses = $this->cekAkses('read');
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'AsignRole?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'AsignRole?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'AsignRole';
            $config['first_url'] = base_url() . 'AsignRole';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->M_AsignRole->total_rows($q);
        $asignrole = $this->M_AsignRole->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'asignrole_data' => $asignrole,
            'q'              => $q,
            'pagination'     => $this->pagination->create_links(),
            'total_rows'     => $config['total_rows'],
            'start'          => $start,
            'title'          => 'Assign Role',
            'akses'          => $akses
		);
		return view('asignrole.view_index', $data);
    }

    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button'         => 'Simpan',
            'title'          => 'Tambah Assign Role',
            'action'         => site_url('AsignRole/create_action'),
            'id_inc'         => set_value('id_inc'),
            'ms_pengguna_id' => set_value('ms_pengguna_id'),
            'ms_role_id'     => set_value('ms_role_id'),
            'role'           => $this->Mrole->get_all(),
            'pengguna'       => $this->Mpengguna->get_all()
        );
        return view('asignrole.view_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            // cek dulu
            $res = $this->menu_db->select('count(id_inc) hsl')
                ->where('ms_pengguna_id', $this->input->post('ms_pengguna_id', TRUE))
                ->where('ms_role_id', $this->input->post('ms_role_id', TRUE))
                ->get('ms_assign_role')->row();

            if ($res->hsl == 0) {
                $data = array(
                    'ms_pengguna_id' => $this->input->post('ms_pengguna_id', TRUE),
                    'ms_role_id'     => $this->input->post('ms_role_id', TRUE),
                );

                $res = $this->M_AsignRole->insert($data);

                if ($res) {
                    $this->alert('success', 'Data berhasil di simpan.');
                } else {
                    $this->alert('warning', 'Data gagal di simpan.');
                }
            } else {
                $this->alert('warning', 'Data sudah ada.');
            }

            redirect(site_url('AsignRole'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->M_AsignRole->get_by_id($id);

        if ($row) {
            $data = array(
                'title'          => 'Edit Assign Role',
                'button'         => 'Update',
                'action'         => site_url('asignrole/update_action'),
                'id_inc'         => set_value('id_inc', $row->id_inc),
                'ms_pengguna_id' => set_value('ms_pengguna_id', $row->ms_pengguna_id),
                'ms_role_id'     => set_value('ms_role_id', $row->ms_role_id),
                'role'           => $this->Mrole->get_all(),
                'pengguna'       => $this->Mpengguna->get_all()
            );
            return view('asignrole.view_form', $data);
        } else {


            $this->alert('warning', 'Data tidak ada.');

            redirect(site_url('AsignRole'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {

            // cek dulu
            $res = $this->menu_db->select('count(id_inc) hsl')
                ->where('ms_pengguna_id', $this->input->post('ms_pengguna_id', TRUE))
                ->where('ms_role_id', $this->input->post('ms_role_id', TRUE))
                ->get('ms_assign_role')->row();

            if ($res->hsl == 0) {
                $data = array(
                    'ms_pengguna_id' => $this->input->post('ms_pengguna_id', TRUE),
                    'ms_role_id' => $this->input->post('ms_role_id', TRUE),
                );

                $res = $this->M_AsignRole->update($this->input->post('id_inc', TRUE), $data);
                if ($res) {
                    $this->alert('success', 'Data berhasil di update.');
                } else {
                    $this->alert('warning', 'Data gagal di update.');
                }
            }

            redirect(site_url('AsignRole'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id = rapikan($ide);
        $row = $this->M_AsignRole->get_by_id($id);

        if ($row) {
            $res = $this->M_AsignRole->delete($id);
            if ($res) {
                $this->alert('success', 'Data berhasil di hapus.');
            } else {
                $this->alert('warning', 'Data gagal di hapus.');
            }
            redirect(site_url('AsignRole'));
        } else {
            $this->alert('warning', 'Data tidak ada.');
            redirect(site_url('AsignRole'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('ms_pengguna_id', 'ms pengguna id', 'trim|required');
        $this->form_validation->set_rules('ms_role_id', 'ms role id', 'trim|required');

        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file AsignRole.php */
/* Location: ./application/controllers/AsignRole.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-22 16:37:00 */
/* http://harviacode.com */
