<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pengawas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mpengawas');
        $this->load->model('Mpengguna');
        $this->load->model('Msipil');
        $this->load->library('form_validation');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->menu_db = $this->load->database('default', true);
    }

    private function cekAkses($var = null)
    {
        $url = 'pengawas';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pengawas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengawas?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pengawas';
            $config['first_url'] = base_url() . 'pengawas';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        $config['total_rows']        = $this->Mpengawas->total_rows($q);

        $pengawas                      = $this->Mpengawas->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengawas_data' => $pengawas,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Pengawas',
            'akses'               => $akses
		);
		return view('pengawas.view_index', $data);
        // $this->template->load('layout', 'pengawas/view_index', $data);
    }

    public function read($ide)
    {
        $this->cekAkses('read');
        $id  = rapikan($ide);
        $row = $this->Mpengawas->get_by_id($id);
        if ($row) {
            $data = array(
                'title' => 'Detail pengawas',
                'id' => $row->id,
                'nama' => $row->nama,
                'instansi' => $row->instansi,
                'kota' => $row->kota,
            );
            $this->template->load('layout', 'pengawas/view_read', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengawas'));
        }
    }

    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button'   => 'Simpan',
            'title'    => 'Tambah pengawas',
            'action'   => site_url('pengawas/create_action'),
            'id'   => set_value('id'),
            'nama_pengawas'     => set_value('nama_pengawas'),
            'nama_cv'     => set_value('nama_cv'),
            'nm_sub_unit' => set_value('nm_sub_unit'),
            'kd_skpd'  => set_value('kd_skpd'),
            'email_pengawas'  => set_value('email_pengawas'),
            'skpd'     => $this->Msipil->get_opd()
        );
		// $this->template->load('layout', 'pengawas/view_form', $data);
		return view('pengawas.view_form', $data);

    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $instansi = explode("/", $this->input->post('kd_skpd', TRUE));
            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');
            $data = array(
                'nama_pengawas' => $this->input->post('nama_pengawas', TRUE),
                'nama_cv' => $this->input->post('nama_cv', TRUE),
                'email_pengawas' => $this->input->post('email_pengawas', TRUE),
                'nm_sub_unit' => $instansi[1],
                'kd_skpd' => $instansi[0],
                'created_by' => $this->id_pengguna,
                'created_at' => $datetime_formatted
            );
            $res = $this->Mpengawas->insert($data);
            if ($res) {

                $row['user'] = $this->menu_db->query("select TOP 1 username from ms_pengguna ORDER BY ref_pengawas_id DESC")->row();
                $row['pengawas'] = $this->menu_db->query("select TOP 1 kd_skpd,nm_sub_unit,nama_pengawas,nama_cv from ref_pengawas ORDER BY id DESC")->row();
                $pesan = $this->load->view('pengawas/kontenmail', $row, true);
                $email_to = array($this->input->post('email_pengawas', TRUE));
                $subject = "PEMBERITAHUAN USERNAME & PASSWORD";
                $qq = $this->Msipil->sendmail($email_to, $subject, $pesan);
                if ($qq == 1) {
                    set_flashdata('success', 'Data berhasil di simpan. Berhasil Kirim Email');
                } else {
                    set_flashdata('success', 'Data berhasil di simpan. Gagal Kirim Email');
                }
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }


            redirect(site_url('pengawas'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mpengawas->get_by_id($id);

        if ($row) {
            $data = array(
                'title'    => 'Edit pengawas',
                'button'   => 'Update',
                'action'   => site_url('pengawas/update_action'),
                'id'   => set_value('id', $row->id),
                'nama_pengawas'     => set_value('nama_pengawas', $row->nama_pengawas),
                'nama_cv'     => set_value('nama_cv', $row->nama_cv),
                'nm_sub_unit' => set_value('nm_sub_unit', $row->nm_sub_unit),
                'kd_skpd'  => set_value('kd_skpd', $row->kd_skpd),
                'email_pengawas' => set_value('email_pengawas', $row->email_pengawas),
                'skpd'     => $this->Msipil->get_opd()
			);
			return view('pengawas.view_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengawas'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $instansi = explode("/", $this->input->post('kd_skpd', TRUE));
            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');
            $data = array(
                'nama_pengawas' => $this->input->post('nama_pengawas', TRUE),
                'nama_cv' => $this->input->post('nama_cv', TRUE),
                'email_pengawas' => $this->input->post('email_pengawas', TRUE),
                'nm_sub_unit' => $instansi[1],
                'kd_skpd' => $instansi[0],
                'updated_by' => $this->id_pengguna,
                'updated_at' => $datetime_formatted
            );

            $res = $this->Mpengawas->update($this->input->post('id', TRUE), $data);
            if ($res) {
                set_flashdata('success', 'Data berhasil di update.');
            } else {
                set_flashdata('warning', 'Data gagal di update.');
            }
            redirect(site_url('pengawas'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id  = rapikan($ide);
        $row = $this->Mpengawas->get_by_id($id);

        if ($row) {
            $res = $this->Mpengawas->delete($id);
            if ($res) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
            redirect(site_url('pengawas'));
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengawas'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_pengawas', 'nama pengawas', 'trim|required');
        $this->form_validation->set_rules('nama_cv', 'nama cv', 'trim|required');
        $this->form_validation->set_rules('kd_skpd', 'instansi', 'trim|required');
        $this->form_validation->set_rules('email_pengawas', 'email', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
