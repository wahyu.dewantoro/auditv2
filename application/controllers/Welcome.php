<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->model('Mdashboard');
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->id_pengawas = $this->session->userdata('audit_id_pengawas');
        $this->role = $this->session->userdata('audit_role');
        // $this->db = $this->load->database('siskeudes', true);
    }

	private function cekAkses($var = null)
    {
        $url = 'welcome';
        return cek($this->id_pengguna, $url, $var);
    }


	public function index()
    {
		$tahun = $this->session->userdata('tahun_anggaran');
        $akses = $this->cekAkses('read');
        $res = get_userdata('audit_role');
        $sx = explode(',', $res);
        

        if (in_array('3', $sx) || in_array('1004', $sx)) {
            // ppk / pengawas
            if ($this->id_ppk <> '') {
                $this->db->where('id_ppk', $this->id_ppk);
            }
            if ($this->id_pengawas <> '') {
                $this->db->where('id_pengawas', $this->id_pengawas);
            }
			$this->db->select('count(1) jum', false);
			$this->db->where('tahun',$tahun);
            $jkeg = $this->db->get('PEKERJAAN_SIPIL')->row()->jum;
            $sbp = "(select id_ppk,id_pengawas,count(1) jum
                    FROM  PEKERJAAN_SIPIL A
                    JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                    LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                    WHERE tahun=$tahun and AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                    AND PERSENTASI_TOTAL IS NULL
                    group by id_ppk,id_pengawas ) a";

            if ($this->id_ppk <> '') {
                $this->db->where('id_ppk', $this->id_ppk);
            }
            if ($this->id_pengawas <> '') {
                $this->db->where('id_pengawas', $this->id_pengawas);
            }
            $this->db->select('sum(jum) jum', false);
            $bprogres = $this->db->get($sbp)->row()->jum;
            $data = array(
                'title'      => 'Dashboard',
                'kegiatan' => $jkeg,
                'bp' => $bprogres
            );
			return view('starter_dashboard', $data);
        } elseif (in_array('1005', $sx)) {
            $pendapatan = $this->db->query("select top 5  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
				from Ta_Rab
				where tahun=$tahun
                and left(kd_rincian,1)='4'
                group by tahun,Kd_Desa
                ) a
                join (select * from ref_desa where tahun=$tahun ) b on a.Kd_Desa=b.Kd_Desa
                join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                order by nilai desc")->result();
                $grafik1 = $this->db->query("select top 10  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_Rab
                    where left(kd_rincian,1)='4' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join ref_desa b on a.Kd_Desa=b.Kd_Desa
                    join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                    order by nilai desc")->result();
            $pendapatandua = $this->db->query("select top 5  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                from Ta_Rab
                where left(kd_rincian,1)='4' and tahun=$tahun
                group by tahun,Kd_Desa
                ) a
                join  (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                order by nilai asc")->result();
                $grafik2 = $this->db->query("select top 10  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_Rab
                    where left(kd_rincian,1)='4' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                    join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                    order by nilai asc")->result();

            $belanja = $this->db->query("select top 5  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                from Ta_RAB
                where left(kd_rincian,1)='5' and tahun=$tahun
                group by tahun,Kd_Desa
                ) a
                join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                join (select * from Ref_Kecamatan where tahun=$tahun ) c on c.Kd_Kec=b.Kd_Kec
                order by nilai desc")->result();
                $grafik3 = $this->db->query("select top 10  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='5' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join (select * from ref_desa where tahun=$tahun ) b on a.Kd_Desa=b.Kd_Desa
                    join (select * from Ref_Kecamatan  where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                    order by nilai desc")->result();
            $belanjadua = $this->db->query("select top 5  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                from Ta_RAB
                where left(kd_rincian,1)='5'  and tahun=$tahun
                group by tahun,Kd_Desa
                ) a
                join (select * from ref_desa  where tahun=$tahun ) b on a.Kd_Desa=b.Kd_Desa
                join (select * from Ref_Kecamatan where tahun=$tahun ) c on c.Kd_Kec=b.Kd_Kec
                order by nilai asc")->result();
                $grafik4 = $this->db->query("select top 10  a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='5' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                    join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                    order by nilai asc")->result();


            $persenrealisasi = $this->db->query("select top 5 a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                from Ta_RAB
                where left(kd_rincian,1)='5' and tahun=$tahun
                group by tahun,Kd_Desa
                ) a
                join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                from Ta_RAB
                where left(kd_rincian,1)='4' and tahun=$tahun
                group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) desc")->result();
                $grafik5 = $this->db->query("select top 10 a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='5' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                    join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                    join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='4' and tahun=$tahun
                    group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                    order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) desc")->result();
                $persenrealisasidua = $this->db->query("select top 5 a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai  as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                    select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='5' and tahun=$tahun
                    group by tahun,Kd_Desa
                    ) a
                    join (select * from ref_desa where tahun=$tahun) b on a.Kd_Desa=b.Kd_Desa
                    join (select * from Ref_Kecamatan where tahun=$tahun) c on c.Kd_Kec=b.Kd_Kec
                    join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                    from Ta_RAB
                    where left(kd_rincian,1)='4' and tahun=$tahun
                    group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                    order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) asc")->result();
                    $grafik6 = $this->db->query("select top 10 a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai  as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                        select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                        from Ta_RAB
                        where left(kd_rincian,1)='5' and tahun=$tahun
                        group by tahun,Kd_Desa
                        ) a
                        join (select * from ref_desa where tahun=$tahun ) b on a.Kd_Desa=b.Kd_Desa
                        join (select * from Ref_Kecamatan where tahun=$tahun ) c on c.Kd_Kec=b.Kd_Kec
                        join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                        from Ta_RAB
                        where left(kd_rincian,1)='4' and tahun=$tahun
                        group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                        order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) asc")->result();
                $data = array(
                    'title'      => 'Siskeudes Kab. Jombang',
                    'script'     => 'scritdepan2',
                    // 'rekap'      =>$rekap,
                    'pendapatan' => $pendapatan,
                    'grafik1' => $grafik1,
                    'belanja'    => $belanja,
                    'grafik2' => $grafik2,
                    'persenrealisasi'    => $persenrealisasi,
                    'grafik3' => $grafik3,
                    'pendapatandua' => $pendapatandua,
                    'grafik4' => $grafik4,
                    'belanjadua'    => $belanjadua,
                    'grafik5' => $grafik5,
                    'persenrealisasidua'    => $persenrealisasidua,
					'grafik6' => $grafik6,
                );
				// $this->template->load('layout', 'siskeudes', $data);
				return view('siskeudes',$data);
        } else {
            // 33,1004
            $q = urldecode($this->input->get('nm_unit', true));
            if ($q <> '') {
                $wh = "where tahun=$tahun and nm_unit ='$q'";
            } else {
                $wh = "where tahun=$tahun";
            }

            $unit  = $this->db->query("SELECT kd_skpd,nm_unit from TB_SPE_REF_UNIT order by kd_skpd")->result();
            $realisasi = $this->db->query("SELECT a.bulan, nama_bulan, ABS(SUM(nilai41) + SUM(nilai42) + SUM(nilai43)) AS pendapatan, SUM(nilai51) + SUM(nilai52) + SUM(nilai53) AS belanja
                                    FROM
                                    ref_nama_bulan a
                                    left join
                                    (
                                    SELECT bulan, Akun_Akrual_2, Nm_Akrual_2, nilai, CASE WHEN akun_akrual_2 = '4.1' THEN nilai ELSE 0 END AS nilai41, CASE WHEN akun_akrual_2 = '4.2' THEN nilai ELSE 0 END AS nilai42,
                                                CASE WHEN akun_akrual_2 = '4.3' THEN nilai ELSE 0 END AS nilai43, CASE WHEN akun_akrual_2 = '5.1' THEN nilai ELSE 0 END AS nilai51, CASE WHEN akun_akrual_2 = '5.2' THEN nilai ELSE 0 END AS nilai52,
                                                CASE WHEN akun_akrual_2 = '5.3' THEN nilai ELSE 0 END AS nilai53
                                                FROM(
                                                    SELECT  MONTH(Tgl_Bukti) AS bulan, Akun_Akrual_2, Nm_Akrual_2, SUM(Debet) - SUM(Kredit) AS nilai
                                                    FROM TB_SPE_DETAIL_LRA
                                                    $wh
                                                    GROUP BY MONTH(Tgl_Bukti), Akun_Akrual_2, Nm_Akrual_2
                                                ) AS lv0
                                    ) AS b
                                    on a.bulan=b.bulan
                                    GROUP BY a.bulan,nama_bulan
                                    order by a.bulan")->result();



            $pendapatan = $this->db->query("select top 5 kd_skpd,nm_unit,persen_pendapatan
                                    from (select * from audit_realisasi_persen where tahun=$tahun ) a
                                    order by persen_pendapatan desc")->result();
            $pendapatandua = $this->db->query("select top 5 kd_skpd,nm_unit,persen_pendapatan
                                    from (select * from audit_realisasi_persen where tahun=$tahun ) a
                                    where persen_pendapatan > 0
                                    order by persen_pendapatan asc")->result();

            $belanja = $this->db->query("select top 5 kd_skpd,nm_unit,persen_belanja
                                    from (select * from audit_realisasi_persen where tahun=$tahun ) a
                                    order by persen_belanja desc")->result();
            $belanjadua = $this->db->query("select top 5 kd_skpd,nm_unit,persen_belanja
                                    from (select * from audit_realisasi_persen where tahun=$tahun ) a
                                    order by persen_belanja asc")->result();

            $data = array(
                'realisasi'  => $realisasi,
                'title'      => 'Realisasi APBD Pemerintah Kab. Jombang',
                'script'     => 'scritdepan',
                'unit'       => $unit,
                'q'          => $q,
                // 'rekap'      =>$rekap,
                'pendapatan' => $pendapatan,
                'belanja'    => $belanja,
                'pendapatandua' => $pendapatandua,
                'belanjadua'    => $belanjadua
            );
			
			return view('starter',$data);
        }
	}
	function siskeudes()
    {
        $jenis = urldecode($this->input->get('jenis'));
        $type = urldecode($this->input->get('type'));
        $asdf=1;
        switch ($jenis) {
            case 'pendapatan':
                # pendapatan
                switch ($type) {
                    case 'tinggi':
                        # tiinggi
                        $sql = "select a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                            select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                            from Ta_Rab
                            where left(kd_rincian,1)='4'
                            group by tahun,Kd_Desa
                            ) a
                            join ref_desa b on a.Kd_Desa=b.Kd_Desa
                            join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                            order by nilai desc";
                        break;

                    default:
                        # rendah
                        $sql = "select a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                            select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                            from Ta_Rab
                            where left(kd_rincian,1)='4'
                            group by tahun,Kd_Desa
                            ) a
                            join ref_desa b on a.Kd_Desa=b.Kd_Desa
                            join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                            order by nilai asc";
                        break;
                }

                break;

                case 'realisasi':
                    # pendapatan
                    switch ($type) {
                        case 'tinggi':
                            # tiinggi
                            $asdf=2;
                            $sql = "select  a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                                from Ta_RAB
                                where left(kd_rincian,1)='5'
                                group by tahun,Kd_Desa
                                ) a
                                join ref_desa b on a.Kd_Desa=b.Kd_Desa
                                join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                                join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                                from Ta_RAB
                                where left(kd_rincian,1)='4'
                                group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                                order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) desc";
                            break;

                        default:
                            # rendah
                            $asdf=2;
                            $sql = "select  a.kd_desa,Nama_Desa,Nama_Kecamatan,d.nilai as pendapatan, a.nilai as realisasi,CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) persen from (
                                select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                                from Ta_RAB
                                where left(kd_rincian,1)='5'
                                group by tahun,Kd_Desa
                                ) a
                                join ref_desa b on a.Kd_Desa=b.Kd_Desa
                                join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                                join (select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                                from Ta_RAB
                                where left(kd_rincian,1)='4'
                                group by tahun,Kd_Desa) d on d.Kd_Desa=b.Kd_Desa
                                order by CONVERT(DECIMAL(10,2),a.nilai/d.nilai*100) asc";
                            break;
                    }

                    break;
            default:
                # belanja
                switch ($type) {
                    case 'tinggi':
                        # tiinggi
                        $sql = "select a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                            select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                            from Ta_RAB
                            where left(kd_rincian,1)='5'
                            group by tahun,Kd_Desa
                            ) a
                            join ref_desa b on a.Kd_Desa=b.Kd_Desa
                            join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                            order by nilai desc";
                        break;

                    default:
                        # rendah
                        $sql = "select a.kd_desa,Nama_Desa,Nama_Kecamatan,nilai from (
                            select tahun,Kd_Desa,case when sum(anggaranStlhpak) > 0 then sum(anggaranstlhpak) else sum(anggaran) end nilai
                            from Ta_RAB
                            where left(kd_rincian,1)='5'
                            group by tahun,Kd_Desa
                            ) a
                            join ref_desa b on a.Kd_Desa=b.Kd_Desa
                            join Ref_Kecamatan c on c.Kd_Kec=b.Kd_Kec
                            order by nilai asc";
                        break;
                }
                break;
        }

        $data = $this->db->query($sql)->result();
        $type=$type=='tinggi'?"Tertinggi":"Terendah";
        $data = array(
            'title' => $asdf==1? ucwords('Peringkat ' . $jenis.' '.$type):ucwords('Peringkat Realisasi '.$type),
            'data' => $data,
            'kembali' => base_url() . 'welcome',
            'asdf' => $asdf
        );

		// $this->template->load('layout', 'detailrealisasi2', $data);
		return view('detailrealisasi2',$data);
	}
    function realisasi()
    {
        $jenis = urldecode($this->input->get('jenis'));
        $type = urldecode($this->input->get('type'));
        switch ($jenis) {
            case 'pendapatan':
                # pendapatan
                switch ($type) {
                    case 'tinggi':
                        # tiinggi
                        $sql = "select kd_skpd,nm_unit,persen_pendapatan hasil
                                from audit_realisasi_persen
                                where persen_pendapatan >0
                                order by persen_pendapatan desc";
                        break;

                    default:
                        # rendah
                        $sql = "select kd_skpd,nm_unit,persen_pendapatan hasil
                                from audit_realisasi_persen
                                where persen_pendapatan >0
                                order by persen_pendapatan asc";
                        break;
                }

                break;

            default:
                # belanja
                switch ($type) {
                    case 'tinggi':
                        # tiinggi
                        $sql = "select kd_skpd,nm_unit,persen_belanja hasil
                                from audit_realisasi_persen
                                order by persen_belanja desc";
                        break;

                    default:
                        # rendah
                        $sql = "select kd_skpd,nm_unit,persen_belanja hasil
                                from audit_realisasi_persen
                                order by persen_belanja asc";
                        break;
                }
                break;
        }



        $data = $this->db->query($sql)->result();
        $data = array(
            'title' => ucwords('Peringkat Realisasi ' . $jenis),
            'data' => $data,
            'kembali' => base_url() . 'welcome',
        );

		// $this->template->load('layout', 'detailrealisasi', $data);
		return view('detailrealisasi',$data);
    }
}
