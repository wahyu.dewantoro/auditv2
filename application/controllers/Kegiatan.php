<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kegiatan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mkegiatan');
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->role = $this->session->userdata('audit_role');
        $this->kd_skpd = $this->session->userdata('audit_kd_skpd');
        $this->load->library('form_validation');
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }
    private function cekAkses($var = null)
    {
        $url = 'kegiatan';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'kegiatan?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kegiatan?q=' . urlencode($q);
            $cetak               = base_url() . 'kegiatan/cetak2?id_ppk=' . urlencode($id_ppk);
        } else {
            $config['base_url']  = base_url() . 'kegiatan';
            $config['first_url'] = base_url() . 'kegiatan';
            $cetak               = base_url() . 'kegiatan/cetak2';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $config['total_rows']        = $this->Mkegiatan->total_rows($q);
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $sipil                       = $this->Mkegiatan->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'akses'      => $akses,
            'title'      => 'Monitoring Pekerjaan Sipil',
            'data'       => $sipil,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'ppk'        => $this->Mkegiatan->getPPK(),
            'id_ppk'     => $id_ppk,
            'cetak'     => $cetak,
        );
        return view('kegiatan.new.view_index', $data);
        // echo "<pre>";
        // print_r($data);
    }

    public function cetak2()
    {
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $sipil      = $this->Mkegiatan->get_all_data();
        $data= array(
            'data' => $sipil,
            'start' => 0
        );
        return view('kegiatan/cetak2', $data);
    }

    function create()
    {
        $akses = $this->cekAkses('create');
        if($this->role==3 && $this->id_ppk<>''){
            $this->db->where("kd_skpd ",$this->kd_skpd);
        }
        $opd=$this->Mkegiatan->get_opd();
        if($this->role==3){
            $this->db->where('kd_skpd',$this->kd_skpd);
        }

        $ppk=$this->Mkegiatan->getPPK();
        $data = array(
            'title'                   => 'Tambah Belanja Modal ',
            'action'                  => base_url() . 'kegiatan/create_action',
            'belanja'                 => $this->Mkegiatan->getJenisBelanja(),
            'opd'                     => $opd,
            'ppk'                     => $ppk,
            // 'konsultan_list'          => $this->Mkegiatan->getKonsultan(),
            'pengawas_list'           => $this->Mkegiatan->getPengawas(),
            'id'                      => set_value('id'),
            'kode_skpd'               => set_value('kode_skpd'),
            'nama_skpd'               => set_value('nama_skpd'),
            'kode_belanja'            => set_value('kode_belanja'),
            'jenis_belanja'           => set_value('jenis_belanja'),
            // 'nama_kegiatan'           => set_value('nama_kegiatan'),
            'sub_kegiatan'           => set_value('sub_kegiatan'),
            'pagu_anggaran'           => set_value('pagu_anggaran'),
            'harga_perkiraan'         => set_value('harga_perkiraan'),
            'nama_ppk'                => set_value('nama_ppk'),
            'id_ppk'                  => set_value('id_ppk', $this->id_ppk),
            'nilai_spk'               => set_value('nilai_spk'),
            'no_spk'                  => set_value('no_spk'),
            'tanggal_spk'             => set_value('tanggal_spk'),
            'mulai_pekerjaan_spk'     => set_value('mulai_pekerjaan_spk'),
            'selesai_pekerjaan_spk' => set_value('selesai_pekerjaan_spk'),
            'nilai_addedum'           => set_value('nilai_addedum'),
            'no_addedum'              => set_value('no_addedum'),
            'tanggal_addedum'         => set_value('tanggal_addedum'),
            'mulai_pekerjaan_addedum' => set_value('mulai_pekerjaan_addedum'),
            'selesai_pekerjaan_addedum' => set_value('selesai_pekerjaan_addedum'),
            'pelaksana'               => set_value('pelaksana'),
            'no_spmk'                 => set_value('no_spmk'),
            'tanggal_spmk'            => set_value('tanggal_spmk'),
            'tanggal_bast'            => set_value('tanggal_bast'),
            'kode_program' => set_value('kode_program'),
            'ket_kegiatan' => set_value('ket_kegiatan'),
        );
        return view('kegiatan.new.view_form', $data);
    }


    function create_action()
    {
        $akses = $this->cekAkses('create');

        $tahun=$this->tahun;
        $this->rules_();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $ppk=explode("/",$this->input->post('nama_ppk', true));


            $kode_skpd               = $this->input->post('kode_skpd', true);
            $nama_skpd               = $this->Mkegiatan->getopdbykode($kode_skpd);
            $kode_belanja            = $this->input->post('kode_belanja', true);
            $jenis_belanja           = $this->Mkegiatan->getNamaBelanja($kode_belanja);
            $nama_kegiatan           = $this->input->post('nama_kegiatan', true);
            $sub_kegiatan           = $this->input->post('sub_kegiatan', true);
            $pagu_anggaran           = $this->input->post('pagu_anggaran', true);
            $harga_perkiraan         = $this->input->post('harga_perkiraan', true);
            $nama_ppk                = $ppk[1];
            $id_ppk                  = $ppk[0];
            $no_spk                  = $this->input->post('no_spk', true);
            $tanggal_spk             = $this->input->post('tanggal_spk', true);
            $mulai_pekerjaan_spk     = $this->input->post('mulai_pekerjaan_spk', true);
            $selesai_pekerjaan_spk = $this->input->post('selesai_pekerjaan_spk', true);
            $nilai_spk               = $this->input->post('nilai_spk', true);
            $no_addedum              = $this->input->post('no_addedum', true);
            $tanggal_addedum         = $this->input->post('tanggal_addedum', true);
            $mulai_pekerjaan_addedum = $this->input->post('mulai_pekerjaan_addedum', true);
            $selesai_pekerjaan_addedum = $this->input->post('selesai_pekerjaan_addedum', true);
            $nilai_addedum           = $this->input->post('nilai_addedum', true);
            $pelaksana               = $this->input->post('pelaksana', true);
            $no_spmk                 = $this->input->post('no_spmk', true);
            $tanggal_spmk            = $this->input->post('tanggal_spmk', true);
            $kode_program = $this->input->post('kode_program');
            $kode_kegiatan = $this->input->post('kode_kegiatan');
            $ket_keg = $this->input->post('ket_kegiatan');
            // program kegiatan
            $ff = $this->db->query("select * from spe_ref_kegiatan where tahun='$tahun' and kd_sub_unit='" . $kode_skpd . "' and  ID_Prog='" . $kode_program . "' and kd_keg='" . $kode_kegiatan . "' and ket_kegiatan='" . $ket_keg . "'")->row();

            // echo $this->db->last_query();
            //     die();

            $kd_prog = "";
            $id_prog = "";
            $ket_prog = "";
            $kd_keg = "";
            $ket_keg = "";

            if ($ff) {
                $kd_prog = $ff->Kd_Prog;
                $id_prog = $ff->ID_Prog;
                $ket_prog = $ff->Ket_Program;
                $kd_keg = $ff->kd_keg;
                $ket_keg = $ff->ket_kegiatan;
            }


            $data = array(
                'kode_skpd'       => $kode_skpd,
                'nama_skpd' => $nama_skpd,
                'kode_belanja'    => $kode_belanja,
                'jenis_belanja' => $jenis_belanja,
                'nama_kegiatan'   => $nama_kegiatan,
                'sub_kegiatan'   => $sub_kegiatan,
                'pagu_anggaran'   => str_replace('.', '', $pagu_anggaran),
                'harga_perkiraan' => str_replace('.', '', $harga_perkiraan),
                'nama_ppk'        => $nama_ppk,
                'id_ppk'          => $id_ppk,
                'no_spk'          => $no_spk,
                'nilai_spk'       => str_replace('.', '', $nilai_spk),
                'no_addedum'      => $no_addedum,
                'nilai_addedum'   => str_replace('.', '', $nilai_addedum),
                'pelaksana'       => $pelaksana,
                'no_spmk'         => $no_spmk,
                'user_create' => $this->id_pengguna,
                'created_at' => date('Y-m-d H:i:s'),
                'kd_prog' => $kd_prog,
                'id_prog' => $id_prog,
                'ket_prog' => $ket_prog,
                'kd_keg' => $kd_keg,
                'ket_keg' => $ket_keg,
                'tahun'=>$tahun
            );


            $this->db->set('tanggal_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_spk))));
            $this->db->set('mulai_pekerjaan_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $mulai_pekerjaan_spk))));
            $this->db->set('selesai_pekerjaan_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $selesai_pekerjaan_spk))));

            // $selesai_pekerjaan_addedum=$this->input->post('selesai_pekerjaan_addedum',true);

            if ($tanggal_addedum <> '') {
                $this->db->set('tanggal_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_addedum))));
            }
            if ($mulai_pekerjaan_addedum <> '') {
                $this->db->set('mulai_pekerjaan_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $mulai_pekerjaan_addedum))));
            }
            if ($selesai_pekerjaan_addedum <> '') {
                $this->db->set('selesai_pekerjaan_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $selesai_pekerjaan_addedum))));
            }

            $this->db->set('tanggal_spmk', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_spmk))));
            $res = $this->db->insert('KEGIATAN_BELANJA', $data);

            // echo "<pre>";
            // print_r($data);


            if ($res) {
                set_flashdata('success', 'Data berhasil di simpan.');
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }

            redirect(site_url('kegiatan'));
        }
    }


    function update($ide)
    {
        $akses = $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mkegiatan->get_by_id($id);


        if ($row) {
            $this->role==3?$this->db->where('kd_skpd',$this->kd_skpd):"";
            $opd=$this->Mkegiatan->get_opd();
            $this->role==3?$this->db->where('kd_skpd',$this->kd_skpd):$this->db->where('kd_skpd',$row->kode_skpd);
            $ppk=$this->Mkegiatan->getPPK();
            $data = array(
                'title'                   => 'Edit Data',
                'action'                  => base_url() . 'kegiatan/update_action',
                'belanja'                 => $this->Mkegiatan->getJenisBelanja(),
                'opd'                     => $opd,
                'ppk'                     => $ppk,
                // 'konsultan_list'          => $this->Mkegiatan->getKonsultan(),
                // 'pengawas_list'           => $this->Mkegiatan->getPengawas(),
                'id'                      => set_value('id', $row->id),
                'kode_skpd'               => set_value('kode_skpd', $row->kode_skpd),
                'nama_skpd'               => set_value('nama_skpd', $row->nama_skpd),
                'kode_belanja'            => set_value('kode_belanja', $row->kode_belanja),
                'jenis_belanja'           => set_value('jenis_belanja', $row->jenis_belanja),
                'nama_kegiatan'           => set_value('nama_kegiatan', $row->nama_kegiatan),
                'sub_kegiatan'           => set_value('sub_kegiatan', $row->sub_kegiatan),
                'pagu_anggaran'           => set_value('pagu_anggaran', angka($row->pagu_anggaran)),
                'harga_perkiraan'         => set_value('harga_perkiraan', angka($row->harga_perkiraan)),
                'nama_ppk'                => set_value('nama_ppk', $row->nama_ppk),
                'id_ppk'                  => set_value('id_ppk', $row->id_ppk),
                'nilai_spk'               => set_value('nilai_spk', angka($row->nilai_spk)),
                'no_spk'                  => set_value('no_spk', $row->no_spk),
                'tanggal_spk'             => set_value('tanggal_spk', $row->tanggal_spk <> '' ? date('d/m/Y', strtotime($row->tanggal_spk)) : ''),
                'mulai_pekerjaan_spk'     => set_value('mulai_pekerjaan_spk', $row->mulai_pekerjaan_spk <> '' ? date('d/m/Y', strtotime($row->mulai_pekerjaan_spk)) : ''),
                'selesai_pekerjaan_spk' => set_value('selesai_pekerjaan_spk', $row->selesai_pekerjaan_spk <> '' ? date('d/m/Y', strtotime($row->selesai_pekerjaan_spk)) : ''),
                'nilai_addedum'           => set_value('nilai_addedum', angka($row->nilai_addedum)),
                'no_addedum'              => set_value('no_addedum', $row->no_addedum),
                'tanggal_addedum'         => set_value('tanggal_addedum', $row->tanggal_addedum <> '' ? date('d/m/Y', strtotime($row->tanggal_addedum)) : ''),
                'mulai_pekerjaan_addedum' => set_value('mulai_pekerjaan_addedum', $row->mulai_pekerjaan_addedum <> '' ? date('d/m/Y', strtotime($row->mulai_pekerjaan_addedum)) : ''),
                'selesai_pekerjaan_addedum' => set_value('selesai_pekerjaan_addedum', $row->selesai_pekerjaan_addedum <> '' ? date('d/m/Y', strtotime($row->selesai_pekerjaan_addedum)) : ''),
                'pelaksana'               => set_value('pelaksana', $row->pelaksana),
                'no_spmk'                 => set_value('no_spmk', $row->no_spmk),
                'tanggal_spmk'            => set_value('tanggal_spmk', $row->tanggal_spmk <> '' ? date('d/m/Y', strtotime($row->tanggal_spmk)) : ''),
                'tanggal_bast'            => set_value('tanggal_bast', $row->tanggal_bast <> '' ? date('d/m/Y', strtotime($row->tanggal_bast)) : ''),
                'kode_program' => set_value('kode_program', $row->kd_prog . '.' . $row->id_prog),
                'lp' => $this->db->query("select Kd_Prog,ID_Prog,Ket_Program from SPE_REF_PROGRAM where kd_sub_unit='" . $row->kode_skpd . "'")->result(),
                'kd_keg' => $row->kd_keg,
                'ket_kegiatan' => $row->ket_keg,
                'lk' => $this->db->query("select * from spe_ref_kegiatan where kd_sub_unit='" . $row->kode_skpd . "' and concat(Kd_Prog,'.',ID_Prog)='" . $row->kd_prog . "." . $row->id_prog . "'")->result()
            );


            return view('kegiatan/view_form', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('kegiatan'));
        }
    }

    function update_action()
    {
        $akses = $this->cekAkses('update');
        $this->rules_();
        if ($this->form_validation->run() == FALSE) {
            $this->update(acak($this->input->post('id')));
        } else {

            $ppk=explode("/",$this->input->post('nama_ppk', true));
            $kode_skpd = $this->input->post('kode_skpd', true);
            $nama_skpd               = $this->Mkegiatan->getopdbykode($kode_skpd);
            $kode_belanja            = $this->input->post('kode_belanja', true);
            $jenis_belanja = $this->Mkegiatan->getNamaBelanja($kode_belanja);
            $nama_kegiatan           = $this->input->post('nama_kegiatan', true);
            $sub_kegiatan           = $this->input->post('sub_kegiatan', true);
            $pagu_anggaran           = $this->input->post('pagu_anggaran', true);
            $harga_perkiraan         = $this->input->post('harga_perkiraan', true);
            $nama_ppk                = $ppk[1];
            $id_ppk                  = $ppk[0];
            $no_spk                  = $this->input->post('no_spk', true);
            $tanggal_spk             = $this->input->post('tanggal_spk', true);
            $mulai_pekerjaan_spk     = $this->input->post('mulai_pekerjaan_spk', true);
            $selesai_pekerjaan_spk     = $this->input->post('selesai_pekerjaan_spk', true);
            $nilai_spk               = $this->input->post('nilai_spk', true);
            $no_addedum              = $this->input->post('no_addedum', true);
            $tanggal_addedum         = $this->input->post('tanggal_addedum', true);
            $mulai_pekerjaan_addedum = $this->input->post('mulai_pekerjaan_addedum', true);
            $selesai_pekerjaan_addedum = $this->input->post('selesai_pekerjaan_addedum', true);
            $nilai_addedum           = $this->input->post('nilai_addedum', true);
            $pelaksana               = $this->input->post('pelaksana', true);
            $no_spmk                 = $this->input->post('no_spmk', true);
            $tanggal_spmk            = $this->input->post('tanggal_spmk', true);
            $kode_program = $this->input->post('kode_program');
            $kode_kegiatan = $this->input->post('kode_kegiatan');
            $ket_keg = $this->input->post('ket_kegiatan');
            // program kegiatan
            $ff = $this->db->query("select * from spe_ref_kegiatan where kd_sub_unit='" . $kode_skpd . "' and concat(Kd_Prog,'.',ID_Prog)='" . $kode_program . "' and kd_keg='" . $kode_kegiatan . "' and ket_kegiatan='" . $ket_keg . "'")->row();

            // echo $this->db->last_query();
            //     die();

            $kd_prog = "";
            $id_prog = "";
            $ket_prog = "";
            $kd_keg = "";
            $ket_keg = "";

            if ($ff) {
                $kd_prog = $ff->Kd_Prog;
                $id_prog = $ff->ID_Prog;
                $ket_prog = $ff->Ket_Program;
                $kd_keg = $ff->kd_keg;
                $ket_keg = $ff->ket_kegiatan;
            }


            $data = array(
                'kode_skpd'       => $kode_skpd,
                'nama_skpd' => $nama_skpd,
                'kode_belanja'    => $kode_belanja,
                'jenis_belanja' => $jenis_belanja,
                'nama_kegiatan'   => $nama_kegiatan,
                'sub_kegiatan'   => $sub_kegiatan,
                'pagu_anggaran'   => str_replace('.', '', $pagu_anggaran),
                'harga_perkiraan' => str_replace('.', '', $harga_perkiraan),
                'nama_ppk'        => $nama_ppk,
                'id_ppk'          => $id_ppk,
                'no_spk'          => $no_spk,
                'nilai_spk'       => str_replace('.', '', $nilai_spk),
                'no_addedum'      => $no_addedum,
                'nilai_addedum'   => str_replace('.', '', $nilai_addedum),
                'pelaksana'       => $pelaksana,
                'no_spmk'         => $no_spmk,
                'user_create' => $this->id_pengguna,
                'created_at' => date('Y-m-d H:i:s'),
                'kd_prog' => $kd_prog,
                'id_prog' => $id_prog,
                'ket_prog' => $ket_prog,
                'kd_keg' => $kd_keg,
                'ket_keg' => $ket_keg
            );


            $this->db->set('tanggal_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_spk))));
            $this->db->set('mulai_pekerjaan_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $mulai_pekerjaan_spk))));
            $this->db->set('selesai_pekerjaan_spk', date('Y-m-d', strtotime(str_replace('/', '-',  $selesai_pekerjaan_spk))));

            if ($tanggal_addedum <> '') {
                $this->db->set('tanggal_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_addedum))));
            }
            if ($mulai_pekerjaan_addedum <> '') {
                $this->db->set('mulai_pekerjaan_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $mulai_pekerjaan_addedum))));
            }

            if ($selesai_pekerjaan_addedum <> '') {
                $this->db->set('selesai_pekerjaan_addedum', date('Y-m-d', strtotime(str_replace('/', '-',  $selesai_pekerjaan_addedum))));
            }

            $this->db->set('tanggal_spmk', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_spmk))));
            $this->db->where('id', $this->input->post('id', true));
            $res = $this->db->update('KEGIATAN_BELANJA', $data);
            if ($res) {
                set_flashdata('success', 'Data berhasil di simpan.');
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }
            // echo $this->db->last_query();
            redirect(site_url('kegiatan'));
        }
    }

    function delete($ide)
    {
        $akses = $this->cekAkses('delete');
        $id = rapikan($ide);

        $res = $this->Mkegiatan->get_by_id($id);

        if ($res) {
            $this->db->trans_start();
            $this->db->where('kegiatan_belanja_id', $id);
            $this->db->delete('KEGIATAN_BELANJA_JADWAL');

            $this->db->where('id', $id);
            $rr = $this->db->delete('KEGIATAN_BELANJA');

            $this->db->where('kegiatan_belanja_id', $id);
            $row=$this->db->get('KEGIATAN_BELANJA_FILE')->result();
            foreach ($row as $rk) {
                if (file_exists($rk->nama_file)){
                    unlink($rk->nama_file);
                    $this->db->where('id', $rk->id);
                    $this->db->delete('KEGIATAN_BELANJA_FILE');
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
        }
        redirect(site_url('kegiatan'));
    }

    function read($ide)
    {
        $akses = $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Mkegiatan->get_by_id($id);
        if ($row) {
            $data = array(
                'title'                   => 'Detail pekerjaan sipil',
                'kode_skpd'               => $row->kode_skpd,
                'nama_skpd'               => $row->nama_skpd,
                'kode_belanja'            => $row->kode_belanja,
                'jenis_belanja'           => $row->jenis_belanja,
                'nama_kegiatan'           => $row->nama_kegiatan,
                'sub_kegiatan'           => $row->sub_kegiatan,
                'pagu_anggaran'           => $row->pagu_anggaran,
                'harga_perkiraan'         => $row->harga_perkiraan,
                'nama_ppk'                => $row->nama_ppk,
                'nilai_spk'               => $row->nilai_spk,
                'no_spk'                  => $row->no_spk,
                'tanggal_spk'             => $row->tanggal_spk,
                'mulai_pekerjaan_spk'     => $row->mulai_pekerjaan_spk,
                'selesai_pekerjaan_spk'     => $row->selesai_pekerjaan_spk,
                'nilai_addedum'           => $row->nilai_addedum,
                'no_addedum'              => $row->no_addedum,
                'tanggal_addedum'         => $row->tanggal_addedum,
                'mulai_pekerjaan_addedum' => $row->mulai_pekerjaan_addedum,
                'selesai_pekerjaan_addedum' => $row->selesai_pekerjaan_addedum,
                'pelaksana'               => $row->pelaksana,
                'no_spmk'                 => $row->no_spmk,
                'tanggal_spmk'            => $row->tanggal_spmk,
                'tanggal_bast'            => $row->tanggal_bast,
                'id'                      => $row->id,
                'akses'                   => $akses,
                'file' => $this->Mkegiatan->get_file($id)
            );
            // $this->template->load('layout', 'kegiatan/view_read', $data);
            return view('kegiatan.new.view_read', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('kegiatan'));
        }
    }


    function rules_()
    {
        $this->form_validation->set_rules('kode_skpd', 'kode_skpd', 'trim|required');
        $this->form_validation->set_rules('kode_belanja', 'kode_belanja', 'trim|required');

        $this->form_validation->set_rules('pagu_anggaran', 'pagu_anggaran', 'trim|required');
        $this->form_validation->set_rules('harga_perkiraan', 'harga_perkiraan', 'trim|required');
        $this->form_validation->set_rules('nama_ppk', 'nama_ppk', 'trim|required');
        $this->form_validation->set_rules('no_spk', 'no_spk', 'trim|required');
        $this->form_validation->set_rules('tanggal_spk', 'tanggal_spk', 'trim|required');
        $this->form_validation->set_rules('mulai_pekerjaan_spk', 'mulai_pekerjaan_spk', 'trim|required');
        $this->form_validation->set_rules('nilai_spk', 'nilai_spk', 'trim|required');

        $this->form_validation->set_rules('pelaksana', 'pelaksana', 'trim|required');
        $this->form_validation->set_rules('no_spmk', 'no_spmk', 'trim|required');
        $this->form_validation->set_rules('tanggal_spmk', 'tanggal_spmk', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    private function _uploadfile($nama_file)
    {
        $config['upload_path']   = './file_progres/';
        $config['allowed_types'] = 'pdf|zip|rar';
        $config['file_name']     = $nama_file;
        $config['overwrite']     = true;
        $config['max_size']      = 2048; // 2MB

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file_progres')) {
            return 'file_progres/' . $this->upload->data("file_name");
        } else {
            return null;
        }
    }

    function cetak($ide)
    {
        $akses = $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Mkegiatan->get_by_id($id);

        if ($row) {

            $data = array(
                'title'                   => 'Detail pekerjaan',
                'kode_skpd'               => $row->kode_skpd,
                'nama_skpd'               => $row->nama_skpd,
                'kode_belanja'            => $row->kode_belanja,
                'jenis_belanja'           => $row->jenis_belanja,
                'nama_kegiatan'           => $row->nama_kegiatan,
                'sub_kegiatan'           => $row->sub_kegiatan,
                'pagu_anggaran'           => $row->pagu_anggaran,
                'harga_perkiraan'         => $row->harga_perkiraan,
                'nama_ppk'                => $row->nama_ppk,
                'nilai_spk'               => $row->nilai_spk,
                'no_spk'                  => $row->no_spk,
                'tanggal_spk'             => $row->tanggal_spk,
                'mulai_pekerjaan_spk'     => $row->mulai_pekerjaan_spk,
                'nilai_addedum'           => $row->nilai_addedum,
                'no_addedum'              => $row->no_addedum,
                'tanggal_addedum'         => $row->tanggal_addedum,
                'mulai_pekerjaan_addedum' => $row->mulai_pekerjaan_addedum,
                'pelaksana'               => $row->pelaksana,
                'no_spmk'                 => $row->no_spmk,
                'tanggal_spmk'            => $row->tanggal_spmk,
                'tanggal_bast'            => $row->tanggal_bast,
                'id'                      => $row->id,
                'akses'                   => $akses,
                'script' => 'progres_sipil/js_progres'
            );
            $mpdf = new \Mpdf\Mpdf(['format' => 'A4-L']);
            $html = $this->load->view('kegiatan/cetak', $data,true);
            $mpdf->WriteHTML($html);
            $mpdf->Output('yourFileName.pdf', 'I');
            // $this->template->load('layout', 'progres_sipil/view_read', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('sipil'));
        }
    }
    function download()
    {
        $akses = $this->cekAkses('read');
        $this->load->helper('download');
        /*print_r($this->input->get());
        echo "<hr>";*/
        $file = str_replace('zz', '/', $this->input->get('file', true));
        // echo $file;
        force_download($file, NULL);
    }

    function getprogram()
    {
        $kd = $this->input->get('opd');
        $res = $this->db->query("select Kd_Prog,ID_Prog,Ket_Program from SPE_REF_PROGRAM where kd_sub_unit='$kd'")->result();
        // $result=[];
        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->Kd_Prog . '.' . $res->ID_Prog,
                'text' => $res->Ket_Program
            );
        }
        print_r(json_encode($data));
    }

    function getkegiatan()
    {
        $kd = $this->input->get('opd');
        $kp = $this->input->get('kp');
        $res = $this->db->query("select * from spe_ref_kegiatan where kd_sub_unit='$kd' and concat(Kd_Prog,'.',ID_Prog)='$kp'")->result();
        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->kd_keg,
                'text' => $res->ket_kegiatan
            );
        }
        print_r(json_encode($data));
    }
    function bast($ide)
    {
        $id = rapikan($ide);
        $row = $this->Mkegiatan->get_by_id($id);
        $data=array(
            'id'=>$id,
            'tanggal_bast' =>$row->tanggal_bast <> '' ? date('d/m/Y', strtotime($row->tanggal_bast)) : '',
			'file' => $this->Mkegiatan->get_file($id),
			'title'=>'Bast'
        );
        return view('kegiatan.new.bast', $data);

    }
    public function updateBast()
    {
        $this->db->trans_start();
        $id=rapikan($this->input->post('id'));
        $tanggal_bast=$this->input->post('tanggal_bast');
        $this->db->where('id', $id);
        $this->db->set('tanggal_bast', date('Y-m-d', strtotime(str_replace('/', '-',  $tanggal_bast))));
        $this->db->update('KEGIATAN_BELANJA');
        for ($i = 0; $i < count($_FILES['file_progres']['name']); $i++) {
            $_FILES['gbr_ikan']['name']     = $_FILES['file_progres']['name'][$i];
            $_FILES['gbr_ikan']['type']     = $_FILES['file_progres']['type'][$i];
            $_FILES['gbr_ikan']['tmp_name'] = $_FILES['file_progres']['tmp_name'][$i];
            $_FILES['gbr_ikan']['error']    = $_FILES['file_progres']['error'][$i];
            $_FILES['gbr_ikan']['size']     = $_FILES['file_progres']['size'][$i];


            // proses upload gambar
            $config['upload_path']   = './file_bast/';
            $config['allowed_types'] = 'pdf|zip|rar|jpg|jpeg';
            $config['encrypt_name']  = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gbr_ikan')) {
                $this->db->set('kegiatan_belanja_id', $id);
                $this->db->set('nama_ori', $_FILES['file_progres']['name'][$i]);
                $this->db->set('nama_file', 'file_bast/' . $this->upload->data("file_name"));
                $this->db->insert('KEGIATAN_BELANJA_FILE');
            }
        }
        $this->db->trans_complete();
        // echo $this->db->trans_status() ; die();
        if ($this->db->trans_status() === true) {
            set_flashdata('success', 'Bast berhasil.');
        } else {
            set_flashdata('warning', 'Bast gagal.');
        }

        redirect('kegiatan/read/' . acak($id));
        // $this->read($this->input->post('id'));
    }
    public function hapus_file()
    {
        $id=$this->input->get('id');

        $this->db->where('id', $id);
        $row=$this->db->get('KEGIATAN_BELANJA_FILE')->row();
        if (file_exists($row->nama_file)){
            unlink($row->nama_file);
            $this->db->where('id', $id);
            $this->db->delete('KEGIATAN_BELANJA_FILE');
        }
    }
    function jadwal($ide)
    {
        $id = rapikan($ide);
        $row = $this->Mkegiatan->get_by_id($id);
        // cek jadwal
        $mulai = $row->mulai_pekerjaan_spk;
        $selesai = $row->selesai_pekerjaan_spk;
        if($row->selesai_pekerjaan_spk==null){
            $time = strtotime($mulai);
            $final = date("Y-m-d", strtotime("+3 month", $time));
            $this->db->where('id', $id);
            $this->db->set('selesai_pekerjaan_spk', $final);
            $this->db->update('PEKERJAAN_SIPIL');
        }
        if ($row->no_addedum <> '') {
            $mulai = $row->mulai_pekerjaan_addedum;
            $selesai = $row->selesai_pekerjaan_addedum;
            if($row->selesai_pekerjaan_addedum==null){
                $time = strtotime($mulai);
                $final = date("Y-m-d", strtotime("+3 month", $time));
                $this->db->where('id', $id);
                $this->db->set('selesai_pekerjaan_addedum', $final);
                $this->db->update('PEKERJAAN_SIPIL');
            }
        }
        $cj = cekjadwalkegiatan($id);
        if ($cj == 0) {
            $mm = rangeMinggu($mulai, $selesai);

            $arr = [];
            $i=1;
            foreach ($mm as $rm) {
                $arr[] = array(
                    'kegiatan_belanja_id' => $row->id,
                    'minggu' => $i,
                    'awal_minggu'=>$rm['tanggal']['awal'],
                    'akhir_minggu'=>$rm['tanggal']['akhir']
                );
                $i++;
            }
            $this->db->insert_batch('kegiatan_belanja_jadwal', $arr);

        }

        // data minggu
        $this->db->where('kegiatan_belanja_id', $row->id);
        $lm = $this->db->get('KEGIATAN_BELANJA_JADWAL')->result();
        // print_r($lm);
        $data = array(
            'lm' => $lm,
            'pekerjaan' => $row,
            'mulai' => $mulai,
            'selesai' => $selesai
        );
        return view('kegiatan.jadwal', $data);
    }

    function updatejadwal()
    {
        for ($i = 0; $i < count($this->input->post('idm')); $i++) {
            $this->db->where('id', $this->input->post('idm')[$i]);
            $this->db->set('persentase', $this->input->post('minggu')[$i]);
            $this->db->update('KEGIATAN_BELANJA_JADWAL');
        }

        redirect('kegiatan');
    }
}
