<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mpengguna');
        $this->load->model('Msipil');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Pengguna';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'pengguna?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengguna?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'pengguna';
            $config['first_url'] = base_url() . 'pengguna';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpengguna->total_rows($q);
        $pengguna                      = $this->Mpengguna->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengguna_data' => $pengguna,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start,
            'title'               => 'Data Pengguna',
            'akses'               => $akses
        );
        return view('pengguna.view_index', $data);
    }

    public function read($ide)
    {
        $this->cekAkses('read');
        $id  = rapikan($ide);
        $row = $this->Mpengguna->get_by_id($id);
        if ($row) {
            $data = array(
                'title' => 'Detail pengguna',
                'id_inc' => $row->id_inc,
                'nama' => $row->nama,
                'username' => $row->username,
                'password' => $row->password,
            );
            $this->template->load('layout', 'pengguna/view_read', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function create()
    {
        $this->cekAkses('create');
        $data = array(
            'button'   => 'Simpan',
            'title'    => 'Tambah pengguna',
            'action'   => site_url('pengguna/create_action'),
            'id_inc'   => set_value('id_inc'),
            'nama'     => set_value('nama'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            'role_id' => set_value('role_id',[]),
            'role' => $this->Mpengguna->getRole()
        );
        return view('pengguna.view_form', $data);
    }

    public function create_action()
    {
        $this->cekAkses('create');
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $this->db->trans_start();
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'username' => $this->input->post('username', TRUE),
                'password' => sha1($this->input->post('password', TRUE)),
            );
            $this->Mpengguna->insert($data);

            for ($i = 0; $i < count($this->input->post('role_id')); $i++) {
                $rl = array(
                    'ms_pengguna_id' => $this->Mpengguna->lastid(),
                    'ms_role_id' => $this->input->post('role_id')[$i]
                );
                $this->Mpengguna->assignRole($rl);
            }

            $this->db->trans_complete();

            if ($this->db->trans_status() === TRUE) {
                set_flashdata('success', 'Data berhasil di simpan.');
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
            }


            redirect(site_url('pengguna'));
        }
    }

    public function update($ide)
    {
        $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $data = array(
                'title'    => 'Edit pengguna',
                'button'   => 'Update',
                'action'   => site_url('pengguna/update_action'),
                'id_inc'   => set_value('id_inc', $row->id_inc),
                'nama'     => set_value('nama', $row->nama),
                'username' => set_value('username', $row->username),
                'password' => set_value('password', $row->password),
                'role_id' => set_value('role_id', $this->Mpengguna->userRole($row->id_inc)),
                'role' => $this->Mpengguna->getRole()

            );
            return view('pengguna.view_form', $data);
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function update_action()
    {
        $this->cekAkses('update');
        $this->_rules_update();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $this->db->trans_start();
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'username' => $this->input->post('username', TRUE),
            );
            $this->Mpengguna->update($this->input->post('id_inc', TRUE), $data);

            for ($i = 0; $i < count($this->input->post('role_id')); $i++) {
                $rl = array(
                    'ms_pengguna_id' => $this->input->post('id_inc', TRUE),
                    'ms_role_id' => $this->input->post('role_id')[$i]
                );
                $this->Mpengguna->assignRole($rl);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                set_flashdata('success', 'Data berhasil di update.');
            } else {
                set_flashdata('warning', 'Data gagal di update.');
            }
            redirect(site_url('pengguna'));
        }
    }

    public function delete($ide)
    {
        $this->cekAkses('delete');
        $id  = rapikan($ide);
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $res = $this->Mpengguna->delete($id);
            if ($res) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
            redirect(site_url('pengguna'));
        } else {
            set_flashdata('warning', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');

        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function _rules_update()
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
