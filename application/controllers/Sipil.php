<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sipil extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Msipil');
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->id_pengawas = $this->session->userdata('audit_id_pengawas');
        $this->role = $this->session->userdata('audit_role');
        $this->load->library('form_validation');
        $this->tahun = $this->session->userdata('tahun_anggaran');
        // $this->kd_skpd = $this->session->userdata('audit_kd_skpd');
    }
    private function cekAkses($var = null)
    {
        $url = 'sipil';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {

        $tahun=$this->tahun;
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'sipil?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sipil?q=' . urlencode($q);
            $cetak               = base_url() . 'sipil/cetak?id_ppk=' . urlencode($id_ppk);
        } else {
            $config['base_url']  = base_url() . 'sipil';
            $config['first_url'] = base_url() . 'sipil';
            $cetak               = base_url() . 'sipil/cetak';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        // -- 3 ppk, 1004 pengawas

        if ($this->id_ppk <> '') {
            $this->db->where('id_ppk', $this->id_ppk);
        }
        if ($this->id_pengawas <> '') {
            $this->db->where('id_pengawas', $this->id_pengawas);
        }
        $config['total_rows']        = $this->Msipil->total_rows($q);

        if ($this->id_ppk <> '') {
            $this->db->where('id_ppk', $this->id_ppk);
        }
        if ($this->id_pengawas <> '') {
            $this->db->where('id_pengawas', $this->id_pengawas);
        }

        $sipil = $this->Msipil->get_limit_data($config['per_page'], $start, $q);


        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'akses'      => $akses,
            'title'      => 'Pekerjaan',
            'data'       => $sipil,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'ppk'        => $this->Msipil->getPPK(),
            'cetak'     => $cetak,
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Pekerjaan',
        );
        // $this->template->load('layout', 'sipil/view_index', $data);
        return view('sipil.new.view_index', $data);
    }

    public function cetak()
    {
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $this->role == 3 ? $this->db->where('id_ppk', $this->id_ppk) : "";
        $this->role != 3 && $id_ppk <> '' ? $this->db->where('id_ppk', $id_ppk) : "";
        $sipil      = $this->Msipil->get_all_data();
        $data = array(
            'data' => $sipil,
            'start' => 0
        );
        return view('sipil/cetak', $data);
    }



    function create()
    {
        $akses = $this->cekAkses('create');

        // cek id ppk
        $ipk = $this->id_ppk;
        $kpk = "";
        $userppk = false;
        if ($ipk <> '') {
            $userppk = true;
            $kpk = $this->db->query("select kd_skpd from ref_ppk  where id='$ipk'")->row()->kd_skpd;
            $this->db->where('kd_skpd', $kpk);
        }
        $opd = $this->Msipil->get_opd();

        if ($ipk <> '') {
            $this->db->where('id', $ipk);
        }
        $ppk = $this->Msipil->getPPK();

        if ($ipk <> '') {
            $this->db->where('kd_skpd', $kpk);
        }
        $pengawas = $this->Msipil->getPengawas();
        $data = array(
            // 'title'                   => 'Tambah Data',
            'action'                  => base_url() . 'sipil/create_action',
            'belanja'                 => $this->Msipil->getJenisBelanja(),
            'opd'                     => $opd,
            'ppk'                     => $ppk,
            'pengawas' => $pengawas,
            'userppk' => $userppk,
            'id' => set_value('id'),
            'id_pengawas' => set_value('id_pengawas'),
            'id_ppk' => set_value('id_ppk', $ipk),
            'nama_ppk' => set_value('nama_ppk'),
            'kode_skpd' => set_value('kode_skpd', $kpk),
            'nama_skpd' => set_value('nama_skpd'),
            // 'kode_belanja' => set_value('kode_belanja'),
            // 'jenis_belanja' => set_value('jenis_belanja'),
            'kd_prog' => set_value('kd_prog'),
            'id_prog' => set_value('id_prog'),
            'ket_prog' => set_value('ket_prog'),
            'kd_keg' => set_value('kd_keg'),
            'ket_keg' => set_value('ket_keg'),
            'sub_kegiatan' => set_value('sub_kegiatan'),
            'pagu_anggaran' => set_value('pagu_anggaran'),
            'harga_perkiraan' => set_value('harga_perkiraan'),
            'nama_konsultansi' => set_value('nama_konsultansi'),
            'no_sk_konsultansi' => set_value('no_sk_konsultansi'),
            'tgl_sk_konsultansi' => set_value('tgl_sk_konsultansi'),
            'tgl_mulai_konsultansi' => set_value('tgl_mulai_konsultansi'),
            'tgl_selesai_konsultansi' => set_value('tgl_selesai_konsultansi'),
            'nilai_konsultansi' => set_value('nilai_konsultansi'),
            'no_addedum_konsultansi' => set_value('no_addedum_konsultansi'),
            'tgl_addedum_konsultansi' => set_value('tgl_addedum_konsultansi'),
            'nilai_addedum_konsultansi' => set_value('nilai_addedum_konsultansi'),
            'tgl_mulai_addedum_konsultansi' => set_value('tgl_mulai_addedum_konsultansi'),
            'tgl_selesai_addedum_konsultansi' => set_value('tgl_selesai_addedum_konsultansi'),
            'nama_pengawas' => set_value('nama_pengawas'),
            'no_sk_pengawas' => set_value('no_sk_pengawas'),
            'tgl_sk_pengawas' => set_value('tgl_sk_pengawas'),
            'tgl_mulai_pengawas' => set_value('tgl_mulai_pengawas'),
            'tgl_selesai_pengawas' => set_value('tgl_selesai_pengawas'),
            'nilai_pengawas' => set_value('nilai_pengawas'),
            'no_addedum_pengawas' => set_value('no_addedum_pengawas'),
            'tgl_addedum_pengawas' => set_value('tgl_addedum_pengawas'),
            'nilai_addedum_pengawas' => set_value('nilai_addedum_pengawas'),
            'tgl_mulai_addedum_pengawas' => set_value('tgl_mulai_addedum_pengawas'),
            'tgl_selesai_addedum_pengawas' => set_value('tgl_selesai_addedum_pengawas'),
            'nama_pelaksana' => set_value('nama_pelaksana'),
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Pekerjaan',
            'title'      => ' Pekerjaan',
        );
        return view('sipil.new.view_form', $data);
    }


    function create_action()
    {
        $akses = $this->cekAkses('create');
        $this->rules_();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $ppk = explode("/", $this->input->post('nama_ppk', true));
            $pengawas = explode("/", $this->input->post('nama_pengawas', true));
            $nm_sub_unit = $this->db->query("select nm_sub_unit from SPE_REF_SUB_UNIT where Kd_SKPD='" . $this->input->post('kode_skpd') . "'")->row()->nm_sub_unit;
            $id_ppk = $ppk[0];
            $nama_ppk = $ppk[1];;
            $kode_skpd = $this->input->post('kode_skpd');

            $kd_prog = $this->input->post('kd_prog');
            $ket_prog = $this->input->post('ket_prog');
            $kode_kegiatan = $this->input->post('kode_kegiatan');
            $ket_keg = $this->input->post('ket_keg');
            $sub_kegiatan = $this->input->post('sub_kegiatan');
            $pagu_anggaran =  str_replace('.', '', $this->input->post('pagu_anggaran'));
            $harga_perkiraan =  str_replace('.', '', $this->input->post('harga_perkiraan'));
            $nama_konsultansi = $this->input->post('nama_konsultansi');
            $nilai_konsultansi = str_replace('.', '', $this->input->post('nilai_konsultansi'));
            $no_sk_konsultansi = $this->input->post('no_sk_konsultansi');
            $tgl_sk_konsultansi = $this->input->post('tgl_sk_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_sk_konsultansi')))) : null;
            $tgl_mulai_konsultansi = $this->input->post('tgl_mulai_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_konsultansi')))) : null;
            $tgl_selesai_konsultansi = $this->input->post('tgl_selesai_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_konsultansi')))) : null;
            $nilai_addedum_konsultansi = str_replace('.', '', $this->input->post('nilai_addedum_konsultansi'));
            $no_addedum_konsultansi = $this->input->post('no_addedum_konsultansi');
            $tgl_addedum_konsultansi = $this->input->post('tgl_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_addedum_konsultansi')))) : null;
            $tgl_mulai_addedum_konsultansi = $this->input->post('tgl_mulai_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_addedum_konsultansi')))) : null;
            $tgl_selesai_addedum_konsultansi = $this->input->post('tgl_selesai_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_addedum_konsultansi')))) : null;
            $id_pengawas = $pengawas[0];
            $nama_pengawas = $pengawas[1];
            $nama_cv_pengawas = $pengawas[2];
            $nilai_pengawas = str_replace('.', '', $this->input->post('nilai_pengawas'));
            $no_sk_pengawas = $this->input->post('no_sk_pengawas');
            $tgl_sk_pengawas = $this->input->post('tgl_sk_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_sk_pengawas')))) : null;
            $tgl_mulai_pengawas = $this->input->post('tgl_mulai_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_pengawas')))) : null;
            $tgl_selesai_pengawas = $this->input->post('tgl_selesai_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_pengawas')))) : null;
            $nilai_addedum_pengawas = str_replace('.', '', $this->input->post('nilai_addedum_pengawas'));
            $no_addedum_pengawas = $this->input->post('no_addedum_pengawas');
            $tgl_addedum_pengawas = $this->input->post('tgl_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_addedum_pengawas')))) : null;
            $tgl_mulai_addedum_pengawas = $this->input->post('tgl_mulai_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_addedum_pengawas')))) : null;
            $tgl_selesai_addedum_pengawas = $this->input->post('tgl_selesai_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_addedum_pengawas')))) : null;


            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');

            $data = array(
                'id_ppk' => $id_ppk,
                'nama_ppk' => $nama_ppk,
                'kode_skpd' => $kode_skpd,
                'nama_skpd' => $nm_sub_unit,
                'kd_prog' => $kd_prog,
                'ket_prog' => $ket_prog,
                'kd_keg' => $kode_kegiatan,
                'ket_keg' => $ket_keg,
                'sub_kegiatan' => $sub_kegiatan,
                'pagu_anggaran' => $pagu_anggaran,
                'harga_perkiraan' => $harga_perkiraan,
                'nama_konsultansi' => $nama_konsultansi,
                'nilai_konsultansi' => $nilai_konsultansi,
                'no_sk_konsultansi' => $no_sk_konsultansi,
                'tgl_sk_konsultansi' => $tgl_sk_konsultansi,
                'tgl_mulai_konsultansi' => $tgl_mulai_konsultansi,
                'tgl_selesai_konsultansi' => $tgl_selesai_konsultansi,
                'nilai_addedum_konsultansi' => $nilai_addedum_konsultansi,
                'no_addedum_konsultansi' => $no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $tgl_addedum_konsultansi,
                'tgl_mulai_addedum_konsultansi' => $tgl_mulai_addedum_konsultansi,
                'tgl_selesai_addedum_konsultansi' => $tgl_selesai_addedum_konsultansi,
                'id_pengawas' => $id_pengawas,
                'nama_pengawas' => $nama_pengawas,
                'nama_cv_pengawas' => $nama_cv_pengawas,
                'nilai_pengawas' => $nilai_pengawas,
                'no_sk_pengawas' => $no_sk_pengawas,
                'tgl_sk_pengawas' => $tgl_sk_pengawas,
                'tgl_mulai_pengawas' => $tgl_mulai_pengawas,
                'tgl_selesai_pengawas' => $tgl_selesai_pengawas,
                'nilai_addedum_pengawas' => $nilai_addedum_pengawas,
                'no_addedum_pengawas' => $no_addedum_pengawas,
                'tgl_addedum_pengawas' => $tgl_addedum_pengawas,
                'tgl_mulai_addedum_pengawas' => $tgl_mulai_addedum_pengawas,
                'tgl_selesai_addedum_pengawas' => $tgl_selesai_addedum_pengawas,
                'created_by' => $this->id_pengguna,
                'created_at' => $datetime_formatted,
                'tahun'=>$this->tahun

            );

            $res = $this->db->insert('PEKERJAAN_SIPIL', $data);

            if ($res) {
                set_flashdata('success', 'Data berhasil di simpan.');
                // redirect ke detail
                $rr = $this->db->query("select max(id) id from PEKERJAAN_SIPIL
                                    where created_by ='" . $this->id_pengguna . "'")->row()->id;

                redirect(site_url('sipil/read/' . acak($rr)));
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
                redirect(site_url('sipil/create'));
            }
        }
    }


    function update($ide)
    {
        $akses = $this->cekAkses('update');
        $id = rapikan($ide);
        $tahun=$this->tahun;
        $row = $this->Msipil->get_by_id($id);
        if ($row) {
            // cek id ppk
            $ipk = $this->id_ppk;
            $kpk = "";
            $userppk = false;
            if ($ipk <> '') {
                $userppk = true;
                $kpk = $this->db->query("select kd_skpd from ref_ppk  where id='$ipk'")->row()->kd_skpd;
                $this->db->where('kd_skpd', $kpk);
            }
            $opd = $this->Msipil->get_opd();

            if ($ipk <> '') {
                $this->db->where('id', $ipk);
            }
            $ppk = $this->Msipil->getPPK();

            if ($ipk <> '') {
                $this->db->where('kd_skpd', $kpk);
            }
            $pengawas = $this->Msipil->getPengawas();

            $lp = $this->db->query("SELECT ID_Prog, Ket_Program FROM ( SELECT Kd_SKPD, ID_Prog, Ket_Program, Kd_Keg, Ket_Kegiatan, CASE WHEN Anggaran_Perubahan_1 IS NULL THEN Anggaran_Awal ELSE Anggaran_Perubahan_1 END nilai_pagu FROM
                                tb_spe_anggaran where tahun='$tahun' ) AS d WHERE Kd_SKPD='$row->kode_skpd' GROUP BY ID_Prog, Ket_Program")->result();

            $lk = $this->db->query("SELECT Kd_SKPD, ID_Prog, Ket_Program, Kd_Keg, Ket_Kegiatan, SUM ( nilai_pagu ) nilai_pagu
                                     FROM   (
                                         SELECT Kd_SKPD,ID_Prog,Ket_Program,Kd_Keg, Ket_Kegiatan, CASE WHEN Anggaran_Perubahan_1 IS NULL THEN Anggaran_Awal ELSE Anggaran_Perubahan_1 END nilai_pagu
                                        FROM tb_spe_anggaran
                                        where tahun='$tahun'
                                    ) AS d
                                    WHERE Kd_SKPD='$row->kode_skpd' AND ID_Prog=$row->kd_prog and Ket_Program='$row->ket_prog'
                                    GROUP BY Kd_SKPD, ID_Prog, Ket_Program, Kd_Keg, Ket_Kegiatan")->result();
            $data = array(
                'action' => base_url() . 'sipil/update_action',
                'opd' => $opd,
                'ppk' => $ppk,
                'pengawas' => $pengawas,
                'userppk' => $userppk,
                'id' => set_value('id', $row->id),
                'id_pengawas' => set_value('id_pengawas', $row->id_pengawas),
                'id_ppk' => set_value('id_ppk', $ipk),
                'nama_ppk' => set_value('nama_ppk', $row->nama_ppk),
                'kode_skpd' => set_value('kode_skpd', $kpk),
                'nama_skpd' => set_value('nama_skpd', $row->nama_skpd),
                'kd_prog' => set_value('kd_prog', $row->kd_prog),
                'id_prog' => set_value('id_prog', $row->id_prog),
                'ket_prog' => set_value('ket_prog', $row->ket_prog),
                'kd_keg' => set_value('kd_keg', $row->kd_keg),
                'ket_keg' => set_value('ket_keg', $row->ket_keg),
                'sub_kegiatan' => set_value('sub_kegiatan', $row->sub_kegiatan),
                'pagu_anggaran' => set_value('pagu_anggaran', number_format($row->pagu_anggaran, 0, '', '.')),
                'harga_perkiraan' => set_value('harga_perkiraan', number_format($row->harga_perkiraan, 0, '', '.')),
                'nama_konsultansi' => set_value('nama_konsultansi', $row->nama_konsultansi),
                'no_sk_konsultansi' => set_value('no_sk_konsultansi', $row->no_sk_konsultansi),
                'tgl_sk_konsultansi' => set_value('tgl_sk_konsultansi', $row->tgl_sk_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_sk_konsultansi)) : ''),
                'tgl_mulai_konsultansi' => set_value('tgl_mulai_konsultansi', $row->tgl_mulai_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_mulai_konsultansi)) : ''),
                'tgl_selesai_konsultansi' => set_value('tgl_selesai_konsultansi', $row->tgl_selesai_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_selesai_konsultansi)) : ''),
                'nilai_konsultansi' => set_value('nilai_konsultansi', number_format($row->nilai_konsultansi, 0, '', '.')),
                'no_addedum_konsultansi' => set_value('no_addedum_konsultansi', $row->no_addedum_konsultansi),
                'tgl_addedum_konsultansi' => set_value('tgl_addedum_konsultansi', $row->tgl_addedum_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_addedum_konsultansi)) : ''),
                'nilai_addedum_konsultansi' => set_value('nilai_addedum_konsultansi', $row->nilai_addedum_konsultansi <> '' ? number_format($row->nilai_addedum_konsultansi, 0, '', '.') : ''),
                'tgl_mulai_addedum_konsultansi' => set_value('tgl_mulai_addedum_konsultansi', $row->tgl_mulai_addedum_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_mulai_addedum_konsultansi)) : ''),
                'tgl_selesai_addedum_konsultansi' => set_value('tgl_selesai_addedum_konsultansi', $row->tgl_selesai_addedum_konsultansi <> '' ? date('d/m/Y', strtotime($row->tgl_selesai_addedum_konsultansi)) : ''),
                'nama_pengawas' => set_value('nama_pengawas', $row->nama_pengawas),
                'no_sk_pengawas' => set_value('no_sk_pengawas', $row->no_sk_pengawas),
                'tgl_sk_pengawas' => set_value('tgl_sk_pengawas', $row->tgl_sk_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_sk_pengawas)) : ''),
                'tgl_mulai_pengawas' => set_value('tgl_mulai_pengawas', $row->tgl_mulai_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_mulai_pengawas)) : ''),
                'tgl_selesai_pengawas' => set_value('tgl_selesai_pengawas', $row->tgl_selesai_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_selesai_pengawas)) : ''),
                'nilai_pengawas' => set_value('nilai_pengawas', number_format($row->nilai_pengawas, 0, '', '.')),
                'no_addedum_pengawas' => set_value('no_addedum_pengawas', $row->no_addedum_pengawas),
                'tgl_addedum_pengawas' => set_value('tgl_addedum_pengawas', $row->tgl_addedum_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_addedum_pengawas)) : ''),
                'nilai_addedum_pengawas' => set_value('nilai_addedum_pengawas', $row->nilai_addedum_pengawas <> '' ? number_format($row->nilai_addedum_pengawas, 0, '', '.') : ''),
                'tgl_mulai_addedum_pengawas' => set_value('tgl_mulai_addedum_pengawas', $row->tgl_mulai_addedum_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_mulai_addedum_pengawas)) : ''),
                'tgl_selesai_addedum_pengawas' => set_value('tgl_selesai_addedum_pengawas', $row->tgl_selesai_addedum_pengawas <> '' ? date('d/m/Y', strtotime($row->tgl_selesai_addedum_pengawas)) : ''),
                'nama_pelaksana' => set_value('nama_pelaksana', $row->nama_pelaksana),
                'lp' => $lp,
                'lk' => $lk,
                'sub_1'=> 'Monitoring Konstruksi',
                'sub_2'=>  'Pekerjaan',
                'title'      => ' Pekerjaan',
            );
            return view('sipil.new.view_form', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('sipil'));
        }
    }

    function update_action()
    {
        $akses = $this->cekAkses('update');
        $this->rules_();
        if ($this->form_validation->run() == FALSE) {
            $this->update(acak($this->input->post('id')));
        } else {

            $ppk = explode("/", $this->input->post('nama_ppk', true));
            $pengawas = explode("/", $this->input->post('nama_pengawas', true));
            $nm_sub_unit = $this->db->query("select nm_sub_unit from SPE_REF_SUB_UNIT where Kd_SKPD='" . $this->input->post('kode_skpd') . "'")->row()->nm_sub_unit;
            $id_ppk = $ppk[0];
            $nama_ppk = $ppk[1];;
            $kode_skpd = $this->input->post('kode_skpd');

            $kd_prog = $this->input->post('kd_prog');
            $ket_prog = $this->input->post('ket_prog');
            $kode_kegiatan = $this->input->post('kode_kegiatan');
            $ket_keg = $this->input->post('ket_keg');
            $sub_kegiatan = $this->input->post('sub_kegiatan');
            $pagu_anggaran =  str_replace('.', '', $this->input->post('pagu_anggaran'));
            $harga_perkiraan =  str_replace('.', '', $this->input->post('harga_perkiraan'));
            $nama_konsultansi = $this->input->post('nama_konsultansi');
            $nilai_konsultansi = str_replace('.', '', $this->input->post('nilai_konsultansi'));
            $no_sk_konsultansi = $this->input->post('no_sk_konsultansi');
            $tgl_sk_konsultansi = $this->input->post('tgl_sk_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_sk_konsultansi')))) : null;
            $tgl_mulai_konsultansi = $this->input->post('tgl_mulai_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_konsultansi')))) : null;
            $tgl_selesai_konsultansi = $this->input->post('tgl_selesai_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_konsultansi')))) : null;
            $nilai_addedum_konsultansi = str_replace('.', '', $this->input->post('nilai_addedum_konsultansi'));
            $no_addedum_konsultansi = $this->input->post('no_addedum_konsultansi');
            $tgl_addedum_konsultansi = $this->input->post('tgl_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_addedum_konsultansi')))) : null;
            $tgl_mulai_addedum_konsultansi = $this->input->post('tgl_mulai_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_addedum_konsultansi')))) : null;
            $tgl_selesai_addedum_konsultansi = $this->input->post('tgl_selesai_addedum_konsultansi') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_addedum_konsultansi')))) : null;
            $id_pengawas = $pengawas[0];
            $nama_pengawas = $pengawas[1];
            $nama_cv_pengawas = $pengawas[2];
            $nilai_pengawas = str_replace('.', '', $this->input->post('nilai_pengawas'));
            $no_sk_pengawas = $this->input->post('no_sk_pengawas');
            $tgl_sk_pengawas = $this->input->post('tgl_sk_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_sk_pengawas')))) : null;
            $tgl_mulai_pengawas = $this->input->post('tgl_mulai_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_pengawas')))) : null;
            $tgl_selesai_pengawas = $this->input->post('tgl_selesai_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_pengawas')))) : null;
            $nilai_addedum_pengawas = str_replace('.', '', $this->input->post('nilai_addedum_pengawas'));
            $no_addedum_pengawas = $this->input->post('no_addedum_pengawas');
            $tgl_addedum_pengawas = $this->input->post('tgl_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_addedum_pengawas')))) : null;
            $tgl_mulai_addedum_pengawas = $this->input->post('tgl_mulai_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_mulai_addedum_pengawas')))) : null;
            $tgl_selesai_addedum_pengawas = $this->input->post('tgl_selesai_addedum_pengawas') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_selesai_addedum_pengawas')))) : null;


            $datetime_variable = new DateTime();
            $datetime_formatted = date_format($datetime_variable, 'Y-m-d H:i:s');

            $data = array(
                'id_ppk' => $id_ppk,
                'nama_ppk' => $nama_ppk,
                'kode_skpd' => $kode_skpd,
                'nama_skpd' => $nm_sub_unit,
                'kd_prog' => $kd_prog,
                'ket_prog' => $ket_prog,
                'kd_keg' => $kode_kegiatan,
                'ket_keg' => $ket_keg,
                'sub_kegiatan' => $sub_kegiatan,
                'pagu_anggaran' => $pagu_anggaran,
                'harga_perkiraan' => $harga_perkiraan,
                'nama_konsultansi' => $nama_konsultansi,
                'nilai_konsultansi' => $nilai_konsultansi,
                'no_sk_konsultansi' => $no_sk_konsultansi,
                'tgl_sk_konsultansi' => $tgl_sk_konsultansi,
                'tgl_mulai_konsultansi' => $tgl_mulai_konsultansi,
                'tgl_selesai_konsultansi' => $tgl_selesai_konsultansi,
                'nilai_addedum_konsultansi' => $nilai_addedum_konsultansi,
                'no_addedum_konsultansi' => $no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $tgl_addedum_konsultansi,
                'tgl_mulai_addedum_konsultansi' => $tgl_mulai_addedum_konsultansi,
                'tgl_selesai_addedum_konsultansi' => $tgl_selesai_addedum_konsultansi,
                'id_pengawas' => $id_pengawas,
                'nama_pengawas' => $nama_pengawas,
                'nama_cv_pengawas' => $nama_cv_pengawas,
                'nilai_pengawas' => $nilai_pengawas,
                'no_sk_pengawas' => $no_sk_pengawas,
                'tgl_sk_pengawas' => $tgl_sk_pengawas,
                'tgl_mulai_pengawas' => $tgl_mulai_pengawas,
                'tgl_selesai_pengawas' => $tgl_selesai_pengawas,
                'nilai_addedum_pengawas' => $nilai_addedum_pengawas,
                'no_addedum_pengawas' => $no_addedum_pengawas,
                'tgl_addedum_pengawas' => $tgl_addedum_pengawas,
                'tgl_mulai_addedum_pengawas' => $tgl_mulai_addedum_pengawas,
                'tgl_selesai_addedum_pengawas' => $tgl_selesai_addedum_pengawas,
                'updated_by' => $this->id_pengguna,
                'updated_at' => $datetime_formatted,
            );

            $this->db->where('id', $this->input->post('id'));
            $result = $this->db->update("PEKERJAAN_SIPIL", $data);


            if ($result) {
                set_flashdata('success', 'Data berhasil di simpan.');
                redirect('sipil/read/' . acak($this->input->post('id')));
            } else {
                set_flashdata('warning', 'Data gagal di simpan.');
                redirect('sipil/update/' . acak($this->input->post('id')));
            }

            redirect(site_url('sipil'));

            die();
        }
    }

    function delete($ide)
    {
        $akses = $this->cekAkses('delete');
        $id = rapikan($ide);

        $res = $this->Msipil->get_by_id($id);

        if ($res) {
            $this->db->trans_start();
            $this->db->where('pekerjaan_sipil_id', $id);
            $this->db->delete('PEKERJAAN_SIPIL_LAPORAN');

            $this->db->where('id', $id);
            $rr = $this->db->delete('PEKERJAAN_SIPIL');
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                set_flashdata('success', 'Data berhasil di hapus.');
            } else {
                set_flashdata('warning', 'Data gagal di hapus.');
            }
        }
        redirect(site_url('sipil'));
    }

    function read($ide)
    {
        $akses = $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Msipil->get_by_id($id);
        if ($row) {
            $prog = $this->Msipil->get_progres_by_pekerjaan($row->id);
            // echo "<pre>";
            // print_r($prog);
            // echo "</pre>";
            // die();

            $perencanaan = $this->db->query("select * from pekerjaan_sipil_jadwal where pekerjaan_sipil_id='$id'")->result();
            $sp2d = $this->db->query("SELECT * FROM PEKERJAAN_SIPIL_SP2D where pekerjaan_sipil_id='$id'")->result();
            $data = array(
                // 'realisasi' => $jre,
                'title' => 'Detail pekerjaan sipil',
                'id' => $row->id,
                'id_pengawas' => $row->id_pengawas,
                'id_ppk' => $row->id_ppk,
                'nama_ppk' => $row->nama_ppk,
                'kode_skpd' => $row->kode_skpd,
                'nama_skpd' => $row->nama_skpd,
                'kode_belanja' => $row->kode_belanja,
                'jenis_belanja' => $row->jenis_belanja,
                'kd_prog' => $row->kd_prog,
                'id_prog' => $row->id_prog,
                'ket_prog' => $row->ket_prog,
                'kd_keg' => $row->kd_keg,
                'ket_keg' => $row->ket_keg,
                'sub_kegiatan' => $row->sub_kegiatan,
                'pagu_anggaran' => $row->pagu_anggaran,
                'harga_perkiraan' => $row->harga_perkiraan,
                'nama_konsultansi' => $row->nama_konsultansi,
                'no_sk_konsultansi' => $row->no_sk_konsultansi,
                'tgl_sk_konsultansi' => $row->tgl_sk_konsultansi,
                'tgl_mulai_konsultansi' => $row->tgl_mulai_konsultansi,
                'tgl_selesai_konsultansi' => $row->tgl_selesai_konsultansi,
                'nilai_konsultansi' => $row->nilai_konsultansi,
                'no_addedum_konsultansi' => $row->no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $row->tgl_addedum_konsultansi,
                'nilai_addedum_konsultansi' => $row->nilai_addedum_konsultansi,
                'tgl_mulai_addedum_konsultansi' => $row->tgl_mulai_addedum_konsultansi,
                'tgl_selesai_addedum_konsultansi' => $row->tgl_selesai_addedum_konsultansi,
                'nama_pengawas' => $row->nama_pengawas,
                'nama_cv_pengawas' => $row->nama_cv_pengawas,
                'no_sk_pengawas' => $row->no_sk_pengawas,
                'tgl_sk_pengawas' => $row->tgl_sk_pengawas,
                'tgl_mulai_pengawas' => $row->tgl_mulai_pengawas,
                'tgl_selesai_pengawas' => $row->tgl_selesai_pengawas,
                'nilai_pengawas' => $row->nilai_pengawas,
                'no_addedum_pengawas' => $row->no_addedum_pengawas,
                'tgl_addedum_pengawas' => $row->tgl_addedum_pengawas,
                'nilai_addedum_pengawas' => $row->nilai_addedum_pengawas,
                'tgl_mulai_addedum_pengawas' => $row->tgl_mulai_addedum_pengawas,
                'tgl_selesai_addedum_pengawas' => $row->tgl_selesai_addedum_pengawas,
                'nama_pelaksana' => $row->nama_pelaksana,
                'no_spmk' => $row->no_spmk,
                'tanggal_spmk' => $row->tanggal_spmk,
                'tanggal_bast' => $row->tanggal_bast,
                'created_by' => $row->created_by,
                'created_at' => $row->created_at,
                'updated_by' => $row->updated_by,
                'updated_at' => $row->updated_at,
                'akses' => $akses,
                'progres' => $prog,
                'perencanaan' => $perencanaan,
                'sp2d' => $sp2d,
                'sub_1'=> 'Monitoring Konstruksi',
                'sub_2'=>  'Pekerjaan',
                'title'      => 'Detail Pekerjaan',
            );
            return view('sipil.new.view_read', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('sipil'));
        }
    }


    function rules_()
    {
        $this->form_validation->set_rules('nama_ppk', 'nama_ppk', 'trim|required');
        $this->form_validation->set_rules('kode_skpd', 'kode_skpd', 'trim|required');
        $this->form_validation->set_rules('kd_prog', 'kd_prog', 'trim|required');
        $this->form_validation->set_rules('ket_prog', 'ket_prog', 'trim|required');
        $this->form_validation->set_rules('kode_kegiatan', 'kode_kegiatan', 'trim|required');
        $this->form_validation->set_rules('ket_keg', 'ket_keg', 'trim|required');
        $this->form_validation->set_rules('sub_kegiatan', 'sub_kegiatan', 'trim|required');
        $this->form_validation->set_rules('pagu_anggaran', 'pagu_anggaran', 'trim|required');
        $this->form_validation->set_rules('harga_perkiraan', 'harga_perkiraan', 'trim|required');
        $this->form_validation->set_rules('nama_konsultansi', 'nama_konsultansi', 'trim|required');
        $this->form_validation->set_rules('nilai_konsultansi', 'nilai_konsultansi', 'trim|required');
        $this->form_validation->set_rules('no_sk_konsultansi', 'no_sk_konsultansi', 'trim|required');
        $this->form_validation->set_rules('tgl_sk_konsultansi', 'tgl_sk_konsultansi', 'trim|required');
        $this->form_validation->set_rules('tgl_mulai_konsultansi', 'tgl_mulai_konsultansi', 'trim|required');
        $this->form_validation->set_rules('tgl_selesai_konsultansi', 'tgl_selesai_konsultansi', 'trim|required');

        $this->form_validation->set_rules('nama_pengawas', 'nama_pengawas', 'trim|required');
        $this->form_validation->set_rules('nilai_pengawas', 'nilai_pengawas', 'trim|required');
        $this->form_validation->set_rules('no_sk_pengawas', 'no_sk_pengawas', 'trim|required');
        $this->form_validation->set_rules('tgl_sk_pengawas', 'tgl_sk_pengawas', 'trim|required');
        $this->form_validation->set_rules('tgl_mulai_pengawas', 'tgl_mulai_pengawas', 'trim|required');
        $this->form_validation->set_rules('tgl_selesai_pengawas', 'tgl_selesai_pengawas', 'trim|required');


        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    private function _uploadfile($nama_file)
    {
        $config['upload_path']   = './file_progres/';
        $config['allowed_types'] = 'pdf|zip|rar';
        $config['file_name']     = $nama_file;
        $config['overwrite']     = true;
        $config['max_size']      = 2048; // 2MB

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file_progres')) {
            return 'file_progres/' . $this->upload->data("file_name");
        } else {
            return null;
        }
    }


    function download()
    {
        $akses = $this->cekAkses('read');
        $this->load->helper('download');
        /*print_r($this->input->get());
        echo "<hr>";*/
        $file = str_replace('zz', '/', $this->input->get('file', true));
        // echo $file;
        force_download($file, NULL);
    }

    function getprogram()
    {
        $kd = $this->input->get('opd');
        $tahun=$this->tahun;
        $res = $this->db->query("SELECT
        ID_Prog,
        Ket_Program
    FROM
        (
        SELECT
            Kd_SKPD,
            ID_Prog,
            Ket_Program,
            Kd_Keg,
            Ket_Kegiatan,
        CASE

                WHEN Anggaran_Perubahan_1 IS NULL THEN
                Anggaran_Awal ELSE Anggaran_Perubahan_1
            END nilai_pagu
    FROM
        tb_spe_anggaran
        where tahun='$tahun'
        and Kd_SKPD='$kd'
        ) AS d
    GROUP BY
        ID_Prog,
        Ket_Program")->result();
        // $result=[];
        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->ID_Prog,
                'text' => $res->Ket_Program
            );
        }
        print_r(json_encode($data));
    }

    function getkegiatan()
    {
        $kd = $this->input->get('opd');
        $kp = $this->input->get('kp');
        // $ket_p = $this->input->get('ket_p');
        $tahun=$this->tahun;
        $res = $this->db->query("SELECT
                                    Kd_SKPD,
                                    ID_Prog,
                                    Ket_Program,
                                    Kd_Keg,
                                    Ket_Kegiatan,
                                    SUM ( nilai_pagu ) nilai_pagu
                                FROM
                                    (
                                    SELECT
                                        Kd_SKPD,
                                        ID_Prog,
                                        Ket_Program,
                                        Kd_Keg,
                                        Ket_Kegiatan,
                                    CASE

                                            WHEN Anggaran_Perubahan_1 IS NULL THEN
                                            Anggaran_Awal ELSE Anggaran_Perubahan_1
                                        END nilai_pagu
                                FROM
                                tb_spe_anggaran
                                WHERE Kd_SKPD='$kd' AND ID_Prog=$kp and tahun=$tahun
                                    ) AS d

                                GROUP BY
                                    Kd_SKPD,
                                    ID_Prog,
                                    Ket_Program,
                                    Kd_Keg,
                                    Ket_Kegiatan")->result();


        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->Kd_Keg,
                'text' => $res->Ket_Kegiatan
            );
        }
        print_r(json_encode($data));
    }
    function getPPK()
    {
        $kd = $this->input->get('opd');
        $kp = $this->input->get('kp');
        $res = $this->db->query("SELECT * FROM [dbo].[REF_PPK] WHERE kd_skpd='$kd'")->result();
        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->id,
                'text' => $res->nama_ppk
            );
        }
        print_r(json_encode($data));
    }
    function getPagu()
    {
        $kd = $this->input->get('opd');
        $kp = $this->input->get('kp');
        // $ket_p = $this->input->get('ket_p');
        $kk = $this->input->get('kk');
        $tahun=$this->tahun;
        $res = $this->db->query("SELECT
                    Kd_SKPD,
                    ID_Prog,
                    Ket_Program,
                    Kd_Keg,
                    Ket_Kegiatan,
                    replace(SUM ( nilai_pagu ),'.00','') nilai_pagu
                FROM
                    (
                    SELECT
                        Kd_SKPD,
                        ID_Prog,
                        Ket_Program,
                        Kd_Keg,
                        Ket_Kegiatan,
                    CASE

                            WHEN Anggaran_Perubahan_1 IS NULL THEN
                            Anggaran_Awal ELSE Anggaran_Perubahan_1
                        END nilai_pagu
                FROM
                   tb_spe_anggaran
                   WHERE Kd_SKPD='$kd' AND ID_Prog=$kp and    Kd_Keg=$kk and tahun='$tahun'
                    ) AS d
                GROUP BY
                    Kd_SKPD,
                    ID_Prog,
                    Ket_Program,
                    Kd_Keg,
                    Ket_Kegiatan")->row();

        echo str_replace('.', ',', $res->nilai_pagu);
    }
    function jadwal($ide)
    {
        $id = rapikan($ide);
        $row = $this->Msipil->get_by_id($id);

        // range spk
        $mulai = $row->tgl_mulai_pengawas;
        $selesai = $row->tgl_selesai_pengawas;

        // cek addedum
        if ($row->no_addedum_pengawas <> '') {
            $selesai = $row->tgl_selesai_addedum_pengawas;
        }
        // cek jadwal
        $cj = cekjadwal($id);
        if ($cj == 0) {
            $mm = rangeMinggu($mulai, $selesai);
            $arr = [];
            $i = 1;
            foreach ($mm as $rm) {
                $arr[] = array(
                    'pekerjaan_sipil_id' => $row->id,
                    'minggu' => $i,
                    'awal_minggu' => $rm['tanggal']['awal'],
                    'akhir_minggu' => $rm['tanggal']['akhir']
                );
                $i++;
            }
            $arr[0]=array(
                'pekerjaan_sipil_id' => $row->id,
                'minggu' => 1,
                'awal_minggu' => $mulai,
                'akhir_minggu' => getEndWeek($mulai)
            );
            $arr[count($mm)-1]=array(
                'pekerjaan_sipil_id' => $row->id,
                'minggu' => count($mm),
                'awal_minggu' => $rm['tanggal']['awal'],
                'akhir_minggu' => $selesai
            );
            $this->db->insert_batch('pekerjaan_sipil_jadwal', $arr);
        } else {
            // backup jadwal sebelum e
            $this->db->query("drop table PEKERJAAN_SIPIL_JADWAL_TMP");
            $this->db->query("select * into PEKERJAAN_SIPIL_JADWAL_TMP
                                from PEKERJAAN_SIPIL_JADWAL where pekerjaan_sipil_id=$id");

            // delete jadwal lama
            $this->db->query("delete from PEKERJAAN_SIPIL_JADWAL where pekerjaan_sipil_id=$id");

            // insert jadwal
            $mm = rangeMinggu($mulai, $selesai);
            $arr = [];
            $i = 1;
            foreach ($mm as $rm) {
                $arr[] = array(
                    'pekerjaan_sipil_id' => $row->id,
                    'minggu' => $i,
                    'awal_minggu' => $rm['tanggal']['awal'],
                    'akhir_minggu' => $rm['tanggal']['akhir']
                );
                $i++;
            }
            $arr[0]=array(
                'pekerjaan_sipil_id' => $row->id,
                'minggu' => 1,
                'awal_minggu' => $mulai,
                'akhir_minggu' => getEndWeek($mulai)
            );
            $arr[count($mm)-1]=array(
                'pekerjaan_sipil_id' => $row->id,
                'minggu' => count($mm),
                'awal_minggu' => $rm['tanggal']['awal'],
                'akhir_minggu' => $selesai
            );
            $this->db->insert_batch('pekerjaan_sipil_jadwal', $arr);

            // update value
            $this->db->query("UPDATE pekerjaan_sipil_jadwal
                                SET  pekerjaan_sipil_jadwal.persentase= t.persentase
                                FROM pekerjaan_sipil_jadwal c
                                    JOIN PEKERJAAN_SIPIL_JADWAL_TMP t
                                        ON c.pekerjaan_sipil_id = t.pekerjaan_sipil_id and c.minggu=t.minggu;");
        }




        // data minggu
        $this->db->where('pekerjaan_sipil_id', $row->id);
        $lm = $this->db->get('PEKERJAAN_SIPIL_JADWAL')->result();
        // print_r($lm);
        $data = array(
            'lm' => $lm,
            'pekerjaan' => $row,
            'mulai' => $mulai,
            'selesai' => $selesai
        );
        return view('sipil.jadwal', $data);
    }

    function updatejadwal()
    {
        for ($i = 0; $i < count($this->input->post('idm')); $i++) {
            $this->db->where('id', $this->input->post('idm')[$i]);
            $this->db->set('persentase',str_replace(',','.', $this->input->post('minggu')[$i]));
            $this->db->update('PEKERJAAN_SIPIL_JADWAL');

            $idas = $this->db->query("select pekerjaan_sipil_id from PEKERJAAN_SIPIL_JADWAL where id='" . $this->input->post('idm')[$i] . "'")->row()->pekerjaan_sipil_id;
        }

        redirect('sipil/read/' . acak($idas));
    }

    function updatespmk()
    {
        $data = array(
            'no_spmk' => $this->input->post('no_spmk', true),
            'tanggal_spmk' => $this->input->post('tgl_spmk') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tgl_spmk')))) : null
        );

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('PEKERJAAN_SIPIL', $data);

        redirect('sipil/read/' . acak($this->input->post('id')));
    }


    function tambahsp2d()
    {
        $no_sp2d = $this->input->post('no_sp2d', true);
        $tanggal_sp2d = $this->input->post('tanggal_sp2d', true);
        $nilai_sp2d = $this->input->post('nilai_sp2d', true);
        $pekerjaan_sipil_id = $this->input->post('id', true);

        $data = array(
            'no_sp2d' => $no_sp2d,
            'tanggal_sp2d' => $this->input->post('tanggal_sp2d') <> '' ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('tanggal_sp2d')))) : null,
            'nilai_sp2d' => str_replace('.', '', $nilai_sp2d),
            'pekerjaan_sipil_id' => $pekerjaan_sipil_id,
            'created_by' => $this->id_pengguna,
            'created_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert('PEKERJAAN_SIPIL_SP2D', $data);
        redirect('sipil/read/' . acak($this->input->post('id')));
    }

    function hapussp2d($ide)
    {
        $id = rapikan($ide);
        $cek = $this->db->query("select * from pekerjaan_sipil_sp2d where id='$id'")->row();
        if ($cek) {
            $this->db->where('id', $id);
            $this->db->delete('PEKERJAAN_SIPIL_SP2D');
            redirect('sipil/read/' . acak($cek->pekerjaan_sipil_id));
        } else {
            redirect('sipil');
        }
    }

    function kirimemail()
    {


        $config = array(
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            /* 'protocol'  => 'smtp',
            'smtp_host' => 'mail.merahputih.id',
            'smtp_user' => 'eauditjombang@merahputih.id',  // Email gmail
            'smtp_pass'   => 'jombang@2345',  // Password gmail
            'smtp_crypto' => 'STARTTLS',
            'smtp_port'   => 587, */

             'protocol' => 'smtp',
            'smtp_host' => 'smtp.mailtrap.io',
            'smtp_port' => 2525,
            'smtp_user' => '940f8a0880dc5e',
            'smtp_pass' => 'c8ef3d61f93a08',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );

        $base = base_url();
        $file = "";
        $result = array();
        $email_to = "wahyudewaaaantoro@gmail.com";
        $pesan = "Ini merupakan testing email";

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('eauditjombang@merahputih.id');
        $this->email->to($email_to);
        $this->email->subject('Teguran');
        $this->email->reply_to('no-replay@merahputih.id', 'no replay');
        $this->email->message($pesan);
        //   $this->email->attach($base.'/storage/pdf/'.$file);

        if ($this->email->send()) {
            echo "Sukses Kirim Email";
        } else {
            show_error($this->email->print_debugger());
            echo "Gagal Kirim Email";
        }
    }
}
