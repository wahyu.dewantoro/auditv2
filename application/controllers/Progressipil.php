<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Progressipil extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Msipil');
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->id_pengawas = $this->session->userdata('audit_id_pengawas');
        $this->role = $this->session->userdata('audit_role');
        $this->load->library('form_validation');
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }
    private function cekAkses($var = null)
    {
        $url = 'progressipil';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $id_ppk = urldecode($this->input->get('id_ppk', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'progressipil?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'progressipil?q=' . urlencode($q);
            $cetak               = base_url() . 'progressipil/cetak2?id_ppk=' . urlencode($id_ppk);
        } else {
            $config['base_url']  = base_url() . 'progressipil';
            $config['first_url'] = base_url() . 'progressipil';
            $cetak               = base_url() . 'progressipil/cetak2';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        if ($this->id_ppk <> '') {
            $this->db->where('id_ppk', $this->id_ppk);
        }
        if ($this->id_pengawas <> '') {
            $this->db->where('id_pengawas', $this->id_pengawas);
        }
        $config['total_rows']        = $this->Msipil->total_rows($q);

        if ($this->id_ppk <> '') {
            $this->db->where('id_ppk', $this->id_ppk);
        }
        if ($this->id_pengawas <> '') {
            $this->db->where('id_pengawas', $this->id_pengawas);
        }
        $sipil                       = $this->Msipil->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data = array(
            'akses'      => $akses,
            'title'      => 'Progres Pekerjaan',
            'data'       => $sipil,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'ppk'        => $this->Msipil->getPPK(),
            'id_ppk'     => $id_ppk,
            'cetak'     => $cetak,
            'role'      => $this->role,
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Progres Pekerjaan',
        );

        return view('progres_sipil.new.view_index', $data);
    }


    function read($ide)
    {
        $akses = $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Msipil->get_by_id($id);
        if ($row) {
            $prog = $this->Msipil->get_progres_by_pekerjaan($row->id);
            $this->db->order_by('created_at', 'asc');
            $progb = $this->Msipil->get_progres_by_pekerjaandua($row->id);
            $progj = $this->Msipil->get_jadwal_by_pekerjaan($row->id);
            $progjj = $this->Msipil->get_jadwal_by_pekerjaan($row->id);

            $perencanaan = $this->db->query("select * from pekerjaan_sipil_jadwal where pekerjaan_sipil_id='$id'")->result();
            $data = array(
                // 'realisasi' => $jre,
                'title'                   => 'Detail pekerjaan',
                'id' => $row->id,
                'id_pengawas' => $row->id_pengawas,
                'id_ppk' => $row->id_ppk,
                'nama_ppk' => $row->nama_ppk,
                'kode_skpd' => $row->kode_skpd,
                'nama_skpd' => $row->nama_skpd,
                'kode_belanja' => $row->kode_belanja,
                'jenis_belanja' => $row->jenis_belanja,
                'kd_prog' => $row->kd_prog,
                'id_prog' => $row->id_prog,
                'ket_prog' => $row->ket_prog,
                'kd_keg' => $row->kd_keg,
                'ket_keg' => $row->ket_keg,
                'sub_kegiatan' => $row->sub_kegiatan,
                'pagu_anggaran' => $row->pagu_anggaran,
                'harga_perkiraan' => $row->harga_perkiraan,
                'nama_konsultansi' => $row->nama_konsultansi,
                'no_sk_konsultansi' => $row->no_sk_konsultansi,
                'tgl_sk_konsultansi' => $row->tgl_sk_konsultansi,
                'tgl_mulai_konsultansi' => $row->tgl_mulai_konsultansi,
                'tgl_selesai_konsultansi' => $row->tgl_selesai_konsultansi,
                'nilai_konsultansi' => $row->nilai_konsultansi,
                'no_addedum_konsultansi' => $row->no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $row->tgl_addedum_konsultansi,
                'nilai_addedum_konsultansi' => $row->nilai_addedum_konsultansi,
                'tgl_mulai_addedum_konsultansi' => $row->tgl_mulai_addedum_konsultansi,
                'tgl_selesai_addedum_konsultansi' => $row->tgl_selesai_addedum_konsultansi,
                'nama_pengawas' => $row->nama_pengawas,
                'nama_cv' => $row->nama_cv_pengawas,
                'no_sk_pengawas' => $row->no_sk_pengawas,
                'tgl_sk_pengawas' => $row->tgl_sk_pengawas,
                'tgl_mulai_pengawas' => $row->tgl_mulai_pengawas,
                'tgl_selesai_pengawas' => $row->tgl_selesai_pengawas,
                'nilai_pengawas' => $row->nilai_pengawas,
                'no_addedum_pengawas' => $row->no_addedum_pengawas,
                'tgl_addedum_pengawas' => $row->tgl_addedum_pengawas,
                'nilai_addedum_pengawas' => $row->nilai_addedum_pengawas,
                'tgl_mulai_addedum_pengawas' => $row->tgl_mulai_addedum_pengawas,
                'tgl_selesai_addedum_pengawas' => $row->tgl_selesai_addedum_pengawas,
                'nama_pelaksana' => $row->nama_pelaksana,
                'no_spmk' => $row->no_spmk,
                'tanggal_spmk' => $row->tanggal_spmk,
                'tanggal_bast' => $row->tanggal_bast,
                'created_by' => $row->created_by,
                'created_at' => $row->created_at,
                'updated_by' => $row->updated_by,
                'updated_at' => $row->updated_at,
                'akses' => $akses,
                'progres' => $progb,
                'progresa' => $this->db->query("select a.id,a.minggu_ke,awal_minggu,akhir_minggu,b.persentase perencanaan,a.persentasi_total realisasi,b.persentase - a.persentasi_total deviasi,a.created_at,a.created_by
                                                from pekerjaan_sipil_laporan a
                                                join pekerjaan_sipil_jadwal b on a.pekerjaan_sipil_id=b.pekerjaan_sipil_id and b.minggu=a.minggu_ke
                                                where a.pekerjaan_sipil_id=$id order by a.minggu_ke asc")->result(),
                'progresj' => $progj,
                'progresjj' => $progjj,
                'perencanaan' => $perencanaan,
                'script' => 'progres_sipil/js_progres',
                'sub_1'=> 'Monitoring Konstruksi',
                'sub_2'=>  'Progres Pekerjaan',
            );
            // $this->template->load('layout', 'progres_sipil.new.view_read_cetak', $data);
            return view('progres_sipil.new.view_read_cetak', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('sipil'));
        }
    }

    function cetak($ide)
    {
        $akses = $this->cekAkses('read');
        $id = rapikan($ide);
        $row = $this->Msipil->get_by_id($id);

        if ($row) {
            $prog = $this->Msipil->get_progres_by_pekerjaan($row->id);
            $progb = $this->Msipil->get_progres_by_pekerjaandua($row->id);
            $progj = $this->Msipil->get_jadwal_by_pekerjaan($row->id);
            $progjj = $this->Msipil->get_jadwal_by_pekerjaan($row->id);
            $jre = 0;
            foreach ($prog as $rpo) {
                $jre += $rpo->nilai_progres;
            }
            $data = array(
                'realisasi' => $jre,
                'title'                   => 'Detail pekerjaan',
                'kode_skpd'               => $row->kode_skpd,
                'nama_skpd'               => $row->nama_skpd,
                'kode_belanja'            => $row->kode_belanja,
                'jenis_belanja'           => $row->jenis_belanja,
                'nama_kegiatan'           => $row->nama_kegiatan,
                'pagu_anggaran'           => $row->pagu_anggaran,
                'harga_perkiraan'         => $row->harga_perkiraan,
                'nama_ppk'                => $row->nama_ppk,
                'konsultan'               => $row->konsultan,
                'no_sk_konsultan'         => $row->no_sk_konsultan,
                'nilai_konsultan'         => $row->nilai_konsultan,
                'pengawas'                => $row->pengawas,
                'no_sk_pengawas'          => $row->no_sk_pengawas,
                'nilai_pengawas'          => $row->nilai_pengawas,
                'nilai_spk'               => $row->nilai_spk,
                'no_spk'                  => $row->no_spk,
                'tanggal_spk'             => $row->tanggal_spk,
                'mulai_pekerjaan_spk'     => $row->mulai_pekerjaan_spk,
                'nilai_addedum'           => $row->nilai_addedum,
                'no_addedum'              => $row->no_addedum,
                'tanggal_addedum'         => $row->tanggal_addedum,
                'mulai_pekerjaan_addedum' => $row->mulai_pekerjaan_addedum,
                'pelaksana'               => $row->pelaksana,
                'no_spmk'                 => $row->no_spmk,
                'tanggal_spmk'            => $row->tanggal_spmk,
                'tanggal_bast'            => $row->tanggal_bast,
                'id'                      => $row->id,
                'akses'                   => $akses,
                'progres' => $progb,
                'progresa' => $progb,
                'progresj' => $progj,
                'progresjj' => $progjj,
                'script' => 'progres_sipil/js_progres'
            );
            $mpdf = new \Mpdf\Mpdf(['format' => 'A4-L']);
            $html = $this->load->view('progres_sipil/cetak', $data, true);
            $mpdf->WriteHTML($html);
            $mpdf->Output('yourFileName.pdf', 'I');
            $this->template->load('layout', 'progres_sipil/view_read', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('sipil'));
        }
    }

    function progres($ide)
    {
        $akses = $this->cekAkses('update');
        $id = rapikan($ide);
        $row = $this->Msipil->get_by_id($id);
        if ($row) {
            $data = array(
                'rja' => $this->db->query("SELECT * FROM PEKERJAAN_SIPIL_JADWAL WHERE PEKERJAAN_SIPIL_ID='$id' and minggu not in (select minggu_ke from PEKERJAAN_SIPIL_laporan where pekerjaan_sipil_id =$id) ")->result(),
                'title' => 'Progres Pekerjaan',
                'email_editor' => set_value('email_editor'),
                'minggu_ke' => set_value('minggu_ke'),
                'persentasi_total' => set_value('persentasi_total'),
                'script' => 'progres_sipil/js_progres_b',
                'id' => $row->id,
                'id_pengawas' => $row->id_pengawas,
                'id_ppk' => $row->id_ppk,
                'nama_ppk' => $row->nama_ppk,
                'kode_skpd' => $row->kode_skpd,
                'nama_skpd' => $row->nama_skpd,
                'kode_belanja' => $row->kode_belanja,
                'jenis_belanja' => $row->jenis_belanja,
                'kd_prog' => $row->kd_prog,
                'id_prog' => $row->id_prog,
                'ket_prog' => $row->ket_prog,
                'kd_keg' => $row->kd_keg,
                'ket_keg' => $row->ket_keg,
                'sub_kegiatan' => $row->sub_kegiatan,
                'pagu_anggaran' => $row->pagu_anggaran,
                'harga_perkiraan' => $row->harga_perkiraan,
                'nama_konsultansi' => $row->nama_konsultansi,
                'no_sk_konsultansi' => $row->no_sk_konsultansi,
                'tgl_sk_konsultansi' => $row->tgl_sk_konsultansi,
                'tgl_mulai_konsultansi' => $row->tgl_mulai_konsultansi,
                'tgl_selesai_konsultansi' => $row->tgl_selesai_konsultansi,
                'nilai_konsultansi' => $row->nilai_konsultansi,
                'no_addedum_konsultansi' => $row->no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $row->tgl_addedum_konsultansi,
                'nilai_addedum_konsultansi' => $row->nilai_addedum_konsultansi,
                'tgl_mulai_addedum_konsultansi' => $row->tgl_mulai_addedum_konsultansi,
                'tgl_selesai_addedum_konsultansi' => $row->tgl_selesai_addedum_konsultansi,
                'nama_pengawas' => $row->nama_pengawas,
                'no_sk_pengawas' => $row->no_sk_pengawas,
                'tgl_sk_pengawas' => $row->tgl_sk_pengawas,
                'tgl_mulai_pengawas' => $row->tgl_mulai_pengawas,
                'tgl_selesai_pengawas' => $row->tgl_selesai_pengawas,
                'nilai_pengawas' => $row->nilai_pengawas,
                'no_addedum_pengawas' => $row->no_addedum_pengawas,
                'tgl_addedum_pengawas' => $row->tgl_addedum_pengawas,
                'nilai_addedum_pengawas' => $row->nilai_addedum_pengawas,
                'tgl_mulai_addedum_pengawas' => $row->tgl_mulai_addedum_pengawas,
                'tgl_selesai_addedum_pengawas' => $row->tgl_selesai_addedum_pengawas,
                'nama_pelaksana' => $row->nama_pelaksana,
                'no_spmk' => $row->no_spmk,
                'tanggal_spmk' => $row->tanggal_spmk,
                'tanggal_bast' => $row->tanggal_bast,
                'created_by' => $row->created_by,
                'created_at' => $row->created_at,
                'updated_by' => $row->updated_by,
                'updated_at' => $row->updated_at,
                'script'=>'progres_sipil/js_prog',
                'sub_1'=> 'Monitoring Konstruksi',
                'sub_2'=>  'Progres Pekerjaan',
            );

            return view('progres_sipil.new.view_form_progres_new', $data);
            // $this->template->load('layout', 'progres_sipil/view_form_progres_new', $data);
        } else {
            set_flashdata('warning', 'Data tidak di temukan.');
            redirect(site_url('progressipil'));
        }
    }


    function progres_action()
    {
        $akses = $this->cekAkses('update');
        $this->rules_progres();
        if ($this->form_validation->run() == FALSE) {
            $this->progres(acak($this->input->post('pekerjaan_sipil_id')));
        } else {
            $pekerjaan_sipil_id = $this->input->post('pekerjaan_sipil_id', true);
            $deskripsi_progres = $this->input->post('deskripsi_progres', true);

            $this->db->trans_start();
            $data = array(
                'pekerjaan_sipil_id'=> $pekerjaan_sipil_id,
                'tanggal_progres'   => date('Y-m-d'),
                'deskripsi_progres' => $deskripsi_progres,
                'created_by'        => $this->id_pengguna,
                'created_at'       => date('Y-m-d H:i:s'),
                'minggu_ke'        => $this->input->post('minggu_ke'),
                'persentasi_total' => str_replace(',','.', $this->input->post('persentasi_total')),
            );

            $rr = $this->db->insert('PEKERJAAN_SIPIL_LAPORAN', $data);

            $idf = $this->db->query("select max(id) id from pekerjaan_sipil_laporan where created_by='" . $this->id_pengguna . "'")->row()->id;

            for ($i = 0; $i < count($_FILES['file_progres']['name']); $i++) {
                $_FILES['gbr_ikan']['name']     = $_FILES['file_progres']['name'][$i];
                $_FILES['gbr_ikan']['type']     = $_FILES['file_progres']['type'][$i];
                $_FILES['gbr_ikan']['tmp_name'] = $_FILES['file_progres']['tmp_name'][$i];
                $_FILES['gbr_ikan']['error']    = $_FILES['file_progres']['error'][$i];
                $_FILES['gbr_ikan']['size']     = $_FILES['file_progres']['size'][$i];

                // proses upload gambar
                $config['upload_path']   = './file_progres/';
                $config['allowed_types'] = 'pdf|zip|rar|jpg|jpeg';
                $config['encrypt_name']  = TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('gbr_ikan')) {
                    $this->db->set('pekerjaan_sipil_laporan_id', $idf);
                    $this->db->set('nama_file', 'file_progres/' . $this->upload->data("file_name"));
                    $this->db->insert('PEKERJAAN_SIPIL_LAPORAN_FILE');
                }
            }


            $this->db->trans_complete();
            if ($this->db->trans_status() === true) {
                set_flashdata('success', 'Progres berhasil.');
            } else {
                set_flashdata('warning', 'Progres gagal.');
            }

            redirect('progressipil/read/' . acak($pekerjaan_sipil_id));
        }
    }

    function rules_progres()
    {
        // $this->form_validation->set_rules('tanggal_progres', 'Tanggal', 'trim|required');
        // $this->form_validation->set_rules('nominal_progres', 'Nominal', 'trim|required');
        // $this->form_validation->set_rules('email_editor','Deskripsi','trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    private function _uploadfile($nama_file)
    {
        $config['upload_path']   = './file_progres/';
        $config['allowed_types'] = 'pdf|zip|rar|jpg|jpeg';
        $config['file_name']     = $nama_file;
        $config['overwrite']     = true;
        $config['max_size']      = 2048; // 2MB

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file_progres')) {
            return 'file_progres/' . $this->upload->data("file_name");
        } else {
            return null;
        }
    }


    function download()
    {
        $akses = $this->cekAkses('read');
        $this->load->helper('download');
        /*print_r($this->input->get());
        echo "<hr>";*/
        $file = str_replace('zz', '/', $this->input->get('file', true));
        // echo $file;
        force_download($file, 'file_progres_' . date('YmdHis'));
    }
    public function detail_belum_progres()
    {
        $id = rapikan(urldecode($this->input->get('id', TRUE)));
        $header = $this->Msipil->get_by_id($id);
        $res = $this->db->query("SELECT a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,minggu,awal_minggu,akhir_minggu,persentasi_total
        FROM  PEKERJAAN_SIPIL A
        JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
        LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
        WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
        AND PERSENTASI_TOTAL IS NULL AND a.id=$id
        ORDER BY A.ID ASC")->result();
        $data = array('res' => $res, 'header' => $header);
        $this->load->view('progres_sipil/detail_belum_progres', $data);
    }

    function belumprogres()
    {

        /* $id_ppk = urldecode($this->input->get('id_ppk', TRUE));
        if ($this->role == 3) {

            $res = $this->db->query("SELECT kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,awal_minggu,awal_minggu
                                    FROM  PEKERJAAN_SIPIL A
                                    JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                                    LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                                    WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                                    AND PERSENTASI_TOTAL IS NULL  and id_ppk=$this->id_ppk
                                    GROUP BY  kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,AKHIR_MINGGU,awal_minggu
                                    ORDER BY A.ID ASC")->result();


            $cetak  = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
        } else {
            if ($id_ppk <> '') {

                $res = $this->db->query("SELECT kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,awal_minggu,awal_minggu
                                    FROM  PEKERJAAN_SIPIL A
                                    JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                                    LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                                    WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                                    AND PERSENTASI_TOTAL IS NULL  and id_ppk=$this->id_ppk
                                    GROUP BY  kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,AKHIR_MINGGU,awal_minggu
                                    ORDER BY A.ID ASC")->result();
                $cetak               = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
            } else {

                $res = $this->db->query("SELECT kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,awal_minggu,awal_minggu
                                    FROM  PEKERJAAN_SIPIL A
                                    JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                                    LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                                    WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                                    AND PERSENTASI_TOTAL IS NULL
                                    GROUP BY  kode_skpd,a.id,nama_skpd,ket_prog,ket_keg,sub_kegiatan,AKHIR_MINGGU,awal_minggu
                                    ORDER BY A.ID ASC")->result();
                $cetak               = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
            }
        } */
        $tahun=$this->tahun;
        $dasar="(
            SELECT kode_skpd,a.id,id_ppk,id_pengawas,nama_skpd,ket_prog,ket_keg,count(1) jum
            FROM  PEKERJAAN_SIPIL A
            JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
            LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
            WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
            AND PERSENTASI_TOTAL IS NULL and a.tahun='$tahun'
            GROUP BY  kode_skpd,a.id,id_ppk,id_pengawas,nama_skpd,ket_prog,ket_keg
            ) A ";

        if($this->id_ppk<>''){
            $this->db->where('id_ppk',$this->id_ppk);
        }
        if($this->id_pengawas<>''){
            $this->db->where('id_pengawas',$this->id_pengawas);
        }
        $res=$this->db->get($dasar)->result();

        $data = array(

            'script2'     => 'progres_sipil/belum_entri_js',
            'res' => $res,
            'title'     => 'Belum Entri Progres',
            'ppk'        => $this->Msipil->getPPK(),
            // 'id_ppk'     => $id_ppk,
            'script'   => 'report/datatables',
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Belum Entri Progres',
        );

        return view('progres_sipil.new.view_belumentri', $data);
        // $this->template->load('layout', 'progres_sipil/view_belumentri', $data);
    }
    function cetak_belumprogres()
    {

        $id_ppk = urldecode($this->input->get('id_ppk', TRUE));
        if ($this->role == 3) {

            $res = $this->db->query("SELECT a.id,nama_skpd,nama_kegiatan,sub_kegiatan
            FROM  PEKERJAAN_SIPIL A
            JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
            LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
            WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
            AND PERSENTASI_TOTAL IS NULL AND id_ppk=$this->id_ppk
            GROUP BY  a.id,nama_skpd,nama_kegiatan,sub_kegiatan
            ORDER BY A.ID ASC")->result();
            $cetak               = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
        } else {
            if ($id_ppk <> '') {

                $res = $this->db->query("SELECT a.id,nama_skpd,nama_kegiatan,sub_kegiatan
                FROM  PEKERJAAN_SIPIL A
                JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                AND PERSENTASI_TOTAL IS NULL AND id_ppk=$id_ppk
                GROUP BY  a.id,nama_skpd,nama_kegiatan,sub_kegiatan
                ORDER BY A.ID ASC")->result();
                $cetak               = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
            } else {

                $res = $this->db->query("SELECT a.id,nama_skpd,nama_kegiatan,sub_kegiatan
                FROM  PEKERJAAN_SIPIL A
                JOIN PEKERJAAN_SIPIL_JADWAL C ON C.PEKERJAAN_SIPIL_ID=A.ID
                LEFT JOIN PEKERJAAN_SIPIL_LAPORAN B ON A.ID=B.PEKERJAAN_SIPIL_ID AND C.MINGGU=B.MINGGU_KE
                WHERE AWAL_MINGGU < CONVERT(DATE, SYSDATETIME()) AND AKHIR_MINGGU<CONVERT(DATE, SYSDATETIME())
                AND PERSENTASI_TOTAL IS NULL
                GROUP BY  a.id,nama_skpd,nama_kegiatan,sub_kegiatan
                ORDER BY A.ID ASC")->result();
                $cetak               = base_url() . 'progressipil/cetak_belumprogres?id_ppk=' . urlencode($id_ppk);
            }
        }
        $data = array(
            'res' => $res,
            'title'     => 'Belum Entri Progres',
            'ppk'        => $this->Msipil->getPPK(),
            'id_ppk'     => $id_ppk,
            'cetak'     => $cetak,
            'script'   => 'report/datatables'
        );
        $this->load->view('progres_sipil/cetak_belumprogres', $data);
    }

    function deviasi()
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        if ($this->id_ppk <> '') {
            $this->db->where('id_ppk', $this->id_ppk);
        }
        if ($this->id_pengawas <> '') {
            $this->db->where('id_pengawas', $this->id_pengawas);
        }

        $res = $this->db->get('SP_PROGRES')->result();
        $data = array(
            'res' => $res,
            'title'     => 'Laproan Deviasi Progres',
            'script'   => 'progres_sipil/datatables',
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Laproan Deviasi Progres',
        );
        // $this->template->load('layout', 'sipil/view_index', $data);
        return view('progres_sipil.new.lapdeviasi', $data);
        // $this->template->load('layout', 'progres_sipil/lapdeviasi', $data);
    }

    function notifemail()
    {
        $id = $this->input->post('id');
        $this->db->where('progres_id', $id);
        $cek = $this->db->get('SP_PROGRES')->row();
        if ($cek) {
            $email_ppk = $this->db->query("select email_ppk from ref_ppk where id=?", $cek->id_ppk)->row()->email_ppk;
            $email_pengawas = $this->db->query("select email_pengawas from ref_pengawas where id=?", $cek->id_pengawas)->row()->email_pengawas;
            $res = array(
                'progres_id' => $cek->progres_id,
                'kode_skpd' => $cek->kode_skpd,
                'nama_skpd' => $cek->nama_skpd,
                'ket_prog' => $cek->ket_prog,
                'ket_keg' => $cek->ket_keg,
                'sub_kegiatan' => $cek->sub_kegiatan,
                'no_sk_pengawas' => $cek->no_sk_pengawas,
                'tgl_sk_pengawas' => $cek->tgl_sk_pengawas,
                'no_addedum_pengawas' => $cek->no_addedum_pengawas,
                'tgl_addedum_pengawas' => $cek->tgl_addedum_pengawas,
                'no_sk_konsultansi' => $cek->no_sk_konsultansi,
                'tgl_sk_konsultansi' => $cek->tgl_sk_konsultansi,
                'no_addedum_konsultansi' => $cek->no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $cek->tgl_addedum_konsultansi,
                'minggu_ke' => $cek->minggu_ke,
                'awal_minggu' => $cek->awal_minggu,
                'akhir_minggu' => $cek->akhir_minggu,
                'perencanaan' => $cek->perencanaan,
                'realisasi' => $cek->realisasi,
                'deviasi' => $cek->deviasi,
                'keterangan' => $cek->keterangan,
                'created_at' => $cek->created_at,
                'created_by' => $cek->created_by,
                'notif' => $cek->notif,
                'id_pengawas' => $cek->id_pengawas,
                'nama_pengawas' => $cek->nama_pengawas,
                'id_ppk' => $cek->id_ppk,
                'nama_ppk' => $cek->nama_ppk,
            );

            $pesan = $this->load->view('progres_sipil/kontenemail', $res, true);
            $email_to = array($email_pengawas, $email_ppk,'inspektorat.eaudit@gmail.com');
            $subject = "PEMBERITAHUAN PEKERJAAN KONSTRUKSI";
            $qq = $this->Msipil->sendmail($email_to, $subject, $pesan);
            if ($qq == 1) {
                // log kirim
                $log['pekerjaan_sipil_id'] = $cek->pekerjaan_id;
                $log['progres_id'] = $cek->progres_id;
                $log['keterangan'] = 'Keterlambatan pekerjaan';
                $log['tanggal_email'] = date('Y-m-d H:i:s');
                $log['penerima'] = $email_ppk . ', ' . $email_pengawas;
                $this->db->insert('PEKERJAAN_EMAIL', $log);
                echo "Email terkirim";
            } else {
                echo "Email tidak terkirim";
            }
        } else {
            echo "Email tidak terkirim";
        }
    }

    function hapuslaporan($id){
        $this->db->where('id',$id);
        $this->db->delete('PEKERJAAN_SIPIL_LAPORAN');

        redirect($_SERVER['HTTP_REFERER']);
    }
}
