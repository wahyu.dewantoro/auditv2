<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maskeudes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Msiskeudes');
        $this->db = $this->load->database('default', true);
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->role = $this->session->userdata('audit_role');
        $this->load->library('form_validation');
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }
    private function cekAkses($var = null)
    {
        $url = 'maskeudes';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $url = 'maskeudes';
        $akses = cek($this->id_pengguna, $url, 'read');
        $data = array(
            'akses'      => $akses,
            'title'      => 'Master Keuangan Desa APBdes 2019',
            'kec'          => $this->Msiskeudes->get_kecamatan(),
            'script'       => 'maskeudes/index_js'
        );
		return view('maskeudes/view_index', $data);
        // $this->template->load('layout', 'maskeudes/view_index', $data);
    }

    function getdesa()
    {
        $kd_kec = $this->input->get('kd_kec');
        $res = $this->Msiskeudes->get_desa($kd_kec);
        // $result=[];
        $data = [];
        foreach ($res as $res) {
            $data[] = array(
                'id' => $res->Kd_Desa,
                'text' => $res->Nama_Desa
            );
        }
        print_r(json_encode($data));
    }

    public function cetak()
    {
        $tahun=$this->tahun;
        $kd_desa=$this->input->post('kd_desa');
        $desa = $this->db->query("select * from Ref_desa where tahun=? and  Kd_Desa=?", array($tahun,$kd_desa))->row()->Nama_Desa;
        $kec = $this->Msiskeudes->get_kecamatan_by_kd_desa($kd_desa);

        $pph = $this->Msiskeudes->get_pphppn($kd_desa);
        $ppd = $this->Msiskeudes->get_ppd($kd_desa);

        $data = array(
            'desa' => $desa,
            'kec'   => $kec,
            'spp' => $this->db->query("SELECT sum(Jumlah) Jumlah FROM [dbo].[Ta_SPP] WHERE Kd_Desa='$kd_desa' and tahun='$tahun'")->row(),
            'kuitansi' => $this->db->query("select sum(nilai) nilai from (
                select sum(nilai) nilai
                from ta_sppbukti
                where Kd_Desa='$kd_desa' and tahun='$tahun'
                union
                select sum(nilai) from Ta_SPJBukti where Kd_Desa='$kd_desa'  and tahun='$tahun' ) asd")->row(),
            'pendapatan_anggaran' => $this->Msiskeudes->get_pendapatan_anggaran($kd_desa),
            'pendapatan_realisasi' => $this->Msiskeudes->get_pendapatan_realisasi($kd_desa),
            'belanja_anggaran' => $this->Msiskeudes->get_belanja_anggaran($kd_desa),
            'belanja_realisasi' => $this->Msiskeudes->get_belanja_realisasi($kd_desa),
            'silpa_tahun_lalu' => $this->Msiskeudes->get_silpa_tahun_lalu($kd_desa),
            'pembiayaan_penerimaan' => $this->Msiskeudes->get_pembiayaan_penerimaan($kd_desa),
            'pembiayaan_pengeluaran' => $this->Msiskeudes->get_pembiayaan_pengeluaran($kd_desa),
            'pphsetor' => $pph->setor,
            'pphterima' => $pph->potongan,
            'ppdsetor' => $ppd->setor,
            'ppdterima' => $ppd->potongan,
            'kas_bank_penerimaan'=>$this->Msiskeudes->getKasPenerimaanBank($kd_desa),
            'kas_bank_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranBank($kd_desa),
            'kas_tunai_penerimaan'=>$this->Msiskeudes->getKasPenerimaanTunai($kd_desa),
            'kas_tunai_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranTunai($kd_desa),
        );
        $this->load->view('maskeudes/cetak', $data);
    }
    public function excel()
    {
        $kd_desa=$this->input->get('kd_desa');
        $desa = $this->db->query("select * from Ref_desa where Kd_Desa=?", array($kd_desa))->row()->Nama_Desa;
        $desa = $this->db->query("select * from Ref_desa where Kd_Desa=?", array($kd_desa))->row()->Nama_Desa;
        $kec = $this->Msiskeudes->get_kecamatan_by_kd_desa($kd_desa);

        $pph = $this->Msiskeudes->get_pphppn($kd_desa);
        $ppd = $this->Msiskeudes->get_ppd($kd_desa);

        $data = array(
            'desa' => $desa,
            'kec'   => $kec,
            'spp' => $this->db->query("SELECT sum(Jumlah) Jumlah FROM [dbo].[Ta_SPP] WHERE Kd_Desa='$kd_desa'")->row(),
            'kuitansi' => $this->db->query("SELECT sum(Nilai) Nilai FROM [dbo].[Ta_SPPBukti] WHERE Kd_Desa='$kd_desa'")->row(),
            'pendapatan_anggaran' => $this->Msiskeudes->get_pendapatan_anggaran($kd_desa),
            'pendapatan_realisasi' => $this->Msiskeudes->get_pendapatan_realisasi($kd_desa),
            'belanja_anggaran' => $this->Msiskeudes->get_belanja_anggaran($kd_desa),
            'belanja_realisasi' => $this->Msiskeudes->get_belanja_realisasi($kd_desa),
            'silpa_tahun_lalu' => $this->Msiskeudes->get_silpa_tahun_lalu($kd_desa),
            'pembiayaan_penerimaan' => $this->Msiskeudes->get_pembiayaan_penerimaan($kd_desa),
            'pembiayaan_pengeluaran' => $this->Msiskeudes->get_pembiayaan_pengeluaran($kd_desa),
            'pphsetor' => $pph->setor,
            'pphterima' => $pph->potongan,
            'ppdsetor' => $ppd->setor,
            'ppdterima' => $ppd->potongan,
            'kas_bank_penerimaan'=>$this->Msiskeudes->getKasPenerimaanBank($kd_desa),
            'kas_bank_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranBank($kd_desa),
            'kas_tunai_penerimaan'=>$this->Msiskeudes->getKasPenerimaanTunai($kd_desa),
            'kas_tunai_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranTunai($kd_desa),
        );
        $this->load->view('maskeudes/excel', $data);
    }
    public function pdf()
    {
        $kd_desa=$this->input->get('kd_desa');
        $desa = $this->db->query("select * from Ref_desa where Kd_Desa=?", array($kd_desa))->row()->Nama_Desa;
        $desa = $this->db->query("select * from Ref_desa where Kd_Desa=?", array($kd_desa))->row()->Nama_Desa;
        $kec = $this->Msiskeudes->get_kecamatan_by_kd_desa($kd_desa);

        $pph = $this->Msiskeudes->get_pphppn($kd_desa);
        $ppd = $this->Msiskeudes->get_ppd($kd_desa);

        $data = array(
            'desa' => $desa,
            'kec'   => $kec,
            'spp' => $this->db->query("SELECT sum(Jumlah) Jumlah FROM [dbo].[Ta_SPP] WHERE Kd_Desa='$kd_desa'")->row(),
            'kuitansi' => $this->db->query("SELECT sum(Nilai) Nilai FROM [dbo].[Ta_SPPBukti] WHERE Kd_Desa='$kd_desa'")->row(),
            'pendapatan_anggaran' => $this->Msiskeudes->get_pendapatan_anggaran($kd_desa),
            'pendapatan_realisasi' => $this->Msiskeudes->get_pendapatan_realisasi($kd_desa),
            'belanja_anggaran' => $this->Msiskeudes->get_belanja_anggaran($kd_desa),
            'belanja_realisasi' => $this->Msiskeudes->get_belanja_realisasi($kd_desa),
            'silpa_tahun_lalu' => $this->Msiskeudes->get_silpa_tahun_lalu($kd_desa),
            'pembiayaan_penerimaan' => $this->Msiskeudes->get_pembiayaan_penerimaan($kd_desa),
            'pembiayaan_pengeluaran' => $this->Msiskeudes->get_pembiayaan_pengeluaran($kd_desa),
            'pphsetor' => $pph->setor,
            'pphterima' => $pph->potongan,
            'ppdsetor' => $ppd->setor,
            'ppdterima' => $ppd->potongan,
            'kas_bank_penerimaan'=>$this->Msiskeudes->getKasPenerimaanBank($kd_desa),
            'kas_bank_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranBank($kd_desa),
            'kas_tunai_penerimaan'=>$this->Msiskeudes->getKasPenerimaanTunai($kd_desa),
            'kas_tunai_pengeluaran'=>$this->Msiskeudes->getKasPengeluaranTunai($kd_desa),
        );
        $this->load->view('maskeudes/excel', $data);

        $mpdf = new \Mpdf\Mpdf(['format' => 'A4-P']);
        $html = $this->load->view('maskeudes/pdf', $data, true);
        $mpdf->WriteHTML($html);
        $mpdf->Output('Master Keuangan '.$desa.' '.str_replace('Kecamatan ','',$kec).'.pdf', 'I');
        // $this->template->load('layout', 'progres_sipil/view_read', $data);
    }
    function siskeudesTenagaKerja_ctrl()
    {
        $akses = cek($this->id_pengguna, 'siskeudestenagakerja', 'read');

        $tahun=$this->tahun;
        $sql = "select c.*,Nama_Desa,Nama_Kecamatan from
        (select * from ref_desa where tahun=$tahun) d
        left join (
            select tahun,Kd_Desa,sum(pagu) pagu,sum(pagu_pak) pagu_pak,sum(anggaran) anggaran,sum(anggaran_pak) anggaran_pak  , case when   sum(anggaran) > 0 and  sum(pagu) > 0 then   sum(anggaran)/sum(pagu) * 100  else null end persen , case when sum(anggaran_pak) > 0 and sum(pagu_pak)>0 then  sum(anggaran_pak)/sum(pagu_pak) * 100 else null end persen_pak
            from (
            select  tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK,sum(anggaran) anggaran,sum(AnggaranStlhPAK) anggaran_pak  , case when a.pagu > 0   then  (sum(anggaran) / pagu) * 100  else 0 end persentase, case when a.Pagu_PAK  > 0 then  (sum(AnggaranStlhPAK) / pagu_pak) * 100  else 0 end persentase_pak
                        from (
                        select  a.tahun,a.kd_desa,a.kd_keg,b.id_keg,Nama_Kegiatan,Obyek kd_akun,Nama_Obyek nama_akun,b.pagu,b.Pagu_PAK,anggaran,AnggaranStlhPAK
                        from ta_rab a
                        join Ta_Kegiatan b on a.Kd_Keg=b.Kd_Keg  and a.tahun=b.tahun and a.kd_desa=b.kd_desa
                        join Ref_Rek4 c on c.Obyek=a.Kd_Rincian
                        where  a.tahun=$tahun and Sumberdana='DDS' and
                        obyek in ( '5.3.4.02.',   '5.3.5.02.',  '5.3.6.02.',    '5.3.7.02.',   '5.3.8.02.')
                        ) a
                        group by tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK
            ) b
            group by tahun,kd_desa
            ) c  on d.Kd_Desa=c.Kd_Desa join Ref_Kecamatan k on d.Kd_Kec=k.Kd_kec
             order by nama_kecamatan,nama_desa asc";

        $res = $this->db->query($sql)->result();
        $data = array(
            'akses' => $akses,
			'res'=>$res,
			'title'=>'Report Honor Tenaga Kerja < 30%'
        );
        return view('maskeudes/reporttenagakerja', $data);
    }
    function panjar_ctrl()
    {
        $akses = cek($this->id_pengguna, 'panjar', 'read');
        $tahun=$this->tahun;
        $sql = "select a.*,b.tgl_spj,b.no_spj,b.jumlah spj,pagu,nama_desa,Nama_Kecamatan
        from (
        select distinct a.tahun,Kd_Keg,a.Kd_Desa,a.No_SPP,a.Tgl_SPP,a.Keterangan,Jumlah panjar
        from ta_spp a
        join Ta_SPPRinci b on a.No_SPP=b.No_SPP and a.Kd_Desa=b.Kd_Desa and a.Tahun=b.Tahun
        where  Jn_SPP='UM' and Sumberdana='DDS'  and a.tahun=$tahun
        ) a
        left join Ta_SPJ b on a.No_SPP=b.no_spp and a.tahun=b.tahun
        left join (select Kd_Keg,case when pagu_PAK is not null then Pagu_PAK else pagu end pagu from Ta_Kegiatan where tahun=$tahun) c on c.Kd_Keg=a.Kd_Keg
        left join (select * from ref_desa where tahun=$tahun) d on d.kd_desa=a.kd_desa
        left join (select * from Ref_Kecamatan where tahun=$tahun ) e on d.Kd_Kec=e.Kd_Kec
        order by tgl_spp desc";

        $res = $this->db->query($sql)->result();
        $data = array(
            'akses' => $akses,
			'res'=>$res,
			'title'=>'Laporan Panjar'
        );
        return view('maskeudes/reportpanjar', $data);
    }


    function anggaranminus_ctrl()
    {
        $akses = cek($this->id_pengguna, 'anggaranminus', 'read');
        $tahun=$this->tahun;
        $sql = "select a.*,realisasi,Nama_Jenis,Nama_Desa
        from (
        select  tahun,kd_desa, akun_3,sum(anggaran) anggaran
        from (
            select tahun,kd_desa,kd_keg,left(Kd_Rincian,5) akun_3,case when AnggaranStlhPAK is not null then AnggaranStlhPAK else Anggaran end anggaran
            from ta_rab
            where tahun=$tahun
        ) a
        group by tahun,kd_desa,akun_3
        ) a
        join (
            select tahun,Kd_Desa,akun_3,sum(nilai) realisasi
        from (
            select tahun,Kd_Desa,left(Kd_Rincian,5) akun_3,case when D_K='D' then debet else Kredit end nilai
            from QrSP_Jurnal
            where tahun=$tahun
            ) a
            group by tahun,Kd_Desa,akun_3
        ) b on a.tahun=b.tahun and a.Kd_Desa=b.Kd_Desa and a.akun_3=b.akun_3
        join Ref_Rek3 c on left(c.jenis,5)=a.akun_3
        join (select * from Ref_Desa where tahun=$tahun) d on d.Kd_Desa=a.Kd_Desa
        where  left(a.akun_3,1)=5 and anggaran < realisasi";

        $res = $this->db->query($sql)->result();
        $data = array(
            'akses' => $akses,
			'res' => $res,
			'title'=> 'Report Anggaran Minus'
        );
        return view('maskeudes/reportanggaranminus', $data);
    }

    function dataReportTenagaKerja()
    {

        $kd_desa = $this->input->post('kd_desa');
        if ($kd_desa <> '') {

            $sql = "select c.*,Nama_Desa from (
                select tahun,Kd_Desa,sum(pagu) pagu,sum(pagu_pak) pagu_pak,sum(anggaran) anggaran,sum(anggaran_pak) anggaran_pak  , case when   sum(anggaran) > 0 and  sum(pagu) > 0 then   sum(anggaran)/sum(pagu) * 100  else null end persen , case when sum(anggaran_pak) > 0 and sum(pagu_pak)>0 then  sum(anggaran_pak)/sum(pagu_pak) * 100 else null end persen_pak
                from (
                select  tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK,sum(anggaran) anggaran,sum(AnggaranStlhPAK) anggaran_pak  , case when a.pagu > 0   then  (sum(anggaran) / pagu) * 100  else 0 end persentase, case when a.Pagu_PAK  > 0 then  (sum(AnggaranStlhPAK) / pagu_pak) * 100  else 0 end persentase_pak
                            from (
                            select  a.tahun,a.kd_desa,a.kd_keg,b.id_keg,Nama_Kegiatan,Obyek kd_akun,Nama_Obyek nama_akun,b.pagu,b.Pagu_PAK,anggaran,AnggaranStlhPAK
                            from ta_rab a
                            join Ta_Kegiatan b on a.Kd_Keg=b.Kd_Keg  and a.tahun=b.tahun and a.kd_desa=b.kd_desa
                            join Ref_Rek4 c on c.Obyek=a.Kd_Rincian
                            where  Sumberdana='DDS' and
                            obyek in ( '5.3.4.02.',   '5.3.5.02.',  '5.3.6.02.',    '5.3.7.02.',   '5.3.8.02.')
                            ) a
                            group by tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK
                ) b
                group by tahun,kd_desa
                ) c
                join ref_desa d on d.Kd_Desa=c.Kd_Desa
                where c.kd_desa=$kd_desa and ( persen < 30 or persen_pak < 30)";

            $data = $this->db->query($sql)->result();
            if ($data) {
                $html = $this->load->view('maskeudes/report_tenagakerja', array('res' => $data), true);
                echo $html;
            } else {
                echo '';
            }
        } else {
            $sql = "select c.*,Nama_Desa from (
                select tahun,Kd_Desa,sum(pagu) pagu,sum(pagu_pak) pagu_pak,sum(anggaran) anggaran,sum(anggaran_pak) anggaran_pak  , case when   sum(anggaran) > 0 and  sum(pagu) > 0 then   sum(anggaran)/sum(pagu) * 100  else null end persen , case when sum(anggaran_pak) > 0 and sum(pagu_pak)>0 then  sum(anggaran_pak)/sum(pagu_pak) * 100 else null end persen_pak
                from (
                select  tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK,sum(anggaran) anggaran,sum(AnggaranStlhPAK) anggaran_pak  , case when a.pagu > 0   then  (sum(anggaran) / pagu) * 100  else 0 end persentase, case when a.Pagu_PAK  > 0 then  (sum(AnggaranStlhPAK) / pagu_pak) * 100  else 0 end persentase_pak
                            from (
                            select  a.tahun,a.kd_desa,a.kd_keg,b.id_keg,Nama_Kegiatan,Obyek kd_akun,Nama_Obyek nama_akun,b.pagu,b.Pagu_PAK,anggaran,AnggaranStlhPAK
                            from ta_rab a
                            join Ta_Kegiatan b on a.Kd_Keg=b.Kd_Keg  and a.tahun=b.tahun and a.kd_desa=b.kd_desa
                            join Ref_Rek4 c on c.Obyek=a.Kd_Rincian
                            where  Sumberdana='DDS' and
                            obyek in ( '5.3.4.02.',   '5.3.5.02.',  '5.3.6.02.',    '5.3.7.02.',   '5.3.8.02.')
                            ) a
                            group by tahun,Kd_Desa,kd_keg,id_keg,Nama_Kegiatan,kd_akun,nama_akun,pagu,Pagu_PAK
                ) b
                group by tahun,kd_desa
                ) c
                join ref_desa d on d.Kd_Desa=c.Kd_Desa
                where  ( persen < 30 or persen_pak < 30)";

            $data = $this->db->query($sql)->result();
            if ($data) {
                $html = $this->load->view('maskeudes/report_tenagakerja', array('res' => $data), true);
                echo $html;
            } else {
                echo '';
            }
        }
    }
}
