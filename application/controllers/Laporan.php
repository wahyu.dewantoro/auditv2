<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Msipil');
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->id_ppk = $this->session->userdata('audit_id_ppk');
        $this->role = $this->session->userdata('audit_role');
        $this->load->library('form_validation');
        $this->kd_skpd = $this->session->userdata('audit_kd_skpd');
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }
    private function cekAkses($var = null)
    {
        $url = 'dibawah80hps';
        return cek($this->id_pengguna, $url, $var);
    }

    public function dibawah80hps()
    {
        $url = 'dibawah80hps';
        $akses= cek($this->id_pengguna, $url,'read');
        // $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '' || $id_ppk<>'') {
            $config['base_url']  = base_url() . 'dibawah80hps?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'dibawah80hps?q=' . urlencode($q);
            $cetaks               = base_url() . 'laporan/cetakdibawah80hps?id_ppk=' . urlencode($id_ppk);
        } else {
            $config['base_url']  = base_url() . 'dibawah80hps';
            $config['first_url'] = base_url() . 'dibawah80hps';
            $cetaks               = base_url() . 'laporan/cetakdibawah80hps';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $config['total_rows']        = $this->Msipil->total_rows_80_hps($q);
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $sipil                       = $this->Msipil->get_limit_data_80_hps($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'akses'      => $akses,
            'title'      => 'Laporan Dibawah 80% HPS',
            'data'       => $sipil,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'ppk'        => $this->Msipil->getPPK(),
            'id_ppk'     => $id_ppk,
            'cetaks'     => $cetaks,
            'jenis'     => 1,
            'sub_1'=> 'Monitoring Konstruksi',
            'sub_2'=>  'Laporan Dibawah 80% HPS',
        );
        // $this->template->load('layout', 'sipil/view_index', $data);
        return view('laporan.new.dibawah80_index2', $data);

        // $this->template->load('layout', 'laporan/dibawah80_index2', $data);
        // $this->template->load('layout', 'sipil/view_index', $data);
        // return view('sipil/view_index', $data);
    }

    public function cetakdibawah80hps()
    {
        // die();
        $id_ppk     = urldecode($this->input->get('id_ppk', TRUE));
        $this->role==3 ? $this->db->where('id_ppk',$this->id_ppk):"";
        $this->role!=3 && $id_ppk <> '' ? $this->db->where('id_ppk',$id_ppk):"";
        $sipil      = $this->Msipil->get_all_data_80_hps();
        $data= array(
            'data' => $sipil,
            'start' => 0
        );
        $this->load->view('laporan/cetakdibawah80hps',$data);
    }
}
