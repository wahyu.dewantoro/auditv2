<?php

use Mpdf\Tag\P;

defined('BASEPATH') or exit('No direct script access allowed');

class PenatausahaanController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('master', 'Tatausaha'));
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }


    function spdCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Penyediaan Dana',
            'subunit' => $this->master->SubUnit($tahun)
        );
        return view('tatausaha.spd', $data);
    }



    function stsCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Tanda Setoran',
            'subunit' => $this->master->SubUnit($tahun)
        );
        return view('tatausaha.sts', $data);
    }

    function stsGetData()
    {

        $Kd_SKPD = $this->input->post('Kd_SKPD', true);
        $tahun = $this->tahun;
        if ($Kd_SKPD <> '') {
            $res = $this->Tatausaha->getSts($tahun, $Kd_SKPD);
            $data = array(
                'tahun' => $tahun,
                'Kd_SKPD' => $Kd_SKPD,
                'res' => $res,
                'start' => 0
            );
            $this->load->view('tatausaha/_sts', $data);
        } else {
            echo '';
        }
    }


    function spdGetData()
    {
        $Kd_SKPD = $this->input->post('Kd_SKPD', true);
        $tahun = $this->tahun;
        if ($Kd_SKPD <> '') {
            $res = $this->Tatausaha->getSpd($tahun, $Kd_SKPD);
            $data = array(
                'tahun' => $tahun,
                'Kd_SKPD' => $Kd_SKPD,
                'res' => $res,
                'start' => 0
            );
            $this->load->view('tatausaha/_spd', $data);
        } else {
            echo '';
        }
    }


    function spdCetak()
    {
        $Kd_SKPD = $this->input->get('Kd_SKPD', true);
        $tahun = $this->tahun;
        if ($Kd_SKPD <> '') {
            $res = $this->Tatausaha->getSpd($tahun, $Kd_SKPD);
            $data = array(
                'tahun' => $tahun,
                'Kd_SKPD' => $Kd_SKPD,
                'res' => $res,
                'start' => 0
            );

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=suratpenyediaandana_" . $Kd_SKPD . ".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->load->view('tatausaha/_spd', $data);
        } else {
            echo '';
        }
    }

    function stsCetak()
    {
        $Kd_SKPD = $this->input->get('Kd_SKPD', true);
        $tahun = $this->tahun;
        if ($Kd_SKPD <> '') {
            $res = $this->Tatausaha->getSts($tahun, $Kd_SKPD);
            $data = array(
                'tahun' => $tahun,
                'Kd_SKPD' => $Kd_SKPD,
                'res' => $res,
                'start' => 0
            );

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=suratTandaSetoran_" . $Kd_SKPD . ".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->load->view('tatausaha/_sts', $data);
        } else {
            echo '';
        }
    }

    function sppCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Permintaan Pembayaran',
            'spp_data' => $this->Tatausaha->getSPP($tahun)
        );
        return view('tatausaha.spp', $data);
    }

    function spjCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Pertanggung Jawaban',
            'spj_data' => $this->Tatausaha->getSPJ($tahun),
            'start' => 0
        );
        return view('tatausaha.spj', $data);
    }

    function spmCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Perintah Pembayaran',
            'spm_data' => $this->Tatausaha->getSPM($tahun),
            'start' => 0
        );
        return view('tatausaha.spm', $data);
    }

    function sppdCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Perintah Pencairan Dana',
            'sppd_data' => $this->Tatausaha->getSPPD($tahun),
            'start' => 0
        );
        return view('tatausaha.sppd', $data);
    }

    function pajakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Pajak',
            'pajak_data' => $this->Tatausaha->getPajak($tahun),
            'start' => 0
        );
        return view('tatausaha.pajak', $data);
    }

    function pajakCetak()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Pajak',
            'pajak_data' => $this->Tatausaha->getPajak($tahun),
            'start' => 0
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=pajak.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha\_pajak', $data, true);
    }

    function pajakDetail()
    {
        $tahun = $this->tahun;
        $kd_skpd       = urldecode($this->input->get('Kd_SKPD', true));
        $nm_unit = urldecode($this->input->get('nm_unit'));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit'));
        $pajak = $this->Tatausaha->getPajakDetail($tahun, $kd_skpd);
        $data  = array(
            'pajak_data'   => $pajak,
            'title'      => 'Pajak',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'pajak',
            'start' => 0,
            'cetak' => base_url() . 'pajak/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit)
        );


        return view('tatausaha.pajakDetail', $data);
    }

    function sppdCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Permintaan Pembayaran',
            'sppd_data' => $this->Tatausaha->getSPPD($tahun),
            'start' => 0
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=datasppd.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha\_sppd', $data, true);
    }

    function sppCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Permintaan Pembayaran',
            'spp_data' => $this->Tatausaha->getSPP($tahun)
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=dataspp.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha\_spp', $data, true);
    }


    function spjCetak()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Pertanggung Jawaban',
            'spj_data' => $this->Tatausaha->getSPJ($tahun),
            'start' => 0
        );
        // return view('tatausaha.spj', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=dataspj.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha\_spj', $data, true);
    }

    function spmCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Perintah Pembayaran',
            'spm_data' => $this->Tatausaha->getSPM($tahun),
            'start' => 0
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=dataspm.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";

        echo $this->load->view('tatausaha\_spm', $data, true);
    }

    function spmDetail()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;
        $spm   = $this->Tatausaha->getSpmDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        // print_r($spm);
        $data  = array(
            'spm_data'   => $spm,
            'title'      => 'Surat Perintah Pembayaran (SPM)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'spm',
            'start' => 0,
            'cetak' => base_url() . 'spm/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q)
        );

        return view('tatausaha.spmDetail', $data);
    }

    function  sppDetail()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;
        $spp   = $this->Tatausaha->getSppDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        $data  = array(
            'spp_data'   => $spp,
            'title'      => 'Surat Permintaan Pembayaran (SPP)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'spp',
            'start' => 0,
            'cetak' => base_url() . 'spp/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q)
        );
        return view('tatausaha.sppDetail', $data);
    }


    function spjDetail()
    {
        $tahun = $this->tahun;
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $q = $this->input->get('q');
        $spj     = $this->Tatausaha->getSPjDetail($tahun, $kd_skpd, $q);
        $data  = array(
            'spj_data'   => $spj,
            'title'      => 'Surat Pertanggung Jawaban (SPJ)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'spj',
            'start' => 0,
            'cetak' => base_url() . 'spj/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q)
        );
        return view('tatausaha.spjDetail', $data);
    }

    function spjCetakDetail()
    {
        $tahun = $this->tahun;
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $q = $this->input->get('q');
        $spj     = $this->Tatausaha->getSPjDetail($tahun, $kd_skpd, $q);
        $data  = array(
            'spj_data'   => $spj,
            'title'      => 'Surat Pertanggung Jawaban (SPJ)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'start' => 0,
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=dataspjDetail.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha/_spjDetail', $data, true);
    }

    function sppdDetail()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;
        $sppd   = $this->Tatausaha->getSppdDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        // print_r($spp);
        $data  = array(
            'sppd_data'   => $sppd,
            'title'      => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'sppd',
            'start' => 0,
            'cetak' => base_url() . 'sppd/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q)

        );

        return view('tatausaha.sppdDetail', $data);
    }

    function sppdDetailCetak()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;
        $sppd   = $this->Tatausaha->getSppdDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        $data  = array(
            'sppd_data'   => $sppd,
            'title'      => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'sppd',
            'start' => 0,
            'cetak' => base_url() . 'sppd/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q)
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=datasppdDetail.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha/_sppdDetail', $data, true);
    }

    function sppDetailCetak()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;

        $spp   = $this->Tatausaha->getSppDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        // print_r($spp);
        $data  = array(
            'spp_data'   => $spp,
            'title'      => 'Surat Permintaan Pembayaran (SPP)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'start' => 0,
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=datasppDetail.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";

        echo $this->load->view('tatausaha\_sppDetail', $data, true);
    }

    function spmDetailCetak()
    {
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $q          = urldecode($this->input->get('q'));
        $tahun = $this->tahun;

        $spm   = $this->Tatausaha->getSpmDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $q);
        $data  = array(
            'spm_data'   => $spm,
            'title'      => 'Surat Perintah Pembayaran (SPM)',
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'start' => 0,
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=dataspmDetail.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";

        echo $this->load->view('tatausaha\_spmDetail', $data, true);
    }

    function sppdpotonganCtrl()
    {
        $tahun = $this->tahun;
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Perintah Pembayaran',
            'sppd_potongan_data' => $this->Tatausaha->getSPPDPotongan($tahun),
            'start' => 0
        );
        return view('tatausaha.sppdPotongan', $data);
    }

    function sppdpotonganCetakCtrl()
    {
        $tahun = $this->tahun;
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Surat Perintah Pembayaran',
            'sppd_potongan_data' => $this->Tatausaha->getSPPDPotongan($tahun),
            'start' => 0
        );
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=sp2dpotongan.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";

        echo $this->load->view('tatausaha\_sppdPotongan', $data, true);
    }

    function sppdpotonganDetail()
    {
        $tahun = $this->tahun;
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit       = urldecode($this->input->get('nm_unit', true));
        $res = $this->Tatausaha->getSPPDPotonganDetail($tahun, $kd_skpd);
        $data = array(
            'sppd_potongan_data' => $res,
            'start'              => 0,
            'title'              => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'kd_skpd'            => $kd_skpd,
            'nm_sub_unit'        => $nm_sub_unit,
            'nm_unit'            => $nm_unit,
            'cetak'                 => base_url() . 'sppdpotongan/cetakDetail?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit),
        );

        return view('tatausaha/sppdPotonganDetail', $data);
    }

    function sppdpotonganDetailCetak()
    {
        $tahun = $this->tahun;
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit       = urldecode($this->input->get('nm_unit', true));
        $res = $this->Tatausaha->getSPPDPotonganDetail($tahun, $kd_skpd);
        $data = array(
            'sppd_potongan_data' => $res,
            'start'              => 0,
            'title'              => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'kd_skpd'            => $kd_skpd,
            'nm_sub_unit'        => $nm_sub_unit,
            'nm_unit'            => $nm_unit,
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=sp2dpotongan.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo  $this->load->view('tatausaha/_sppdPotonganDetail', $data, true);
        // return view('tatausaha/sppdPotonganDetail', $data);
    }

    function pihakketigaCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Monitor Pihak Ke tiga',
            'report_data' => $this->Tatausaha->MonitorPihakKetiga($tahun),
            'start' => 0
        );
        return view('tatausaha.pihakketiga', $data);
    }

    function limaduadualebihtigapuluhCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Belanja Barang dan Jasa Lebih dari 30 jt',
            'data' => $this->Tatausaha->Monitor523($tahun),
            'start' => 0
        );
        return view('tatausaha.limaduadualebihtigapuluh', $data);
    }

    function limaduatigakurangsejutaCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Belanja Modal kurang dari 1 juta            ',
            'data' => $this->Tatausaha->Monitor523be($tahun),
            'start' => 0
        );
        return view('tatausaha.limaduatigakurang', $data);
    }

    function sp2dtuupCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'SP2D UP & TU',
            'data' => $this->Tatausaha->SP2dTU($tahun),
            'start' => 0
        );
        return view('tatausaha.sp2dtuup', $data);
    }

    function sp2dtuupCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'SP2D UP & TU',
            'data' => $this->Tatausaha->SP2dTU($tahun),
            'start' => 0
        );
        // return view('tatausaha.sp2dtuup', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=data.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo         $this->load->view('tatausaha/_sp2dtuup', $data, true);
    }

    function limaduatigakurangsejutaCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Belanja Modal kurang dari 1 juta            ',
            'data' => $this->Tatausaha->Monitor523be($tahun),
            'start' => 0
        );
        // return view('tatausaha.limaduatigakurang', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=data.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $this->load->view('tatausaha/_limaduatigakurang', $data, true);
    }

    function limaduadualebihtigapuluhCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Belanja Barang dan Jasa Lebih dari 30 jt',
            'data' => $this->Tatausaha->Monitor523($tahun),
            'start' => 0
        );
        // return view('tatausaha.limaduadualebihtigapuluh', $data);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=data.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $this->load->view('tatausaha/_limaduadualebihtigapuluh', $data, true);
    }

    function pihakketigaCetakCtrl()
    {
        $tahun = $this->tahun;
        $data = array(
            'title' => 'Monitor Pihak Ke tiga',
            'report_data' => $this->Tatausaha->MonitorPihakKetiga($tahun),
            'start' => 0
        );

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=pihakketiga523.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<style>
        a {
            color: black;
            text-decoration: none;
          }
        </style>";
        echo $this->load->view('tatausaha/_pihakketiga', $data, true);
    }
}
