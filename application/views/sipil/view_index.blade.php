@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Data Pekerjaan
    <div class="float-right">
        <?php if ($akses['create'] == 1) {
            echo anchor('sipil/create', "<i class='mdi mdi-plus'></i> Tambah", 'class="btn btn-primary"');
        } ?>
    </div>
</h2>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-table card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                List Data
                <div class="tools float-right">
                    <!-- @if (get_userdata('audit_role')!=3)
                    @else
                    <?php // anchor($cetak, '<i class="mdi mdi-print"></i> Cetak', 'class="btn btn-success" target="_blank"') ?>
                    @endif -->
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>OPD</th>
                            <th>Kegiatan</th>
                            <th>Sub Kegiatan</th>
                            <th>Anggaran</th>
                            <th>Kontrak</th>
                            <th>HPS</th>
                            <th>Persentase </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $sipil) {
                            $persentase = number_format($sipil->nilai_pengawas / $sipil->harga_perkiraan * 100, 2, ",", "."); ?>
                            <tr>
                                <td width="10px" align="center"><?= ++$start ?></td>
                                <td class="cell-detail"><?= $sipil->kode_skpd; ?>
                                    <span class="cell-detail-description"><?= $sipil->nama_skpd; ?></span></td>
                                <td class="cell-detail"> <?= $sipil->ket_keg; ?>
                                    <span class="cell-detail-description"><?= $sipil->ket_prog ?></span>
                                </td>
                                <td> <?= $sipil->sub_kegiatan; ?></td>

                                <td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
                                <td align="right"><?= angka($sipil->nilai_pengawas); ?></td>
                                <td align="right"><?= angka($sipil->harga_perkiraan); ?></td>
                                <td align="right"><?= $persentase ?> % </td>

                                <td width="100px" align="center">
                                    <div class="btn-group">
                                        {{-- @if(cekjadwal($sipil->id)==0) --}}
                                        {{-- anchor('sipil/jadwal/' . acak($sipil->id), '<i class="mdi mdi-calendar"></i> ', ' class="btn btn-xs btn-success"'); --}}
                                        {{-- @endif     --}}
                                        {!! anchor('sipil/read/' . acak($sipil->id), '<i class="mdi mdi-eye"></i> ', ' class="btn btn-xs btn-primary"'); !!}


                                        <?php

                                            if ($akses['update'] == 1) {
                                                echo anchor('sipil/update/' . acak($sipil->id), '<i class="mdi mdi-edit"></i>  ', ' class="btn btn-xs btn-success"');
                                            }

                                            if ($akses['delete'] == 1) {
                                                echo anchor('sipil/delete/' . acak($sipil->id), '<i class="mdi mdi-delete"></i>  ', ' class="btn btn-xs btn-danger" onclick="return confirm(\'Seluruh data progres pekerjaan juga akan terhapus dan tidak bisa di kembalikan, Apakah anda yakin ?\')" ');
                                            }
                                            ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
                <div class="float-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection