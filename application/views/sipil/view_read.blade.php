@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Data Pekerjaan
  <div class="float-right">
    {!! anchor('sipil/jadwal/' . acak($id), '<i class="mdi mdi-calendar"></i> Jadwal', 'class="btn btn-sm btn-warning"')
    !!}
    {!! anchor('sipil','<i class="mdi mdi-flip-to-back"></i> Kembali','class="btn btn-sm btn-primary"') !!}
  </div>
</h2>
@endsection
@section('content')
<div class="row">
  <div class="col-12 col-lg-12">
    <div class="card">
      <div class="tab-container">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item"><a class="nav-link active" href="#home" data-toggle="tab" role="tab"><i class="mdi mdi-file-text"></i> Pekerjaan</a></li>
          <li class="nav-item"><a class="nav-link" href="#profile" data-toggle="tab" role="tab"><i class="mdi mdi-library"></i> Jasa Konsultansi</a></li>
          <li class="nav-item"><a class="nav-link" href="#messages" data-toggle="tab" role="tab"><i class="mdi mdi-eye"></i> Jasa Konstruksi</a></li>
          <li class="nav-item"><a class="nav-link" href="#tatausaha" data-toggle="tab" role="tab"><i class="mdi mdi-file-text"></i> Administrasi</a></li>
          <li class="nav-item"><a class="nav-link" href="#jadwal" data-toggle="tab" role="tab"><i class="mdi mdi-calendar"></i> Perencanaan</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="jadwal" role="tabpanel">
            <div class="row">
              <?php foreach ($perencanaan as $pr) { ?>
                <div class="col-md-6">
                  <div class="card card-border card-contrast">
                    <div class="card-header card-header-contrast">
                      Minggu Ke <?= $pr->minggu ?>
                    </div>
                    <div class="card-body">
                      <p>Pekerjaan <?= mediumdate_indo($pr->awal_minggu) ?> s/d <?= mediumdate_indo($pr->akhir_minggu) ?></p>
                      <p><b>Kumulatif perencanaan</b> <?= $pr->persentase <> '' ? $pr->persentase : 0; ?>%</p>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
          <div class="tab-pane" id="tatausaha" role="tabpanel">
            <div class="row">
              <div class="col-md-4">
                <b>SPMK</b> <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#form-bp1" type="button"><i class="mdi mdi-refresh"></i> </button>
                <table>
                  <tr>
                    <td width="30%">Nomer</td>
                    <td>: <?= $no_spmk ?> </td>
                  </tr>
                  <tr>
                    <td>Tanggal</td>
                    <td>: <?= date_indo($tanggal_spmk) ?> </td>
                  </tr>
                </table>
              </div>
              <div class="col-md-8">
                <b>SP2D</b> <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#form-sp2d" type="button"><i class="mdi mdi-plus-square"></i> </button>
                <div class="row">
                  <?php foreach ($sp2d as $sp2d) { ?>
                    <div class="col-lg-4">
                      <div class="card">
                        <div class="card-header card-header-divide">
                            <?= $sp2d->no_sp2d ?>
                            <span class="card-subtitle"><?= date_indo($sp2d->tanggal_sp2d)?> <br> <b><?= number_format($sp2d->nilai_sp2d,0,'','.')?></b><br><a onclick="return confirm('Apakah anda yakin ?')" class="card-link" href="<?= base_url().'sipil/hapussp2d/'.acak($sp2d->id) ?>">Hapus</a></span>
                          </div>
                        <!-- <div class="card-body">

                        </div> -->
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>

            </div>


          </div>
          <div class="tab-pane active" id="home" role="tabpanel">
            <?php if (cekjadwal($id) == 0 && get_userdata('audit_role')==1004) { ?>
              <div class="alert alert-warning alert-dismissible" role="alert">
                <!-- <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button> -->
                <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                <div class="message"><strong>Warning!</strong> Perencanaan progres mingguan belum di isi. {!! anchor('sipil/jadwal/' . acak($id), '<i class="mdi mdi-edit"></i> Lengkapi', 'class="btn btn-sm btn-success"')!!}</div>
              </div>
            <?php } ?>

            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">OPD</td>
                  <td class="icon">:</td>
                  <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                </tr>
                <tr>
                  <td class="item">Program</td>
                  <td class="icon">:</td>
                  <td><?= $ket_prog ?></td>
                </tr>
                <tr>
                  <td class="item">Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $ket_keg ?></td>
                </tr>
                <tr>
                  <td class="item">Sub Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $sub_kegiatan ?></td>
                </tr>
                <tr>
                  <td class="item"> Anggaran</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                </tr>
                <tr>
                  <td class="item">Harga Perkiraan Sendiri</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                </tr>
              </tbody>
            </table>

          </div>
          <div class="tab-pane" id="profile" role="tabpanel">

            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">Nama</td>
                  <td class="icon">:</td>
                  <td><?= $nama_konsultansi ?></td>
                </tr>
                <tr>
                  <td class="item">No Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= $no_sk_konsultansi ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_sk_konsultansi) ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai Kontrak</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_konsultansi) ?></td>
                </tr>
                <tr>
                  <td class="item">Periode Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_mulai_konsultansi) . ' s/d ' . date_indo($tgl_selesai_konsultansi) ?></td>
                </tr>
              </tbody>
            </table>

            <?php if ($no_addedum_konsultansi) { ?>
              <hr>
              <table class="no-border no-strip skills">
                <tbody class="no-border-x no-border-y">

                  <tr>
                    <td class="item">No Addedum</td>
                    <td class="icon">:</td>
                    <td><?= $no_addedum_konsultansi ?></td>
                  </tr>
                  <tr>
                    <td class="item">Tanggal Addedum</td>
                    <td class="icon">:</td>
                    <td><?= $tgl_addedum_konsultansi ?></td>
                  </tr>
                  <tr>
                    <td class="item">Nilai Addedum</td>
                    <td class="icon">:</td>
                    <td>Rp.<?= angka($nilai_addedum_konsultansi) ?></td>
                  </tr>
                  <tr>
                    <td class="item">Periode Addedum</td>
                    <td class="icon">:</td>
                    <td><?= date_indo($tgl_mulai_addedum_konsultansi) . ' s/d ' . date_indo($tgl_selesai_addedum_konsultansi) ?></td>
                  </tr>
                </tbody>
              </table>

            <?php } ?>

          </div>
          <div class="tab-pane" id="messages" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">Nama</td>
                  <td class="icon">:</td>
                  <td><?= $nama_pengawas ?> / <?= $nama_cv_pengawas ?></td>
                </tr>
                <tr>
                  <td class="item">No Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= $no_sk_pengawas ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_sk_pengawas) ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai Kontrak</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_pengawas) ?></td>
                </tr>
                <tr>
                  <td class="item">Periode Kontrak</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_mulai_pengawas) . ' s/d ' . date_indo($tgl_selesai_pengawas) ?></td>
                </tr>
              </tbody>
            </table>

            <?php if ($no_addedum_pengawas) { ?>
              <hr>
              <table class="no-border no-strip skills">
                <tbody class="no-border-x no-border-y">

                  <tr>
                    <td class="item">No Addedum</td>
                    <td class="icon">:</td>
                    <td><?= $no_addedum_pengawas ?></td>
                  </tr>
                  <tr>
                    <td class="item">Tanggal Addedum</td>
                    <td class="icon">:</td>
                    <td><?= $tgl_addedum_pengawas ?></td>
                  </tr>
                  <tr>
                    <td class="item">Nilai Addedum</td>
                    <td class="icon">:</td>
                    <td>Rp.<?= angka($nilai_addedum_pengawas) ?></td>
                  </tr>
                  <tr>
                    <td class="item">Periode Addedum</td>
                    <td class="icon">:</td>
                    <td><?= date_indo($tgl_mulai_addedum_pengawas) . ' s/d ' . date_indo($tgl_selesai_addedum_pengawas) ?></td>
                  </tr>
                </tbody>
              </table>

            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="row">
  <div class="col-12">
    <div class="card  card-border-color card-border-color-primary">
      <div class="card-header card-header-divider">
        Timeline Progres
      </div>
      <div class="card-body">
        <div class="col-md-12">
          <div style="display:inline-block;width:100%;overflow-y:auto;">

            <?php if (!empty($progres)) { ?>
              <ul class="timeline timeline-horizontal">

                <?php $dd = 1;
                  foreach ($progres as $pp) { ?>
                  <li class="timeline-item">
                    <div class="timeline-badge success"></div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        <b>
                          <h5 class="timeline-title"><?= date_indo(date('Y-m-d', strtotime($pp->created_at))) ?></h5>
                        </b>
                      </div>
                      <div class="timeline-body">
                        <p><small class="text-muted"><i class="mdi mdi-long-arrow-tab"></i> Kumulatif Realisasi :
                            <?= number_format($pp->realisasi,1) ?> %</small></p>
                        <p><small class="text-muted"><i class="mdi mdi-long-arrow-tab"></i> Kumulatif Perencanaan :
                            <?= number_format($pp->perencanaan,1) ?> %</small></p>
                        <p><small class="text-muted"><i class="mdi mdi-long-arrow-tab"></i> Deviasi :
                            <?= number_format($pp->deviasi,1)  ?> %</small></p>
                        <p><small class="text-muted"><i class="mdi mdi-long-arrow-tab"></i> Pekerjaan
                            <?= gabungTanggal2($pp->awal_minggu, $pp->akhir_minggu) ?> </small></p>

                      </div>
                    </div>
                  </li>

                <?php $dd++;
                  } ?>
              </ul>
            <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade colored-header colored-header-primary" id="form-sp2d" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?= base_url() . 'sipil/tambahsp2d' ?>" method="post">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="modal-header modal-header-colored">
          <h3 class="modal-title">Form SP2D</h3>
          <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" required name="no_sp2d" class="form-control form-control-sm" required placeholder="Nomer SP2D">
          </div>
          <div class="form-group">
            <input type="text" required name="tanggal_sp2d" class="form-control form-control-sm datepicker" required placeholder="Tanggal SP2D">
          </div>
          <div class="form-group">
            <input type="text" required name="nilai_sp2d" class="form-control form-control-sm" onkeyup="formatangka(this)" required placeholder="Nilai SP2D">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary  " type="submit"><i class="mdi mdi-cloud-done"></i> Proses</button>
          <button class="btn btn-secondary md-close" type="button" data-dismiss="modal"><i class="mdi mdi-close"></i> Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade colored-header colored-header-primary" id="form-bp1" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?= base_url() . 'sipil/updatespmk' ?>" method="post">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="modal-header modal-header-colored">
          <h3 class="modal-title">Update SPMK</h3>
          <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>No SPMK</label>
            <input class="form-control" type="text" name="no_spmk" required value="<?= $no_spmk ?>">
          </div>
          <div class="form-group">
            <label>Tanggal SPMK</label>
            <input class="form-control datepicker" type="text" name="tgl_spmk" required value="<?= $tanggal_spmk ?>">
          </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary  " type="submit"><i class="mdi mdi-cloud-done"></i> Proses</button>
          <button class="btn btn-secondary md-close" type="button" data-dismiss="modal"><i class="mdi mdi-close"></i> Cancel</button>
        </div>
      </form>
    </div>
  </div>
  <style>
    /* Timeline */
    .timeline,
    .timeline-horizontal {
      list-style: none;
      padding: 20px;
      position: relative;
    }

    .timeline:before {
      top: 40px;
      bottom: 0;
      position: absolute;
      content: " ";
      width: 3px;
      background-color: #eeeeee;
      left: 50%;
      margin-left: -1.5px;
    }

    .timeline .timeline-item {
      margin-bottom: 20px;
      position: relative;
    }

    .timeline .timeline-item:before,
    .timeline .timeline-item:after {
      content: "";
      display: table;
    }

    .timeline .timeline-item:after {
      clear: both;
    }

    .timeline .timeline-item .timeline-badge {
      color: #fff;
      width: 54px;
      height: 54px;
      line-height: 52px;
      font-size: 22px;
      text-align: center;
      position: absolute;
      top: 18px;
      left: 50%;
      margin-left: -25px;
      background-color: #7c7c7c;
      border: 3px solid #ffffff;
      z-index: 100;
      border-top-right-radius: 50%;
      border-top-left-radius: 50%;
      border-bottom-right-radius: 50%;
      border-bottom-left-radius: 50%;
    }

    .timeline .timeline-item .timeline-badge i,
    .timeline .timeline-item .timeline-badge .fa,
    .timeline .timeline-item .timeline-badge .glyphicon {
      top: 2px;
      left: 0px;
    }

    .timeline .timeline-item .timeline-badge.primary {
      background-color: #1f9eba;
    }

    .timeline .timeline-item .timeline-badge.info {
      background-color: #5bc0de;
    }

    .timeline .timeline-item .timeline-badge.success {
      background-color: #59ba1f;
    }

    .timeline .timeline-item .timeline-badge.warning {
      background-color: #d1bd10;
    }

    .timeline .timeline-item .timeline-badge.danger {
      background-color: #ba1f1f;
    }

    .timeline .timeline-item .timeline-panel {
      position: relative;
      width: 46%;
      float: left;
      right: 16px;
      border: 1px solid #c0c0c0;
      background: #ffffff;
      border-radius: 2px;
      padding: 20px;
      -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
      box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline .timeline-item .timeline-panel:before {
      position: absolute;
      top: 26px;
      right: -16px;
      display: inline-block;
      border-top: 16px solid transparent;
      border-left: 16px solid #c0c0c0;
      border-right: 0 solid #c0c0c0;
      border-bottom: 16px solid transparent;
      content: " ";
    }

    .timeline .timeline-item .timeline-panel .timeline-title {
      margin-top: 0;
      color: inherit;
    }

    .timeline .timeline-item .timeline-panel .timeline-body>p,
    .timeline .timeline-item .timeline-panel .timeline-body>ul {
      margin-bottom: 0;
    }

    .timeline .timeline-item .timeline-panel .timeline-body>p+p {
      margin-top: 5px;
    }

    .timeline .timeline-item:last-child:nth-child(even) {
      float: right;
    }

    .timeline .timeline-item:nth-child(even) .timeline-panel {
      float: right;
      left: 16px;
    }

    .timeline .timeline-item:nth-child(even) .timeline-panel:before {
      border-left-width: 0;
      border-right-width: 14px;
      left: -14px;
      right: auto;
    }

    .timeline-horizontal {
      list-style: none;
      position: relative;
      padding: 20px 0px 20px 0px;
      display: inline-block;
    }

    .timeline-horizontal:before {
      height: 3px;
      top: auto;
      bottom: 26px;
      left: 56px;
      right: 0;
      width: 100%;
      margin-bottom: 20px;
    }

    .timeline-horizontal .timeline-item {
      display: table-cell;
      height: 280px;
      width: 20%;
      min-width: 200px;
      float: none !important;
      padding-left: 0px;
      padding-right: 20px;
      margin: 0 auto;
      vertical-align: bottom;
    }

    .timeline-horizontal .timeline-item .timeline-panel {
      top: auto;
      bottom: 64px;
      display: inline-block;
      float: none !important;
      left: 0 !important;
      right: 0 !important;
      width: 100%;
      margin-bottom: 20px;
    }

    .timeline-horizontal .timeline-item .timeline-panel:before {
      top: auto;
      bottom: -16px;
      left: 28px !important;
      right: auto;
      border-right: 16px solid transparent !important;
      border-top: 16px solid #c0c0c0 !important;
      border-bottom: 0 solid #c0c0c0 !important;
      border-left: 16px solid transparent !important;
    }

    .timeline-horizontal .timeline-item:before,
    .timeline-horizontal .timeline-item:after {
      display: none;
    }

    .timeline-horizontal .timeline-item .timeline-badge {
      top: auto;
      bottom: 0px;
      left: 43px;
    }
  </style>
  @endsection
  @section('script')
  <script>
    function formatangka(objek) {
      a = objek.value;
      b = a.replace(/[^\d]/g, "");
      c = "";
      panjang = b.length;
      j = 0;
      for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
          c = b.substr(i - 1, 1) + "." + c;
        } else {
          c = b.substr(i - 1, 1) + c;
        }
      }
      objek.value = c;
    };
  </script>
  @endsection