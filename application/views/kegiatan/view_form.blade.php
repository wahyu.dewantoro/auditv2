@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Belanja Modal
    <div class="float-right">
        {!! anchor('kegiatan','Kembali','class="btn btn-sm btn-primary"') !!}
    </div>
</h2>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured card-header-divider">
                Form Kegiatan Belanja Modal
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form id="regForm" method="post" action="<?= $action ?>">
                    <div class="tab">
                        <div class="row">
                            <div class="col-12">
                                <address><strong>Data pekerjaan</strong></address>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">OPD <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_skpd" class="form-control form-control-sm select2 required" id="kode_skpd" data-placeholder="Pilih OPD">
                                            <option value=""></option>
                                            <?php foreach ($opd as $opd) { ?>
                                                <option <?php if ($opd->kd_skpd == $kode_skpd) {
                                                                echo "selected";
                                                            } ?> value="<?= $opd->kd_skpd ?>"><?= $opd->nm_sub_unit ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('kode_skpd'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Jenis Belanja
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_belanja" class="form-control select2 form-control-sm required" id="kode_belanja" data-placeholder="Pilih Jenis ">
                                            <option value=""></option>>
                                            <?php foreach ($belanja as $belanja) { ?>
                                                <option <?php if ($kode_belanja == $belanja->kode) {
                                                                echo "selected";
                                                            } ?> value="<?= $belanja->kode ?>">
                                                    <?= $belanja->jenis_belanja_modal ?></option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('kode_belanja'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Program <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_program" class="form-control select2 form-control-sm required" id="kode_program" data-placeholder="Pilih Program">
                                            <option value=""></option>>
                                            @if(isset($lp))
                                            @foreach($lp as $lp)
                                            <option @if($kode_program==$lp->Kd_Prog.'.'.$lp->ID_Prog) selected @endif
                                                value="{{ $lp->Kd_Prog.'.'.$lp->ID_Prog }}">{{ $lp->Ket_Program }}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Kegiatan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_kegiatan" class="form-control select2 form-control-sm required" id="kode_kegiatan" data-placeholder="Pilih Kegiatan">
                                            <option value=""></option>>
                                            @if(isset($lk))
                                            @foreach($lk as $lk)
                                            <option @if($lk->ket_kegiatan==$ket_kegiatan && $kd_keg==$lk->kd_keg)
                                                selected @endif value="{{ $lk->kd_keg }}">{{ $lk->ket_kegiatan }}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <input type="hidden" id="ket_kegiatan" value="{{ $ket_kegiatan }}" name="ket_kegiatan">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Sub Kegiatan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <div class="input-group mb-3">
                                            <input type="text" name="sub_kegiatan" class="form-control form-control-sm required" id="sub_kegiatan" value="<?= $sub_kegiatan ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Pagu Anggaran
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="pagu_anggaran" class="form-control form-control-sm required" onkeyup="formatangka(this)" id="pagu_anggaran" value="<?= $pagu_anggaran ?>">
                                        <?= form_error('pagu_anggaran'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Harga Perkiraan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="harga_perkiraan" class="form-control form-control-sm required" onkeyup="formatangka(this)" id="harga_perkiraan" value="<?= $harga_perkiraan ?>">
                                        <?= form_error('harga_perkiraan'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nama PPK
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="nama_ppk" class="form-control form-control-sm select2 required" id="nama_ppk" data-placeholder="Pilih PPK">
                                            <option value=""></option>
                                            <?php foreach ($ppk as $ppk) { ?>
                                                <option <?php if ($ppk->id == $id_ppk) {
                                                                echo "selected";
                                                            } ?> value="<?= $ppk->id . "/" . $ppk->nama_ppk ?>"><?= $ppk->nama_ppk ?></option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('nama_ppk'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <p><input placeholder="First name..." oninput="this.className = ''" name="fname"></p>
                        <p><input placeholder="Last name..." oninput="this.className = ''" name="lname"></p> -->
                    </div>
                    <div class="tab">

                        <div class="row">
                            <div class="col-12">
                                <address><strong>Data SPK</strong></address>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No Kontrak/No SK<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_spk" id="no_spk" value="<?= $no_spk ?>" class="required form-control form-control-sm">
                                        <?= form_error('no_spk'); ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tanggal_spk" id="tanggal_spk" value="<?= $tanggal_spk ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tanggal_spk'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Mulai Pekerjaan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="mulai_pekerjaan_spk" id="mulai_pekerjaan_spk" value="<?= $mulai_pekerjaan_spk ?>" class="required datepicker form-control form-control-sm">
                                        <?= form_error('mulai_pekerjaan_spk'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Selesai Pekerjaan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="selesai_pekerjaan_spk" id="selesai_pekerjaan_spk" value="<?= $selesai_pekerjaan_spk ?>" class="required datepicker form-control form-control-sm">
                                        <?= form_error('selesai_pekerjaan_spk'); ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_spk" id="nilai_spk" onkeyup="formatangka(this)" value="<?= $nilai_spk ?>" class="required form-control form-control-sm">
                                        <?= form_error('nilai_spk'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <address><strong>Data Pelaksana</strong></address>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Pelaksana
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="pelaksana" id="pelaksana" value="<?= $pelaksana ?>" class="required form-control form-control-sm">
                                        <?= form_error('pelaksana') ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No SP <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_spmk" id="no_spmk" value="<?= $no_spmk ?>" class="required form-control form-control-sm">
                                        <?= form_error('no_spmk') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal SP
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tanggal_spmk" id="tanggal_spmk" value="<?= $tanggal_spmk ?>" class="required datepicker form-control form-control-sm">
                                        <?= form_error('tanggal_spmk') ?>
                                        <input type="hidden" name="id" value="<?= $id ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
                        <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p> -->
                    </div>
                    <div class="tab">
                        <div class="row">
                            <div class="col-12">
                                <address><strong>Data Addedum</strong></address>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No SK</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_addedum" id="no_addedum" value="<?= $no_addedum ?>" class="form-control form-control-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tanggal_addedum" id="tanggal_addedum" value="<?= $tanggal_addedum ?>" class="form-control form-control-sm datepicker">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_addedum" id="nilai_addedum" onkeyup="formatangka(this)" value="<?= $nilai_addedum ?>" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Mulai Pekerjaan</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="mulai_pekerjaan_addedum" id="mulai_pekerjaan_addedum" value="<?= $mulai_pekerjaan_addedum ?>" class="datepicker form-control form-control-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Selesai Pekerjaan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="selesai_pekerjaan_addedum" id="selesai_pekerjaan_addedum" value="<?= $selesai_pekerjaan_addedum ?>" class=" datepicker form-control form-control-sm">
                                        <?= form_error('selesai_pekerjaan_addedum'); ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div style="overflow:auto;">
                        <div style="float:right;">
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)"><i class="mdi mdi-arrow-left"></i>
                                Previous</button>
                            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next <i class="mdi mdi-arrow-right"></i> </button>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                </form>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('css')
<style>
    * {
        box-sizing: border-box;
    }

    body {
        background-color: #f1f1f1;
    }

    #regForm {
        background-color: #ffffff;
        /* margin: 100px auto; */
        /* font-family: Raleway; */
        padding: 10px;
        /* width: 70%; */
        min-width: 300px;
    }

    h1 {
        text-align: center;
    }

    input {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        font-family: Raleway;
        border: 1px solid #aaaaaa;
    }

    /* Mark input boxes that gets an error on validation: */
    input.invalid {
        background-color: #ffdddd;
    }

    /* Hide all steps by default: */
    .tab {
        display: none;
    }

    button {
        background-color: #4CAF50;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 17px;
        font-family: Raleway;
        cursor: pointer;
    }

    button:hover {
        opacity: 0.8;
    }

    #prevBtn {
        background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbbbbb;
        border: none;
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        background-color: #4CAF50;
    }
</style>
@endsection
@section('script')
<script>
    function formatangka(objek) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + "." + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;
    };
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "<i class='mdi mdi-cloud-done'></i> Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next <i class='mdi mdi-arrow-right'></i>";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            // alert('asdasf');
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByClassName("required");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
<script>
    $(document).ready(function() {
        // edit
        $('#kode_skpd').on('change', function() {
            getProgramOpd($(this).val());
            getPPK($(this).val());
        });

        $('#kode_program').on('change', function() {
            getKegiatanList();
        });

        $('#kode_kegiatan').on('change', function() {
            $('#ket_kegiatan').val($("#kode_kegiatan option:selected").text());
            getPagu();
        });

        function getProgramOpd(opd) {
            var _url = "<?php echo base_url() ?>" + "sipil/getprogram";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#kode_program").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#kode_program')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }

        function getKegiatanList() {
            var opd = $('#kode_skpd').val();
            var kp = $('#kode_program').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getkegiatan";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd,
                    'kp': kp
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#kode_kegiatan").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#kode_kegiatan')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }

        function getPPK() {
            var opd = $('#kode_skpd').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getPPK";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#nama_ppk").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#nama_ppk')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }

        function getPagu() {
            var opd = $('#kode_skpd').val();
            var kp = $('#kode_program').val();
            var kk = $('#kode_kegiatan').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getPagu";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd,
                    'kk': kk,
                    'kp': kp
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {

                    // $("#pagu_anggaran").val(data.pagu);
                    $.each(data, function(key, item) {
                        $('#pagu_anggaran')
                            .val(item.pagu);
                    });
                }
            });
        }
    });
</script>

@endsection