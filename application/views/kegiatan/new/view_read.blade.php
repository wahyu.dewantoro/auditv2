@extends('metronic.master')
@section('toolbar')
{!! anchor('kegiatan/cetak/' . acak($id), '<i class="flaticon2-printer"></i> Cetak', ' style="color:white"  class="btn btn-danger"'); !!}
{!! anchor('kegiatan/bast/' . acak($id), '<i class="flaticon-folder"></i> Bast', ' class="btn btn-success""'); !!}
{!! anchor('kegiatan', '<i class="flaticon2-left-arrow-1"></i> Kembali', ' class="btn btn-primary""'); !!}
@endsection
@section('content')
<div class="row">

  <div class="col-12 col-lg-8">
		<div class="kt-portlet kt-portlet--tabs">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_3_1_tab_content" role="tab" aria-selected="true">
								<i class="la la-tasks" aria-hidden="true"></i>Kegiatan
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_2_tab_content" role="tab" aria-selected="false">
								<i class="flaticon-profile-1" aria-hidden="true"></i>SPK
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#kt_portlet_base_demo_3_3_tab_content" role="tab" aria-selected="false">
								<i class="flaticon-buildings" aria-hidden="true"></i>Addedum
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#kt_portlet_base_demo_3_4_tab_content" role="tab" aria-selected="false">
								<i class="flaticon-list-2" aria-hidden="true"></i>Foto Pendukung
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="tab-content">
					<div class="tab-pane active" id="kt_portlet_base_demo_3_1_tab_content" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">OPD</td>
                  <td class="icon">:</td>
                  <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                </tr>
                <tr>
                  <td class="item">Jenis Belanja</td>
                  <td class="icon">:</td>
                  <td><?= $kode_belanja ?> <?= $jenis_belanja ?></td>
                </tr>
                <tr>
                  <td class="item">Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $nama_kegiatan ?></td>
                </tr>
                <tr>
                  <td class="item">Sub Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $sub_kegiatan ?></td>
                </tr>
                <tr>
                  <td class="item">Nama PPK</td>
                  <td class="icon">:</td>
                  <td><?= $nama_ppk ?></td>
                </tr>
                <tr>

                  <td class="item">Pagu Anggaran</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                </tr>
                <tr>
                  <td class="item">Harga Perkiraan Sendiri</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                </tr>
                <tr>
                  <td class="item">pelaksana</td>
                  <td class="icon">:</td>
                  <td><?= $pelaksana ?></td>
                </tr>
                <tr>
                  <td class="item">No SP</td>
                  <td class="icon">:</td>
                  <td><?= $no_spmk ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal SP</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_spmk <> '' ? date_indo($tanggal_spmk) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal BAST</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_bast <> '' ? date_indo($tanggal_bast) : '' ?></td>
                </tr>
              </tbody>
            </table>
					</div>
					<div class="tab-pane" id="kt_portlet_base_demo_3_2_tab_content" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">No Kontrak/No SK</td>
                  <td class="icon">:</td>
                  <td><?= $no_spk ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tanggal_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Mulai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($mulai_pekerjaan_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Selesai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($selesai_pekerjaan_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_spk) ?></td>
                </tr>
              </tbody>
            </table>
					</div>
					<div class="tab-pane" id="kt_portlet_base_demo_3_3_tab_content" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">Nomer</td>
                  <td class="icon">:</td>
                  <td><?= $no_addedum ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_addedum <> '' ? date_indo($tanggal_addedum) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Mulai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= $mulai_pekerjaan_addedum <> '' ? date_indo($mulai_pekerjaan_addedum) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_addedum) ?></td>
                </tr>
              </tbody>
            </table>
					</div>
					<div class="tab-pane" id="kt_portlet_base_demo_3_4_tab_content" role="tabpanel">
						@foreach ($file as $file)

						<a target="_blank" href="{{base_url().$file->nama_file}}"><i class="mdi mdi-attachment-alt"></i> {{$file->nama_ori}}</a> <br>
						@endforeach
					</div>
				</div>
			</div>
		</div>
  </div>
  <div class="col-lg-4 col-12">
		<div class="kt-portlet kt-callout kt-callout--success">
			<div class="kt-portlet__body">
				<div class="kt-callout__body">
					<div class="kt-callout__content">
						<h3 class="kt-callout__title">Rekapitulasi</h3>
						<p class="kt-callout__desc">
							<strong>SPK</strong> : Rp.<?= angka($nilai_spk) ?> <br>
							<strong>Addedum</strong> : Rp.<?= angka($nilai_addedum) ?><br>
						</p>
					</div>
					<div class="kt-callout__action">
						<a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><i class="la la-calendar"></i> </span> <span class="frecuency"><?= date('d') . '/' . medium_bulan(date('m')) . '/' . date('y') ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
