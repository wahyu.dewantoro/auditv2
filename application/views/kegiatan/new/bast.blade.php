@extends('metronic.master')
@section('toolbar')
{!! anchor('kegiatan', '<i class="flaticon2-left-arrow-1"></i> Kembali', ' class="btn btn-primary""'); !!}
@endsection
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Form
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
				<form class="kt-form" action="{{ base_url().'kegiatan/updateBast' }}" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group row">
								<label for="">Tanggal BAST</label><br>
								<input autocomplete="off" type="text" name="tanggal_bast" id="tanggal_bast" value="<?= $tanggal_bast?>"
									class="required form-control form-control-sm datepicker">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group row">
								<label for="">
									<i class="la la-file"></i> (.pdf/.zip/.rar/.jpg)
									<button id='addButton' type="button" class="btn btn-xs btn-success"><i
											class="la la-plus"></i></button>
									<button id='removeButton' type="button" class="btn btn-xs btn-danger"><i
											class="la la-trash"></i></button>
								</label>
								<div id='TextBoxesGroup'>
									<div class="col-md-10" id="TextBoxDiv1">
										<input type="file" class="form-control form-control-sm " name="file_progres[]" id="file_progres">
									</div>
								</div>
							<input type="hidden" name="id" value="<?= acak($id)?>">
							</div>

						</div>
						<div class="col-md-4">

                            @foreach ($file as $file)

                            <a target="_blank" href="{{base_url().$file->nama_file}}"><i class="mdi mdi-attachment-alt"></i> {{$file->nama_ori}}</a> <span data-id="{{ $file->id }}" class="hapusdok"><i class="la la-trash"></i></span> <br>
                            @endforeach
						</div>
                        <div class="col-12">
                            <div class="float-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i>
                                    Simpan</button>
                            </div>
                        </div>
					</div>
                </form>

            </div>
        </div><!-- end card-->
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
    var counter = 2;
    $("#addButton").click(function() {
      if (counter > 10) {
        alert("Only 10 textboxes allow");
        return false;
      }
      var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter).attr("class", 'col-md-10');
      newTextBoxDiv.after().html('<input type="file" class="form-control" name="file_progres[]" id="file_progres">');
      newTextBoxDiv.appendTo("#TextBoxesGroup");

      counter++;
    });


    $("#removeButton").click(function() {
      if (counter == 2) {
        alert("No more textbox to remove");
        return false;
      }
      counter--;

      $("#TextBoxDiv" + counter).remove();
    });

    $(function() {
		$(document).on('click', '.hapusdok', function(e) {
			e.preventDefault();
			if($('.hapusdok').length==1){
				alert('Tidak dapat Menghapus, Dokumen Pendukung Harus ada');
			} else{

			if (confirm('Apakah anda akan menghapus file?')) {

				$.ajax({
					url: "<?= base_url().'kegiatan/hapus_file'?>",
					method: 'GET',
					data: {
						id: $(this).attr('data-id')
					},
					success: function(data) {
						// alert(data);
						location.reload(true);
					}
				});
			}
			}
		});
	});
  </script>
<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/bootstrap-datepicker2.js" type="text/javascript"></script>
@endsection
