@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Data Kegiatan
    <div class="float-right">
        <?php if ($akses['create'] == 1) {
            echo anchor('kegiatan/create', "<i class='mdi mdi-plus'></i> Tambah", 'class="btn btn-primary"');
        } ?>
    </div>
</h2>
@endsection
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>

		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<?php if ($akses['create'] == 1) {
                            echo anchor('kegiatan/create', "<i class='la la-plus'></i> Tambah", 'class="btn btn-brand btn-elevate btn-icon-sm"');
						} ?>
                    @if (get_userdata('audit_role')!=3)

                    @else

                    <?php // anchor($cetak, '<i class="mdi mdi-print"></i> Cetak', 'class="btn btn-success" target="_blank"') ?>
                    @endif
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>OPD</th>
								<th>Jenis Belanja</th>
								<!-- <th>Kegiatan</th> -->
								<th>Sub Kegiatan</th>
								<th>Pagu</th>
								<th>Nilai SPK</th>
								<th>HPS</th>
								<th>Persentase </th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($data as $sipil) {
								$persentase = number_format($sipil->nilai_spk / $sipil->harga_perkiraan * 100, 2, ",", "."); ?>
								<tr>
									<td width="10px" align="center"><?= ++$start ?></td>
									<td>
										<span style="width: 200px;">
											<div class="kt-user-card-v2">
												<div class="kt-user-card-v2__details"> <span
														class="kt-user-card-v2__name"><?= $sipil->kode_skpd; ?></span> <span
														class="kt-user-card-v2__desc"><?= $sipil->nama_skpd; ?></span>
												</div>
											</div>
										</span>
									</td>
									<td>
										<span style="width: 200px;">
											<div class="kt-user-card-v2">
												<div class="kt-user-card-v2__details"> <span
														class="kt-user-card-v2__name"><?= $sipil->kode_belanja; ?></span> <span
														class="kt-user-card-v2__desc"><?= $sipil->jenis_belanja; ?></span>
												</div>
											</div>
										</span>
									</td>
									<!-- <td><?= $sipil->nama_kegiatan; ?></td> -->
									<td><?= $sipil->sub_kegiatan; ?></td>
									<td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
									<td align="right"><?= angka($sipil->nilai_spk); ?></td>
									<td align="right"><?= angka($sipil->harga_perkiraan); ?></td>
									<td align="right"><?= $persentase ?> % </td>


									<td width="100px" align="center">
										<div class="btn-group">

											{!! anchor('kegiatan/read/' . acak($sipil->id), '<i class="la la-eye"></i> ', ' class="btn btn-xs btn-primary btn-icon btn-icon-md"'); !!}


											<?php

											if ($akses['update'] == 1) {
												echo anchor('kegiatan/update/' . acak($sipil->id), '<i class="la la-edit"></i>  ', ' class="btn btn-xs btn-success btn-icon btn-icon-md"');
											}

											if ($akses['delete'] == 1) {
												echo anchor('kegiatan/delete/' . acak($sipil->id), '<i class="la la-trash"></i>  ', ' class="btn btn-xs btn-danger btn-icon btn-icon-md" onclick="return confirm(\'Seluruh data progres pekerjaan juga akan terhapus dan tidak bisa di kembalikan, Apakah anda yakin ?\')" ');
											}
											?>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
				</div>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection
