<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured card-header-divider">
                    Form Monitoring Pekerjaan Sipil
                 <div class="tools">

                 </div>
            </div>
            <div class="card-body">

                <form method="post" action="<?= $action ?>">
                  <div class="row">
                      <div class="col-md-4">
                          <address><strong>Data pekerjaan</strong></address>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">OPD  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <!-- <input type="text"  value="<?= $kode_skpd?>"> -->
                                <select name="kode_skpd" class="form-control form-control-sm select2" id="kode_skpd">
                                    <option value="">Pilih OPD</option>
                                    <?php foreach($opd as $opd){?>
                                        <option <?php if($opd->kd_skpd==$kode_skpd){echo "selected";}?> value="<?= $opd->kd_skpd?>"><?= $opd->nm_sub_unit ?></option>
                                    <?php } ?>
                                </select>
                                <?= form_error('kode_skpd'); ?>
                                </div>
                          </div>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Jenis Belanja  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <!-- <input type="text"  value="<?= $kode_belanja?>"> -->
                                <select name="kode_belanja" class="form-control select2 form-control-sm" id="kode_belanja">
                                    belanja
                                    <option value="">Pilih Jenis </option>>
                                    <?php foreach($belanja as $belanja){?>
                                        <option <?php if($kode_belanja==$belanja->kode){echo "selected";}?> value="<?= $belanja->kode ?>"><?= $belanja->jenis_belanja_modal ?></option>
                                    <?php } ?>
                                </select>
                                <?= form_error('kode_belanja'); ?>
                                </div>
                          </div>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nama Kegiatan  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <input type="text" name="nama_kegiatan" class="form-control form-control-sm" id="nama_kegiatan" value="<?= $nama_kegiatan?>">
                                <?= form_error('nama_kegiatan'); ?>
                                </div>
                          </div>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Pagu Anggaran  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <input type="text" name="pagu_anggaran" class="form-control form-control-sm" onkeyup="formatangka(this)" id="pagu_anggaran" value="<?= $pagu_anggaran?>">
                                <?= form_error('pagu_anggaran'); ?>
                                </div>
                          </div>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Harga Perkiraan  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <input type="text" name="harga_perkiraan" class="form-control form-control-sm" onkeyup="formatangka(this)" id="harga_perkiraan" value="<?= $harga_perkiraan?>">
                                <?= form_error('harga_perkiraan'); ?>
                                </div>
                          </div>
                          <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nama PPK  <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                <input type="text" name="nama_ppk" class="form-control form-control-sm" id="nama_ppk" value="<?= $nama_ppk?>">
                                <?= form_error('nama_ppk'); ?>
                                </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <address><strong>Data Konsultan</strong></address>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Konsultan <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="konsultan" id="konsultan" class="form-control form-control-sm" value="<?= $konsultan ?>">
                                    <?= form_error('konsultan'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">No SK <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="no_sk_konsultan" id="no_sk_konsultan" class="form-control form-control-sm" value="<?= $no_sk_konsultan ?>">
                                    <?= form_error('no_sk_konsultan'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="nilai_konsultan" id="nilai_konsultan" onkeyup="formatangka(this)" class="form-control form-control-sm" value="<?= $nilai_konsultan ?>">
                                    <?= form_error('nilai_konsultan'); ?>
                                </div>
                            </div>
                            <address><strong>Data Pengawas</strong></address>

                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">pengawas <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="pengawas" id="pengawas" value="<?= $pengawas ?>" class="form-control form-control-sm">
                                    <?= form_error('pengawas'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">No SK <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="no_sk_pengawas" id="no_sk_pengawas" value="<?= $no_sk_pengawas ?>" class="form-control form-control-sm">
                                    <?= form_error('no_sk_pengawas'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="nilai_pengawas" id="nilai_pengawas" onkeyup="formatangka(this)" value="<?= $nilai_pengawas ?>" class="form-control form-control-sm">
                                    <?= form_error('nilai_pengawas'); ?>
                                </div>
                            </div>
                      </div>
                        <div class="col-md-4">
                            <address><strong>Data SPK</strong></address>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">No SK <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="no_spk" id="no_spk" value="<?= $no_spk ?>" class="form-control form-control-sm">
                                    <?= form_error('no_spk'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Tanggal <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="tanggal_spk" id="tanggal_spk" value="<?= $tanggal_spk ?>" class="form-control form-control-sm datepicker">
                                    <?= form_error('tanggal_spk'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Mulai Pekerjaan <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="mulai_pekerjaan_spk" id="mulai_pekerjaan_spk" value="<?= $mulai_pekerjaan_spk ?>" class="datepicker form-control form-control-sm">
                                    <?= form_error('mulai_pekerjaan_spk'); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="nilai_spk" id="nilai_spk" onkeyup="formatangka(this)" value="<?= $nilai_spk ?>" class="form-control form-control-sm">
                                    <?= form_error('nilai_spk'); ?>
                                </div>
                            </div>
                            <address><strong>Data Addedum</strong></address>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">No SK</label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="no_addedum" id="no_addedum" value="<?= $no_addedum ?>" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Tanggal</label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="tanggal_addedum" id="tanggal_addedum" value="<?= $tanggal_addedum ?>" class="form-control form-control-sm datepicker">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Mulai Pekerjaan</label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="mulai_pekerjaan_addedum" id="mulai_pekerjaan_addedum" value="<?= $mulai_pekerjaan_addedum ?>" class="datepicker form-control form-control-sm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Nilai</label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="nilai_addedum" id="nilai_addedum"  onkeyup="formatangka(this)" value="<?= $nilai_addedum ?>" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="row">
                      <div class="col-md-4">
                          <address><strong>Data Pelaksana</strong></address>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Pelaksana <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="pelaksana" id="pelaksana" value="<?= $pelaksana ?>" class="form-control form-control-sm" >
                                    <?= form_error('pelaksana')?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">No SPMK <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="no_spmk" id="no_spmk" value="<?= $no_spk ?>" class="form-control form-control-sm" >
                                    <?= form_error('no_spmk')?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-lg-4 col-form-label text-right">Tanggal SPMK <sup>*</sup></label>
                                <div class="col-6 col-lg-8">
                                    <input type="text" name="tanggal_spmk" id="tanggal_spmk" value="<?= $tanggal_spmk ?>" class="datepicker form-control form-control-sm" >
                                    <?= form_error('tanggal_spmk')?>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?= $id ?>">
                                <div class="col-12">
                                    <p class="text-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i> Simpan</button>
                                <?= anchor('kegiatan','<i class="mdi mdi-close"></i> Batal','class="btn btn-sm btn-warning"')?>
                            </p>
                                </div>
                            </div>
                      </div>
                  </div>
              </form>
            </div>
        </div><!-- end card-->
    </div>
</div>