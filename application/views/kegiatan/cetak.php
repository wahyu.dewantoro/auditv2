<!DOCTYPE html>
<html>

<head>
    <title>KEGIATAN BELANJA</title>
    <style type="text/css">
        body {
            font-family: "Serif";
            font-size: 13;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .center {
            text-align: justify;
            letter-spacing: 0;
        }

        .left {
            margin-left: 500px
        }


        .table {
            border-collapse: collapse;
        }

        .table tbody tr {
            border: none;
        }

        .table thead th {
            border: none;

        }

        .table tfoot td {
            border: none;
        }
    </style>
</head>

<body>
    <h3>Detail Kegiatan</h3>

    <table width="100%" border='0'>
        <tr>
            <th align="left">OPD</th>
            <td style="font-size:9px">></td>
            <td style="width: 20%; white-space: nowrap;" ><?= $kode_skpd ?> <?= $nama_skpd ?></td>
            <td style="width: 17%; white-space: nowrap;" class="item"><strong>Pagu Anggaran</strong></td>
            <td style="font-size:9px">></td>
            <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
            <td class="item"><strong>No Kontrak/No SK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $no_spk ?></td>
        </tr>
        <tr>
            <td class="item"><strong>Jenis Belanja</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $kode_belanja ?> <?= $jenis_belanja ?></td>
            <td class="item"><strong>Harga Perkiraan Sendiri</strong></td>
            <td style="font-size:9px">></td>
            <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
            <td class="item"><strong>Tanggal SPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= date_indo($tanggal_spk) ?></td>
        </tr>
        <tr>

            <td class="item"><strong>Kegiatan / Sub Kegiatan</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $nama_kegiatan ?> / <?= $sub_kegiatan ?></td>
            <td class="item"><strong>pelaksana</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $pelaksana ?></td>
            <td class="item"><strong>Mulai Pekerjaan SPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= date_indo($mulai_pekerjaan_spk) ?></td>
        </tr>
        <tr>

            <td class="item"><strong>Nama PPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $nama_ppk ?></td>
            <td class="item"><strong>No SP</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $no_spmk ?></td>
            <td class="item"><strong>Nilai SPK</strong></td>
            <td style="font-size:9px">></td>
            <td>Rp.<?= angka($nilai_spk) ?></td>
        </tr>
    </table>

</body>

</html>