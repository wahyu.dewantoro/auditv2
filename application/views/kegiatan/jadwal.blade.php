@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Target Progres Kegiatan
    <div class="float-right">
        {!! anchor('kegiatan', "<i class='mdi mdi-flip-to-back'></i> Kembali", 'class="btn btn-primary"'); !!}
    </div>
</h2>
@endsection
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Form
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form action="{{ base_url().'kegiatan/updatejadwal' }}" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Pekerjaan</label>
                                <input type="text" class="form-control" readonly value="{{ $pekerjaan->nama_kegiatan }}">
                            </div>
                            <div class="form-group">
                                <label>Mulai</label>
                                <input type="text" class="form-control" readonly value="{{ date_indo($mulai) }}">
                            </div>
                            <div class="form-group">
                                <label>Selesai</label>
                                <input type="text" class="form-control" readonly value="{{ date_indo($selesai) }}">
                                <input type="hidden" name="pekerjaan_sipil_id" value="{{ $pekerjaan->id }}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                @foreach($lm as $lm)
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Progres Minggu Ke {{ $lm->minggu }} <br> {{ gabungTanggal($lm->awal_minggu,$lm->akhir_minggu) }}</label>
                                        <input type="number" class="form-control" required name="minggu[]" value="{{ $lm->persentase }}" placeholder="dalam persen (%)">
                                        <input type="hidden" name="idm[]" value="{{ $lm->id}}">
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="float-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i> Simpan</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection