<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/logo-fav.png">
    <title>E- Audit</title>

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/app.css" type="text/css" />    

</head>

<body>
  <div class="loading">
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
  </div>
  <div class="be-wrapper  be-fixed-sidebar">
    <?php
    include APPPATH . 'views\layouts\header.blade.php';
    include APPPATH . 'views\layouts\sidebar.blade.php';
    ?>
    <div class="be-content">
      <div class="page-head">
        <h2 class="page-head-title page-title"><?= $title ?>
        
        </h2>
      </div>
      <div class="main-content container-fluid">
        
        <?= $contents ?>
      </div>
    </div>
  </div>

  <script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-flot/plugins/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/countup/countUp.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
     
    @yield('scriptsrc')
    <script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(document).ready(function(){
      	//-initialize the javascript
      	App.init();
      	App.dashboard();
      
      });
    </script>
</body>

</html>