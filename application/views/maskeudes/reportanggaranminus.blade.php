@extends('metronic.master')
@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Report Anggaran Minus
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="table-responsive">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Desa</th>
                                <th class="text-center">Uraian</th>
                                <th class="text-center">Anggaran</th>
                                <th class="text-center">Relaisasi</th>
                                <th class="text-center">Surplus / Defisit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($res as $res) { ?>
                                <tr>
									<td><?= $no++; ?></td>
									<td>
										<span style="width: 200px;">
											<div class="kt-user-card-v2">
												<div class="kt-user-card-v2__details"> <span
														class="kt-user-card-v2__name"><?= $res->kd_desa ; ?></span> <span
														class="kt-user-card-v2__desc"><?= $res->Nama_Desa?></span>
												</div>
											</div>
										</span>
									</td>
									<td>
										<span style="width: 200px;">
											<div class="kt-user-card-v2">
												<div class="kt-user-card-v2__details"> <span
														class="kt-user-card-v2__name"><?= $res->akun_3 ; ?></span> <span
														class="kt-user-card-v2__desc"><?= $res->Nama_Jenis?></span>
												</div>
											</div>
										</span>
									</td>
                                    <td align="right"><?= number_format($res->anggaran,0,'','.') ?></td>
                                    <td align="right"><?= number_format($res->realisasi,0,'','.') ?></td>
                                    <td align="right"><?= number_format($res->anggaran-$res->realisasi,0,'','.') ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
				</div>
			</div>
		 </div>
	</div>
</div>
@endsection
@section('css')
<link href="<?=base_url()?>metro/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        //-initialize the javascript
        // App.init();
        App.dataTables();
    });
</script>
@endsection
