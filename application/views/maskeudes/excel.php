<?php
 $nama_file = date('dmYHis');
 header("Content-Type: application/vnd.ms-excel");
 header("Expires: 0");
 header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 header("Content-disposition: attachment; filename=Master_Keuangan".str_replace(" ","_",$desa)."_".str_replace(" ","_",str_replace('KECAMATAN ','',$kec)).".xls");
?>

<table cellspacing="0" cellpadding="0" width="100%" style="font-size:12px" border="1">
    <tr>
        <td align="center" colspan="9"><b>INSPEKTORAT KABUPATEN JOMBANG <br>Jln. Gatot Subroto No. 169 Jombang </b></td>
    </tr>
</table>
<br>
<table cellspacing="0" cellpadding="0" width="100%" style="font-size:12px">
    <tr>
        <td rowspan="9"  style=" border-bottom:1pt solid black; "></td>
        <td rowspan="9"  style=" border-bottom:1pt solid black; "></td>
        <td colspan="3">Nama Auditan : <?= $desa ?></td>
        <td  rowspan="7"></td>
        <td colspan="2">KKA No :</td>
        <td>:</td>
        <td width="20%"></td>
    </tr>

    <tr>
        <td colspan="3">Kecamatan : <?= str_replace('KECAMATAN ','',$kec) ?></td>
        <td colspan="2">Dilaksanakan oleh:</td>
        <td>:</td>
        <td width="20%"></td>
    </tr>
    <tr>
        <td colspan="3">Keg. Yg. Diaudit : Pengelolaan Keuangan</td>
        <td colspan="2">Tgl/ Paraf</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">Periode yg. Diaudit : 2019</td>
        <td colspan="2">Direviu Ketua Tim</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">Tanggal Cetak: <?= date_indo(date('Y-m-d'))." ".date('H:i:s')?></td>
        <td colspan="2">Tgl/ Paraf</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"  align="center"></td>
        <td colspan="2" >Direviu Pengendali Teknis</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td colspan="2">Tgl/ Paraf</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td height="20px"></td>
    </tr>
    <tr>
        <td colspan="9" style=" border-bottom:1pt solid black; " align="center">Kertas Kerja Audit			</td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<br>
<table cellspacing="0" cellpadding="0" style="font-size:14px" width="100%" >
    <tr>
        <td width="5px">I</td>
        <td colspan="8"  style="width:98%">Register SPP dan Kuitansi </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Register SPP</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  width="10%" align="right"> <?= number_format($spp->Jumlah,2,",",".")?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Register Kuitansi</td>
        <td  width="10px">:</td>
        <td  style=" border-bottom:1pt solid black;" width="10px">Rp</td>
        <td  style=" border-bottom:1pt solid black;" width="10%" align="right">
            <?= number_format($kuitansi->Nilai,2,",",".")?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >Selisih</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"><?= number_format($spp->Jumlah-$kuitansi->Nilai,2,",",".")?> </td>
    </tr>
    <tr>
        <td  width="10px">1)</td>
        <td  colspan="6">Anggaran, Realisasi dan Sisa Anggaran Tahun 2019.</td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">a)</td>
        <td  width="10px" colspan="5">Pendapatan (Anggaran Pendapatan di APBDes)</td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Anggaran Pendapatan Tahun 2019</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($pendapatan_anggaran,2,",",".")?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >(Anggaran Pendapatan di APBDesa) </td>
        <td  width="10px"></td>
        <td  width="10px"></td>
        <td ></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Real. Pendapatan s/d Pemeriksaan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format($pendapatan_realisasi,2,',','.')?></u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >(Berdasarkan LRA) </td>
        <td  width="10px"></td>
        <td  width="10px"></td>
        <td ></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Kurang Realisasi</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($pendapatan_anggaran   - $pendapatan_realisasi,2,',','.')?> </td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">b)</td>
        <td  width="10px" colspan="5">Belanja (Anggaran Belanja di APBDesa). </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Anggaran Belanja Tahun 2019</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($belanja_anggaran,2,",",".")?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >(Anggaran Belanja di APBDesa) </td>
        <td  width="10px"></td>
        <td  width="10px"></td>
        <td ></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Real. Belanja s/d Pemeriksaan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u>  <?= number_format($belanja_realisasi,2,",",".")?> </u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >(Berdasarkan LRA) </td>
        <td  width="10px"></td>
        <td  width="10px"></td>
        <td ></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Kurang Belanja</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right">  <?= number_format( $belanja_anggaran -  $belanja_realisasi,2,",",".")?>  </td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">c)</td>
        <td  width="10px" colspan="5">Surplus/Defisit Pendapatan dan Belanja sampai dengan pemeriksaan </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Realisasi Pendapatan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($pendapatan_realisasi,2,',','.')?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Realisasi Belanja</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format($belanja_realisasi,2,',','.')?> </u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Surplus</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?php  $surplus=$pendapatan_realisasi - $belanja_realisasi;  echo  number_format($surplus,2,',','.')?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td >(Berdasarkan LRA) </td>
        <td  width="10px"></td>
        <td  width="10px"></td>
        <td ></td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">d)</td>
        <td  width="10px" colspan="5">Pembiayaan </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >SILPA Tahun Lalu</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right">  <?= number_format($silpa_tahun_lalu ,2,',','.')?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Penerimaan Pembiayaan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right" style=" border-bottom:1pt solid black;">  <?= number_format($pembiayaan_penerimaan ,2,',','.')?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px"></td>
        <td ></td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right">  <?= number_format($silpa_tahun_lalu - $pembiayaan_penerimaan ,2,',','.')?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Pengeluaran Pembiayaan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right" style=" border-bottom:1pt solid black;">  <?= number_format($pembiayaan_pengeluaran ,2,',','.')?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Pembiayaan Netto</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?php $netto=($silpa_tahun_lalu - $pembiayaan_penerimaan) - $pembiayaan_pengeluaran;  if($netto<0){ echo  '('.number_format(abs($netto) ,2,',','.').')'; }else{ echo number_format(abs($netto) ,2,',','.'); }  ?></td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">e)</td>
        <td  width="10px" colspan="5">SILPA Tahun Ini </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Surplus</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($surplus,2,',','.'); ?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Pembiayaan Netto</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format($netto,2,',','.'); ?></u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >SILPA</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($netto+$surplus,2,',','.'); ?></td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td >2)</td>
        <td  colspan="6">Penutupan Buku Kas Umum  per tanggal pemeriksaan ( <?= date('d / m / Y ')?> )</td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">a)</td>
        <td  width="10px" colspan="5">Buku pembantu kas </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Penerimaan *)</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($kas_tunai_penerimaan,2,',','.') ?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Pengeluaran *)</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format($kas_tunai_pengeluaran,2,',','.') ?> </u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Saldo Kas Tunai</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?php $saldo_tunai=$kas_tunai_penerimaan - $kas_tunai_pengeluaran; echo number_format( $saldo_tunai,2,',','.') ?> </td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">b)</td>
        <td  width="10px" colspan="5">Buku Pembantu Bank </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Penerimaan *)</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format($kas_bank_penerimaan,2,',','.') ?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Pengeluaran *)</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format($kas_bank_pengeluaran,2,',','.') ?></u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Saldo Kas Di Bank</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?php $saldo_bank=$kas_bank_penerimaan - $kas_bank_pengeluaran; echo number_format( $saldo_bank,2,',','.') ?> </td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px" colspan="5">*) Termasuk transaksi pungutan/setoran pajak</td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px" colspan="5">**) Termasuk Bunga Bank </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px" colspan="5">***) Termasuk Pajak dan Admin Bank </td>
    </tr>
    <tr>
        <td  height='20px'></td>
    </tr>
    <tr>
        <td ></td>
        <td  width="10px">c)</td>
        <td  width="10px" colspan="5">Buku Kas Umum </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Jumlah Penerimaan</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format( $kas_tunai_penerimaan + $kas_bank_penerimaan ,2,',','.') ?></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Jumlah Pengeluaran</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <u> <?= number_format( $kas_tunai_pengeluaran + $kas_bank_pengeluaran ,2,',','.') ?> </u></td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Saldo Kas</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format( ($kas_tunai_penerimaan + $kas_bank_penerimaan) - ($kas_tunai_pengeluaran + $kas_bank_pengeluaran) ,2,',','.') ?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Saldo Kas Tunai</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format( $saldo_tunai,2,',','.') ?> </td>
    </tr>
    <tr>
        <td  colspan="2"></td>
        <td  width="10px">-</td>
        <td >Saldo Kas Di Bank</td>
        <td  width="10px">:</td>
        <td  width="10px">Rp</td>
        <td  align="right"> <?= number_format( $saldo_bank,2,',','.') ?> </td>
    </tr>

    <tr>
        <td height='20px'></td>
    </tr>
    <tr>
        <td>3)</td>
        <td colspan="6">Surat Pertanggung Jawaban ( SPJ ) (sesuai LRA) : </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Jumlah pengeluaran (LRA) </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <?= number_format($belanja_realisasi,2,",",".")?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Telah dipertanggungjawabkan / SPJ (Kuitansi)
        </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <u> <?= number_format($kuitansi->Nilai,2,",",".")?> </u></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Sisa yang belum di SPJ kan </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <?= number_format(abs($belanja_realisasi- $kuitansi->Nilai),2,",",".")?> </td>
    </tr>    <tr>
        <td height='20px'></td>
    </tr>
    <tr>
        <td >4)</td>
        <td colspan="6">Penerimaan PPN / PPh Tahun 2019					 </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Penerimaan	 </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <?= number_format(abs($pphterima),2,",",".")?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Penyetoran       </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <u>  <?= number_format($pphsetor,2,",",".")?> </u></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Sisa Belum Disetor	 </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right">  <?= number_format(abs($pphterima-$pphsetor),2,",",".")?> </td>
    </tr>    <tr>
        <td height='20px'></td>
    </tr>
    <tr>
        <td >5)</td>
        <td colspan="6">Penerimaan Pajak Daerah (Pajak Restoran) Tahun 2019</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Penerimaan	 </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <?= number_format(abs($ppdterima),2,",",".")?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Penyetoran       </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right"> <u>  <?= number_format($ppdsetor,2,",",".")?> </u></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td width="10px">-</td>
        <td>Sisa Belum Disetor	 </td>
        <td width="10px">:</td>
        <td width="10px">Rp</td>
        <td align="right">  <?= number_format(abs($ppdterima-$ppdsetor),2,",",".")?> </td>
    </tr>
</table>