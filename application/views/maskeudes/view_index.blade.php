
@extends('metronic.master')
@section('content')
<div class="kt-portlet">
    <div class="kt-portlet__body">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">OPD</label>
					<div class=" col-lg-9 col-md-9 col-sm-12">
						<select class="form-control kt-select2-general" id="kec" name="param">
							<option value=""></option>
							<?php foreach ($kec as $rk) { ?>
								<option value="<?= $rk->Kd_Kec ?>"><?= str_replace('KECAMATAN ','',$rk->Nama_Kecamatan) ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group row">
					<label class="col-form-label col-lg-3 col-sm-12">Desa</label>
					<div class=" col-lg-9 col-md-9 col-sm-12">
						<select class="form-control kt-select2-general" id="desa" name="param">
							<option value=""></option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group row">
					<div class="col-12">
						<a id="excel" href="#" target="_blank" class="btn btn-success"><i class="mdi mdi-print"></i> Excel</a>
					</div>
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group row">
					<div class="col-12">
						<a id="pdf" href="#" target="_blank" class="btn btn-danger"><i class="mdi mdi-print"></i> PDF</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="hasil"></div>
			</div>
		</div>
    </div>
</div>
<div id="hasil"></div>
@endsection
@section('css')
@endsection
@section('script')
<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script>
    $('#excel').hide();
    $('#pdf').hide();
    $('#kec').on('change', function() {
        $('.loading').show();
        var _url = "<?php echo base_url() ?>" + "maskeudes/getdesa";
        $.ajax({
            url: _url,
            data: {
                'kd_kec': $(this).val()
            },
            dataType: 'json',
            type: 'get',
            success: function(data) {
                $('.loading').hide();
                $("#desa").html("<option value=''></option>");
                $.each(data, function(key, item) {
                    $('#desa')
                        .append($("<option></option>")
                            .attr("value", item.id)
                            .text(item.text));
                });
            },
            error: function(res) {
                $('.loading').hide();
                alert('sistem error');
            }
        });
    });
    // $("#excel").attr("href","<?php echo base_url() ?>" + "maskeudes/excel?kd_desa=");
    $('#desa').on('change', function() {
        var _url = "<?php echo base_url() ?>" + "maskeudes/cetak";
        $("#excel").attr("href","<?php echo base_url() ?>" + "maskeudes/excel?kd_desa="+$(this).val());
        $("#pdf").attr("href","<?php echo base_url() ?>" + "maskeudes/pdf?kd_desa="+$(this).val());
        $('.loading').show();
        // if ($(this).val() != null) {
        // $('.loading').hide();
        $.ajax({
            url: _url,
            data: {
                'kd_desa': $(this).val()
            },
            type: 'post',
            success: function(data) {
                $('.loading').hide();
                $('#hasil').html("<hr>"+data);
                $('#excel').show();
                $('#pdf').show();
            },
            error: function(res) {
                $('.loading').hide();
                alert('sistem error');
                $('#hasil').html("");
            }
        });
    });
</script>
@endsection
