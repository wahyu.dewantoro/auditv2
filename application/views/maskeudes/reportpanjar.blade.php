@extends('metronic.master')
@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Laporan Panjar
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="table-responsive">
			<div class="row">
				<div class="col-md-12">
					<table class="table  table-hover table-bordered" id="table1">
						<thead>
							<tr>
								<th>No </th>
								<th>Desa</th>
								<th>Keterangan</th>
								<th>Pagu</th>
								<th>NO SPP</th>
								<th>Nilai Panjar</th>
								<th>NO SPJ</th>
								<th>Nilai SPJ</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
							foreach ($res as $res) {
							  $a=  date_create($res->Tgl_SPP);$b= date_create($res->tgl_spj);$diff  = date_diff($a, $b); ?>
							<tr <?= $diff->d>11?'style="background-color: #ffb8b8"':"" ?>>
								<td align="center"><?= $no++?> </td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $res->nama_desa ; ?></span> <span
													class="kt-user-card-v2__desc"><?= $res->Nama_Kecamatan?></span>
											</div>
										</div>
									</span>
								</td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $res->Keterangan ; ?></span> <span
													class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($res->Kd_Keg))) ?></span>
											</div>
										</div>
									</span>
								</td>
								<td align="right"><?= number_format($res->pagu, 0, '', '.')  ?></td>
								<td nowrap>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $res->No_SPP ; ?></span> <span
													class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d',strtotime($res->Tgl_SPP))) ?></span>
											</div>
										</div>
									</span>
								</td>
								<td align="right"><?= number_format($res->panjar, 0, '', '.')  ?></td>
								<td nowrap>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $res->no_spj ; ?></span> <span
													class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d',strtotime($res->tgl_spj))) ?></span>
											</div>
										</div>
									</span>
								</td>
								<td align="right"><?= number_format($res->spj, 0, '', '.')  ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<p><b>* warna merah menunjukan tgl spj > 10 hari dari tgl spp *</b></p>
				</div>
			</div>
		 </div>
	</div>
</div>
@endsection
@section('css')
<link href="<?=base_url()?>metro/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        //-initialize the javascript
        // App.init();
        App.dataTables();
    });
</script>
@endsection
