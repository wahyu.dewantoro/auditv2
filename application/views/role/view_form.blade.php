@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
                <form action="<?php echo $action; ?>" method="post">
					<div class="form-group">
						<label for="varchar">Nama Role <?php echo form_error('nama_role') ?></label>
						<input type="text" class="form-control form-control-xs" name="nama_role" id="nama_role" placeholder="Nama Role" value="<?php echo $nama_role; ?>" />
					</div>
					<input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
					<button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button>
					<a class="btn btn-sm btn-warning" href="<?php echo site_url('role') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
				</form>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection