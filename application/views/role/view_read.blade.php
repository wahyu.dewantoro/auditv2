
@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
				<form method="post" action="<?php echo base_url().'role/prosessettingrole'?>">
					<button class="btn float-right btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> Submit</button>
					<input type="hidden" name="kode_group"  value="<?php echo $id_inc;?>">


					<table  class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th colspan="4">Akses</th>
								<th rowspan="2">menu</th>
								<th rowspan="2">Parent</th>
							</tr>
							<tr>
								<td width="3%">Read</td>
								<td width="3%">Create</td>
								<td width="3%">Update</td>
								<td width="3%">Delete</td>
							</tr>
						</thead>
						<tbody>
								<?php
								foreach($role as $row){?>
							<tr>
								<td align="center"> <input <?php if($row['STATUS']==1){ echo "checked";}?> type="checkbox" name="role[]" value="<?php echo $row['kode_role']; ?>"></td>
								<td align="center"> <input <?php if($row['created']==1){ echo "checked";}?> type="checkbox" name="create[<?php echo $row['kode_role']; ?>]" value="1"></td>
								<td align="center"> <input <?php if($row['updated']==1){ echo "checked";}?> type="checkbox" name="update[<?php echo $row['kode_role']; ?>]" value="1"></td>
								<td align="center"> <input <?php if($row['deleted']==1){ echo "checked";}?> type="checkbox" name="delete[<?php echo $row['kode_role']; ?>]" value="1"></td>
								<td><?php echo ucwords($row['nama_menu']);?></td>
								<td><?php echo ucwords($row['parent']);?></td>
							</tr>
						<?php }?>
						</tbody>
					</table>
			</form>

			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection