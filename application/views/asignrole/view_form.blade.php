@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
                <form action="<?php echo $action; ?>" method="post">
					<div class="form-group">
						<label for="int">Pengguna <?php echo form_error('ms_pengguna_id') ?></label>
						<!-- <input type="text"  placeholder="Ms Pengguna Id" value="<?php echo $ms_pengguna_id; ?>" /> -->
						<select class="form-control form-control-xs kt-select2-general" name="ms_pengguna_id" id="ms_pengguna_id" >
							<option value="">Pilh user</option>
							<?php foreach($pengguna as $pengguna){?>
							<option <?php if($pengguna->id_inc==$ms_pengguna_id){echo "selected"; }?> value="<?= $pengguna->id_inc ?>"><?= $pengguna->nama ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label for="int">Role <?php echo form_error('ms_role_id') ?></label>
						<!-- <input type="text"  placeholder="Ms Role Id" value="<?php echo $ms_role_id; ?>" /> -->
						<select class="form-control form-control-xs kt-select2-general" name="ms_role_id" id="ms_role_id" >
							<option value="">Pilih role</option>
							<?php foreach($role as $role){ ?>
							<option <?php if($ms_role_id==$role->id_inc){echo "selected";}?> value="<?= $role->id_inc ?>"><?= $role->nama_role ?></option>
							<?php } ?>
						</select>
					</div>
					<input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
					<button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button>
					<a class="btn btn-sm btn-warning" href="<?php echo site_url('asignrole') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
				</form>

			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection

@section('script')

<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
@endsection
