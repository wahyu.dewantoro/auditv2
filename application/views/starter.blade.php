
@extends('metronic.master')
@section('judul')
@endsection
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">

				<div class="row">
					<div class=" col-lg-3 col-xl-3">
						<div class="card card-table card-border card-contrast">
							<div style="background-color:#3bbf5e; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/incomedua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Tertinggi</b>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>OPD</th>
												<th>%</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($pendapatan as $pen){?>
												<tr>
													<td><?= $pen->nm_unit?></td>
													<td align="center"><?= number_format($pen->persen_pendapatan,2,',','.') ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>

									<?= anchor('welcome/realisasi?jenis='.urlencode('pendapatan').'&type='.urlencode('tinggi'),"Lihat semua >>")?>

								</div>
							</div>
						</div>
					</div>
					<div class=" col-lg-3 col-xl-3">
						<div class="card card-table card-border card-contrast">
							<div style="background-color:#ed7065; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanja.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Tertinggi</b>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>OPD</th>
												<th>%</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($belanja as $bel){?>
												<tr>
													<td><?= $bel->nm_unit?></td>
													<td align="center"><?= number_format($bel->persen_belanja,2,',','.') ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
									<?= anchor('welcome/realisasi?jenis='.urlencode('belanja').'&type='.urlencode('tinggi'),"Lihat semua >>")?>
								</div>
							</div>
						</div>
					</div>
					<div class=" col-lg-3 col-xl-3">
						<div class="card card-table card-border card-contrast">
							<div style="background-color:#6da2f6; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanjadua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Terendah</b>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>OPD</th>
												<th>%</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($pendapatandua as $pena){?>
												<tr>
													<td><?= $pena->nm_unit?></td>
													<td align="center"><?= number_format($pena->persen_pendapatan,2,',','.') ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
									<?= anchor('welcome/realisasi?jenis='.urlencode('pendapatan').'&type='.urlencode('rendah'),"Lihat semua >>")?>
								</div>
							</div>
						</div>
					</div>
					<div class=" col-lg-3 col-xl-3">
						<div class="card card-table card-border card-contrast">
							<div style="background-color:#f7c771; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/income.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Terendah</b>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>OPD</th>
												<th>%</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($belanjadua as $bela){?>
												<tr>
													<td><?= $bela->nm_unit?></td>
													<td align="center"><?= number_format($bela->persen_belanja,2,',','.') ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
									<?= anchor('welcome/realisasi?jenis='.urlencode('belanja').'&type='.urlencode('rendah'),"Lihat semua >>")?>
								</div>
							</div>
						</div>
					</div>
			</div>
			<div>
				 <div class="col-12 col-lg-12 col-xl-12">
						<div class="card card-table card-border-color card-border-color-dark">
							<div class="card-header card-header-divider">
								<span class="title">Timeline Realisasi (Dalam Milyar)</span>
							</div>
							<div class="card-body">
								<div class="row">
										<div class="col-12 col-lg-12 col-xl-12">
										<form action="<?= base_url().'welcome' ?>">
											<div class="row">
													<div class="col-2 col-lg-2 col-xl-2">
														<b><p  class="text-right">OPD/SKPD</p></b>
													</div>
													<div class="col-6 col-lg-6 col-xl-6">
														<select class="form-control  kt-select2-general" name="nm_unit" >
																<option value="">Semua OPD</option>
																<?php foreach($unit as $ru){?>
																	<option <?php if($ru->nm_unit==$q){echo "selected";}?> value="<?= $ru->nm_unit?>"><?= $ru->kd_skpd.' - '.$ru->nm_unit?></option>
																<?php } ?>
														</select>
													</div>
													<div class="col-2 col-lg-2 col-xl-2">
														<button class="btn btn-success">Tampilkan</button>
													</div>
											</div>
										</form>
									</div>
								</div>
								<div id="bismillah" style="height: 250px;"></div>
							</div>
						</div>
					</div>
			</div>

			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/raphael/raphael.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Morris.Line({
			element: 'bismillah',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($realisasi as $ra) {
					if ($ra->pendapatan != 0) {
						$a += $ra->pendapatan;
					} else {
						$a = 0;
					}

					if ($ra->belanja != 0) {
						$b += $ra->belanja;
					} else {
						$b = 0;
					}

					?> {
						y: '<?= $ra->nama_bulan ?>',
						a: <?= $a ?>,
						b: <?= $b ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a', 'b'],
			labels: ['Pendapatan', 'Belanja'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
	});
</script>
<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>

@endsection
