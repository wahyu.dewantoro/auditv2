

@extends('metronic.master')
@section('toolbar')
{!! anchor('welcome', '<i class="flaticon2-left-arrow-1"></i> Kembali', ' class="btn btn-primary""'); !!}
@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">

	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card mb-3">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th width="10px">No</th>
										<th>OPD</th>
										<th>%</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; foreach($data as $rk){?>
										<tr>
											<td align="center"><?= $no ?></td>

											<td class="cell-detail"><?= $rk->nm_unit?> <span class="cell-detail-description"><?= $rk->kd_skpd?></span></td>
											<td align="center"><?= number_format($rk->hasil,2,',','.') ?></td>
										</tr>
									<?php $no++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- end card-->
			</div>
		</div>
	</div>
</div>
@endsection
@section('css')

@endsection
@section('script')

@endsection
