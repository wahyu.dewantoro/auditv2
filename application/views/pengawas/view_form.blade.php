@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
				<div class="row">
					<div class="col-lg-4 col-xl-4">
						<div class="card card-border-color card-border-color-primary">
							<div class="card-body">
								<form action="<?php echo $action; ?>" method="post">
									<div class="form-group">
										<label for="varchar">OPD <?php echo form_error('kd_skpd') ?></label>
										<select name="kd_skpd" class="form-control form-control-sm kt-select2-general required" id="kd_skpd" data-placeholder="Pilih Instansi">
											<option value=""></option>
											<?php foreach ($skpd as $rk) { ?>
												<option <?php if ($rk->kd_skpd == $kd_skpd) {
																echo "selected";
															} ?> value="<?= $rk->kd_skpd . "/" . $rk->nm_sub_unit ?>">
													<?= $rk->nm_sub_unit ?>
												</option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group">
										<label for="varchar">Nama <?php echo form_error('nama_pengawas') ?></label>
										<input type="text" class="form-control form-control-xs" name="nama_pengawas" id="nama_pengawas" placeholder="Nama pengawas" value="<?php echo $nama_pengawas; ?>" />
									</div>
									<div class="form-group">
										<label for="varchar">Nama CV<?php echo form_error('nama_cv') ?></label>
										<input type="text" class="form-control form-control-xs" name="nama_cv" id="nama_cv" placeholder="Nama CV" value="<?php echo $nama_cv; ?>" />
									</div>
									<div class="form-group">
										<label for="varchar">Email <?php echo form_error('email_pengawas') ?></label>
										<input type="text" class="form-control form-control-xs" name="email_pengawas" id="email_pengawas" placeholder="Email" value="<?php echo $email_pengawas; ?>" />
									</div>


									<input type="hidden" name="id" value="<?php echo $id; ?>" />
									<button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i>
										<?php echo $button ?></button>
									<a class="btn btn-sm btn-warning" href="<?php echo site_url('pengawas') ?>"><i class="mdi mdi-close"></i>
										Cancel</a>
								</form>
							</div>
						</div><!-- end card-->
					</div>
				</div>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection
@section('script')

<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
@endsection
