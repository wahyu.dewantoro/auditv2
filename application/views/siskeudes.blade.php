@extends('metronic.master')
@section('judul')
@endsection
@section('content')
<div class="row">

	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#6da2f6; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanjadua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Terendah</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Nilai</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($pendapatandua as $pena){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $pena->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($pena->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($pena->nilai,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<?= anchor('welcome/siskeudes?jenis='.urlencode('pendapatan').'&type='.urlencode('rendah'),"Lihat semua >>")?>
				</div>
			</div>
		</div>
	</div>
	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#3bbf5e; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/incomedua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Tertinggi</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Nilai</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($pendapatan as $pen){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $pen->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($pen->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($pen->nilai,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

					<?= anchor('welcome/siskeudes?jenis='.urlencode('pendapatan').'&type='.urlencode('tinggi'),"Lihat semua >>")?>

				</div>
			</div>
		</div>
	</div>

	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#f7c771; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/income.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Terendah</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Nilai</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($belanjadua as $bela){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $bela->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($bela->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($bela->nilai,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<?= anchor('welcome/siskeudes?jenis='.urlencode('belanja').'&type='.urlencode('rendah'),"Lihat semua >>")?>
				</div>
			</div>
		</div>
	</div>
	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#ed7065; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanja.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Tertinggi</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Nilai</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($belanja as $bel){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $bel->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($bel->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($bel->nilai,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<?= anchor('welcome/siskeudes?jenis='.urlencode('belanja').'&type='.urlencode('tinggi'),"Lihat semua >>")?>
				</div>
			</div>
		</div>
	</div>

	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#6da2f6; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanjadua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Realisasi Terendah</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Pendapatan</th>
								<th>Realisasi</th>
								<th>%</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($persenrealisasidua as $persenb){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $persenb->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($persenb->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($persenb->pendapatan,2,',','.') ?></td>
									<td align="center"><?= number_format($persenb->realisasi,2,',','.') ?></td>
									<td align="center"><?= number_format($persenb->persen,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<?= anchor('welcome/siskeudes?jenis='.urlencode('realisasi').'&type='.urlencode('rendah'),"Lihat semua >>")?>
				</div>
			</div>
		</div>
	</div>
	<div class=" col-lg-6 col-xl-6">
		<div class="card card-border card-contrast">
			<div style="background-color:#f7c771; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/income.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Realisasi Tertinggi</b>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Desa</th>
								<th>Kecamatan</th>
								<th>Pendapatan</th>
								<th>Realisasi</th>
								<th>%</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($persenrealisasi as $persena){?>
								<tr>
									<td class="text-center"><?=$no++?></td>
									<td><?= $persena->Nama_Desa?></td>
									<td><?= str_replace('Kecamatan ','',ucwords(strtolower($persena->Nama_Kecamatan))) ?></td>
									<td align="center"><?= number_format($persena->pendapatan,2,',','.') ?></td>
									<td align="center"><?= number_format($persena->realisasi,2,',','.') ?></td>
									<td align="center"><?= number_format($persena->persen,2,',','.') ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					<?= anchor('welcome/siskeudes?jenis='.urlencode('realisasi').'&type='.urlencode('tinggi'),"Lihat semua >>")?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Pendapatan Tertinggi (Dalam Milyar)</span>
			</div>
			<div class="card-body">
				<div id="grafik1" style="height: 250px;"></div>
			</div>
		</div>
	</div>
	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Pendapatan Terendah (Dalam Milyar)</span>
			</div>
			<div class="card-body">
				<div id="grafik2" style="height: 250px;"></div>
			</div>
		</div>
	</div>

	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Belanja Tertinggi (Dalam Milyar)</span>
			</div>
			<div class="card-body">
				<div id="grafik3" style="height: 250px;"></div>
			</div>
		</div>
	</div>

	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Belanja Terendah (Dalam Milyar)</span>
			</div>
			<div class="card-body">
				<div id="grafik4" style="height: 250px;"></div>
			</div>
		</div>
	</div>

	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Realisasi Tertinggi </span>
			</div>
			<div class="card-body">
				<div id="grafik5" style="height: 250px;"></div>
			</div>
		</div>
	</div>

	<div class="col-12 col-lg-12 col-xl-12">
		<div class="card card-border-color card-border-color-dark">
			<div class="card-header card-header-divider">
				<span class="title">Realisasi Terendah </span>
			</div>
			<div class="card-body">
				<div id="grafik6" style="height: 250px;"></div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/raphael/raphael.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
<script type="text/javascript">
var $arrColors = ['#34495E', '#26B99A',  '#666', '#3498DB'];
	$(document).ready(function() {
		Morris.Bar({
			element: 'grafik1',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik1 as $gf1) {

					?> {
						y: '<?= $gf1->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf1->Nama_Kecamatan) ?>' ,
						a: <?= $gf1->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Pendapatan'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik2',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik2 as $gf2) {

					?> {
						y: '<?= $gf2->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf2->Nama_Kecamatan) ?>' ,
						a: <?= $gf2->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Pendapatan'],
			barColors:['#34495E'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
	});
	Morris.Bar({
			element: 'grafik3',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik3 as $gf3) {

					?> {
						y: '<?= $gf3->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf3->Nama_Kecamatan) ?>' ,
						a: <?= $gf3->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Belanja'],
			barColors:['#3498DB'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik4',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik4 as $gf4) {

					?> {
						y: '<?= $gf4->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf4->Nama_Kecamatan) ?>' ,
						a: <?= $gf4->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Belanja'],
			barColors:['#666'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik5',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik5 as $gf5) {

					?> {
						y: '<?= $gf5->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf5->Nama_Kecamatan) ?>' ,
						a: <?= $gf5->persen ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Realisasi'],
			barColors:['#26B99A'],

		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik6',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik6 as $gf6) {

					?> {
						y: '<?= $gf6->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf6->Nama_Kecamatan) ?>' ,
						a: <?= $gf6->persen ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Realisasi'],
			barColors:['#9b5cd4'],
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
</script>
@endsection
