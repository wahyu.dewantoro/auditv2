@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>

		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
                    <form action="<?php echo site_url('ppk/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                                <div class="btn-group">
                                    <?php if ($q <> '') { ?>
                                        <a href="<?php echo site_url('ppk'); ?>" class="btn btn-warning"><i class="la la-close"></i> Reset</a>
                                    <?php }   ?>
                                    <button class="btn btn-primary" type="submit"><i class="la la-search"></i> Cari</button>
                                    <?php
                                    if ($akses['create'] == 1) {
                                        echo anchor(site_url('ppk/create'), '<i class="la la-plus"></i> Tambah', 'class="btn btn-success  "');
                                    }
                                    ?>
                                </div>
                            </span>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="10px">No</th>
								<th>Nama</th>
								<th>Email</th>

								<th>Instansi</th>
								<th width="20px "> </th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($ppk_data as $rk) { ?>
								<tr>
									<td align="center"><?php echo ++$start ?></td>
									<td><?php echo $rk->nama_ppk ?></td>
									<td><?php echo $rk->email_ppk ?></td>
									<td>
										<span style="width: 200px;">
											<div class="kt-user-card-v2">
												<div class="kt-user-card-v2__details"> <span
														class="kt-user-card-v2__name"><?= $rk->kd_skpd; ?></span> <span
														class="kt-user-card-v2__desc"><?= $rk->nm_sub_unit; ?></span>
												</div>
											</div>
										</span>
									</td>
									<td style="text-align:center">
										<div class="btn-group">
											<?php
												// if ($akses['read'] == 1) {
												//     echo anchor(site_url('ppk/read/' . acak($rk->id)), '<i class="la la-search"></i>', 'class="btn btn-xs btn-primary"');
												// }
												if ($akses['update'] == 1) {
													echo anchor(site_url('ppk/update/' . acak($rk->id)), '<i class="la la-edit"></i>', 'class="btn btn-xs btn-success btn-icon btn-icon-md"');
												}
												if ($akses['delete'] == 1) {
													echo anchor(site_url('ppk/delete/' . acak($rk->id)), '<i class="la la-trash"></i>', 'class="btn btn-xs btn-danger btn-icon btn-icon-md" onclick="javasciprt: return confirm(\'apakah anda yakin untuk menghapus ?\')"');
												}
												?>
										</div>
									</td>
								</tr>
							<?php  }   ?>
						</tbody>
					</table>
					<button class="btn  btn-space btn-secondary" disabled>Total Record :
						<?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
				</div>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection