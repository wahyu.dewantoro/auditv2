<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?= base_url() ?>assets/img/logo-fav.png">
<title>Audit</title>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jqvmap/jqvmap.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/select2/css/select2.min.css" />
<link rel="stylesheet" href="<?= base_url() ?>assets/css/app.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/morrisjs/morris.css" />
<style>
  .pendek {
    width: 5em;
    background-color: #1E90FF;
  }
</style>
  <style type="text/css">
    .loading {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 100;
      /*background-color: */
    }

    .loading-bar {
      display: inline-block;
      width: 16px;
      height: 50px;
      border-radius: 16px;
      animation: loading 1s ease-in-out infinite;
    }

    .loading-bar:nth-child(1) {
      background-color: #3498db;
      animation-delay: 0;
    }

    .loading-bar:nth-child(2) {
      background-color: #c0392b;
      animation-delay: 0.09s;
    }

    .loading-bar:nth-child(3) {
      background-color: #f1c40f;
      animation-delay: .18s;
    }

    .loading-bar:nth-child(4) {
      background-color: #27ae60;
      animation-delay: .27s;
    }

    @keyframes  loading {
      0% {
        transform: scale(1);
      }

      20% {
        transform: scale(1, 2.2);
      }

      40% {
        transform: scale(1);
      }
    }
  </style><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/layouts/head.blade.php ENDPATH**/ ?>