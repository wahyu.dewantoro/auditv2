<meta charset="utf-8" />
<title>E - Audit | <?php if(!empty($title)){ echo $title ;}else{ echo "Inspektorat";} ?></title>
<meta name="description" content="Page with empty content">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--begin::Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
<!--end::Fonts -->

<!--begin::Page Vendors Styles(used by this page) -->
<link href="<?= base_url() ?>metro/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

<!--end::Page Vendors Styles -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<?= base_url() ?>metro/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>metro/css/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->
<!--begin::Layout Skins(used by all pages) -->
<link href="<?= base_url() ?>metro/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>metro/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>metro/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>metro/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

<!--end::Layout Skins -->
<link rel="shortcut icon" href="<?= base_url() ?>metro/media/logos/favicon.ico" /><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/metronic/head.blade.php ENDPATH**/ ?>