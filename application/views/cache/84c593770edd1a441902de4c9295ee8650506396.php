<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title"> Progres Pekerjaan
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-table card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Data Pekerjaan
                <div class="tools float-right">

                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>OPD</th>
                            <th>Kegiatan</th>
                            <th>Sub Kegiatan</th>
                            <th>Anggaran</th>
                            <th>HPS</th>
                            <th>Kontrak</th>
                            <th>Persentase</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $sipil) { ?>
                            <tr>
                                <td width="10px" align="center"><?= ++$start ?></td>
                                <td class="cell-detail"><?= $sipil->kode_skpd; ?>
                                    <span class="cell-detail-description"><?= $sipil->nama_skpd; ?></span></td>
                                <td class="cell-detail"> <?= $sipil->ket_keg; ?>
                                    <span class="cell-detail-description"><?= $sipil->ket_prog ?></span>
                                </td>
                                <td> <?= $sipil->sub_kegiatan; ?></td>
                                <td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
                                <td align="right"><?= angka($sipil->harga_perkiraan); ?></td>
                                <td align="right"><?= angka($sipil->nilai_pengawas); ?></td>
                                <td align="center"> <?=  number_format(($sipil->nilai_pengawas/$sipil->harga_perkiraan) * 100,2,',','.') ?> % </td>
                                <td width="100px" align="center">
                                    <div class="btn-group">
                                        <?php
                                            echo anchor('progressipil/read/' . acak($sipil->id), '<i class="mdi mdi-eye"></i>  ', ' class="btn btn-xs btn-primary"');
                                            if ($akses['create'] == 1 && $role!=3) {
                                                echo anchor('progressipil/progres/' . acak($sipil->id), '<i class="mdi mdi-calendar-check"></i>  ', ' class="btn btn-xs btn-warning"');
                                            }
                                            ?>
                                    </div>


                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
                <div class="float-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/progres_sipil/view_index.blade.php ENDPATH**/ ?>