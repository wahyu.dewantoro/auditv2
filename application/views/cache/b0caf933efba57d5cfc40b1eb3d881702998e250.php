<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title">Perencanaan Progres kegiatan
    <div class="float-right">
        <?php echo anchor($_SERVER['HTTP_REFERER'], "<i class='mdi mdi-flip-to-back'></i> Kembali", 'class="btn btn-primary"');; ?>

    </div>
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Kumulatif Perencanaan
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form action="<?php echo e(base_url().'sipil/updatejadwal'); ?>" method="post">
                    <div class="row">
                        <div class="col-md-4">

                            <table class="no-border no-strip skills">
                                <tbody class="no-border-x no-border-y">
                                    <tr valign="top">
                                        <td class="item">OPD</td>
                                        <td class="icon">:</td>
                                        <td><?= $pekerjaan->kode_skpd ?> <?= $pekerjaan->nama_skpd ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Program</td>
                                        <td class="icon">:</td>
                                        <td><?= $pekerjaan->ket_prog ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Kegiatan</td>
                                        <td class="icon">:</td>
                                        <td><?= $pekerjaan->ket_keg ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Sub Kegiatan</td>
                                        <td class="icon">:</td>
                                        <td><?= $pekerjaan->sub_kegiatan ?></td>
                                    </tr>

                                    <tr valign="top">

                                        <td class="item"> Anggaran</td>
                                        <td class="icon">:</td>
                                        <td><?= 'Rp.' . angka($pekerjaan->pagu_anggaran) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Harga Perkiraan Sendiri</td>
                                        <td class="icon">:</td>
                                        <td><?= 'Rp.' . angka($pekerjaan->harga_perkiraan) ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <?php $__currentLoopData = $lm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><b>Kumulatif Minggu Ke <?php echo e($lm->minggu); ?></b> <br> <?php echo e(gabungTanggal($lm->awal_minggu,$lm->akhir_minggu)); ?> </label>
                                        <input type="text" class="form-control number"  required name="minggu[]" value="<?php echo e($lm->persentase); ?>" placeholder="persentase (%)">
                                        <input type="hidden" name="idm[]" value="<?php echo e($lm->id); ?>">
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="float-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i> Simpan</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div><!-- end card-->
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/sipil/jadwal.blade.php ENDPATH**/ ?>