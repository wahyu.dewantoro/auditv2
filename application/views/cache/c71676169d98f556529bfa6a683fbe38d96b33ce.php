 <div class="kt-subheader   kt-grid__item" id="kt_subheader">
     <div class="kt-container  kt-container--fluid ">
         <div class="kt-subheader__main">

             <span class="kt-subheader__separator kt-hidden"></span>
             <div class="kt-subheader__breadcrumbs">
                 <h3 class="kt-subheader__title">
                     <?= $title ?> </h3>
                     <?php if(isset($sub_1)) { ?>
                     <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                 <span class="kt-subheader__breadcrumbs-separator"></span>
                 <a href="" class="kt-subheader__breadcrumbs-link">
                     <?= $sub_1 ?> </a>
                     <?php } ?>
                 <?php if (isset($sub_2) && isset($sub_1)) { ?>
                     <span class="kt-subheader__breadcrumbs-separator"></span>
                     <a href="" class="kt-subheader__breadcrumbs-link">
                         <?= $sub_2 ?> </a>
                 <?php } ?>

                 <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
             </div>
         </div>

     </div>
 </div><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/metronic/subheader.blade.php ENDPATH**/ ?>