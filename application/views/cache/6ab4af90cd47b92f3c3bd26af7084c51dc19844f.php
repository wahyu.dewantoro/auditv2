<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title"> Pekerjaan Sipil
    <div class="float-right">
        <?php echo anchor('sipil','Kembali','class="btn btn-sm btn-primary"'); ?>

    </div>
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured card-header-divider">
                <!-- Form Monitoring Pekerjaan Sipil -->
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form id="regForm" method="post" action="<?= $action ?>">
                <input type="hidden" name="id" value="<?= $id ?>" >
                    <div class="tab">
                        <div class="row">
                            <div class="col-12">
                                <address><strong>Data pekerjaan</strong></address>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row ilang">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nama PPK <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="nama_ppk" class="form-control form-control-sm select2 required" id="nama_ppk" data-placeholder="Pilih PPK">
                                            <option value=""></option>
                                            <?php foreach ($ppk as $ppk) { ?>
                                                <option <?php if ($ppk->id == $id_ppk) {
                                                                echo "selected";
                                                            } ?> value="<?= $ppk->id . "/" . $ppk->nama_ppk ?>"><?= $ppk->nama_ppk ?></option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('nama_ppk'); ?>
                                    </div>
                                </div>
                                <div class="form-group row ilang">
                                    <label class="col-6 col-lg-4 col-form-label text-right">OPD <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_skpd" class="form-control form-control-sm select2 required" id="kode_skpd" data-placeholder="Pilih OPD">
                                            <option value=""></option>
                                            <?php foreach ($opd as $opd) { ?>
                                                <option <?php if ($opd->kd_skpd == $kode_skpd) {
                                                                echo "selected";
                                                            } ?> value="<?= $opd->kd_skpd ?>"><?= $opd->nm_sub_unit ?></option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('kode_skpd'); ?>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Program <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kd_prog" class="form-control select2 form-control-sm required" id="kd_prog" data-placeholder="Pilih Program">
                                            <option value=""></option>>
                                            <?php if(isset($lp)): ?>
                                                    <?php $__currentLoopData = $lp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($kd_prog==$lp->ID_Prog && $ket_prog == $lp->Ket_Program): ?> selected <?php endif; ?> value="<?php echo e($lp->ID_Prog); ?>"><?php echo e($lp->Ket_Program); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <input type="hidden" id="ket_prog" value="<?php echo e($ket_prog); ?>" name="ket_prog">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Kegiatan  <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="kode_kegiatan" class="form-control select2 form-control-sm required" id="kode_kegiatan" data-placeholder="Pilih Kegiatan">
                                            <option value=""></option>>
                                            <?php if(isset($lk)): ?>
                                            <?php $__currentLoopData = $lk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php if($lk->Ket_Kegiatan==$ket_keg && $kd_keg==$lk->Kd_Keg): ?> selected <?php endif; ?> value="<?php echo e($lk->Kd_Keg); ?>"><?php echo e($lk->Ket_Kegiatan); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <input type="hidden" id="ket_keg" value="<?php echo e($ket_keg); ?>" name="ket_keg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Sub Kegiatan
                                        <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <div class="input-group mb-3">
                                            <input type="text" name="sub_kegiatan" class="form-control form-control-sm required" id="sub_kegiatan" value="<?= $sub_kegiatan ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Pagu Anggaran <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="pagu_anggaran" class="form-control form-control-sm required" onkeyup="formatangka(this)" id="pagu_anggaran" value="<?= $pagu_anggaran ?>">
                                        <?= form_error('pagu_anggaran'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Harga Perkiraan <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="harga_perkiraan" class="form-control form-control-sm required" onkeyup="formatangka(this)" id="harga_perkiraan" value="<?= $harga_perkiraan ?>">
                                        <?= form_error('harga_perkiraan'); ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="tab">
                        <div class="row">
                            <div class="col-12">
                                <address><strong> Penyedia Jasa Konsultansi</strong></address>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nama <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" value="<?= $nama_konsultansi ?>" name="nama_konsultansi" class="form-control form-control-sm required" id="nama_konsultansi" placeholder="Nama">
                                        <?= form_error('nama_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_konsultansi" id="nilai_konsultansi" onkeyup="formatangka(this)" class="required form-control form-control-sm" value="<?= $nilai_konsultansi ?>">
                                        <?= form_error('nilai_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No Kontrak <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_sk_konsultansi" id="no_sk_konsultansi" class="required form-control form-control-sm" value="<?= $no_sk_konsultansi ?>">
                                        <?= form_error('no_sk_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_sk_konsultansi" id="tgl_sk_konsultansi" value="<?= $tgl_sk_konsultansi ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_sk_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Mulai Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_mulai_konsultansi" id="tgl_mulai_konsultansi" value="<?= $tgl_mulai_konsultansi ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_mulai_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Selesai Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_selesai_konsultansi" id="tgl_selesai_konsultansi" value="<?= $tgl_selesai_konsultansi ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_selesai_konsultansi'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai Addedum </label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_addedum_konsultansi" id="nilai_addedum_konsultansi" onkeyup="formatangka(this)" class=" form-control form-control-sm" value="<?= $nilai_addedum_konsultansi ?>">
                                        <?= form_error('nilai_addedum_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No Addedum </label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_addedum_konsultansi" id="no_addedum_konsultansi" class=" form-control form-control-sm" value="<?= $no_addedum_konsultansi ?>">
                                        <?= form_error('no_addedum_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_addedum_konsultansi" id="tgl_addedum_konsultansi" value="<?= $tgl_addedum_konsultansi ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_addedum_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Mulai Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_mulai_addedum_konsultansi" id="tgl_mulai_addedum_konsultansi" value="<?= $tgl_mulai_addedum_konsultansi ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_mulai_addedum_konsultansi'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Selesai Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_selesai_addedum_konsultansi" id="tgl_selesai_addedum_konsultansi" value="<?= $tgl_selesai_addedum_konsultansi ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_selesai_addedum_konsultansi'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab">
                        <address><strong>Penyedia Jasa Konstruksi</strong></address>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nama <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <select name="nama_pengawas" style="width: 100%" class="form-control form-control-sm select2 required js-example-responsive" id="nama_pengawas" data-placeholder="Pilih Pengawas">
                                            <option value=""></option>
                                            <?php foreach ($pengawas as $pengawas) { ?>
                                                <option <?php if ($pengawas->id == $id_pengawas) {
                                                                echo "selected";
                                                            } ?> value="<?= $pengawas->id . "/" . $pengawas->nama_pengawas . "/" . $pengawas->nama_cv?>"><?= $pengawas->nama_pengawas ?> - <?= $pengawas->nama_cv ?></option>
                                            <?php } ?>
                                        </select>
                                        <?= form_error('nama_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_pengawas" id="nilai_pengawas" onkeyup="formatangka(this)" class="required form-control form-control-sm" value="<?= $nilai_pengawas ?>">
                                        <?= form_error('nilai_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No Kontrak <sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_sk_pengawas" id="no_sk_pengawas" class="required form-control form-control-sm" value="<?= $no_sk_pengawas ?>">
                                        <?= form_error('no_sk_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_sk_pengawas" id="tgl_sk_pengawas" value="<?= $tgl_sk_pengawas ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_sk_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Mulai Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_mulai_pengawas" id="tgl_mulai_pengawas" value="<?= $tgl_mulai_pengawas ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_mulai_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Selesai Kontrak<sup>*</sup></label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_selesai_pengawas" id="tgl_selesai_pengawas" value="<?= $tgl_selesai_pengawas ?>" class="required form-control form-control-sm datepicker">
                                        <?= form_error('tgl_selesai_pengawas'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Nilai Addedum </label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="nilai_addedum_pengawas" id="nilai_addedum_pengawas" onkeyup="formatangka(this)" class=" form-control form-control-sm" value="<?= $nilai_addedum_pengawas ?>">
                                        <?= form_error('nilai_addedum_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">No Addedum </label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="no_addedum_pengawas" id="no_addedum_pengawas" class=" form-control form-control-sm" value="<?= $no_addedum_pengawas ?>">
                                        <?= form_error('no_addedum_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_addedum_pengawas" id="tgl_addedum_pengawas" value="<?= $tgl_addedum_pengawas ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_addedum_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Mulai Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_mulai_addedum_pengawas" id="tgl_mulai_addedum_pengawas" value="<?= $tgl_mulai_addedum_pengawas ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_mulai_addedum_pengawas'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-6 col-lg-4 col-form-label text-right">Tanggal Selesai Addedum</label>
                                    <div class="col-6 col-lg-8">
                                        <input type="text" name="tgl_selesai_addedum_pengawas" id="tgl_selesai_addedum_pengawas" value="<?= $tgl_selesai_addedum_pengawas ?>" class=" form-control form-control-sm datepicker">
                                        <?= form_error('tgl_selesai_addedum_pengawas'); ?>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div style="overflow:auto;">
                        <div style="float:right;">
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)"><i class="mdi mdi-arrow-left"></i> Previous</button>
                            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next <i class="mdi mdi-arrow-right"></i> </button>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <!-- <span class="step"></span>
                        <span class="step"></span> -->
                    </div>
                </form>
            </div>
        </div><!-- end card-->
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    * {
        box-sizing: border-box;
    }

    .ilang {
        /* display: none */
    }

    body {
        background-color: #f1f1f1;
    }

    #regForm {
        background-color: #ffffff;
        /* margin: 100px auto; */
        /* font-family: Raleway; */
        padding: 10px;
        /* width: 70%; */
        min-width: 300px;
    }

    h1 {
        text-align: center;
    }

    input {
        padding: 10px;
        width: 100%;
        font-size: 17px;
        font-family: Raleway;
        border: 1px solid #aaaaaa;
    }

    /* Mark input boxes that gets an error on validation: */
    input.invalid {
        background-color: #ffdddd;
    }

    /* Hide all steps by default: */
    .tab {
        display: none;
    }

    button {
        background-color: #4CAF50;
        color: #ffffff;
        border: none;
        padding: 10px 20px;
        font-size: 17px;
        font-family: Raleway;
        cursor: pointer;
    }

    button:hover {
        opacity: 0.8;
    }

    #prevBtn {
        background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbbbbb;
        border: none;
        border-radius: 50%;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        background-color: #4CAF50;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    function formatangka(objek) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + "." + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;
    };

    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "<i class='mdi mdi-cloud-done'></i> Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next <i class='mdi mdi-arrow-right'></i>";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            // alert('asdasf');
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByClassName("required");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
    $(".js-example-responsive").select2({
        width: 'resolve' // need to override the changed default
    });
</script>
<script>
    $(document).ready(function() {

        // boody onload

        <?php if (isset($lp)) { ?>
            // alert('enek');
        <?php } else { ?>
            getProgramOpd($('#kode_skpd').val());
            // alert('ora')
        <?php } ?>

        // edit
        $('#kode_skpd').on('change', function() {
            getProgramOpd($(this).val());
            // getPPK($(this).val());
        });

        $('#kd_prog').on('change', function() {
            $('#ket_prog').val($("#kd_prog option:selected").text());
            getKegiatanList();
        });

        $('#kode_kegiatan').on('change', function() {
            $('#ket_keg').val($("#kode_kegiatan option:selected").text());
            getPagu();
        });

        function getProgramOpd(opd) {
            var _url = "<?php echo base_url() ?>" + "sipil/getprogram";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#kd_prog").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#kd_prog')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }



        function getKegiatanList() {
            var opd = $('#kode_skpd').val();
            var kp = $('#kd_prog').val();
            var ket_p = $('#ket_prog').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getkegiatan";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd,
                    'ket_p': ket_p,
                    'kp': kp
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#kode_kegiatan").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#kode_kegiatan')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }

        function getPPK() {
            var opd = $('#kode_skpd').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getPPK";
            $.ajax({
                url: _url,
                data: {
                    'opd': opd
                },
                dataType: 'json',
                type: 'get',
                success: function(data) {
                    $("#nama_ppk").html("<option value=''></option>");
                    $.each(data, function(key, item) {
                        $('#nama_ppk')
                            .append($("<option></option>")
                                .attr("value", item.id)
                                .text(item.text));
                    });
                }
            });
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }

        function getPagu() {
            var opd = $('#kode_skpd').val();
            var kp = $('#kd_prog').val();
            var ket_p = $('#ket_prog').val();
            var kk = $('#kode_kegiatan').val();
            var _url = "<?php echo base_url() ?>" + "sipil/getPagu";
            $.ajax({
                url: _url,
                data: {
                    'ket_p': ket_p,
                    'opd': opd,
                    'kk': kk,
                    'kp': kp
                },
                type: 'get',
                success: function(data) {
                    x = data.split(",")
                    $('#pagu_anggaran')
                        .val(numberWithCommas(x[0]));
                }
            });
        }
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/sipil/view_form.blade.php ENDPATH**/ ?>