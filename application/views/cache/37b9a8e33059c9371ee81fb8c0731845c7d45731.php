<nav class="navbar navbar-expand fixed-top be-top-header">
      <div class="container-fluid">
        <div class="be-navbar-header"><a class="navbar-brand" href="<?= base_url() . 'welcome' ?>"></a>
        </div>
        <div class="be-right-navbar">
          <ul class="nav navbar-nav float-right be-user-nav">
            <li class="nav-items"><span class="badge badge-danger">Penarikan Data : <?= date_indo(date('Y-m-d', strtotime(get_userdata('tanggal')))) ?></span></li>

            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="<?= base_url() ?>assets/img/avatar.png" alt="Avatar"><span class="user-name"><?= get_userdata('audit_nama') ?></span></a>
              <div class="dropdown-menu" role="menu">
                <div class="user-info">
                  <div class="user-name"><?= get_userdata('audit_nama') ?></div>
                </div><a class="dropdown-item" href="<?= base_url('auth/logout') ?>"><span class="icon mdi mdi-power"></span>Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/layouts/header.blade.php ENDPATH**/ ?>