<script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/countup/countUp.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".loading").hide();
        function formatangka(objek) {
            a = objek.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + "." + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            objek.value = c;
        };

        App.init();
        $(".select2").select2({
            allowClear: true
        });

        $('.datepicker').datetimepicker({
            minView: 2,
            format: 'dd/mm/yyyy',
            autoclose: true
        });


        App.ChartJs();
        // format angka


        // notifikasi
        setTimeout(function() {
            $(".pesan").fadeIn('slow');
        }, 500);

    });





    // //            angka 500 dibawah ini artinya pesan akan muncul dalam 0,5 detik setelah document ready
    // $(document).ready(function() {
    //     setTimeout(function() {
    //         $(".pesan").fadeIn('slow');
    //     }, 500);
    // });
    // //            angka 3000 dibawah ini artinya pesan akan hilang dalam 3 detik setelah muncul
    // setTimeout(function() {
    //     $(".pesan").fadeOut('slow');
    // }, 3000);
</script>

<script>
   $('.number').keypress(function(event) {
   if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
     event.preventDefault();
   }
 });
 </script><?php /**PATH C:\xampp723\htdocs\auditv2\application\views/layouts/footer.blade.php ENDPATH**/ ?>