

@extends('metronic.master')
@section('toolbar')
{!! anchor('welcome', '<i class="flaticon2-left-arrow-1"></i> Kembali', ' class="btn btn-primary""'); !!}
@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">

	<div class="kt-portlet__body">
		<div class="table-responsive">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="card mb-3">
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Desa</th>
											<th>Kecamatan</th>
											<?= $asdf==1?"":"  <th>Pendapatan</th><th>Realisasi</th>"?>
											<th><?= $asdf==1?"Nilai":"%"?></th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($data as $rk){?>
										<tr>
											<td class="text-center"><?=$no?></td>
											<td><?= $rk->Nama_Desa?></td>
											<td><?= str_replace('Kecamatan ','',ucwords(strtolower($rk->Nama_Kecamatan))) ?></td>

											<?= $asdf==1?"":" <td>".number_format($rk->pendapatan,2,',','.')."</td><td>".number_format($rk->realisasi,2,',','.')."</td>"?>
											<td align="center">
												<?= $asdf==1?number_format($rk->nilai,2,',','.'):number_format($rk->persen,2,',','.')?>
											</td>
										</tr>
										<?php $no++; }?>
									</tbody>
								</table>
							</div>
						</div>
					</div><!-- end card-->
				</div>
			</div>
		 </div>
	</div>
</div>
@endsection
@section('css')

@endsection
@section('script')

@endsection
