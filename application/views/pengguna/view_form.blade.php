@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
                <form action="<?php echo $action; ?>" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="varchar">Nama <?php echo form_error('nama') ?></label>
                                <input type="text" class="form-control form-control-xs" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Username <?php echo form_error('username') ?></label>
                                <input type="text" class="form-control form-control-xs" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
                            </div>

                            <?php if ($id_inc == '') { ?>
                                <div class="form-group">
                                    <label for="varchar">Password <?php echo form_error('password') ?></label>
                                    <input type="text" class="form-control form-control-xs" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                                </div>
                            <?php } ?>

                            <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
                            <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i>
                                <?php echo $button ?></button>
                            <a class="btn btn-sm btn-warning" href="<?php echo site_url('pengguna') ?>"><i class="mdi mdi-close"></i> Cancel</a>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <?php foreach ($role as $rk) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-12 col-sm-8 col-lg-6 mt-1">
                                                <label class="custom-control custom-checkbox">
                                                    <input  <?php if(in_array($rk->id_inc,$role_id)){ echo "checked";}?> class="custom-control-input" name="role_id[]" type="checkbox" value="<?php echo $rk->id_inc ?>" ><span class="custom-control-label"><?php echo $rk->nama_role?></span>
                                                </label>
                                                <!-- <label class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Option 2</span>
                                                </label>
                                                <label class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Option 3</span>
                                                </label> -->
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </form>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection

@section('script')

<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
@endsection
