<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts/head')
    @yield('css')
</head>

<body>
    <div class="loading">
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
    </div>
    <div class="be-wrapper  be-fixed-sidebar">
        @include('layouts/header')
        @include('layouts/sidebar')
        <div class="be-content">
            <div class="page-head">
                @yield('judul')
            </div>
            <div class="main-content container-fluid">

                <div class="pesan">@php get_flashdata('message') @endphp</div>
                @yield('content')
            </div>
        </div>
    </div>
    @include('layouts/footer')
    @yield('script')
</body>

</html>