<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="table-responsive">
                  <table class="table table-striped table-hover table-bordered" id="table1">
                    <thead>
                      <tr>
                        <th width="15x">No</th>
                            
                        <th>SKPD</th>
                        <th>SP2D</th>
                        <th>keterangan</th>
                        <th>Nilai</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($data as $rk){?>
                        <tr>
                            <td align="center"><?= $no++ ?></td>
                             <td class="cell-detail"><?= $rk->kd_skpd ?>
                             <span class='cell-detail-description'><?php if($rk->nm_unit==$rk->nm_sub_unit){echo $rk->nm_sub_unit;}else{echo $rk->nm_unit.' / '.$rk->nm_sub_unit ;}?></span>
                             </td>
                             
                             
                             <td class="cell-detail"><?= $rk->no_sp2d ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?></span></td>
                             <td><?= $rk->keterangan ?></td>
                             <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                  </div>
            </div>
        </div><!-- end card-->
    </div>
</div>