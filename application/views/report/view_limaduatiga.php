<div class="row">
  <div class="col-12">
    <div class="card mb-3">
      <div class="card-body">
        <form action="<?php echo $action ?>" class="" method="get">
          <div class="row">
            <div class="col-md-6">
              <select class="form-control form-control-sm select2" name="q" data-placeholder='Silahkan pilih SKPD/OPD'>
                <option value=""></option>
                <?php foreach ($subunit as $su) { ?>
                  <option <?php if ($su->kd_skpd == $q) {
                              echo "selected";
                            } ?> value="<?= $su->kd_skpd ?>"><?= $su->nm_sub_unit ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-4">
              <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
              <?php if ($q <> '') { ?>
                <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
              <?php }
              ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <div class="card mb-3">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-hover table-bordered" id="table1">
            <thead>
              <tr>
                <th width="15x">No</th>

                <th>No SP2D</th>

                <th>Keterangan</th>
                <th> SKPD</th>
                <th>Rekening</th>
                <th>Nilai</th>
                <!-- <th>Aksi</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $no = 1;
              foreach ($data as $rk) { ?>
                <tr>
                  <td align="center"><?= $no++ ?></td>

                  <td class="cell-detail"><?= $rk->no_sp2d ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?> </span></td>
                  <td><?= $rk->keterangan ?></td>

                  <td class="cell-detail"><?= $rk->kd_skpd ?><span class="cell-detail-description"><?= $rk->nm_sub_unit ?></span></td>

                  <td class="cell-detail"><?= $rk->kd_rek_gabung ?><span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
                  <td align="right"><?= number_format($rk->nilai, '2', ',', '.') ?></td>

                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div><!-- end card-->
  </div>
</div>