<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Belanja Modal kurang dari 1 juta.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Belanja Modal kurang dari 1 juta<br>
</h3>


<table class="tablee" border="1">
                      <thead>
                            <tr>
                              <th>No</th>
                              <th>Tanggal SP2D</th>
                              <th>No SP2D</th>
                              <th>Kode SKPD</th>
                              <td>Sub Unit</td>
                              <th>Kode Rekening</th>
                              <th>Rekening</th>
                              <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($data as $rk)  { ?>
                            <tr>
                <td valign="top"  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                <td valign="top" style='mso-number-format:"\@"'><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?> </td>
                             <td valign="top"><?= $rk->no_sp2d ?> </td>
                            <td valign="top"><?= $rk->kd_skpd ?></td>
                            <td valign="top"><?= $rk->nm_sub_unit ?></td>
                            <td valign="top"><?= $rk->kd_rek_gabung ?></td>
                            <td valign="top"><?= $rk->nm_rek_5 ?></td>
                            <td valign="top" align="right"><?= number_format($rk->nilai,'2',',','.') ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
</table>