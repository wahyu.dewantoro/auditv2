<table class="table   table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th>No</th>
            <th>No SPM</th>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>Jenis</th>
            <th>Nilai</th>

        </tr>
    </thead>
    <tbody>
        <?php
        $tn = 0;
        $tj = 0;
        foreach ($sppd_data as $rk) {
            
        ?>
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td class="cell-detail"><?= $rk->no_sp2d ?>
                </td>
                <td><span class="cell-detail-description"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?></span></td>
                <td><?= $rk->keterangan ?></td>
                <td align="center"><?= $rk->jenis_sp2d ?></td>
                <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>