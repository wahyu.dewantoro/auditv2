@extends('metronic.master')
@section('content')
<div class="kt-portlet">
    <div class="kt-portlet__body">
        <div class="form-group row">
            <label class="col-form-label col-lg-3 col-sm-12">OPD</label>
            <div class=" col-lg-4 col-md-9 col-sm-12">
                <select class="form-control kt-select2-general" id="Kd_SKPD" name="param">
                    <option></option>
                    <?php foreach ($subunit as $rs) { ?>
                        <option value='<?= $rs->Kd_SKPD ?>'><?= $rs->Nm_Sub_Unit ?> </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>
<div id="hasil"></div>
@endsection
@section('css')

@endsection
@section('script')
<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script>
    $('#Kd_SKPD').on('change', function() {
        $('.loading').show();
        $('#hasil').html('');
        var _url = "<?php echo base_url() ?>" + "sts/getData";
        $.ajax({
            url: _url,
            data: {
                'Kd_SKPD': $(this).val()
            },

            type: 'post',
            success: function(data) {
                $('.loading').hide();
                $('#hasil').html(data);
            },
            error: function(res) {
                $('.loading').hide();
                $('#hasil').html('');
                alert('sistem error');
            }
        });
    });
</script>
@endsection
