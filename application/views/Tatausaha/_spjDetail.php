<table class="table table-striped table-hover table-bordered" id="table2">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">Nomer</th>
            <th class="text-center">Tanggal</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Jenis</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($spj_data as $rk) { ?>
            <tr>
                <td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
                <td class='cell-detail' nowrap><?= $rk->no_spj ?> <span class="cell-detail-description"></span> </td>
                <td nowrap><?= date_indo(date('Y-m-d', strtotime($rk->tgl_spj))) ?></td>
                <td><?= $rk->keterangan ?> </td>
                <td align="center"><?= $rk->jenis_spj ?> </td>
                <td align="center"><?= $rk->jum ?> </td>
                <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?> </td>
            </tr>
        <?php  }   ?>
    </tbody>
</table>
