<table class="table   table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th>No</th>
            <th>No SPP</th>
            <th>Tanggal</th>
            <th>Uraian</th>
            <th>Jenis</th>
            <th>Nilai</th>

        </tr>
    </thead>
    <tbody>
        <?php
        $tn = 0;
        $tj = 0;
        foreach ($spp_data as $rk) {
            $tn += $rk->nilai;
            $tj += $rk->jum;
        ?>
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td class="cell-detail"><?= $rk->no_spp ?>
                </td>
                <td><span class="cell-detail-description"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_spp))) ?></span></td>
                <td><?= $rk->uraian ?></td>
                <td align="center"><?= $rk->jenis_spp ?></td>
                <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>