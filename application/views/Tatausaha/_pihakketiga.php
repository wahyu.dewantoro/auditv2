<table class="table   table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">OPD</th>
            <th class="text-center">SP2D</th>
            <th class="text-center">SPM</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Penerima</th>
            <th class="text-center">Rekening</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($report_data as $rk) { ?>
            <tr>

				<td valign="top" align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_sub_unit ; ?></span> <span
									class="kt-user-card-v2__desc"><?= $rk->kd_skpd ?></span>
							</div>
						</div>
					</span>
				</td>
				<td valign="top" nowrap>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->no_sp2d ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?></span>
							</div>
						</div>
					</span>
				</td>
				<td valign="top" nowrap>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->no_spm ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_spm))) ?></span>
							</div>
						</div>
					</span>
				</td>
                <td valign="top"><?= $rk->keterangan ?></td>
				<td valign="top" align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>
				<td valign="top" nowrap>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_penerima ; ?></span> <span
									class="kt-user-card-v2__desc">NPWP: <?= $rk->npwp ?></span>
							</div>
						</div>
					</span>
				</td>
                <td valign="top"><?= $rk->rek_penerima ?></td>
            </tr>
        <?php   }   ?>
    </tbody>
</table>
