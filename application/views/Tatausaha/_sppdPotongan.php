<table class="table table-striped table-hover table-bordered" id="table2">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">OPD</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $tj = 0;
        $tn = 0;
        foreach ($sppd_potongan_data as $rk) {
            $tj += $rk->jum;
            $tn += $rk->nilai;
        ?>
            <tr>
				<td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
				<td>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_sub_unit ; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->nm_unit; ?></span><br> <span
									class="kt-user-card-v2__desc"><?=  $rk->kd_skpd; ?></span>
							</div>
						</div>
					</span>
				</td>
                <td align="center"><?= $rk->jum ?></td>
                <td align="right"><a href="<?= base_url() . 'sppdpotongan/read?kd_skpd='.urlencode($rk->kd_skpd).'&nm_sub_unit='.urlencode($rk->nm_sub_unit).'&nm_unit='.urlencode($rk->nm_unit) ?>"> <?= number_format($rk->nilai, '0', '', '.') ?></a> </td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>
