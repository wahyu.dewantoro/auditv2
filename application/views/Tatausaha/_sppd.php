<table class="table table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" rowspan="2" width="10px">No</th>
            <th class="text-center" rowspan="2">OP</th>
            <th class="text-center" colspan="2">UP</th>
            <th class="text-center" colspan="2">GU</th>
            <th class="text-center" colspan="2">TU</th>
            <th class="text-center" colspan="2">LS</th>
            <th class="text-center" colspan="2">NIHIL</th>

        </tr>
        <tr>

            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($sppd_data as $rk) {

        ?>
            <tr>
                <td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
                <td class="cell-detail"><?= $rk->nm_sub_unit ?><span class="cell-detail-description"><?= $rk->nm_unit ?> </span> <span class="cell-detail-description"><?= $rk->kd_skpd ?> </span></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsup > 0) {
                                                                                                                                                                            echo anchor('sppd/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('up') , $rk->jnsup);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaiup, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsgu > 0) {
                                                                                                                                                                            echo anchor('sppd/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('gu') , $rk->jnsgu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaigu, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnstu > 0) {
                                                                                                                                                                            echo anchor('sppd/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('tu') , $rk->jnstu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaitu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsls > 0) {
                                                                                                                                                                            echo anchor('sppd/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('ls') , $rk->jnsls);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilails, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsnihil > 0) {
                                                                                                                                                                            echo anchor('sppd/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('nihil') , $rk->jnsnihil);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        }
                    ?>
                </td>
                <td align="right"><?= number_format($rk->nilainihil, '0', '', '.') ?></td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>
