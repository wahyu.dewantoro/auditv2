<table class="table table-striped table-bordered table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">Tahun</th>
            <th class="text-center">No Bukti</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Rekening</th>
            <th nowrap class="text-center">D / K</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pajak_data as $rk) { ?>
            <tr>
                <td align="center"><?php echo ++$start ?></td>
				<td><?php echo $rk->Tahun ?></td>
				<td nowrap>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->No_Bukti ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->Tgl_Bukti))) ?></span>
							</div>
						</div>
					</span>
				</td>
                <td><?php echo $rk->Keterangan ?></td>
                <td align="right"><?php echo number_format($rk->Nilai, '2', ',', '.') ?></td>
                <td nowrap class="cell-detail"><span><?php echo $rk->Kd_Rek_Gabung ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Rek_5 ?></span></td>
                <td align="center"><?php echo $rk->D_K ?></td>
            </tr>
        <?php  }   ?>
    </tbody>
</table>
