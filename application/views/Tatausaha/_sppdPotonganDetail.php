<table class="table table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">No SP2D</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sppd_potongan_data as $rk) { ?>
            <tr>
                <td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
				<td>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->no_sp2d ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?></span>
							</div>
						</div>
					</span>
				</td>
                <td><?= $rk->keterangan ?></td>
                <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>
            </tr>
        <?php  }   ?>
    </tbody>
</table>
