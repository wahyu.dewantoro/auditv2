<table class="table table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" width="10px">No</th>
            <th class="text-center">SKPD</th>
            <th class="text-center">Sub Unit</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $tj = 0;
        foreach ($pajak_data as $rk) {
            $tj += $rk->nilai;
        ?>
            <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td><?php echo $rk->kd_skpd ?></td>
                <td><?php echo $rk->nm_sub_unit ?></td>
                <td align="right"><a href="<?= base_url() . 'pajak/read?Kd_SKPD='.urlencode($rk->kd_skpd).'&nm_sub_unit='.urlencode($rk->nm_sub_unit).'&nm_unit='.urlencode($rk->nm_unit) ?>"><?php echo number_format($rk->nilai, '2', ',', '.') ?></a></td>
            </tr>
        <?php  }   ?>
    </tbody>

</table><!--  -->
