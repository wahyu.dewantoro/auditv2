<table class="table table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" width="15x">No</th>
            <th class="text-center">No SP2D</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">SKPD</th>
            <th class="text-center">Rekening</th>
            <th class="text-center">Nilai</th>
            <!-- <th></th> -->
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($data as $rk) { ?>
            <tr>
                <td align="center"><?= $no++ ?></td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->no_sp2d ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?> </span>
							</div>
						</div>
					</span>
				</td>
				<td><?php echo $rk->keterangan ?></td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->kd_skpd ; ?></span> <span
									class="kt-user-card-v2__desc"><?= $rk->nm_sub_unit ?></span>
							</div>
						</div>
					</span>
				</td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->kd_rek_gabung ; ?></span> <span
									class="kt-user-card-v2__desc"><?= $rk->nm_rek_5 ?></span>
							</div>
						</div>
					</span>
				</td>
                <td align="right"><?= number_format($rk->nilai, '2', ',', '.') ?></td>

            </tr>
        <?php } ?>
    </tbody>
</table>
