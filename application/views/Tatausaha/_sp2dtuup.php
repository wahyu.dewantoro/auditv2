<table class="table table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th width="15x">No</th>
            <th>SKPD</th>
            <th>SP2D</th>
            <th>keterangan</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($data as $rk) { ?>
            <tr>
				<td align="center"><?= $no++ ?></td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->kd_skpd ; ?></span> <span
									class="kt-user-card-v2__desc">                        <?php
                        if ($rk->nm_unit == $rk->nm_sub_unit) {
                            echo $rk->nm_sub_unit;
                        } else {
                            echo $rk->nm_unit . ' / ' . $rk->nm_sub_unit;
                        } ?></span>
							</div>
						</div>
					</span>
				</td>
				<td valign="top">
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->no_sp2d ; ?></span> <span
									class="kt-user-card-v2__desc"><?= date_indo(date('Y-m-d', strtotime($rk->tgl_sp2d))) ?> </span>
							</div>
						</div>
					</span>
				</td>
                <td><?= $rk->keterangan ?></td>
                <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
