<table class="table table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th rowspan="2" width="10px">No</th>
            <th rowspan="2">OP</th>
            <th colspan="2">UP</th>
            <th colspan="2">GU</th>
            <th colspan="2">TU</th>
            <th colspan="2">LS</th>
            <th colspan="2">NIHIL</th>

        </tr>
        <tr>

            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($spm_data as $rk) {

        ?>
            <tr>
                <td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
				<td>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_sub_unit ; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->nm_unit; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->kd_skpd; ?></span>
							</div>
						</div>
					</span>
				</td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsup > 0) {
                                                                                                                                                                            echo anchor('spm/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('up') , $rk->jnsup);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaiup, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsgu > 0) {
                                                                                                                                                                            echo anchor('spm/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('gu') , $rk->jnsgu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaigu, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnstu > 0) {
                                                                                                                                                                            echo anchor('spm/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('tu') , $rk->jnstu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaitu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsls > 0) {
                                                                                                                                                                            echo anchor('spm/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('ls') , $rk->jnsls);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        } ?>
                </td>
                <td align="right"><?= number_format($rk->nilails, '0', '', '.') ?></td>
                <td align="center">
                    <?php
                                                                                                                                                                        if ($rk->jnsnihil > 0) {
                                                                                                                                                                            echo anchor('spm/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('nihil') , $rk->jnsnihil);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo
                                                                                                                                                                                '-';
                                                                                                                                                                        }
                    ?>
                </td>
                <td align="right"><?= number_format($rk->nilainihil, '0', '', '.') ?></td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>
