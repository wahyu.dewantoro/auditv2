<table class="table  table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th rowspan="2">OPD</th>
            <th colspan="2">UP</th>
            <th colspan="2">GU</th>
            <th colspan="2">TU</th>
            <th colspan="2">LS</th>
            <th colspan="2">NIHIL</th>
        </tr>
        <tr>

            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
            <th>Jumlah</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $jnsup = 0;
        $nilaiup = 0;
        $jnsgu = 0;
        $nilaigu = 0;
        $jnstu = 0;
        $nilaitu = 0;
        $jnsls = 0;
        $nilails = 0;
        $jnsnihil = 0;
        $nilainihil = 0;
        foreach ($spp_data as $rk) {
            $jnsup += $rk->jnsup;
            $nilaiup += $rk->nilaiup;
            $jnsgu += $rk->jnsgu;
            $nilaigu += $rk->nilaigu;
            $jnstu += $rk->jnstu;
            $nilaitu += $rk->nilaitu;
            $jnsls += $rk->jnsls;
            $nilails += $rk->nilails;
            $jnsnihil += $rk->jnsnihil;
            $nilainihil += $rk->nilainihil;
        ?>
            <tr>
				<td>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_sub_unit ; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->nm_unit; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->kd_skpd; ?></span>
							</div>
						</div>
					</span>
				</td>
                <td align="center">
                    <?php if ($rk->jnsup > 0) {
                                        echo  anchor('spp/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('UP'), $rk->jnsup);
                                    } else {
                                        echo '-';
                                    } ?>

                </td>
                <td align="right"><?= number_format($rk->nilaiup, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsgu > 0) {
                                                                                                                                                                            echo  anchor('spp/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('GU'), $rk->jnsgu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo '-';
                                                                                                                                                                        } ?>

                </td>
                <td align="right"><?= number_format($rk->nilaigu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnstu > 0) {
                                                                                                                                                                            echo  anchor('spp/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('TU'), $rk->jnstu);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo '-';
                                                                                                                                                                        } ?>

                </td>
                <td align="right"><?= number_format($rk->nilaitu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsls > 0) {
                                                                                                                                                                            echo  anchor('spp/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('LS'), $rk->jnsls);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo '-';
                                                                                                                                                                        } ?>

                </td>
                <td align="right"><?= number_format($rk->nilails, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsnihil > 0) {
                                                                                                                                                                            echo  anchor('spp/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('NIHIL'), $rk->jnsnihil);
                                                                                                                                                                        } else {
                                                                                                                                                                            echo '-';
                                                                                                                                                                        } ?>

                </td>
                <td align="right"><?= number_format($rk->nilainihil, '0', '', '.') ?></td>
                <!-- <td align="center">
                                    <form action="<?php //base_url().'spp/read'
                                                    ?>">
                                       <input type="hidden" name="nm_unit" value="<?php // $rk->nm_unit
                                                                                    ?>">
                                       <input type="hidden" name="nm_sub_unit" value="<?php // $rk->nm_sub_unit
                                                                                        ?>">
                                       <input type="hidden" name="kd_skpd" value="<?php // $rk->kd_skpd
                                                                                    ?>">
                                       <input type="hidden" name="tanggal1" value="<?php // $tanggal1
                                                                                    ?>">
                                       <input type="hidden" name="tanggal2" value="<?php // $tanggal2
                                                                                    ?>">
                                       <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>
                                </td> -->
            </tr>
        <?php  }   ?>
    </tbody>

</table>
