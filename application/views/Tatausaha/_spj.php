<table class="table  table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" rowspan="2" width="10px">No</th>
            <th class="text-center" rowspan="2">OPD</th>
            <th class="text-center" colspan="2">GU</th>
            <th class="text-center" colspan="2">TU</th>
            <th class="text-center" colspan="2">NIHIL</th>
        </tr>
        <tr>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($spj_data as $rk) {
        ?>
            <tr>
				<td align="center"><?php echo number_format(++$start, '0', '', '.') ?></td>
				<td>
					<span style="width: 200px;">
						<div class="kt-user-card-v2">
							<div class="kt-user-card-v2__details"> <span
									class="kt-user-card-v2__name"><?= $rk->nm_sub_unit ; ?></span> <span
									class="kt-user-card-v2__desc"><?=  $rk->nm_unit; ?></span><br> <span
									class="kt-user-card-v2__desc"><?=  $rk->kd_skpd; ?></span>
							</div>
						</div>
					</span>
				</td>

                <td align="center">
                    <?php if ($rk->jnsgu <> '') {
                                                                                                                                                                            echo anchor('spj/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('gu'), $rk->jnsgu);
                                                                                                                                                                        } else { ?>
                        -
                    <?php } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaigu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnstu <> '') {
                                                                                                                                                                            echo anchor('spj/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('tu'), $rk->jnstu);
                                                                                                                                                                        } else { ?>
                        -
                    <?php } ?>
                </td>
                <td align="right"><?= number_format($rk->nilaitu, '0', '', '.') ?></td>
                <td align="center">
                    <?php if ($rk->jnsnihil <> '') {
                                                                                                                                                                            echo anchor('spj/read?nm_unit=' . urlencode($rk->nm_unit) . '&nm_sub_unit=' . urlencode($rk->nm_sub_unit) . '&kd_skpd=' . urlencode($rk->kd_skpd) . '&q=' . urlencode('nihil'), $rk->jnsnihil);
                                                                                                                                                                        } else { ?>
                        -
                    <?php } ?>
                </td>
                <td align="right"><?= number_format($rk->nilainihil, '0', '', '.') ?></td>

            </tr>
        <?php  }   ?>
    </tbody>

</table>
