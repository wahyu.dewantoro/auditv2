<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fa fa-file"></i>
            </span>
            <h3 class="kt-portlet__head-title">
			Surat Penyediaan Dana
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a class="btn btn-xs btn-primary" href="<?= base_url().'spd/cetak?Kd_SKPD='.$Kd_SKPD?>"><span class="fa fa-cloud-download-alt"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive">
				<table class="table table-striped table-hover table-bordered ">
                    <thead>
                        <tr>
                            <th class="text-center" width="10px">No</th>
                            <th class="text-center" width="100px">No SPD</th>
                            <th class="text-center"  width="200px">Tanggal</th>
                            <th class="text-center">Uraian</th>
                            <th class="text-center">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $jumlah = 0;
                        if (!empty($res)) {


                            foreach ($res as $rk) { ?>
                                <tr>
                                    <td align="center"><?php echo ++$start ?></td>
                                    <td><?php echo $rk->no_spd ?></td>
                                    <td align="center"><?php echo date_indo(date('Y-m-d',strtotime($rk->tgl_spd))) ?></td>
                                    <td class="cell-detail"><?php echo $rk->uraian ?></td>
                                    <td align="right"><?php echo number_format($rk->nilai, '2', ',', '.') ?></td>
                                </tr>
                            <?php
                                $jumlah += $rk->nilai;
                            }
                        } else {   ?>
                            <tr>
                                <th class="text-center" colspan="5">Tidak ada data!</th>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="4">Total</th>
                            <td align="right"><?= number_format($jumlah, 2, ',', '.'); ?> </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
