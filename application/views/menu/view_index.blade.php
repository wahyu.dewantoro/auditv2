@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				List Data
			</h3>
		</div>

		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
                    <form action="<?php echo site_url('menu/index'); ?>" class="form-inline" method="get">
						<div class="input-group">
							<input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
								<span class="input-group-btn">
								<div class="btn-group">

									<button class="btn btn-primary" type="submit"><i class="la la-search"></i> Cari</button>
									<?php if ($q <> '')  { ?>
										<a href="<?php echo site_url('menu'); ?>" class="btn btn-warning"><i class="la la-close"></i> Reset</a>
								  <?php }

										if($akses['create']==1){
										 echo anchor(site_url('menu/create'),'<i class="la la-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
										}
									 ?>
								</div>
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">
				<div class="table-responsive">
                    <table class="table  table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Nama Menu</th>
								<th>Link Menu</th>
								<th>Parent</th>
								<th>Sort</th>
								<th>Icon</th>
								<th width="20px "> </th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($menu_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->nama_menu ?></td>
								<td><?php echo $rk->link_menu ?></td>
								<td><?php echo $rk->parent ?></td>
								<td><?php echo $rk->sort ?></td>
								<td><?php echo $rk->icon ?></td>
								<td style="text-align:center" >
									<div class="btn-group">
										<?php
										//  if($akses['read']==1){ echo anchor(site_url('menu/read/'.acak($rk->id_inc)),'<i class="la la-search"></i>','class="btn btn-xs btn-primary  btn-icon btn-icon-md"'); }
										 if($akses['update']==1){ echo anchor(site_url('menu/update/'.acak($rk->id_inc)),'<i class="la la-edit"></i>','class="btn btn-xs btn-success  btn-icon btn-icon-md"'); }
										 if($akses['delete']==1){ echo anchor(site_url('menu/delete/'.acak($rk->id_inc)),'<i class="la la-trash"></i>','class="btn btn-xs btn-danger  btn-icon btn-icon-md" onclick="javasciprt: return confirm(\'apakah anda yakin untuk menghapus ?\')"'); }
										?>
									</div>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					<button class="btn  btn-space btn-secondary" disabled>Total Record :
						<?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
				</div>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection