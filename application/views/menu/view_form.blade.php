
@extends('metronic.master')
@section('content')
<div class="kt-portlet">
	<div class="kt-portlet__body">

		<!--begin::Section-->
		<div class="kt-section">
			<div class="kt-section__content">

				<form action="<?php echo $action; ?>" method="post">
					<div class="form-group">
						<label for="varchar">Nama Menu <?php echo form_error('nama_menu') ?></label>
						<input type="text" class="form-control form-control-xs" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
					</div>
					<div class="form-group">
						<label for="varchar">Link Menu <?php echo form_error('link_menu') ?></label>
						<input type="text" class="form-control form-control-xs" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>" />
					</div>
					<div class="form-group">
						<label for="int">Parent <?php echo form_error('parent') ?></label>
						<!-- <input type="text"  placeholder="Parent" value="<?php echo $parent; ?>" /> -->
						<select class="kt-select2-general form-control form-control-xs" name="parent" id="parent">
							<option value="0">Parent</option>
							<?php foreach($lp as $lp){?>
							<option value="<?= $lp->id_inc?>" <?php if($lp->id_inc==$parent){echo "selected";}?>><?= $lp->nama_menu ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label for="int">Sort <?php echo form_error('sort') ?></label>
						<input type="text" class="form-control form-control-xs" name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>" />
					</div>
					<div class="form-group">
						<label for="varchar">Icon <?php echo form_error('icon') ?></label>
						<input type="text" class="form-control form-control-xs" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" />
					</div>
					<input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />
					<button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button>
					<a class="btn btn-sm btn-warning" href="<?php echo site_url('menu') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
				</form>
			</div>
		</div>

		<!--end::Section-->
	</div>

	<!--end::Form-->
</div>
@endsection

@section('script')

<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
@endsection
