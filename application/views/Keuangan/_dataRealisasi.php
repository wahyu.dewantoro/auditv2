<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fa fa-file"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                <?= $title ?>
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a class="btn btn-xs btn-primary" href="<?= base_url() . 'realisasi/download?opd=' . urlencode($opd) ?>"><span class="fa fa-cloud-download-alt"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No Urut</th>
                                <th>Uraian</th>
                                <th>Anggaran</th>
                                <th>Realisasi</th>
                                <th>(%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $da = 0;
                            $db = 0;
                            foreach ($realisasi as $rka) { ?>
                                <tr>
                                    <td><?php echo $rka->Akun_Akrual_3 ?></td>
                                    <td><?php echo $rka->Nm_Akrual_3 ?></td>
                                    <td align="right"><?= number_format($rka->anggaran, 0, '', '.') ?></td>
                                    <td align="right"><?= number_format(abs($rka->realisasi), 0, '', '.') ?></td>
                                    <td align="center"><?php if ($rka->realisasi != 0) {
                                                            echo number_format(abs((100 - ($rka->selisih  / $rka->anggaran * 100))), 2, ',', '.');
                                                        ?> % <?php } else {
                                                                        echo '-';
                                                                    } ?></td>
                                </tr>
                            <?php $da += $rka->anggaran;
                                $db += abs($rka->realisasi);
                            } ?>
                            <tr>

                                <td align="center" colspan='2'>Surplus / Defisit</td>
                                <td align="right"><?= number_format($da, 0, '', '.') ?></td>
                                <td align="right"><?= number_format($db, 0, '', '.') ?></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>