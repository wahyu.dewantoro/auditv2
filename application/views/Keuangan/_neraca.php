 <table class="table table-striped table-hover table-bordered " id="table1">
     <thead>
         <tr>
             <th rowspan="2">OPD</th>
             <th colspan="2">Aset</th>
             <th colspan="2">Kewajiban</th>
             <th rowspan="2">Ekuitas</th>
         </tr>
         <tr>

             <th>Saldo Awal</th>
             <th>Mutasi</th>
             <th>Saldo Awal</th>
             <th>Mutasi</th>

         </tr>
     </thead>
     <tbody>
         <?php foreach ($data as $rk) { ?>
             <tr>

                 <td class="cell-detail"><?= $rk->nm_unit ?> <span class="cell-detail-description"><?= $rk->kd_skpd ?></span></td>
                 <td align="right"><?php if (abs($rk->saldoawalaset) > 0) {
                                                                                                        echo anchor('neraca/read?jenis=' . urlencode('saldoawalaset') . '&nm_unit=' . urlencode($rk->nm_unit), number_format(abs($rk->saldoawalaset), 0, '', '.'));
                                                                                                    } else {
                                                                                                        echo "0";
                                                                                                    } ?></td>
                 <td align="right"><?php if (abs($rk->mutasiaset) > 0) {
                                                                                                        echo anchor('neraca/read?jenis=' . urlencode('mutasiaset') . '&nm_unit=' . urlencode($rk->nm_unit), number_format(abs($rk->mutasiaset), 0, '', '.'));
                                                                                                    } else {
                                                                                                        echo "0";
                                                                                                    } ?></td>
                 <td align="right"><?php if (abs($rk->saldoawalkewajiban) > 0) {
                                                                                                        echo anchor('neraca/read?jenis=' . urlencode('saldoawalkewajiban') . '&nm_unit=' . urlencode($rk->nm_unit), number_format(abs($rk->saldoawalkewajiban), 0, '', '.'));
                                                                                                    } else {
                                                                                                        echo "0";
                                                                                                    } ?></td>
                 <td align="right"><?php if (abs($rk->mutasikewajiban) > 0) {
                                                                                                        echo anchor('neraca/read?jenis=' . urlencode('mutasikewajiban') . '&nm_unit=' . urlencode($rk->nm_unit), number_format(abs($rk->mutasikewajiban), 0, '', '.'));
                                                                                                    } else {
                                                                                                        echo "0";
                                                                                                    } ?></td>
                 <td align="right"><?php if (abs($rk->mutasiekuitas) > 0) {
                                                                                                        echo anchor('neraca/read?jenis=' . urlencode('mutasiekuitas') . '&nm_unit=' . urlencode($rk->nm_unit), number_format(abs($rk->mutasiekuitas), 0, '', '.'));
                                                                                                    } else {
                                                                                                        echo "0";
                                                                                                    } ?></td>
             </tr>
         <?php } ?>
     </tbody>
 </table>