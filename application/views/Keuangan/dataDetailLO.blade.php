@extends('metronic.master')
@section('toolbar')
{!! anchor('lodetail', '<i class="flaticon2-left-arrow-1"></i> Kembali', ' class="btn btn-primary""'); !!}
@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				<strong><?= $nm_unit ?> - <?= $akun ?></strong>
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<a class="btn btn-xs btn-primary" href="<?= base_url() . 'lodetail/cetakDetail?kd_skpd='.urlencode($kd_skpd).'&nm_unit=' . urlencode($nm_unit).'&akun='.urlencode($akun) ?>"><span class="fa fa-cloud-download-alt"></span></a>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="table-responsive">
			<?php echo $result ?>
		 </div>
	</div>
</div>
@endsection
@section('css')

@endsection
@section('script')

@endsection
