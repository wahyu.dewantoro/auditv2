<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fa fa-file"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Saldo Awal
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a class="btn btn-xs btn-primary" href="<?= base_url().'getSaldoAwal/cetak?Kd_SKPD='.$Kd_SKPD?>"><span class="fa fa-cloud-download-alt"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive">
				<table class="table table-striped table-hover table-bordered ">
                    <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th>Tahun</th>
                            <th>Jurnal</th>
                            <th>Tgl Bukti</th>
                            <th>Uraian</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $debet = 0;
                        $kredit = 0;

                        if (!empty($saldoawal_data)) {


                            foreach ($saldoawal_data as $rk) { ?>
                                <tr>
                                    <td align="center"><?php echo ++$start ?></td>
                                    <td><?php echo $rk->Tahun ?></td>
                                    <td><?php echo $rk->Nm_Jurnal ?></td>
                                    <td><?php echo $rk->Tgl_Bukti ?></td>
                                    <td class="cell-detail"><span><?php echo $rk->Akun_Akrual_5 ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Akrual_5 ?></span></td>
                                    <td align="right"><?php echo number_format($rk->Saldo_Awal_Debet, '2', ',', '.') ?></td>
                                    <td align="right"><?php echo number_format($rk->Saldo_Awal_Kredit, '2', ',', '.') ?></td>
                                </tr>
                            <?php
                                                                                                                                                $debet += $rk->Saldo_Awal_Debet;
                                                                                                                                                $kredit += $rk->Saldo_Awal_Kredit;
                                                                                                                                            }
                                                                                                                                        } else {   ?>
                            <tr>
                                <th class="text-center" colspan="7">Tidak ada data!</th>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5">surplus / defisit</th>
                            <td align="right"><?= number_format($debet, 2, ',', '.'); ?> </td>
                            <td align="right"><?= number_format($kredit, 2, ',', '.'); ?> </td>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
