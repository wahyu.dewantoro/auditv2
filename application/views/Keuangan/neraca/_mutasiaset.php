 <table class="table table-bordered" border='1'>
     <thead>
         <th class="text-center" colspan="7">
             <?= $akun ?>
         </th>
         <tr>
             <th class="text-center" colspan="3">Akun</th>
             <th class="text-center" colspan="3">Rekening</th>
             <th class="text-center">Nilai</th>
         </tr>
     </thead>
     <tbody>
         <?php foreach ($neraca_data as $rk) { ?>
             <tr style="font-size:1.2rem">
                 <td colspan="3"><strong><?php echo $rk->akun_akrual_2 ?></strong></td>
                 <td colspan="3"><strong><?php echo $rk->nm_akrual_2 ?></strong></td>
                 <td align="right"><strong><?php echo number_format(abs($rk->nilai), '0', '', '.') ?></strong></td>
             </tr>
             <?php
                $level3 = $this->Keuangan->mutasiAsetTiga($tahun,$nm_unit, $rk->akun_akrual_2);
                foreach ($level3 as $rn) {  ?>
                 <tr style="color: green; font-size:1.1rem">
                     <td></td>
                     <td colspan="2"><?php echo $rn->akun_akrual_3 ?></td>
                     <td></td>
                     <td colspan="2"><?php echo $rn->nm_akrual_3 ?></td>
                     <td align="right"><?php echo number_format(abs($rn->nilai), '0', '', '.') ?></td>
                 </tr>
                 <?php
                        $level4 = $this->Keuangan->mutasiAsetEmpat($tahun,$nm_unit, $rn->akun_akrual_3);
                        foreach ($level4 as $rm) { ?>
                     <tr style="color: red;font-size:0.9rem">
                         <td></td>
                         <td></td>
                         <td><i><?php echo $rm->akun_akrual_4 ?></i></td>
                         <td></td>
                         <td></td>
                         <td><i><?php echo $rm->nm_akrual_4 ?></i></td>
                         <td align="right"><i><?php echo number_format(abs($rm->nilai), '0', '', '.') ?></i></td>
                     </tr>
         <?php }
            }
        } ?>
     </tbody>
 </table>
