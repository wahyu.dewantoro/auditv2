<table class="table table-striped table-hover table-bordered" id="table1">
    <thead>
        <tr>
            <th class="text-center" rowspan="2">OPD</th>
            <th class="text-center" colspan="9">Akun</th>
        </tr>
        <tr>
            <th>Pendapatan Asli Daerah (PAD) (8.1)</th>
            <th>Pendapatan Transfer (8.2)</th>
            <th>Lain-Lain Pendapatan Daerah yang Sah (8.3)</th>
            <th>Surplus Non Operasional (8.4)</th>
            <th>Pendapatan Luar Biasa (8.5)</th>
            <th>Beban Operasi (9.1)</th>
            <th>Beban Transfer (9.2)</th>
            <th>Defisit Non Operasional (9.3)</th>
            <th>Beban Luar Biasa (9.4)</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($lodetail_data as $rk) { ?>
            <tr>

                <td class="cell-detail"><span><?= $rk->nm_unit ?></span><span class="cell-detail-description"><?= $rk->kd_skpd ?></span></td>
                <td align="right">
                    <?php if (abs($rk->sumnilai81) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=8.1', number_format(abs($rk->sumnilai81), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai82) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=8.2', number_format(abs($rk->sumnilai82), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai83) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=8.3', number_format(abs($rk->sumnilai83), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai84) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=8.4', number_format(abs($rk->sumnilai84), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai85) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=8.5', number_format(abs($rk->sumnilai85), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai91) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=9.1', number_format(abs($rk->sumnilai91), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai92) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=9.2', number_format(abs($rk->sumnilai92), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai93) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=9.3', number_format(abs($rk->sumnilai93), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
                <td align="right">
                    <?php if (abs($rk->sumnilai94) > 0) {
                                                                                                                    echo  anchor('lodetail/read?kd_skpd=' . $rk->kd_skpd . '&nm_unit=' . $rk->nm_unit . '&akun=9.4', number_format(abs($rk->sumnilai94), '0', '', '.'));
                                                                                                                } else {
                                                                                                                    echo "-";
                                                                                                                } ?>
                </td>
            </tr>
        <?php


                                                                                                            }   ?>
    </tbody>
</table>
