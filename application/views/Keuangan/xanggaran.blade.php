@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> <?= $title ?>
</h2>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">OPD</label>
                    <div class="col-12 col-sm-8 col-lg-6">
                        <div class="input-group mb-3">
                            <select class="select2" id="Kd_SKPD" data-placeholder="Silahkan Pilih">
                                <option value=""></option>
                                <?php foreach ($subunit as $rs) { ?>
                                    <option value='<?= $rs->Kd_SKPD ?>'><?= $rs->Nm_Sub_Unit ?> </option>
                                <?php } ?>
                            </select>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div id="hasil"></div>
@endsection
@section('css')

@endsection
@section('script')
<script>
    $('#Kd_SKPD').on('change', function() {
        $('.loading').show();
        $('#hasil').html('');
        var _url = "<?php echo base_url() ?>" + "getAnggaran";
        $.ajax({
            url: _url,
            data: {
                'Kd_SKPD': $(this).val()
            },
            
            type: 'post',
            success: function(data) {
                $('.loading').hide();
                $('#hasil').html(data);
            },
            error: function(res) {
                $('.loading').hide();
                $('#hasil').html('');
                alert('sistem error');
            }
        });
    });
</script>
@endsection