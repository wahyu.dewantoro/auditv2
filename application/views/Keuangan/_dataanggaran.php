<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fa fa-file"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                <?= $title ?>
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a class="btn btn-xs btn-primary" href="<?= base_url() . 'anggaran/download?opd=' . urlencode($opd) ?>"><span class="fa fa-cloud-download-alt"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th colspan="3">Akun</th>
                                <th colspan="3">Rekening</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nilai = 0;
                            foreach ($anggaran_data as $rk) { ?>
                                <tr>
                                    <td colspan="3"><strong><?= $rk->akun_akrual_2 ?></strong></td>
                                    <td colspan="3"><strong><?= $rk->nm_akrual_2 ?></strong></td>
                                    <td align="right"><strong><?php echo number_format($rk->nilai, 0, '', '.'); ?></strong></td>
                                </tr>
                                <!-- GetLeveltiga -->
                                <?php
                                // $tahun, $opd,$akun
                                $tiga = $this->Keuangan->dataAnggaranAkunTiga($tahun, $opd, $rk->akun_akrual_2);
                                foreach ($tiga as $rn) { ?>
                                    <tr style="color: green;">
                                        <td></td>
                                        <td colspan="2"><?= $rn->akun_akrual_3 ?></td>
                                        <td></td>
                                        <td colspan="2"><?= $rn->nm_akrual_3 ?></td>
                                        <td align="right"><?php echo number_format($rn->nilai, 0, '', '.'); ?></td>
                                    </tr>
                                    <?php
                                    $lima = $this->Keuangan->dataAnggaranAkunEmpat($tahun, $opd, $rn->akun_akrual_3);
                                    foreach ($lima as $rm) {     ?>
                                        <tr style="color: red;">
                                            <td></td>
                                            <td></td>
                                            <td><i><?= $rm->akun_akrual_4 ?></i></td>
                                            <td></td>
                                            <td></td>
                                            <td><i><?= $rm->nm_akrual_4 ?></i></td>
                                            <td align="right"><i><?php echo number_format($rm->nilai, 0, '', '.');
                                                                    $nilai += $rm->nilai; ?></i></td>
                                        </tr>
                            <?php }
                                }
                            } ?>
                        </tbody>
                        <tfoot>
                            <th colspan="6">Jumlah</th>
                            <td align="right"><strong><?= number_format($nilai, '0', '', '.'); ?></strong></td>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>