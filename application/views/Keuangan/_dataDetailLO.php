<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="3">Akun</th>
            <th colspan="3">Rekening</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lodetail_data as $rk) { ?>
            <tr style="font-size:1.2rem">
                <td colspan="3"><strong><?php echo $rk->akun_akrual_3 ?></strong></td>
                <td colspan="3"><strong><?php echo $rk->nm_akrual_3 ?></strong></td>
                <td align="right"><strong><?php echo number_format(abs($rk->nilai), '0', '', '.') ?></strong></td>
            </tr>
            <?php
                                            $level4 = $this->Keuangan->getLoLevel4($tahun,$nm_unit, $rk->akun_akrual_3);
                                            foreach ($level4 as $rn) {  ?>
                <tr style="color:green;font-size:1.1rem">
                    <td></td>
                    <td colspan="2"><?php echo $rn->akun_akrual_4 ?></td>
                    <td></td>
                    <td colspan="2"><?php echo $rn->nm_akrual_4 ?></td>
                    <td align="right"><?php echo number_format(abs($rn->nilai), '0', '', '.') ?></td>
                </tr>
                <?php
                                                $level5 = $this->Keuangan->getLoLevel5($tahun,$nm_unit, $rn->akun_akrual_4);
                                                foreach ($level5 as $rm) { ?>
                    <tr style="color:red;font-size:0.9rem">
                        <td></td>
                        <td></td>
                        <td><i><?php echo $rm->akun_akrual_5 ?></i></td>
                        <td></td>
                        <td></td>
                        <td><i><?php echo $rm->nm_akrual_5 ?></i></td>
                        <td align="right"><i><?php echo number_format(abs($rm->nilai), '0', '', '.') ?></i></td>
                    </tr>
        <?php }
                                            }
                                        } ?>
    </tbody>
</table>