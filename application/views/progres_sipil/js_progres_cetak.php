<script src="<?= base_url() ?>Chart.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>utils.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/capture/html2canvas.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/capture/canvas2image.js"></script>

<?php
if ($nilai_addedum_pengawas > 0) {
    $tt = $nilai_addedum_pengawas;
} else {
    $tt = $nilai_pengawas;
}
?>
<script>
    var lineChartData = {
        datasets: [{
            label: 'realisasi (Dalam Persentase)',
            borderColor: window.chartColors.red,
            backgroundColor: window.chartColors.red,
            fill: false,
            data: [
                <?php $tmp = 0;
                foreach ($progres as $rl) { ?>
                    <?= $rl->persentasi_total; ?>,
                <?php } ?>
                <?php if ($tmp != $tt) { ?>
                    null,
                <?php } ?>
            ],
            yAxisID: 'y-axis-1',
        }, {
            label: 'Target (Dalam Persentase)',
            borderColor: window.chartColors.blue,
            backgroundColor: window.chartColors.blue,
            fill: false,
            fill: false,
            data: [
                <?php $tmp = 0;
                foreach ($progresjj as $rl) { ?>
                    <?= $rl->persentase ?>,
                <?php } ?>
                <?php if ($tmp != $tt) { ?>
                    null,
                <?php } ?>
            ],
            yAxisID: 'y-axis-1',
        }],
        labels: [
            <?php foreach ($progresj as $zx) { ?> '<?= gabungTanggal2($zx->awal_minggu, $zx->akhir_minggu) ?>',
            <?php } ?>
            <?php if ($tmp != $tt) { ?> ''
            <?php } ?>
        ]
    };
    window.onload = function() {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = Chart.Line(ctx, {
            data: lineChartData,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                title: {
                    display: true,
                    text: 'Progres Pekerjaan (Nilai Kontrak  : <?= angka($tt) ?>)'
                },
                scales: {
                    yAxes: [{
                            type: 'linear',
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                            ticks: {
                                suggestedMin: 0,
                                suggestedMax: 100,
                            }
                        },

                    ],
                }
            }
        });
        //  window.print();

        // window.render();
        // window.print();
    };

    // document.getElementById("printChart").addEventListener("click",function(){
    // 	chart.print();
    // });  	


    /* document.getElementById('randomizeData').addEventListener('click', function() {
        lineChartData.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });
        });

        window.myLine.update();
    }); */
</script>
<!-- <script type="text/javascript">
    var test = $("#cetak").get(0);
    // to canvas
    $('.toCanvas').click(function(e) {
        html2canvas(test).then(function(canvas) {
            // canvas width
            var canvasWidth = canvas.width;
            // canvas height
            var canvasHeight = canvas.height;
            // render canvas
            Canvas2Image.saveAsImage(canvas, canvasWidth, canvasHeight, 'png', 'progres_sipil');

        });
    });


    // window.onload = function() { window.print(); }

   
</script> -->
<!-- $(window).bind("load", function() {
        // code here
        window.print();
    }); -->