<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3 card-table">
            <div class="card-header">
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>OPD</th>
                                <th>Kegiatan</th>
                                <th>Sub Kegiatan</th>
                                <th>Minggu</th>
                                <th>Deviasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($res as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td class="cell-detail">
                                        <?= $rk->kode_skpd ?>
                                        <span class="cell-detail-description"><?= $rk->nama_skpd ?></span>
                                    </td>
                                    <td class="cell-detail"><?= $rk->ket_keg ?>
                                        <span class="cell-detail-description"><?= $rk->ket_prog ?></span>
                                    </td>
                                    <td><?= $rk->sub_kegiatan ?></td>
                                    <td class="cell-detail"> Ke <?= $rk->minggu_ke ?> , <?= gabungTanggal2($rk->awal_minggu, $rk->awal_minggu) ?>
                                        <span class="cell-detail-description"> Perencanaan : <?= number_format($rk->perencanaan,1) ?></span>
                                        <span class="cell-detail-description"> Realisasi : <?= number_format($rk->realisasi,1) ?></span>
                                    </td>
                                    <td align="center"><?= number_format($rk->deviasi,1) ?> %</td>

                                    <td>
                                        <?php if (($rk->perencanaan <= 70 && $rk->deviasi < -10) || ($rk->perencanaan > 70 && $rk->deviasi < -5)) { ?>
                                            <button data-id="<?= $rk->progres_id ?>" class="email btn btn-xs btn-success"><i class="mdi mdi-email"></i> Email </button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>