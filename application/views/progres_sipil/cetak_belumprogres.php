<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Belum_progres.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    .str {
        mso-number-format: \@;
    }

</style>

<h3>Belum Progres</h3>
<table class="table table-bordered table-striped" border="1" cellspacing="0" cellpadding="0" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>OPD</th>
            <th>Kegiatan</th>
            <th>Sub Kegiatan</th>
            <th>Minggu Ke</th>
            <th>Awal</th>
            <th>Akhir</th>
            <th>Persentase</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($res as $rk) { ?>
            <tr>
                <td class="str" align="center"><?= $no ?></td>
                <td class="str"><?= $rk->nama_skpd ?></td>
                <td class="str"><?= $rk->nama_kegiatan ?></td>
                <td class="str"><?= $rk->sub_kegiatan ?></td>
                <td class="str" align="center"><?= $rk->minggu ?></td>
                <td class="str" align="left"><?= date_indo($rk->awal_minggu) ?></td>
                <td class="str" align="left"><?= date_indo($rk->akhir_minggu) ?></td>
                <td class="str"><?= $rk->persentasi_total ?></td>
            </tr>
        <?php $no++;
        } ?>
    </tbody>
</table>