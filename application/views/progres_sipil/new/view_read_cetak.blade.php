@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Progres Pekerjaan
</h2>
@endsection
@section('content')
<style>
    @media print {
        body * {
            visibility: hidden;
        }

        #section-to-print,
        #section-to-print * {
            visibility: visible;
        }

        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>

<div class="row " id="section-to-print">
    <div class="col-md-8">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">

                <div class="tools ">
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <b>Pekerjaan</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">OPD</td>
                                    <td class="icon">:</td>
                                    <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Program</td>
                                    <td class="icon">:</td>
                                    <td><?= $ket_prog ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Kegiatan</td>
                                    <td class="icon">:</td>
                                    <td><?= $ket_keg ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Sub Kegiatan</td>
                                    <td class="icon">:</td>
                                    <td><?= $sub_kegiatan ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item"> Anggaran</td>
                                    <td class="icon">:</td>
                                    <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Harga Perkiraan Sendiri</td>
                                    <td class="icon">:</td>
                                    <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <b>Jasa Konsultansi</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">Nama</td>
                                    <td class="icon">:</td>
                                    <td><?= $nama_konsultansi ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">No Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= $no_sk_konsultansi ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Tanggal Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo(trim($tgl_sk_konsultansi)) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Nilai Kontrak</td>
                                    <td class="icon">:</td>
                                    <td>Rp.<?= angka($nilai_konsultansi) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Periode Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_mulai_konsultansi) . ' s/d ' . date_indo($tgl_selesai_konsultansi) ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if ($no_addedum_konsultansi) { ?>
                            <hr>
                            <table class="table">
                                <tbody class="no-border-x no-border-y">

                                    <tr valign="top">
                                        <td class="item">No Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $no_addedum_konsultansi ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Tanggal Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_addedum_konsultansi) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Nilai Addedum</td>
                                        <td class="icon">:</td>
                                        <td>Rp.<?= angka($nilai_addedum_konsultansi) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Periode Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_mulai_addedum_konsultansi) . ' s/d ' . date_indo($tgl_selesai_addedum_konsultansi) ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <b>Jasa Konstruksi</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">Nama</td>
                                    <td class="icon">:</td>
                                    <td><?= $nama_pengawas ?> / <?= $nama_cv ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">No Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= $no_sk_pengawas ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Tanggal Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_sk_pengawas) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Nilai Kontrak</td>
                                    <td class="icon">:</td>
                                    <td>Rp.<?= angka($nilai_pengawas) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Periode Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_mulai_pengawas) . ' s/d ' . date_indo($tgl_selesai_pengawas) ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if ($no_addedum_pengawas) { ?>
                            <hr>
                            <table class="table">
                                <tbody class="no-border-x no-border-y">

                                    <tr valign="top">
                                        <td class="item">No Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $no_addedum_pengawas ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Tanggal Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_addedum_pengawas) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Nilai Addedum</td>
                                        <td class="icon">:</td>
                                        <td>Rp.<?= angka($nilai_addedum_pengawas) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Periode Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_mulai_addedum_pengawas) . ' s/d ' . date_indo($tgl_selesai_addedum_pengawas) ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        <?php } ?>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-4">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">
                Riwayat Progres
                <div class="tools ">
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered" border="1" colpadding="0" rowpadding="0">
                    <thead>
                        <tr valign="top">
                            <th>No</th>
                            <th>Minggu</th>
                            <th>Kumulatif</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($progresa)) { ?>
                            <?php $akm = 0;
                                $dd = 1;
                                foreach ($progresa as $pp) {
                                    ?>
                                <tr valign="top">
                                    <td align="center"><?= $dd ?> </td>
                                    <td>
                                        <span style="width: 200px;">
                                            <div class="kt-user-card-v2">
                                                <div class="kt-user-card-v2__details"> <span
                                                        class="kt-user-card-v2__name">Ke <?= $pp->minggu_ke ?></span> <span
                                                        class="kt-user-card-v2__desc"><?= gabungTanggal2($pp->awal_minggu, $pp->awal_minggu) ?></span> </div>
                                            </div>
                                        </span>
                                    </td>
                                    <td>
                                        <span style="width: 200px;">
                                            <div class="kt-user-card-v2">
                                                <div class="kt-user-card-v2__details"> <span
                                                        class="kt-user-card-v2__name">Realisasi : <?= number_format($pp->realisasi,2,',','.') ?> %</span> <span
                                                        class="kt-user-card-v2__desc">Deviasi : <?= $pp->deviasi > 0 ? number_format($pp->deviasi,2,',','.') : 0 ?> % </span> </div>
                                            </div>
                                        </span>
                                    </td>
                                    <th align="center"><a class="btn btn-sm btn-danger btn-icon btn-icon-md" onclick="return confirm('Apakah anda yakin ?')" href="<?php echo base_url().'progressipil/hapuslaporan/'.$pp->id  ?>"><i class="la la-trash"></i></a></th>
                                </tr>
                            <?php $dd++;
                                } ?>
                        <?php } else { ?>
                            <tr valign="top">
                                <td colspan="4" align="center">Data tidak ada</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">
                <div class="tools">
                    <a class="btn btn-xs btn-success "  onclick="myFunction()"><i class="la la-print"></i> Cetak</a>
                    <?php
                    if ($akses['create'] == 1) {
                        echo anchor('progressipil/progres/' . acak($id), '<i class="la la-calendar-check-o"></i> Progress', ' class="btn btn-xs btn-warning"');
                    }  ?>
                </div>
            </div>
            <div class="card-body" id="reportPage">
                <canvas id="canvas"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>Chart.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>utils.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/capture/html2canvas.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/capture/canvas2image.js"></script>

<?php
if ($nilai_addedum_pengawas > 0) {
	$tt = $nilai_addedum_pengawas;
} else {
	$tt = $nilai_pengawas;
}
?>
<script>
	var lineChartData = {

		datasets: [{
			label: 'Kumulatif Realisasi (%)',
			borderColor: window.chartColors.red,
			backgroundColor: window.chartColors.red,
			fill: false,
			data: [
				0,
				<?php $tmp = 0;
				foreach ($progres as $rl) { ?>
					<?= $rl->persentasi_total; ?>,
				<?php } ?>
				<?php if ($tmp != $tt) { ?>
					null,
				<?php } ?>
			],
			yAxisID: 'y-axis-1',
		}, {
			label: 'Kumulatif Perencanaan (%)',
			borderColor: window.chartColors.blue,
			backgroundColor: window.chartColors.blue,
			fill: false,
			fill: false,
			data: [
				0,
				<?php $tmp = 0;
				foreach ($progresjj as $rl) { ?>
					<?= $rl->persentase ?>,
				<?php } ?>
				<?php if ($tmp != $tt) { ?>
					null,
				<?php } ?>
			],
			yAxisID: 'y-axis-1',
		}],
		labels: [
			'<?php echo date_indo($tgl_mulai_pengawas)?>',
			<?php foreach ($progresj as $zx) { ?> '<?php echo date_indo($zx->akhir_minggu); // gabungTanggal2($zx->awal_minggu, $zx->akhir_minggu)
														?>',
			<?php } ?>
			<?php if ($tmp != $tt) { ?> ''
			<?php } ?>
		]
	};
	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = Chart.Line(ctx, {
			data: lineChartData,
			options: {
				responsive: true,
				hoverMode: 'index',
				stacked: false,
				title: {
					display: true,
					text: 'Progres Pekerjaan (Nilai Kontrak  : <?= angka($tt) ?>)'
				},
				scales: {
					yAxes: [{
							type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
							display: true,


							position: 'left',
							id: 'y-axis-1',
							ticks: {
								suggestedMin: 0,
								suggestedMax: 100,
							}
						},

					],
				}
			}
		});
	};

	document.getElementById('randomizeData').addEventListener('click', function() {
		lineChartData.datasets.forEach(function(dataset) {
			dataset.data = dataset.data.map(function() {
				return randomScalingFactor();
			});
		});

		window.myLine.update();
	});
</script>
<script type="text/javascript">
	var test = $("#cetak").get(0);
	// to canvas
	$('.toCanvas').click(function(e) {
		html2canvas(test).then(function(canvas) {
			// canvas width
			var canvasWidth = canvas.width;
			// canvas height
			var canvasHeight = canvas.height;
			// render canvas

			Canvas2Image.saveAsImage(canvas, canvasWidth, canvasHeight, 'png', 'progres_sipil');

		});
	});
</script>
<script>
function myFunction() {
  window.print();
}
</script>
@endsection
