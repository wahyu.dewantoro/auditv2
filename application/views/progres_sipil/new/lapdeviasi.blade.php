@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Laproan Deviasi Progres
</h2>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3 card-table">
                <div class="card-header">
                        Laproan Deviasi Progres
                    </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>OPD</th>
                                <th>Kegiatan</th>
                                <th>Sub Kegiatan</th>
                                <th>Minggu</th>
                                <th>Deviasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($res as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td class="cell-detail">
                                        <?= $rk->kode_skpd ?>
                                        <span class="cell-detail-description"><?= $rk->nama_skpd ?></span>
                                    </td>
                                    <td class="cell-detail"><?= $rk->ket_keg ?>
                                        <span class="cell-detail-description"><?= $rk->ket_prog ?></span>
                                    </td>
                                    <td><?= $rk->sub_kegiatan ?></td>
                                    <td class="cell-detail"> Ke <?= $rk->minggu_ke ?> , <?= gabungTanggal2($rk->awal_minggu, $rk->awal_minggu) ?>
                                        <span class="cell-detail-description"> Perencanaan : <?= number_format($rk->perencanaan,1) ?></span>
                                        <span class="cell-detail-description"> Realisasi : <?= number_format($rk->realisasi,1) ?></span>
                                    </td>
                                    <td align="center"><?= number_format($rk->deviasi,1) ?> %</td>

                                    <td>
                                        <?php if (($rk->perencanaan <= 70 && $rk->deviasi < -10) || ($rk->perencanaan > 70 && $rk->deviasi < -5)) { ?>
                                            <button data-id="<?= $rk->progres_id ?>" class="email btn btn-xs btn-success"><i class="mdi mdi-email"></i> Email </button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

  <script type="text/javascript">
  $(document).ready(function(){
      //-initialize the javascript
      // App.init();
      App.dataTables();
  });
</script>
{{-- <script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script> --}}
@endsection
