@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Progres Pekerjaan
</h2>
@endsection
@section('content')
<style>
    @media print {
        body * {
            visibility: hidden;
        }

        #section-to-print,
        #section-to-print * {
            visibility: visible;
        }

        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>

<div class="row hidden-print" id="section-to-print">
    <div class="col-md-6">
        <div class="card  card-border-color card-border-color-primary">
            <div class="card-header ">
                <b>Form progres</b>
            </div>
            <div class="card-body">
                <form id="formprogres" method="post" action="<?= base_url() . 'progressipil/progres_action' ?>" enctype="multipart/form-data">
                    <input type="hidden" name="pekerjaan_sipil_id" value="<?= $id ?>">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <select class="form-control kt-select2-general" name="minggu_ke" required data-placeholder="Minggu ke">
                                    <option value=""></option>
                                    <?php foreach ($rja as $rja) { ?>
                                        <option value="<?= $rja->minggu ?>">Minggu ke <?= $rja->minggu ?> (<?= gabungTanggal($rja->awal_minggu, $rja->akhir_minggu) ?>) </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input    class="form-control form-control-xs" type="text" placeholder="Kumulatif progres * (%) *" name="persentasi_total" id="persentasi_total" onkeyup="formatangka(this)" value="<?= $persentasi_total ?>">
                                <?= form_error('persentasi_total'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea id="email-editor" class="form-control" name="deskripsi_progres" placeholder="Deskripsi"><?= $email_editor ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id='TextBoxesGroup'>
                                            <div id="TextBoxDiv1">
                                                <input type="file" class="form-control" name="file_progres[]" id="file_progres">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button id='addButton' type="button" class="btn btn-sm btn-success"><i class="la la-file"></i> (.pdf/.zip/.rar/.jpg) &nbsp;&nbsp;</button>
                                        <button id='removeButton' type="button" class="btn btn-sm btn-danger"><i class="la la-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group float-right">
                                <button class="btn btn-primary btn-space" type="submit"><i class="la la-check"></i> Simpan</button>
                                <?= anchor('progressipil', '<i class="la la-close"></i> Batal', 'class="btn btn-warning btn-space"') ?>
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="kt-portlet kt-portlet--tabs">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-toolbar">
                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">          <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_3_1_tab_content" role="tab" aria-selected="true">
                                      <i class="la la-tasks" aria-hidden="true"></i>Pekerjaan
                                    </a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_2_tab_content" role="tab" aria-selected="false">
                                      <i class="flaticon-profile-1" aria-hidden="true"></i>Jasa Konsultasi
                                    </a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#kt_portlet_base_demo_3_3_tab_content" role="tab" aria-selected="false">
                                      <i class="flaticon-buildings" aria-hidden="true"></i>Jasa Konstruksi
                                    </a>
                                  </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_portlet_base_demo_3_1_tab_content" role="tabpanel">
                                    <?php if (cekjadwal($id) == 0) { ?>
                                        <div class="alert alert-warning alert-dismissible" role="alert">
                                            <!-- <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button> -->
                                            <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                                            <div class="message"><strong>Warning!</strong> Perencanaan progres mingguan belum di isi.<?= anchor('sipil/jadwal/' . acak($id), '<i class="mdi mdi-edit"></i> Lengkapi', 'class="btn btn-sm btn-success"') ?></div>
                                        </div>
                                    <?php } ?>

                                    <table class="no-border no-strip skills">
                                        <tbody class="no-border-x no-border-y">
                                            <tr>
                                                <td class="item">OPD</td>
                                                <td class="icon">:</td>
                                                <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                                            </tr>
                                            <tr>
                                                <td class="item">Program</td>
                                                <td class="icon">:</td>
                                                <td><?= $ket_prog ?></td>
                                            </tr>
                                            <tr>
                                                <td class="item">Kegiatan</td>
                                                <td class="icon">:</td>
                                                <td><?= $ket_keg ?></td>
                                            </tr>
                                            <tr>
                                                <td class="item">Sub Kegiatan</td>
                                                <td class="icon">:</td>
                                                <td><?= $sub_kegiatan ?></td>
                                            </tr>
                                            <tr>
                                                <td class="item"> Anggaran</td>
                                                <td class="icon">:</td>
                                                <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                                            </tr>
                                            <tr>
                                                <td class="item">Harga Perkiraan Sendiri</td>
                                                <td class="icon">:</td>
                                                <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                            <div class="tab-pane" id="kt_portlet_base_demo_3_2_tab_content" role="tabpanel">
                                    <table class="no-border no-strip skills">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <td class="item">Nama</td>
                                                    <td class="icon">:</td>
                                                    <td><?= $nama_konsultansi ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="item">No Kontrak</td>
                                                    <td class="icon">:</td>
                                                    <td><?= $no_sk_konsultansi ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="item">Tanggal Kontrak</td>
                                                    <td class="icon">:</td>
                                                    <td><?= date_indo($tgl_sk_konsultansi) ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="item">Nilai Kontrak</td>
                                                    <td class="icon">:</td>
                                                    <td>Rp.<?= angka($nilai_konsultansi) ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="item">Periode Kontrak</td>
                                                    <td class="icon">:</td>
                                                    <td><?= date_indo($tgl_mulai_konsultansi) . ' s/d ' . date_indo($tgl_selesai_konsultansi) ?></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <?php if ($no_addedum_konsultansi) { ?>
                                            <hr>
                                            <table class="no-border no-strip skills">
                                                <tbody class="no-border-x no-border-y">

                                                    <tr>
                                                        <td class="item">No Addedum</td>
                                                        <td class="icon">:</td>
                                                        <td><?= $no_addedum_konsultansi ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="item">Tanggal Addedum</td>
                                                        <td class="icon">:</td>
                                                        <td><?= $tgl_addedum_konsultansi ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="item">Nilai Addedum</td>
                                                        <td class="icon">:</td>
                                                        <td>Rp.<?= angka($nilai_addedum_konsultansi) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="item">Periode Addedum</td>
                                                        <td class="icon">:</td>
                                                        <td><?= date_indo($tgl_mulai_addedum_konsultansi) . ' s/d ' . date_indo($tgl_selesai_addedum_konsultansi) ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        <?php } ?>
                            </div>
                            <div class="tab-pane" id="kt_portlet_base_demo_3_3_tab_content" role="tabpanel">

                    <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="item">Nama</td>
                                    <td class="icon">:</td>
                                    <td><?= $nama_pengawas ?></td>
                                </tr>
                                <tr>
                                    <td class="item">No Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= $no_sk_pengawas ?></td>
                                </tr>
                                <tr>
                                    <td class="item">Tanggal Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_sk_pengawas) ?></td>
                                </tr>
                                <tr>
                                    <td class="item">Nilai Kontrak</td>
                                    <td class="icon">:</td>
                                    <td>Rp.<?= angka($nilai_pengawas) ?></td>
                                </tr>
                                <tr>
                                    <td class="item">Periode Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_mulai_pengawas) . ' s/d ' . date_indo($tgl_selesai_pengawas) ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if ($no_addedum_pengawas) { ?>
                            <hr>
                            <table class="no-border no-strip skills">
                                <tbody class="no-border-x no-border-y">

                                    <tr>
                                        <td class="item">No Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $no_addedum_pengawas ?></td>
                                    </tr>
                                    <tr>
                                        <td class="item">Tanggal Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $tgl_addedum_pengawas ?></td>
                                    </tr>
                                    <tr>
                                        <td class="item">Nilai Addedum</td>
                                        <td class="icon">:</td>
                                        <td>Rp.<?= angka($nilai_addedum_pengawas) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="item">Periode Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_mulai_addedum_pengawas) . ' s/d ' . date_indo($tgl_selesai_addedum_pengawas) ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
@endsection
@section('script')

<script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
@endsection