@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Belum Entri Progres
</h2>
@endsection
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="card mb-3 card-table">
			<div class="card-header">
				Data Pekerjaan
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table  table-striped table-hover table-bordered" id="table2">
						<thead>
							<tr>
								<th>No</th>
								<th>OPD</th>
								<th>Kegiatan</th>
								<!-- <th>Sub Kegiatan</th> -->
								<th>Jumlah</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
                            foreach ($res as $rk) { ?>
							<tr>
								<td align="center"><?= $no ?></td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $sipil->kode_skpd; ?></span> <span
													class="kt-user-card-v2__desc"><?= $sipil->nama_skpd; ?></span>
											</div>
										</div>
									</span>
								</td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $sipil->ket_keg; ?></span> <span
													class="kt-user-card-v2__desc"><?= $sipil->ket_prog ?></span> </div>
										</div>
									</span>
								</td>
								<td><?= $rk->jum ?> Minggu</td>
								<td class="text-center"><button data-toggle="modal" data-target="#mod-success"
										data-id="<?= acak($rk->id) ?>" class="btn btn-primary btn-sm lihat"><i
											class="la la-eye"></i> </button></td>
							</tr>
							<?php $no++;
                            } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- end card-->
	</div>
</div>
<div class="modal fade" id="mod-success" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true"><span
						class="mdi mdi-close"></span></button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="detaildata">

					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

  <script type="text/javascript">
  $(document).ready(function(){
      //-initialize the javascript
      // App.init();
      App.dataTables();
  });
</script>
{{-- <script src="<?= base_url() ?>metro/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script> --}}
@endsection
