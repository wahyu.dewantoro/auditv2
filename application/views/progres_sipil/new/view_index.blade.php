@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Progres Pekerjaan
</h2>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-table card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Data Pekerjaan
                <div class="tools float-right">

                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>OPD</th>
                            <th>Kegiatan</th>
                            <th>Sub Kegiatan</th>
                            <th>Anggaran</th>
                            <th>HPS</th>
                            <th>Kontrak</th>
                            <th>Persentase</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $sipil) { ?>
                            <tr>
                                <td width="10px" align="center"><?= ++$start ?></td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $sipil->kode_skpd; ?></span> <span
													class="kt-user-card-v2__desc"><?= $sipil->nama_skpd; ?></span>
											</div>
										</div>
									</span>
								</td>
								<td>
									<span style="width: 200px;">
										<div class="kt-user-card-v2">
											<div class="kt-user-card-v2__details"> <span
													class="kt-user-card-v2__name"><?= $sipil->ket_keg; ?></span> <span
													class="kt-user-card-v2__desc"><?= $sipil->ket_prog ?></span> </div>
										</div>
									</span>
								</td>
                                <td> <?= $sipil->sub_kegiatan; ?></td>
                                <td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
                                <td align="right"><?= angka($sipil->harga_perkiraan); ?></td>
                                <td align="right"><?= angka($sipil->nilai_pengawas); ?></td>
                                <td align="center"> <?=  number_format(($sipil->nilai_pengawas/$sipil->harga_perkiraan) * 100,2,',','.') ?> % </td>
                                <td width="100px" align="center">
                                    <div class="btn-group">
                                        <?php
                                            echo anchor('progressipil/read/' . acak($sipil->id), '<i class="la la-eye"></i>  ', ' class="btn btn-sm btn-primary btn-icon btn-icon-md"');
                                            if ($akses['create'] == 1 && $role!=3) {
                                                echo anchor('progressipil/progres/' . acak($sipil->id), '<i class="la la-calendar-check-o"></i>  ', ' class="btn btn-sm btn-warning btn-icon btn-icon-md"');
                                            }
                                            ?>
                                    </div>


                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
                <div class="float-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<script>

</script>
@endsection