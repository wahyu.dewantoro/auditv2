<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3 card-table">
            <div class="card-header">
                Data Pekerjaan
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>OPD</th>
                                <th>Kegiatan</th>
                                <!-- <th>Sub Kegiatan</th> -->
                                <th>Jumlah</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($res as $rk) { ?>
                                <tr>
                                    <td align="center"><?= $no ?></td>
                                    <td class="cell-detail"> 
                                    <?= $rk->kode_skpd ?>
                                        <span class="cell-detail-description"><?= $rk->nama_skpd ?></span>
                                    </td>
                                    <td class="cell-detail"><?= $rk->ket_keg ?>
                                        <span class="cell-detail-description"><?= $rk->ket_prog ?></span>
                                    </td>
                                    <td><?= $rk->jum ?> Minggu</td>
                                    <td class="text-center"><button data-toggle="modal" data-target="#mod-success" data-id="<?= acak($rk->id) ?>" class="btn btn-primary btn-xs lihat"><i class="mdi mdi-eye"></i> </button></td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<div class="modal fade" id="mod-success" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="detaildata">

                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>