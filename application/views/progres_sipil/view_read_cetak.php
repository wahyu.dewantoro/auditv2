<style>
    @media print {
        body * {
            visibility: hidden;
        }

        #section-to-print,
        #section-to-print * {
            visibility: visible;
        }

        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>

<div class="row " id="section-to-print">
    <div class="col-md-8">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">

                <div class="tools ">
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <b>Pekerjaan</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">OPD</td>
                                    <td class="icon">:</td>
                                    <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Program</td>
                                    <td class="icon">:</td>
                                    <td><?= $ket_prog ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Kegiatan</td>
                                    <td class="icon">:</td>
                                    <td><?= $ket_keg ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Sub Kegiatan</td>
                                    <td class="icon">:</td>
                                    <td><?= $sub_kegiatan ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item"> Anggaran</td>
                                    <td class="icon">:</td>
                                    <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Harga Perkiraan Sendiri</td>
                                    <td class="icon">:</td>
                                    <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <b>Jasa Konsultansi</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">Nama</td>
                                    <td class="icon">:</td>
                                    <td><?= $nama_konsultansi ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">No Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= $no_sk_konsultansi ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Tanggal Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo(trim($tgl_sk_konsultansi)) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Nilai Kontrak</td>
                                    <td class="icon">:</td>
                                    <td>Rp.<?= angka($nilai_konsultansi) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Periode Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_mulai_konsultansi) . ' s/d ' . date_indo($tgl_selesai_konsultansi) ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if ($no_addedum_konsultansi) { ?>
                            <hr>
                            <table class="table">
                                <tbody class="no-border-x no-border-y">

                                    <tr valign="top">
                                        <td class="item">No Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $no_addedum_konsultansi ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Tanggal Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_addedum_konsultansi) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Nilai Addedum</td>
                                        <td class="icon">:</td>
                                        <td>Rp.<?= angka($nilai_addedum_konsultansi) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Periode Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_mulai_addedum_konsultansi) . ' s/d ' . date_indo($tgl_selesai_addedum_konsultansi) ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <b>Jasa Konstruksi</b>
                        <table class="table">
                            <tbody class="no-border-x no-border-y">
                                <tr valign="top">
                                    <td class="item">Nama</td>
                                    <td class="icon">:</td>
                                    <td><?= $nama_pengawas ?> / <?= $nama_cv ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">No Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= $no_sk_pengawas ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Tanggal Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_sk_pengawas) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Nilai Kontrak</td>
                                    <td class="icon">:</td>
                                    <td>Rp.<?= angka($nilai_pengawas) ?></td>
                                </tr>
                                <tr valign="top">
                                    <td class="item">Periode Kontrak</td>
                                    <td class="icon">:</td>
                                    <td><?= date_indo($tgl_mulai_pengawas) . ' s/d ' . date_indo($tgl_selesai_pengawas) ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if ($no_addedum_pengawas) { ?>
                            <hr>
                            <table class="table">
                                <tbody class="no-border-x no-border-y">

                                    <tr valign="top">
                                        <td class="item">No Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= $no_addedum_pengawas ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Tanggal Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_addedum_pengawas) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Nilai Addedum</td>
                                        <td class="icon">:</td>
                                        <td>Rp.<?= angka($nilai_addedum_pengawas) ?></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="item">Periode Addedum</td>
                                        <td class="icon">:</td>
                                        <td><?= date_indo($tgl_mulai_addedum_pengawas) . ' s/d ' . date_indo($tgl_selesai_addedum_pengawas) ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        <?php } ?>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-4">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">
                Riwayat Progres
                <div class="tools ">
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered" border="1" colpadding="0" rowpadding="0">
                    <thead>
                        <tr valign="top">
                            <th>No</th>
                            <th>Minggu</th>
                            <th>Kumulatif</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($progresa)) { ?>
                            <?php $akm = 0;
                                $dd = 1;
                                foreach ($progresa as $pp) {
                                    ?>
                                <tr valign="top">
                                    <td align="center"><?= $dd ?> </td>
                                    <td class="cell-detail"> Ke <?= $pp->minggu_ke ?>
                                        <span class="cell-detail-description"> <?= gabungTanggal2($pp->awal_minggu, $pp->awal_minggu) ?> </span>
                                    </td>
                                    <td class="cell-detail">Realisasi : <?= number_format($pp->realisasi,2,',','.') ?> %
                                        <span class="cell-detail-description">Perencanaan : <?= number_format($pp->perencanaan,2,',','.') ?> % </span>
                                        <span class="cell-detail-description">Deviasi : <?= $pp->deviasi > 0 ? number_format($pp->deviasi,2,',','.') : 0 ?> % </span>
                                    </td>
                                    <th align="center"><a class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda yakin ?')" href="<?php echo base_url().'progressipil/hapuslaporan/'.$pp->id  ?>"><i class="mdi mdi-delete"></i></a></th>
                                </tr>
                            <?php $dd++;
                                } ?>
                        <?php } else { ?>
                            <tr valign="top">
                                <td colspan="4" align="center">Data tidak ada</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card  card-border-color card-border-color-primary card-table">
            <div class="card-header ">
                <div class="tools">
                    <a class="btn btn-xs btn-success"  onclick="myFunction()"><i class="mdi mdi-print"></i> Cetak</a>
                    <?php
                    if ($akses['create'] == 1) {
                        echo anchor('progressipil/progres/' . acak($id), '<i class="mdi mdi-calendar-check"></i> Progress', ' class="btn btn-xs btn-warning"');
                    }  ?>
                </div>
            </div>
            <div class="card-body" id="reportPage">
                <canvas id="canvas"></canvas>
            </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
  window.print();
}
</script>
