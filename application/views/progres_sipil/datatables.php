  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      //-initialize the javascript
      // App.init();
      App.dataTables();
    });
  </script>
  <script>
    $(function() {
      $(document).on('click', '.email', function(e) {
        $(".loading").show();
        e.preventDefault();
        id_ = $(this).attr('data-id');

        $.ajax({
          type: "POST",
          url: "<?= base_url() . 'progressipil/notifemail' ?>",
          data: {
            id: id_
          },
          success: function(msg) {
            // alert("Data Saved: " + msg);
            $(".loading").hide();
            alert(msg);
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            $(".loading").hide();
            alert("some error");

          }
        });

        // $.post("",
        //     {id:id_},
        //     function(html){
        //       $(".loading").hide();
        //         alert(html);
        //     }
        // );
      });
    });
  </script>