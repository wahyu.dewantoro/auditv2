@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Laporan dibawah 80% HPS
</h2>
@endsection
@section('content')<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-table card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                List Data
                <div class="tools float-right">
                    <?php  //if(get_userdata('audit_role')!=3){?>

                    <!-- <form action="<?php //echo site_url('dibawah80hps'); ?>" class="form-inline" method="get">

                        <select name="id_ppk" class="form-control form-control-sm select2 required" id="id_ppk"
                            data-placeholder="Pilih OPD">
                            <option value=""></option>
                            <?php foreach ($ppk as $ppk) { ?>
                            <option <?php if ($ppk->id == $id_ppk) {
                                                        echo "selected";
                                        } ?> value="<?= $ppk->id ?>"><?= $ppk->instansi ?></option>
                            <?php } ?>
                        </select>
                        <div class="input-group">

                            <span class="input-group-btn">
                                <div class="btn-group">
                                    <?php if ($id_ppk <> '')  { ?>
                                    <a href="<?php echo site_url('dibawah80hps'); ?>" class="btn btn-warning"><i
                                            class="mdi mdi-close"></i> Reset</a>
                                    <?php }   ?>
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i>
                                        Cari</button>
                                    <?= anchor($cetaks,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success" target="_blank"')?>
                                </div>
                            </span>
                        </div>
                    </form> -->
                    <?php //} else {?>

                    <?php // anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success" target="_blank"')?>
                    <?php //}?>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>OPD</th>
                            <th>Jenis Belanja</th>
                            <!-- <th>Kegiatan</th> -->
                            <th>Sub Kegiatan</th>
                            <th>Pagu</th>
                            <th>Nilai SPK</th>
                            <th>HPS</th>
                            <th>Persentase </th>
                            <?php if($jenis==1){ ?>

                            <th></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $sipil) { $persentase=number_format($sipil->nilai_spk/$sipil->harga_perkiraan*100,2,",","."); ?>
                        <tr>
                            <td width="10px" align="center"><?= ++$start ?></td>
							<td>
								<span style="width: 200px;">
									<div class="kt-user-card-v2">
										<div class="kt-user-card-v2__details"> <span
												class="kt-user-card-v2__name"><?= $sipil->kode_skpd; ?></span> <span
												class="kt-user-card-v2__desc"><?= $sipil->nama_skpd; ?></span>
										</div>
									</div>
								</span>
							</td>
							<td>
								<span style="width: 200px;">
									<div class="kt-user-card-v2">
										<div class="kt-user-card-v2__details"> <span
												class="kt-user-card-v2__name"><?= $sipil->kode_belanja; ?></span> <span
												class="kt-user-card-v2__desc"><?= $sipil->jenis_belanja ?></span> </div>
									</div>
								</span>
							</td>
                            <!-- <td class="cell-detail"><?= $sipil->ket_keg; ?>
                            <span class="cell-detail-description"><?= $sipil->ket_prog; ?></span>
                        </td> -->
                            <td><?= $sipil->sub_kegiatan; ?></td>
                            <td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
                            <td align="right"><?= angka($sipil->nilai_spk); ?></td>
                            <td align="right"><?= angka($sipil->harga_perkiraan); ?></td>
                            <td align="right"><?= $persentase ?> % </td>

                            <?php if($jenis==1){ ?>

                            <th> <?= anchor('progressipil/read/' . acak($sipil->id), '<i class="mdi mdi-eye"></i> ', ' class="btn btn-xs btn-primary"');?></th>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
                <div class="float-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('script')
@endsection
