<div class="kt-header__topbar">


    <!--begin: User Bar -->
    <div class="kt-header__topbar-item kt-header__topbar-item--user">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
            <div class="kt-header__topbar-user">
                <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                <span class="kt-header__topbar-username kt-hidden-mobile"><?= get_userdata('audit_nama') ?></span>
                <img class="kt-hidden" alt="Pic" src="<?= base_url() ?>metro/media/users/300_25.jpg" />

                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">S</span>
            </div>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

            <!--begin: Head -->


            <!--end: Head -->

            <!--begin: Navigation -->
            <div class="kt-notification">

                <div class="kt-notification__custom kt-space-between">
                    <a href="#" target="_blank" class="btn btn-clean btn-sm btn-bold"></a>
                    <a href="<?= base_url('auth/logout') ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
                </div>
            </div>

            <!--end: Navigation -->
        </div>
    </div>

    <!--end: User Bar -->
</div>