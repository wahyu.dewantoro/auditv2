@extends('metronic.master')
@section('judul')
<h2 class="page-head-title"> Laporan dibawah 80% HPS
</h2>
@endsection
@section('content')<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="card card-table card-contrast">
			<div class="card-header card-header-contrast card-header-featured ">
				List Data
				<div class="tools float-right">

				</div>
			</div>
			<div class="card-body">

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>OPD</th>
							<th>Jenis Belanja</th>
							<th>Kegiatan</th>
							<th>Sub Kegiatan</th>
							<th>Pagu</th>
							<th>Tanggal Pekerjaan SPK</th>
							<th>Tanggal BAST</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $sipil) {
                            $persentase = number_format($sipil->nilai_spk / $sipil->harga_perkiraan * 100, 2, ",", "."); ?>
						<tr>
							<td width="10px" align="center"><?= ++$start ?></td>
							<td>
								<span style="width: 200px;">
									<div class="kt-user-card-v2">
										<div class="kt-user-card-v2__details"> <span
												class="kt-user-card-v2__name"><?= $sipil->kode_skpd; ?></span> <span
												class="kt-user-card-v2__desc"><?= $sipil->nama_skpd; ?></span>
										</div>
									</div>
								</span>
							</td>
							<td>
								<span style="width: 200px;">
									<div class="kt-user-card-v2">
										<div class="kt-user-card-v2__details"> <span
												class="kt-user-card-v2__name"><?= $sipil->kode_belanja; ?></span> <span
												class="kt-user-card-v2__desc"><?= $sipil->jenis_belanja; ?></span>
										</div>
									</div>
								</span>
							</td>
							<td><?= $sipil->nama_kegiatan; ?></td>
							<td><?= $sipil->sub_kegiatan; ?></td>
							<td align="right"><?= angka($sipil->pagu_anggaran); ?></td>
							<td align="right">
								<?= date_indo($sipil->mulai_pekerjaan_spk) . " s/d " . date_indo($sipil->selesai_pekerjaan_spk); ?>
							</td>
							<td align="right"><?= date_indo($sipil->tanggal_bast); ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
				<div class="float-right">
					<?php echo $pagination ?>
				</div>
			</div>
		</div><!-- end card-->
	</div>
</div>

@endsection
@section('script')
@endsection
