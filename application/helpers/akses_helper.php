<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Session Helper
 *
 * A simple session class helper for Codeigniter
 *
 * @package     Codeigniter Session Helper
 * @author      Dwayne Charrington
 * @copyright   Copyright (c) 2014, Dwayne Charrington
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @link        http://ilikekillnerds.com
 * @since       Version 1.0
 * @filesource
 */
if (!function_exists('showMenu')) {
  function showMenu($kode_group)
  {
    $CI = get_instance();
    $CI->menu_db = $CI->load->database('default', true);
    $qmenu0  = $CI->menu_db->query("SELECT distinct a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='0'
                                          ORDER BY sort ASC")->result_array();

    $varmenu = '';
    foreach ($qmenu0 as $row0) {
      $parent  = $row0['id_inc'];
      $qmenu1  = $CI->menu_db->query("SELECT distinct a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='$parent'
                                          ORDER BY sort ASC");
      $cekmenu = $qmenu1->num_rows();
      $dmenu1  = $qmenu1->result_array();
      if ($cekmenu > 0) {
        $varmenu .= "<li class='parent'>
                      <a href='#' > <i class='" . $row0['icon'] . "'></i>  <span>" . ucwords($row0['nama_menu']) . "</span> <span class='menu-arrow'></span></a>";
        $varmenu .= "<ul class='sub-menu'>";
        foreach ($dmenu1 as $row1) {
          $qwa = explode('/', $row1['link_menu']);
          $dda = strtolower($CI->uri->segment(1)) == strtolower($qwa[0]) ? 'active' : '';
          $varmenu .= "<li class='" . $dda . "'>" . anchor(strtolower($row1['link_menu']), "<i class='mdi mdi-chevron-right'></i> <span>" . ucwords($row1['nama_menu'] . "</span>"), 'class="nav-link "') . "</li>";
        }
        $varmenu .= "</ul>
                      </li>";
      } else {
        $qaw = explode('/', $row0['link_menu']);
        $dd = strtolower($CI->uri->segment(1)) == strtolower($qaw[0]) ? 'active' : '';
        $varmenu .= "<li  class='" . $dd . "'>" . anchor(strtolower($row0['link_menu']), "<i class='" . $row0['icon'] . "'></i> <span>" . ucwords($row0['nama_menu'] . '</span>'), 'class="nav-link "') . "</li>";
      }
    }
    echo $varmenu;
  }
}


function parentActive($var)
{
  $CI = get_instance();
  $CI->menu_db = $CI->load->database('default', true);
  $res = $CI->menu_db->query("SELECT distinct a.*,c.nama_menu nama_parent
                                FROM ms_menu a
                              left join (select * from ms_menu where parent=0) c on c.id_inc=a.parent
                                WHERE a.link_menu='$var'")->row();
  if($res){
    return $res->nama_parent;
  }else{
    return '';
  }

  
}

if (!function_exists('showMenuMetronic')) {
  function showMenuMetronic($kode_group)
  {
    $CI = get_instance();
    $CI->menu_db = $CI->load->database('default', true);
    $qmenu0  = $CI->menu_db->query("SELECT distinct a.*,c.nama_menu nama_parent
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          left join (select * from ms_menu where parent=0) c on c.id_inc=a.parent
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND a.parent='0'
                                          ORDER BY sort ASC")->result_array();

    $varmenu = '';
    foreach ($qmenu0 as $row0) {
      $aqwa = explode('/', $row0['link_menu']);
      $ddab = strtolower($row0['nama_menu']) == strtolower(parentActive(strtolower($CI->uri->segment(1)))) ? 'kt-menu__item--open kt-menu__item--here' : '';
      $parent  = $row0['id_inc'];
      $qmenu1  = $CI->menu_db->query("SELECT distinct a.*,c.nama_menu nama_parent
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          left join (select * from ms_menu where parent=0) c on c.id_inc=a.parent
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND a.parent='$parent'
                                          ORDER BY sort ASC");
      $cekmenu = $qmenu1->num_rows();
      $dmenu1  = $qmenu1->result_array();

      // $dac="kt-menu__item--open kt-menu__item--here";

      if ($cekmenu > 0) {

        $varmenu .= "<li class=\"kt-menu__item  kt-menu__item--submenu " . $ddab . " \" aria-haspopup=\"true\" data-ktmenu-submenu-toggle=\"hover\">
                      <a href=\"javascript:;\" class=\"kt-menu__link kt-menu__toggle\"> <i class=\"kt-menu__link-icon " . $row0['icon_v2'] . "\"></i><span class=\"kt-menu__link-text\">" . ucwords($row0['nama_menu']) . "</span><i class=\"kt-menu__ver-arrow la la-angle-right\"></i></a>";
        $varmenu .= "<div class=\"kt-menu__submenu \"><span class=\"kt-menu__arrow\"></span>
        <ul class='kt-menu__subnav'>";
        foreach ($dmenu1 as $row1) {
          $qwa = explode('/', $row1['link_menu']);
          $dda = strtolower($CI->uri->segment(1)) == strtolower($qwa[0]) ? 'kt-menu__item--active' : '';
          $varmenu .= "<li  class=\"kt-menu__item " . $dda . " \" >" . anchor(strtolower($row1['link_menu']), "<i class=\"kt-nav__link-icon flaticon-signs\"></i> <span class=\"kt-nav__link-text\"> <span>&nbsp; </span>" . ucwords($row1['nama_menu'] . "</span>"), 'class="kt-menu__link "') . "</li>";
        }
        $varmenu .= "</ul></div>
                      </li>";
      } else {
        $qaw = explode('/', $row0['link_menu']);
        $dd = strtolower($CI->uri->segment(1)) == strtolower($qaw[0]) ? 'kt-menu__item--active' : '';
        $varmenu .= "<li  class=\"kt-menu__item " . $dd . "\" aria-haspopup=\"true\"  >" . anchor(strtolower($row0['link_menu']), "<i class=\"kt-menu__link-icon " . $row0['icon_v2'] . "\"><span></span></i> <span class=\"kt-menu__link-text\">" . ucwords($row0['nama_menu'] . '</span>'), 'class="kt-menu__link "') . "</li>";
      }
    }
    echo $varmenu;
  }
}


if (!function_exists('errorbos')) {
  function errorbos()
  {
    $CI = &get_instance();
    $CI->load->view('errors/403', '', true);
    die();
  }
}

function cekjadwal($id)
{
  $CI = get_instance();
  $res = $CI->db->query("select count(1) res from pekerjaan_sipil_jadwal where pekerjaan_sipil_id=$id")->row();
  return $res->res;
}
function cekjadwalkegiatan($id)
{
  $CI = get_instance();
  $res = $CI->db->query("select count(1) res from kegiatan_belanja_jadwal where kegiatan_belanja_id=$id")->row();
  return $res->res;
}

// cek nama role
function AssignRole($id)
{
  if ($id == null) {
    return "";
  }

  $role = "";
  $CI = get_instance();
  $res = $CI->db->query("select distinct ms_pengguna_id,nama_role
                      from e_audit_app.dbo.ms_assign_role a
                      join e_audit_app.dbo.ms_role b on a.ms_role_id=b.id_inc
                      where ms_pengguna_id=$id")->result();

  foreach ($res as $res) {
    $role .= $res->nama_role . ', ';
  }

  if ($role <> '') {
    $role = substr($role, 0, -2);
  }

  return $role;
}

function namaInstansiPPK($id)
{
  if ($id == null) {
    return "";
  }
  $CI = get_instance();
  $res = $CI->db->query("select * from ( select id_inc,nm_sub_unit from e_audit_app.dbo.ms_pengguna a join e_audit_app.dbo.ref_ppk b on b.id=a.ref_ppk_id
            where a.ref_ppk_id is not null
            union all
  select id_inc,nm_sub_unit from e_audit_app.dbo.ms_pengguna a
  join e_audit_app.dbo.ref_pengawas b on b.id=a.ref_pengawas_id where a.ref_pengawas_id is not null ) asd where id_inc='$id'")->row();
  return !empty($res->nm_sub_unit) ? $res->nm_sub_unit : '-';
}
if (!function_exists('cek')) {
  function cek($pengguna, $url, $var)
  {
    $CI = get_instance();
    $CI->menu_db = $CI->load->database('default', true);

    if (empty($var)) {
      errorbos();
    }
    switch ($var) {
      case 'read':
        # code...
        $vv = 'status';
        break;
      case 'create':
        # code...
        $vv = 'created';
        break;
      case 'update':
        $vv = 'updated';
        # code...
        break;
      case 'delete':
        $vv = 'deleted';
        # code...
        break;
      default:
        # code...
        $vv = '';
        break;
    }

    $res = $CI->menu_db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND REPLACE(link_menu,'/','.')='$url' AND a." . $vv . " ='1'")->row();

    if ($res) {
      return array(
        'read' => $res->status,
        'create' => $res->created,
        'update' => $res->updated,
        'delete' => $res->deleted
      );
    } else {
      errorbos();
    }
  }

  function rangeMinggu($start, $end)
  {
    $format = 'Y-m-d';
    $array = array();
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $vstart = new DateTime($start);
    $period = new DatePeriod($vstart, $interval, $realEnd);

    foreach ($period as $date) {
      $array[] = $date->format($format);
    }

    $minggu = [];
    $rk = [];
    foreach ($array as $rd) {
      $adate = new DateTime($rd);

      $minggu[] = array(
        'week' => $adate->format("W"),
        'tahun' => $adate->format("Y"),
        'tanggal' => getStartAndEndDate($adate->format("W"), $adate->format("Y"))
      );
    }
    return array_map("unserialize", array_unique(array_map("serialize", $minggu)));
  }

  function getStartAndEndDate($week, $year)
  {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['awal'] = $dto->format('Y-m-d');
    $dto->modify('+6 days');
    $ret['akhir'] = $dto->format('Y-m-d');
    return $ret;
  }
  function getEndWeek($date)
  {
    $adate = new DateTime($date);
    $dto = new DateTime();
    $dto->setISODate($adate->format("Y"), $adate->format("W"));
    $dto->modify('+6 days');
    $ret = $dto->format('Y-m-d');
    return $ret;
  }
  function gabungTanggal($da, $db)
  {

    $bln = array(1 => "Januari", "Februaru", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");


    $sta = strtotime($da);
    $stb = strtotime($db);
    $res = "";

    // cek tahun sama
    if (date('Y', $sta) == date('Y', $stb)) {
      // echo "Tahun sama";
      if (date('m', $sta) == date('m', $stb)) {
        // bulan sama
        $res = date('d', $sta) . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $sta)] . ' ' . date('Y', $sta);
      } else {
        //bulan tak sama
        $res = date('d', $sta) . ' ' . $bln[(int) date('m', $sta)] . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $stb)] . ' ' . date('Y', $stb);
      }
    } else {
      // echo "Tahun tak sama";
      $res = date('d', $sta) . ' ' . $bln[(int) date('m', $sta)] . ' ' . date('Y', $sta) . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $stb)] . ' ' . date('Y', $stb);
    }

    return $res;
  }
  function gabungTanggal2($da, $db)
  {

    $bln = array(1 => "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
    $sta = strtotime($da);
    $stb = strtotime($db);
    $res = "";

    // cek tahun sama
    if (date('Y', $sta) == date('Y', $stb)) {
      // echo "Tahun sama";
      if (date('m', $sta) == date('m', $stb)) {
        // bulan sama
        $res = date('d', $sta) . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $sta)] . ' ' . date('Y', $sta);
      } else {
        //bulan tak sama
        $res = date('d', $sta) . ' ' . $bln[(int) date('m', $sta)] . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $stb)] . ' ' . date('Y', $stb);
      }
    } else {
      // echo "Tahun tak sama";
      $res = date('d', $sta) . ' ' . $bln[(int) date('m', $sta)] . ' ' . date('Y', $sta) . ' - ' . date('d', $stb) . ' ' . $bln[(int) date('m', $stb)] . ' ' . date('Y', $stb);
    }

    return $res;
  }
}
