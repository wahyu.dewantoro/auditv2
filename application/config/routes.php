<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// KeuanganController


$route['anggaran'] = 'KeuanganController/anggaran';
$route['getAnggaran'] = 'KeuanganController/DataAnggaran';
$route['anggaran/download'] = 'KeuanganController/DataAnggaranDownload';


$route['realisasi'] = 'KeuanganController/realisasi';
$route['getRealisasi'] = 'KeuanganController/dataRealisasi';
$route['realisasi/download'] = 'KeuanganController/dataRealisasiDownload';

// neraca
$route['neraca'] = 'KeuanganController/neraca';
$route['neraca/cetak'] = 'KeuanganController/cetakNeraca';
$route['neraca/read'] = 'KeuanganController/NeracaSubDetail';
$route['neraca/cetakdetail'] = 'KeuanganController/NeracadetailCetak';

// LO
$route['lodetail'] = 'KeuanganController/dataLO';
$route['lodetail/cetak'] = 'KeuanganController/dataLOCetak';
$route['lodetail/read'] = 'KeuanganController/dataLOread';
$route['lodetail/cetakDetail'] = 'KeuanganController/dataLOreadCetak';

// saldoawal
$route['saldoawal'] = 'KeuanganController/saldoAwal';
$route['getSaldoAwal'] = 'KeuanganController/saldoAwaldata';
$route['getSaldoAwal/cetak'] = 'KeuanganController/saldoAwaldataCetak';


//STS
$route['sts'] = 'PenatausahaanController/stsCtrl';
$route['sts/getData'] = 'PenatausahaanController/stsGetData';
$route['sts/cetak'] = 'PenatausahaanController/stsCetak';

// spd
$route['spd'] = 'PenatausahaanController/spdCtrl';
$route['spd/getData'] = 'PenatausahaanController/spdGetData';
$route['spd/cetak'] = 'PenatausahaanController/spdCetak';

// spp
$route['spp'] = 'PenatausahaanController/sppCtrl';
$route['spp/cetak'] = 'PenatausahaanController/sppCetakCtrl';
$route['spp/read'] = 'PenatausahaanController/sppDetail';
$route['spp/cetakDetail'] = 'PenatausahaanController/sppDetailCetak';

// spm
$route['spm'] = 'PenatausahaanController/spmCtrl';
$route['spm/cetak'] = 'PenatausahaanController/spmCetakCtrl';
$route['spm/read'] = 'PenatausahaanController/spmDetail';
$route['spm/cetakDetail'] = 'PenatausahaanController/spmDetailCetak';


// sp2d
$route['sppd'] = 'PenatausahaanController/sppdCtrl';
$route['sppd/cetak'] = 'PenatausahaanController/sppdCetakCtrl';
$route['sppd/read'] = 'PenatausahaanController/sppdDetail';
$route['sppd/cetakDetail'] = 'PenatausahaanController/sppdDetailCetak';

// sppdpotongan
$route['sppdpotongan'] = 'PenatausahaanController/sppdpotonganCtrl';
$route['sppdpotongan/cetak'] = 'PenatausahaanController/sppdpotonganCetakCtrl';
$route['sppdpotongan/read'] = 'PenatausahaanController/sppdpotonganDetail';
$route['sppdpotongan/cetakDetail'] = 'PenatausahaanController/sppdpotonganDetailCetak';

// spj
$route['spj'] = 'PenatausahaanController/spjCtrl';
$route['spj/read'] = 'PenatausahaanController/spjDetail';
$route['spj/cetak'] = 'PenatausahaanController/spjCetak';
$route['spj/cetakDetail'] = 'PenatausahaanController/spjCetakDetail';


// pajak
$route['pajak'] = 'PenatausahaanController/pajakCtrl';
$route['pajak/read'] = 'PenatausahaanController/pajakDetail';
$route['pajak/cetak'] = 'PenatausahaanController/pajakCetak';
$route['pajak/cetakDetail'] = 'PenatausahaanController/pajakCetakDetail';

// report
$route['pihakketiga'] = 'PenatausahaanController/pihakketigaCtrl';
$route['pihakketiga/cetak'] = 'PenatausahaanController/pihakketigaCetakCtrl';

$route['limaduadualebihtigapuluh'] = 'PenatausahaanController/limaduadualebihtigapuluhCtrl';
$route['limaduadualebihtigapuluh/cetak'] = 'PenatausahaanController/limaduadualebihtigapuluhCetakCtrl';

$route['limaduatigakurangsejuta'] = 'PenatausahaanController/limaduatigakurangsejutaCtrl';
$route['limaduatigakurangsejuta/cetak'] = 'PenatausahaanController/limaduatigakurangsejutaCetakCtrl';

$route['sp2dtuup'] = 'PenatausahaanController/sp2dtuupCtrl';
$route['sp2dtuup/cetak'] = 'PenatausahaanController/sp2dtuupCetakCtrl';


$route['belumprogres'] = 'progressipil/belumprogres';
$route['dibawah80hps'] = 'laporan/dibawah80hps';
$route['kegiatan_dibawah80hps'] = 'laporan_kegiatan/dibawah80hps';
$route['deviasi'] = 'progressipil/deviasi';
$route['bastlebihdarispk'] = 'laporan_kegiatan/bastlebihdarispk';
$route['siskeudestenagakerja']='maskeudes/siskeudesTenagaKerja_ctrl';
$route['anggaranminus']='maskeudes/anggaranminus_ctrl';
$route['panjar']='maskeudes/panjar_ctrl';
