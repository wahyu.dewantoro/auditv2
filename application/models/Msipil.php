<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Msipil extends CI_Model
{



    function __construct()
    {
        parent::__construct();
        $this->table = 'PEKERJAAN_SIPIL';
        $this->id = 'ID';
        $this->order = 'DESC';
        $this->hps80='(SELECT a.*, harga_perkiraan *80/100 as hps FROM  PEKERJAAN_SIPIL a ) a';
        $this->tahun = $this->session->userdata('tahun_anggaran');
        // $this->db = $this->load->database('menu', true);
    }


    function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('PEKERJAAN_SIPIL')->row();
    }

    function get_opd()
    {
        $this->db->select("distinct kd_skpd,case when kode_unit='4.1.4' then concat(nm_unit,' ',Nm_Sub_Unit) else Nm_Sub_Unit end nm_sub_unit", false);
        $this->db->where("nm_sub_unit!='*'", '', false);
        return $this->db->get('TB_SPE_REF_SUB_UNIT')->result();
    }

    function getopdbykode($kode)
    {
        $this->db->where('kd_skpd', $kode);
        $rr = $this->get_opd();

        $nama = '';
        if ($rr) {
            foreach ($rr as $rr) {
                $nama .= $rr->nm_sub_unit;
            }
        }

        return $nama;
    }


    function getJenisBelanja()
    {
        return $this->db->get('ref_belanja_modal')->result();
    }
    function getPPK()
    {
        return $this->db->get('REF_PPK')->result();
    }
    function getKonsultan()
    {
        return $this->db->get('MS_KONSULTAN')->result();
    }
    function getPengawas()
    {
        return $this->db->get('REF_PENGAWAS')->result();
    }
    function getNamaBelanja($kode)
    {
        // jenis_belanja_modal
        $this->db->where('kode', $kode);
        $res = $this->getJenisBelanja();
        $nama = "";
        if ($res) {
            foreach ($res as $res) {
                $nama .= $res->jenis_belanja_modal;
            }
        }
        return $nama;
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->from($this->hps80);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);
        // $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,sub_kegiatan');
        $this->db->where('tahun',$tahun);
        return $this->db->get($this->hps80)->result();
    }
    function total_rows_80_hps($q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->where('nilai_pengawas < hps','',false);
        $this->db->from($this->hps80);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_80_hps($limit, $start = 0, $q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->where('nilai_pengawas < hps','',false);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);
        // $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,sub_kegiatan');
        return $this->db->get($this->hps80)->result();
    }
    function get_all_data_80_hps()
    {

        $this->db->order_by($this->id, $this->order);
        $this->db->where('nilai_pengawas < hps','',false);
        // $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,sub_kegiatan');
        return $this->db->get($this->hps80)->result();
    }
    function get_all_data()
    {

        $this->db->order_by($this->id, $this->order);
        // $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,sub_kegiatan');
        return $this->db->get($this->table)->result();
    }
    function get_progres_by_pekerjaan($id)
    {
        $this->db->where('pekerjaan_id', $id);
        $this->db->order_by('created_at', 'desc');
        return $this->db->get('SP_PROGRES')->result();
    }
    function get_jadwal_by_pekerjaan($id)
    {
        $this->db->where('pekerjaan_sipil_id', $id);
        $this->db->order_by('awal_minggu', 'asc');
        return $this->db->get('PEKERJAAN_SIPIL_JADWAL')->result();
    }
    function get_progres_by_pekerjaandua($id)
    {
        $this->db->where('pekerjaan_sipil_id', $id);
        $this->db->order_by('tanggal_progres', 'asc');
        return $this->db->get('PEKERJAAN_SIPIL_LAPORAN')->result();
    }


    function sendmail($email_to,$subject,$pesan){

        $config = array(
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
           /*  'protocol'  => 'smtp',
            'smtp_host' => 'mail.merahputih.id',
            'smtp_user' => 'eauditjombang@merahputih.id',
            'smtp_pass'   => 'jombang@2345',
            'smtp_port'   => 587,
            'crlf'    => "\r\n",
            'newline' => "\r\n" */

            'protocol' => 'smtp',
            'smtp_host' => 'smtp.mailtrap.io',
            'smtp_port' => 2525,
            'smtp_user' => '940f8a0880dc5e',
            'smtp_pass' => 'c8ef3d61f93a08',
            'crlf' => "\r\n",
            'newline' => "\r\n" 
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('eauditjombang@merahputih.id');
        $this->email->to($email_to);
        $this->email->subject($subject);
        $this->email->reply_to('no-replay@merahputih.id', 'no replay');
        $this->email->message($pesan);
        //   $this->email->attach($base.'/storage/pdf/'.$file);

        if ($this->email->send()) {
            // echo "Sukses Kirim Email";
            return 1;
        } else {
           return show_error($this->email->print_debugger());
            // echo "Gagal Kirim Email";
        }
    }
}
