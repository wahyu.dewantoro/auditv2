<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tatausaha extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getSts($tahun, $Kd_SKPD)
    {

        $this->db->where('Tahun', $tahun);
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->select("tgl_sts,no_sts,keterangan,sum(nilai) nilai", false);
        $this->db->group_by("tgl_sts,no_sts,keterangan");
        return $this->db->get('TB_SPE_STS')->result();
    }

    function getSpd($tahun, $Kd_SKPD)
    {
        $this->db->where('Tahun', $tahun);
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->select("tgl_spd,no_spd,uraian,sum(nilai) nilai", false);
        $this->db->group_by("tgl_spd,no_spd,uraian");
        return $this->db->get('TB_SPE_SPD')->result();
    }

    function getSPP($tahun)
    {
        $table = "(select kd_skpd,nm_unit,nm_sub_unit,no_spp,uraian,jenis_spp,tgl_spp, sum(nilai) nilai
                    from tb_spe_spp
                    where tahun='$tahun'
                    group by kd_skpd,nm_unit,nm_sub_unit,no_spp,uraian,jenis_spp,tgl_spp) temp";

        $this->db->select("kd_skpd,nm_unit,nm_sub_unit
                ,sum(case when jenis_spp ='up' then 1 end) jnsup
                ,sum(case when jenis_spp ='up' then nilai end) nilaiup
                ,sum(case when jenis_spp ='gu' then 1 end) jnsgu
                ,sum(case when jenis_spp ='gu' then nilai end) nilaigu
                ,sum(case when jenis_spp ='tu' then 1 end) jnstu
                ,sum(case when jenis_spp ='tu' then nilai end) nilaitu
                ,sum(case when jenis_spp ='ls' then 1 end) jnsls
                ,sum(case when jenis_spp ='ls' then nilai end) nilails
                ,sum(case when jenis_spp ='nihil' then 1 end) jnsnihil
                ,sum(case when jenis_spp ='nihil' then nilai end) nilainihil", false);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit', false);
        return $this->db->get($table, false)->result();
    }

    function getSppDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $jenis)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_spp,no_spp,uraian,jenis_spp,count(*) jum,sum(nilai) nilai", false);

        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('tahun', $tahun);
        $this->db->where('jenis_spp', $jenis);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit,tgl_spp,no_spp,uraian,jenis_spp', false);
        return $this->db->get('tb_spe_spp', false)->result();
    }

    function getSPM($tahun)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit ,sum(case when jenis_spm ='up' then 1 end) jnsup ,sum(case when jenis_spm ='up' then nilai end) nilaiup ,sum(case when jenis_spm ='gu' then 1 end) jnsgu ,sum(case when jenis_spm ='gu' then nilai end) nilaigu ,sum(case when jenis_spm ='tu' then 1 end) jnstu ,sum(case when jenis_spm ='tu' then nilai end) nilaitu ,sum(case when jenis_spm ='ls' then 1 end) jnsls ,sum(case when jenis_spm ='ls' then nilai end) nilails ,sum(case when jenis_spm ='nihil' then 1 end) jnsnihil ,sum(case when jenis_spm ='nihil' then nilai end) nilainihil", false);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit', false);
        return $this->db->get("(select kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm,tgl_spm,sum(nilai) nilai
                               from tb_spe_spm
                               where tahun='$tahun'
                               group by kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm,tgl_spm) temp", false)->result();
    }

    function getSpmDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $jenis)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_spm,no_spm,uraian,jenis_spm,count(*) jum,sum(nilai) nilai", false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('tahun', $tahun);
        $this->db->where('jenis_spm', $jenis);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit,tgl_spm,no_spm,uraian,jenis_spm', false);
        return $this->db->get('tb_spe_spm', false)->result();
    }

    function getSPPD($tahun)
    {
        $table = "(
            select kd_skpd,nm_unit,nm_sub_unit,no_sp2d,keterangan,jenis_sp2d,tgl_sp2d,sum(nilai) nilai
            from tb_spe_sp2d
            where tahun='$tahun'
            group by kd_skpd,nm_unit,nm_sub_unit,no_sp2d,keterangan,jenis_sp2d,tgl_sp2d
         ) temp";

        $this->db->select("kd_skpd,nm_unit,nm_sub_unit ,sum(case when jenis_sp2d ='up' then 1 end) jnsup ,sum(case when jenis_sp2d ='up' then nilai end) nilaiup ,sum(case when jenis_sp2d ='gu' then 1 end) jnsgu ,sum(case when jenis_sp2d ='gu' then nilai end) nilaigu ,sum(case when jenis_sp2d ='tu' then 1 end) jnstu ,sum(case when jenis_sp2d ='tu' then nilai end) nilaitu ,sum(case when jenis_sp2d ='ls' then 1 end) jnsls ,sum(case when jenis_sp2d ='ls' then nilai end) nilails ,sum(case when jenis_sp2d ='nihil' then 1 end) jnsnihil ,sum(case when jenis_sp2d ='nihil' then nilai end) nilainihil");
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit");
        return $this->db->get($table, false)->result();
    }

    function getSppdDetail($tahun, $nm_unit, $nm_sub_unit, $kd_skpd, $jenis)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,jenis_sp2d,count(*) jum,sum(nilai) nilai", false);

        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('tahun', $tahun);
        $this->db->where('jenis_sp2d', $jenis);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,jenis_sp2d', false);
        return $this->db->get('tb_spe_sp2d', false)->result();
    }

    function getSPPDPotongan($tahun)
    {
        $table = "(
            select kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
            from tb_spe_sp2d_potongan
            where tahun='$tahun'
            group by kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan
         ) temp";
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,count(*) jum,sum(nilai) nilai", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit", false);
        return $this->db->get($table, false)->result();
    }

    function getSPPDPotonganDetail($tahun, $kd_skpd)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,count(*) jum,sum(nilai) nilai", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan", false);
        $this->db->where("kd_skpd", $kd_skpd);
        // $this->db->where("nm_sub_unit", $nm_sub_unit);
        // $this->db->where("nm_unit", $nm_unit);
        $this->db->where("tahun", $tahun);
        return $this->db->get("tb_spe_sp2d_potongan", false)->result();
    }

    function getSPJ($tahun)
    {
        $table = "(select kd_skpd,nm_unit,nm_sub_unit,no_spj,keterangan,jenis_spj,tgl_spj,sum(nilai) nilai
        from TB_SPE_SPJ
        where tahun='$tahun'
        group by kd_skpd,nm_unit,nm_sub_unit,no_spj,keterangan,jenis_spj,tgl_spj
        ) temp";
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,count(*) jum,sum(case when jenis_spj ='up' then 1 end) jnsup ,sum(case when jenis_spj ='up' then nilai end) nilaiup ,sum(case when jenis_spj ='gu' then 1 end) jnsgu ,sum(case when jenis_spj ='gu' then nilai end) nilaigu ,sum(case when jenis_spj ='tu' then 1 end) jnstu ,sum(case when jenis_spj ='tu' then nilai end) nilaitu ,sum(case when jenis_spj ='ls' then 1 end) jnsls ,sum(case when jenis_spj ='ls' then nilai end) nilails ,sum(case when jenis_spj ='nihil' then 1 end) jnsnihil ,sum(case when jenis_spj ='nihil' then nilai end) nilainihil", false);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit', false);
        return $this->db->get($table, false)->result();
    }

    function getSPjDetail($tahun, $kd_skpd, $jenis)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_spj,no_spj,keterangan,jenis_spj,sum(nilai) nilai,count(*) jum", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit,tgl_spj,no_spj,keterangan,jenis_spj", false);
        $this->db->where('tahun', $tahun);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('jenis_spj', $jenis);
        return $this->db->get("TB_SPE_SPJ", false)->result();
    }

    function getPajak($tahun)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,sum(nilai) nilai", false);
        $this->db->order_by("kd_skpd", "asc");
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('nm_unit', false);
        $this->db->where('tahun', $tahun);
        return $this->db->get("TB_SPE_PAJAK")->result();
    }

    function getPajakDetail($tahun, $Kd_SKPD)
    {
        $this->db->where('kd_skpd', $Kd_SKPD);
        $this->db->where('Tahun', $tahun);
        return $this->db->get("TB_SPE_PAJAK")->result();
    }

    function MonitorPihakKetiga($tahun)
    {
        $this->db->select("tahun,kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,tgl_spm,no_spm,keterangan,nm_penerima,rek_penerima,npwp,sum(nilai) nilai", false);
        $this->db->where("tahun=$tahun and Jenis_SP2D='LS' and Kd_Rek_1=5 and Kd_Rek_2=2 and Kd_Rek_3=3", '', false);
        $this->db->group_by("tahun,kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,nm_penerima,rek_penerima,npwp,tgl_spm,no_spm", false);
        return $this->db->get('TB_SPE_SP2D')->result();
    }


    function Monitor523($tahun)
    {
        $this->db->select("kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,tgl_sp2d, no_sp2d,kd_rek_gabung, nilai, nm_rek_5,keterangan", false);
        $this->db->where("tahun=$tahun and nilai > 300000000 and kd_rek_1=5 and kd_rek_2=2 and kd_rek_3=2 and kd_rek_4=2 and kd_rek_5 in (1,2)");
        return $this->db->get('tb_spe_sp2d')->result();
    }

    function Monitor523be($tahun)
    {
        $this->db->select("kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,tgl_sp2d, no_sp2d,kd_rek_gabung, nilai, nm_rek_5,keterangan", false);
        $this->db->where("tahun=$tahun and nilai < 10000000 and kd_rek_1=5 and kd_rek_2=2 and kd_rek_3=2 and kd_rek_4=2 and kd_rek_5 in (1,2)");
        return $this->db->get('tb_spe_sp2d')->result();
    }

    function SP2dTU($tahun)
    {
        return $this->db->query("SELECT kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                                FROM tb_spe_sp2d
                                WHERE  (Jenis_SP2D = 'TU' or Jenis_SP2D = 'UP')  and tahun=$tahun
                                group by kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan")->result();
    }
}
