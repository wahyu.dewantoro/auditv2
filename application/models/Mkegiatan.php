<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mkegiatan extends CI_Model
{



    function __construct()
    {
        parent::__construct();
        $this->table = 'KEGIATAN_BELANJA';
        $this->id = 'ID';
        $this->order = 'DESC';
        $this->hps80='(SELECT id,tahun,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,id_ppk, CAST(harga_perkiraan AS BIGINT)*80/100 as hps,sub_kegiatan,tanggal_bast,mulai_pekerjaan_spk,selesai_pekerjaan_spk FROM [dbo].[KEGIATAN_BELANJA] )a';
        // $this->db = $this->load->database('menu', true);
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }


    function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('KEGIATAN_BELANJA')->row();
    }

    function get_opd()
    {
        $this->db->select("distinct kd_skpd,case when kode_unit='4.1.4' then concat(nm_unit,' ',Nm_Sub_Unit) else Nm_Sub_Unit end nm_sub_unit", false);
        $this->db->where("nm_sub_unit!='*'", '', false);
        return $this->db->get('TB_SPE_REF_SUB_UNIT')->result();
    }

    function getopdbykode($kode)
    {
        $this->db->where('kd_skpd', $kode);
        $rr = $this->get_opd();

        $nama = '';
        if ($rr) {
            foreach ($rr as $rr) {
                $nama .= $rr->nm_sub_unit;
            }
        }

        return $nama;
    }


    function getJenisBelanja()
    {
        return $this->db->get('ref_belanja_modal')->result();
    }
    function getPPK()
    {
        return $this->db->get('ref_ppk')->result();
    }

    function getKonsultan()
    {
        return $this->db->get('MS_KONSULTAN')->result();
    }

    function getPengawas()
    {
        return $this->db->get('REF_PENGAWAS')->result();
    }
    function getNamaBelanja($kode)
    {
        // jenis_belanja_modal
        $this->db->where('kode', $kode);
        $res = $this->getJenisBelanja();
        $nama = "";
        if ($res) {
            foreach ($res as $res) {
                $nama .= $res->jenis_belanja_modal;
            }
        }
        return $nama;
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->from($this->hps80);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);
        
        $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,hps,sub_kegiatan,tanggal_bast,mulai_pekerjaan_spk,selesai_pekerjaan_spk');
        return $this->db->get($this->hps80)->result();
    }
    function get_all_data()
    {
        $this->db->order_by($this->id, $this->order);
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->select('id,kode_skpd,nama_skpd,kode_belanja,jenis_belanja,nama_kegiatan,pagu_anggaran,nama_ppk,nilai_spk,harga_perkiraan,hps,sub_kegiatan,tanggal_bast,mulai_pekerjaan_spk,selesai_pekerjaan_spk');
        return $this->db->get($this->hps80)->result();
    }
    function get_progres_by_pekerjaan($id)
    {
        $this->db->where('KEGIATAN_BELANJA_id', $id);
        $this->db->order_by('tanggal_progres', 'desc');
        return $this->db->get('KEGIATAN_BELANJA_LAPORAN')->result();
    }
    function get_jadwal_by_pekerjaan($id)
    {
        $this->db->where('KEGIATAN_BELANJA_id', $id);
        $this->db->order_by('awal_minggu', 'asc');
        return $this->db->get('KEGIATAN_BELANJA_JADWAL')->result();
    }
    function get_progres_by_pekerjaandua($id)
    {
        $this->db->where('KEGIATAN_BELANJA_id', $id);
        $this->db->order_by('tanggal_progres', 'asc');
        return $this->db->get('KEGIATAN_BELANJA_LAPORAN')->result();
    }
    function get_file($id)
    {
        $this->db->where('kegiatan_belanja_id', $id);
        return $this->db->get('KEGIATAN_BELANJA_FILE')->result();
    }
}
