<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Keuangan extends CI_Model
{


        /*public $id = 'id_inc';
    public $order = 'DESC';*/

        function __construct()
        {
                parent::__construct();
        }

        function dataAnggaranAkunDua($tahun, $opd)
        {
                return $this->db->query("SELECT akun_akrual_2, 
                                        nm_akrual_2, 
                                        Sum(nilai) nilai 
                                            FROM   (SELECT a.*, 
                                                            CASE 
                                                            WHEN anggaran_perubahan_1 IS NULL THEN anggaran_awal 
                                                            ELSE anggaran_perubahan_1 
                                                            END nilai 
                                                    FROM   tb_spe_anggaran a 
                                                    WHERE  kd_skpd = '$opd' 
                                                            AND tahun = '$tahun') a 
                                            GROUP  BY akun_akrual_2, 
                                                    nm_akrual_2 
                                            ORDER  BY akun_akrual_2 ASC ")->result();
        }

        function dataAnggaranAkunTiga($tahun, $opd, $akun)
        {
                return $this->db->query("SELECT akun_akrual_3, 
                                        nm_akrual_3, 
                                        Sum(nilai) nilai 
                                FROM   (SELECT a.*, 
                                                CASE 
                                                WHEN anggaran_perubahan_1 IS NULL THEN anggaran_awal 
                                                ELSE anggaran_perubahan_1 
                                                END nilai 
                                        FROM   tb_spe_anggaran a 
                                        WHERE  kd_skpd = '$opd' 
                                                AND tahun = '$tahun' 
                                                AND akun_akrual_2 = '$akun') a 
                                GROUP  BY akun_akrual_3, 
                                        nm_akrual_3 
                                ORDER  BY akun_akrual_3 ASC ")->result();
        }

        function dataAnggaranAkunEmpat($tahun, $opd, $akun)
        {
                return $this->db->query("SELECT akun_akrual_4, 
                                        nm_akrual_4, 
                                        Sum(nilai) nilai 
                                FROM   (SELECT a.*, 
                                                CASE 
                                                WHEN anggaran_perubahan_1 IS NULL THEN anggaran_awal 
                                                ELSE anggaran_perubahan_1 
                                                END nilai 
                                        FROM   tb_spe_anggaran a 
                                        WHERE  kd_skpd = '$opd' 
                                                AND tahun = '$tahun' 
                                                AND akun_akrual_3 = '$akun') a 
                                GROUP  BY akun_akrual_4, 
                                        nm_akrual_4 
                                ORDER  BY akun_akrual_4 ASC ")->result();
        }


        function realisasiAnggaran($tahun, $opd)
        {
                $sql = "select a.*,realisasi,anggaran - realisasi selisih
        from (
        select Akun_Akrual_3,Nm_Akrual_3, case when sum(Anggaran_Perubahan_1) >0 then sum(Anggaran_Perubahan_1) else sum(Anggaran_Awal) end anggaran
        from TB_SPE_ANGGARAN
        where Kd_SKPD='$opd'
        and tahun='$tahun'
        group by Akun_Akrual_3,Nm_Akrual_3
        ) a 
        left join (
        select Akun_Akrual_3,Nm_Akrual_3,sum(debet) - sum(kredit) realisasi
        from tb_spe_detail_lra
        where Kd_SKPD='$opd'
        and tahun='$tahun'
        group by Akun_Akrual_3,Nm_Akrual_3
        ) b on a.Akun_Akrual_3=b.Akun_Akrual_3 
        order by a.Akun_Akrual_3 asc";
                return $this->db->query($sql)->result();
        }

        function getNeraca($tahun)
        {
                $sql = "SELECT vneraca_lv0.kd_skpd, 
                                vneraca_lv0.nm_unit, 
                                vsaldoawal_lv0.sumnilai_1 saldoawalaset, 
                                vsaldoawal_lv0.sumnilai_2 saldoawalkewajiban, 
                                vneraca_lv0.sumnilai_1    mutasiaset, 
                                vneraca_lv0.sumnilai_2    mutasikewajiban, 
                                vneraca_lv0.sumnilai_3    mutasiekuitas 
                        FROM   vneraca_lv0 
                                LEFT JOIN vsaldoawal_lv0 
                                ON vneraca_lv0.nm_unit = vsaldoawal_lv0.nm_unit 
                                        AND vneraca_lv0.tahun = vsaldoawal_lv0.tahun 
                        WHERE  vneraca_lv0.tahun = '$tahun' 
                        ORDER  BY vneraca_lv0.kd_skpd ";
                return $this->db->query($sql)->result();
        }

        function saldoAwalAset($tahun, $unit)
        {
                $vtahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                          ,akun_akrual_2
                                          ,nm_akrual_2
                                          ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                      from tb_spe_saldo_awal
                                     where akun_akrual_1='1'
                                      and nm_unit='$unit'
                                      and tahun='$vtahun'
                                      group by  nm_unit
                                          ,akun_akrual_2
                                          ,nm_akrual_2
                                    order by akun_akrual_2")->result();
        }

        function saldoAwalAsetTiga($tahun, $unit, $akun)
        {
                $vtahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                          ,akun_akrual_3
                                          ,nm_akrual_3
                                          ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                      from tb_spe_saldo_awal
                                     where akun_akrual_2='$akun'
                                      and nm_unit='$unit'
                                      and tahun='$vtahun'
                                      group by  nm_unit
                                          ,akun_akrual_3
                                          ,nm_akrual_3
                                    order by akun_akrual_3")->result();
        }

        function saldoAwalAsetEmpat($tahun, $unit, $akun)
        {
                $vtahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_3='$akun'
                                  and nm_unit='$unit'
                                  and tahun='$vtahun'
                                  group by  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                order by akun_akrual_4")->result();
        }

        function mutasiAset($tahun, $unit)
        {
                $tahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                      ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                                  FROM TB_SPE_DETAIL_NERACA
                                      where akun_akrual_1='1'
                                  and nm_unit='$unit'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2 order by akun_akrual_2 ")->result();
        }


        function mutasiAsetTiga($tahun, $unit, $akun)
        {
                $tahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                      ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                                  FROM TB_SPE_DETAIL_NERACA
                                      where akun_akrual_2='$akun'
                                  and nm_unit='$unit'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3 order by akun_akrual_3")->result();
        }

        function mutasiAsetEmpat($tahun, $unit, $akun)
        {
                $tahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                      ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                                  FROM TB_SPE_DETAIL_NERACA
                                      where akun_akrual_3='$akun'
                                  and nm_unit='$unit'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4 order by akun_akrual_4")->result();
        }

        function mutasiKewajiban($tahun, $unit)
        {
                $tahun = $tahun - 1;
                return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                      ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                                  FROM TB_SPE_DETAIL_NERACA
                                      where akun_akrual_1='2'
                                  and nm_unit='$unit'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2")->result();
        }


        function getLoAllData($tahun){
                return $this->db->query("SELECT tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                , sum(nilai81) sumnilai81, sum(nilai82) sumnilai82, sum(nilai83) sumnilai83, sum(nilai84) sumnilai84, sum(nilai85) sumnilai85
                , sum(nilai91) sumnilai91, sum(nilai92) sumnilai92, sum(nilai93) sumnilai93, sum(nilai94) sumnilai94
                from (
                select *
                ,case when akun_akrual_2 ='8.1' then nilai end nilai81
                ,case when akun_akrual_2 ='8.2' then nilai end nilai82
                ,case when akun_akrual_2 ='8.3' then nilai end nilai83
                ,case when akun_akrual_2 ='8.4' then nilai end nilai84
                ,case when akun_akrual_2 ='8.5' then nilai end nilai85
                ,case when akun_akrual_2 ='9.1' then nilai end nilai91
                ,case when akun_akrual_2 ='9.2' then nilai end nilai92
                ,case when akun_akrual_2 ='9.3' then nilai end nilai93
                ,case when akun_akrual_2 ='9.4' then nilai end nilai94
                 from 
                (
                    select  nm_unit
                          ,akun_akrual_2
                          ,nm_akrual_2 
                          ,sum(debet)-sum(kredit) nilai     
                      from tb_spe_detail_lo
                      where tahun='$tahun'
                      group by  nm_unit
                          ,akun_akrual_2
                          ,nm_akrual_2
                    ) lv0
                ) lv1 left join tb_spe_ref_unit on lv1.nm_unit=tb_spe_ref_unit.nm_unit
                group by tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                      order by kd_skpd,lv1.nm_unit")->result();
        }

        function getLoLevel3($tahun,$unit, $akun)
        {
            return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3 
                                      ,sum(debet)-sum(kredit) nilai     
                                  from tb_spe_detail_lo
                                  where nm_unit='$unit' 
                                  and akun_akrual_2='$akun'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                order by akun_akrual_3 asc
                                ")->result();
        }

        function getLoLevel4($tahun,$unit, $akun)
        {
            return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4 
                                      ,sum(debet)-sum(kredit) nilai     
                                  from tb_spe_detail_lo
                                  where nm_unit='$unit' 
                                  and akun_akrual_3='$akun'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                order by akun_akrual_4 asc
                                ")->result();
        }
    
    
        function getLoLevel5($tahun,$unit, $akun)
        {
            return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_5
                                      ,nm_akrual_5 
                                      ,sum(debet)-sum(kredit) nilai     
                                  from tb_spe_detail_lo
                                  where nm_unit='$unit' 
                                  and akun_akrual_4='$akun'
                                  and tahun='$tahun'
                                  group by  nm_unit
                                      ,akun_akrual_5
                                      ,nm_akrual_5
                                order by akun_akrual_5 asc
                                ")->result();
        }

        function getSaldoAwal($tahun,$kd_skpd){
                $tahun=$tahun-1;
                $this->db->where('Tahun',$tahun);
                $this->db->where('Kd_SKPD',$kd_skpd);
                return $this->db->get("TB_SPE_SALDO_AWAL",false)->result();
        }
}
