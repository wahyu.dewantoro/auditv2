<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function SubUnit($tahun)
    {
        $this->db->where('tahun', $tahun);
        return $this->db->get('TB_SPE_REF_SUB_UNIT')->result();
    }

    function subunitBykode($tahun, $kode)
    {
        $this->db->where('tahun', $tahun);
        $this->db->where('Kd_SKPD', $kode);
        return $this->db->get('TB_SPE_REF_SUB_UNIT')->row();
    }
}
