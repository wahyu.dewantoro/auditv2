<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpengawas extends CI_Model
{

    public $table = 'ref_pengawas';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
         $this->db = $this->load->database('default', true);
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        // $this->db->like('id', $q);
        $this->db->like('nama_pengawas', $q);
        $this->db->or_like('email_pengawas', $q);
        $this->db->or_like('kd_skpd', $q);
        $this->db->or_like('nm_sub_unit', $q);
        $this->db->from($this->table);
        $this->db->join('MS_PENGGUNA','REF_PENGAWAS.id=MS_PENGGUNA.ref_pengawas_id');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by('REF_PENGAWAS.id', $this->order);
        $this->db->like('nama_pengawas', $q);
        $this->db->or_like('email_pengawas', $q);
        $this->db->or_like('kd_skpd', $q);
        $this->db->or_like('nm_sub_unit', $q);
        $this->db->limit($limit, $start);
        $this->db->join('MS_PENGGUNA','REF_PENGAWAS.id=MS_PENGGUNA.ref_pengawas_id');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        return  $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return   $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->delete($this->table);
    }
}
