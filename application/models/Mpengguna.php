<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpengguna extends CI_Model
{

    public $table = 'ms_pengguna';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        // $this->db = $this->load->database('menu', true);
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id_inc', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('username', $q);
        $this->db->or_like('password', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_inc', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('username', $q);
        $this->db->or_like('password', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        return $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return   $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->delete($this->table);
    }

    function getRole(){
        return $this->db->get('ms_role')->result();
    }

    function assignRole($data){
        return $this->db->insert('ms_assign_role', $data);
    }

    function lastid(){
        return $this->db->query("select max(id_inc) id  from ms_pengguna")->row()->id;
    }

    function userRole($id){
        $es=$this->db->query("select ms_role_id from ms_assign_role where ms_pengguna_id=$id")->result();
        $role=[];
        foreach($es as $es){
            array_push($role,$es->ms_role_id);
        }
        return $role;
    }

    function deleteRole($id){ 
        $this->db->where('ms_pengguna_id',$id);
        return $this->db->delete('ms_assign_role');

    }
    
}
 