<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msiskeudes extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        // $this->db = $this->load->database('siskeudes', true);
        $this->tahun = $this->session->userdata('tahun_anggaran');
    }

    // get all
    function get_kecamatan()
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->order_by('Nama_Kecamatan', 'ASC');
        return $this->db->get('Ref_Kecamatan')->result();
    }
    function get_kecamatan_by_kd_desa($kd_desa)
    {
        $kd_kec=$this->db->query("select * from Ref_desa where Kd_Desa=?", array($kd_desa))->row()->Kd_Kec;
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->where('Kd_Kec', $kd_kec);
        return $this->db->get('Ref_Kecamatan')->row()->Nama_Kecamatan;
    }
    // get data by id
    function get_desa($kd_kec)
    {
        $this->db->where('Kd_Kec', $kd_kec);
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->order_by('Nama_Desa', 'ASC');
        return $this->db->get('Ref_Desa')->result();
    }
    function get_spp()
    {
        $tahun=$this->tahun;
        $this->db->where('tahun',$tahun);
        $this->db->order_by('Nama_Desa', 'ASC');
        return $this->db->get('Ref_Desa')->result();
    }


    function get_pendapatan_anggaran($kd_desa)
    {
        $tahun=$this->tahun;
        $jml = $this->db->query("select case when sum(anggaranstlhpak) is not null or sum(anggaranstlhpak) !=0 then sum(anggaranstlhpak) else  sum(anggaran) end anggaran
                                        from ta_rab
                                        where kd_desa='$kd_desa' and tahun='$tahun'
                                        and left(Kd_Rincian,1)=4")->row()->anggaran;
        return $jml;
    }


    function get_jurnal($kd_desa, $jns)
    {
        $tahun=$this->tahun;
        $res = $this->db->query("select sum(nilai) nilai  from (
            select kd_desa,Kd_Rincian,case when D_K='D' then debet else Kredit end nilai
            from QrSP_Jurnal
            where tahun=? and Kd_Desa=? and LEFT(Kd_Rincian,1)=? ) a", array($tahun, $kd_desa, $jns))->row()->nilai;
        return $res;
    }

    function get_pendapatan_realisasi($kd_desa)
    {
        return $this->get_jurnal($kd_desa, '4');
    }

    function get_belanja_realisasi($kd_desa)
    {
        return $this->get_jurnal($kd_desa, '5');
    }

    function get_silpa_tahun_lalu($kd_desa)
    {
        $tahun=$this->tahun;
        $res = $this->db->query("select case when AnggaranStlhPAK is null then anggaran else AnggaranStlhPAK end nilai from Ta_RAB
        where Kd_Desa=? and tahun=?
        and Kd_Rincian='6.1.1.01.' ", array($kd_desa,$tahun))->row()->nilai;
        return $res;
    }


    function get_belanja_anggaran($kd_desa)
    {
        $tahun=$this->tahun;
        $a = $this->db->query("select case when sum(anggaranstlhpak) is not null or sum(anggaranstlhpak) !=0 then sum(anggaranstlhpak) else  sum(anggaran) end anggaran
        from ta_rab
        where kd_desa='$kd_desa' and tahun='$tahun'
        and left(Kd_Rincian,1)=5")->row()->anggaran;

        return $a;
    }

    /* function get_belanja($kd_desa)
    {
        $a = $this->db->query("select sum(jumlah) jum from ta_spp
        where kd_desa=?", $kd_desa)->row()->jum;
        return $a;
    } */

    function get_pembiayaan($kd_desa, $jns)
    {
        $tahun=$this->tahun;
        $res = $this->db->query("select sum(nilai)  nilai from (
            select kd_desa,Kd_Rincian,case when D_K='D' then debet else Kredit end nilai
            from QrSP_Jurnal
            where tahun=? and Kd_Desa=? and LEFT(Kd_Rincian,3)=? ) a", array( $tahun,$kd_desa, $jns))->row()->nilai;

        return $res;
    }

    function get_pembiayaan_penerimaan($kd_desa)
    {
        return $this->get_pembiayaan($kd_desa, '6.1');
    }

    function get_pembiayaan_pengeluaran($kd_desa)
    {
        return $this->get_pembiayaan($kd_desa, '6.2');
    }

    function get_pphppn($kd_desa)
    {
        $tahun=$this->tahun;
        return $this->db->query("select sum(debet) potongan , sum(kredit) setor
        from QrSP_Jurnal
        where tahun=$tahun and Kd_Desa='$kd_desa' and left(Kd_Rincian,5) ='7.1.1'")->row();
    }

    function get_ppd($kd_desa)
    {
        $tahun=$this->tahun;
        return $this->db->query("select sum(debet) potongan , sum(kredit) setor
        from QrSP_Jurnal
        where tahun=$tahun and Kd_Desa='$kd_desa' and left(Kd_Rincian,5) ='7.1.2'")->row();
    }


    function _kas($kd_desa, $kode)
    {
        $tahun=$this->tahun;
        return $this->db->query("select sum(debet) penerimaan ,sum(kredit) pengeluaran
        from (
         select debet,Kredit
         from QrSP_Jurnal
         where   tahun=$tahun and Kd_Desa='$kd_desa'
         and Kd_Rincian='$kode'
         union all
         select debet,Kredit
         from Ta_SaldoAwal
         where tahun=$tahun and Kd_Desa='$kd_desa'
         and Kd_Rincian='$kode'
         ) asd")->row();
    }

    function getKasPenerimaanBank($kd_desa)
    {
        $ar=$this->_kas($kd_desa,'1.1.1.02.');
        return $ar->penerimaan;
    }

    function getKasPengeluaranBank($kd_desa)
    {
        $ar=$this->_kas($kd_desa,'1.1.1.02.');
        return $ar->pengeluaran;
    }

    function getKasPenerimaanTunai($kd_desa)
    {
        $ar=$this->_kas($kd_desa,'1.1.1.01.');
        return $ar->penerimaan;
    }

    function getKasPengeluaranTunai($kd_desa)
    {
        $ar=$this->_kas($kd_desa,'1.1.1.01.');
        return $ar->pengeluaran;
    }
}
