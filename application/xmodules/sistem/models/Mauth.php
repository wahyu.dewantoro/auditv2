<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mauth extends CI_Model
{


    /*public $id = 'id_inc';
    public $order = 'DESC';*/

    function __construct()
    {
        parent::__construct();
        $this->table = 'ms_pengguna';
        // $this->db = $this->load->database('menu', true);
    }

    function proseslogin($data = array())
    {

        $username = $data['username'];
        $password = $data['password'];
        $tahun = $data['tahun'];
        $ee = $this->db->query("select max(tgl_sp2d) tanggal from tb_spe_sp2d where tahun=?", $tahun)->row();

        $query = $this->db->query("SELECT a.*
                                    FROM e_audit_app.dbo.ms_pengguna a
                                    WHERE (status_active is  null or status_active =1) and username=? and password=?", array($username, $password))->row();

        if (!empty($query)) {


            $this->db->where('ms_pengguna_id', $query->id_inc);
            $dd = $this->db->get('MS_ASSIGN_ROLE')->result();
            $role_id = '';
            foreach ($dd as $dd) {
                $role_id .= $dd->ms_role_id . ',';
            }

            $str_role = substr($role_id, 0, -1);
            // tanggal grab
            $opdrole = '';
            if ($query->ref_ppk_id <> '') {
                $opdrole = $this->db->query("select * from ref_ppk where id=?", array($query->ref_ppk_id))->row()->kd_skpd;
            }
            if ($query->ref_pengawas_id <> '') {
                $opdrole = $this->db->query("select * from ref_pengawas where id=?", array($query->ref_pengawas_id))->row()->kd_skpd;
            }

            set_userdata('tanggal', $ee->tanggal);
            set_userdata('audit_app', 1);
            set_userdata('audit_id_pengguna', $query->id_inc);
            set_userdata('audit_id_ppk', $query->ref_ppk_id);
            set_userdata('audit_id_pengawas', $query->ref_pengawas_id);
            set_userdata('audit_nama', $query->nama);
            set_userdata('audit_username', $query->username);
            set_userdata('audit_role', $str_role);
            set_userdata('audit_kd_skpd', $opdrole);
            set_userdata('tahun_anggaran', $tahun);

            return 'true';
        } else {
            return 'false';
        }
    }
}
