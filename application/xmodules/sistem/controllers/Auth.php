<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Mauth');
	}

	public function index()
	{
		$this->load->view('formlogin');
		
	}

	function redirec_halaman()
	{
		// $url = "http://" . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
		// $er = substr($url, 0, -8);
		// header('Location:' . $er);
		$this->index();
	}

	function proses()
	{
		
			$username = urldecode(urldecode($this->input->post('username')));
			$password = sha1(urldecode(urldecode($this->input->post('password'))));
			$tahun = $this->input->post('tahun');
			$data = array('password' => $password, 'username' => $username,'tahun'=>$tahun);

			$res = $this->Mauth->proseslogin($data);
			
			if ($res == 'true') {
				redirect('welcome');
			} else {
				// $this->redirec_halaman();
				$this->logout();
			}
	
	}

	function logout()
	{
		session_destroy();
		redirect('auth');
		// $this->redirec_halaman();
	}


	function keterlambatan(){
		$this->load->model('Msipil');
		//list
		// 
		$data=$this->db->query("select top 5 * from SP_PROGRES where notif=1 and email is null")->result();
		/* echo "<pre>";
		print_r($data); */
		foreach($data as $cek){
			$email_ppk = $this->db->query("select email_ppk from ref_ppk where id=?", $cek->id_ppk)->row()->email_ppk;
            $email_pengawas = $this->db->query("select email_pengawas from ref_pengawas where id=?", $cek->id_pengawas)->row()->email_pengawas;
            $res = array(
                'progres_id' => $cek->progres_id,
                'kode_skpd' => $cek->kode_skpd,
                'nama_skpd' => $cek->nama_skpd,
                'ket_prog' => $cek->ket_prog,
                'ket_keg' => $cek->ket_keg,
                'sub_kegiatan' => $cek->sub_kegiatan,
                'no_sk_pengawas' => $cek->no_sk_pengawas,
                'tgl_sk_pengawas' => $cek->tgl_sk_pengawas,
                'no_addedum_pengawas' => $cek->no_addedum_pengawas,
                'tgl_addedum_pengawas' => $cek->tgl_addedum_pengawas,
                'no_sk_konsultansi' => $cek->no_sk_konsultansi,
                'tgl_sk_konsultansi' => $cek->tgl_sk_konsultansi,
                'no_addedum_konsultansi' => $cek->no_addedum_konsultansi,
                'tgl_addedum_konsultansi' => $cek->tgl_addedum_konsultansi,
                'minggu_ke' => $cek->minggu_ke,
                'awal_minggu' => $cek->awal_minggu,
                'akhir_minggu' => $cek->akhir_minggu,
                'perencanaan' => $cek->perencanaan,
                'realisasi' => $cek->realisasi,
                'deviasi' => $cek->deviasi,
                'keterangan' => $cek->keterangan,
                'created_at' => $cek->created_at,
                'created_by' => $cek->created_by,
                'notif' => $cek->notif,
                'id_pengawas' => $cek->id_pengawas,
                'nama_pengawas' => $cek->nama_pengawas,
                'id_ppk' => $cek->id_ppk,
                'nama_ppk' => $cek->nama_ppk,
            );

            $pesan = $this->load->view('progres_sipil/kontenemail', $res, true);
            $email_to = array($email_pengawas, $email_ppk);
            $subject = "PEMBERITAHUAN PEKERJAAN KONSTRUKSI";
            $qq = $this->Msipil->sendmail($email_to, $subject, $pesan);
            
            if ($qq == 1) {
                // log kirim
                $log['pekerjaan_sipil_id'] = $cek->pekerjaan_id;
                $log['progres_id'] = $cek->progres_id;
                $log['keterangan'] = 'Keterlambatan pekerjaan (Otomatis)';
                $log['tanggal_email'] = date('Y-m-d H:i:s');
				$log['penerima'] = $email_ppk . ', ' . $email_pengawas;
				$this->db->insert('PEKERJAAN_EMAIL', $log);
				
				$this->db->query("update PEKERJAAN_SIPIL_LAPORAN set notif_email=1 where id='".$cek->progres_id."'");
            } 
		}
	}
}
